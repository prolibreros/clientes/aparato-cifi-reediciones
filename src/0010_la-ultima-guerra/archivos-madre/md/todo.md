<section epub:type="chapter" role="doc-chapter">

# I

Tres habían sido las grandes revoluciones de que se tenía noticia: la
que pudiéramos llamar Revolución cristiana, que en modo tal modificó la
sociedad y la vida en todo el haz del planeta; la Revolución francesa,
que, eminentemente justiciera, vino, a cercén de guillotina, a igualar
derechos y cabezas, y la Revolución socialista, la más reciente de
todas, aunque remontaba al año 2030 de la era cristiana.
Inútil sería insistir sobre el horror y la unanimidad de esta última
revolución, que conmovió la Tierra hasta en sus cimientos y que de una
manera tan radical reformó ideas, condiciones, costumbres, partiendo en
dos el tiempo, de suerte que en adelante ya no pudo decirse sino: «Antes
de la Revolución social»; «Después de la Revolución social». Solo haremos
notar que hasta la propia fisonomía de la especie, merced a esta gran
conmoción, se modificó en cierto modo. Cuéntase, en efecto, que antes de
la Revolución había, sobre todo en los últimos años que la precedieron,
ciertos signos muy visibles que distinguían físicamente a las clases
llamadas entonces privilegiadas, de los proletarios, a saber: las manos
de los individuos de las primeras, sobre todo de las mujeres, tenían
dedos afilados, largos, de una delicadeza superior al pétalo de un
jazmín, en tanto que las manos de los proletarios, fuera de su notable
aspereza o del espesor exagerado de sus dedos, solían tener seis de
estos en la diestra, encontrándose el sexto ---un poco rudimentario, a
decir verdad, y más bien formado por una callosidad semiarticulada---
entre el pulgar y el índice, generalmente. Otras muchas marcas
delataban, a lo que se cuenta, la diferencia de las clases, y mucho
temeríamos fatigar la paciencia del oyente enumerándolas. Solo diremos
que los gremios de conductores de vehículos y locomóviles de cualquier
género, tales como aeroplanos, aeronaves, aerociclos, automóviles,
expresos magnéticos, directísimos transetéreo lunares, etc., cuya
característica en el trabajo era la perpetua inmovilidad de piernas,
habían llegado a la atrofia absoluta de estas, al grado de que,
terminadas sus tareas, se dirigían a sus domicilios en pequeños carros
eléctricos especiales, usando de ellos para cualquier traslación
personal. La Revolución social vino, empero, a cambiar de tal suerte la
condición humana, que todas estas características fueron desapareciendo
en el transcurso de los siglos, y en el año 3502 de
la nueva era ---o sea 5532 de la era
cristiana--- no quedaba ni un vestigio de tal desigualdad dolorosa entre
los miembros de la humanidad.

La Revolución social se maduró, no hay niño de escuela que no lo sepa,
con la anticipación de muchos siglos. En realidad, la Revolución
francesa la preparó, fue el segundo eslabón de la cadena de progresos y
de libertades que empezó con la revolución cristiana; pero hasta el
siglo +++XIX+++ de la vieja era no empezó a definirse el movimiento unánime de
los hombres hacia la igualdad. En el año de la era cristiana 1950 murió el
último rey, un rey del Extremo Oriente, visto como una positiva
curiosidad por las gentes de aquel tiempo. Europa, que, según la
predicción de un gran capitán ---a decir verdad, considerado hoy por
muchos historiadores como un personaje mítico---, en los comienzos del
siglo +++XX+++ ---_post_ +++J.C.+++--- tendría que ser republicana o cosaca se convirtió,
en efecto, en el año de 1916, en los Estados Unidos de Europa,
federación creada a imagen y semejanza de los Estados Unidos de América
---cuyo recuerdo en los anales de la humanidad ha sido tan brillante, y
que en aquel entonces ejercían en los destinos del viejo continente una
influencia omnímoda---.

</section>
<section epub:type="chapter" role="doc-chapter">

# II

Pero no divaguemos: ya hemos usado más de tres cilindros de
fonotelerradiógrafo en pensar estas reminiscencias,@note y no llegamos
aún al punto capital de nuestra narración.

Como decíamos al principio, tres habían sido las grandes revoluciones de
que se tenía noticia; pero después de ellas, la humanidad, acostumbrada
a una paz y a una estabilidad inconmovibles, así en el terreno
científico, merced a lo definitivo de los principios conquistados, como
en el terreno social, gracias a la maravillosa sabiduría de las leyes y
a la alta moralidad de las costumbres, había perdido hasta la noción de
lo que era la vigilancia y cautela, y a pesar de su aprendizaje de
sangre, tan largo, no sospechaba los terribles acontecimientos que
estaban a punto de producirse.

La ignorancia del inmenso complot que se fraguaba en todas partes se
explica, por lo demás, perfectamente, por varias razones: en primer
lugar, el lenguaje hablado por los animales, lenguaje primitivo, pero
pintoresco y bello, era conocido por muy pocos hombres, y esto se
comprende; los seres vivientes estaban divididos entonces en dos únicas
porciones: los hombres, la clase superior, la élite, como si dijéramos
del planeta, iguales todos en derechos y casi, casi en intelectualidad,
y los animales, humanidad inferior que iba progresando muy lentamente a
través de los milenarios, pero que se encontraba en aquel entonces, por
lo que ve a los mamíferos, sobre todo, en ciertas condiciones de
perfectibilidad relativa muy apreciables. Ahora bien: la élite, el
hombre, hubiera juzgado indecoroso para su dignidad aprender cualquiera
de los dialectos animales llamados inferiores.

En segundo lugar, la separación entre ambas porciones de la humanidad
era completa, pues aun cuando cada familia de hombres alojaba en su
habitación propia a dos o tres animales que ejecutaban todos los
servicios, hasta los más pesados, como los de la cocina ---preparación
química de pastillas y de jugos para inyecciones---, el aseo de la casa,
el cultivo de la tierra, etc., no era común tratar con ellos, sino para
darles órdenes en el idioma patricio, o sea el del hombre, que todos
ellos aprendían.

En tercer lugar, la dulzura del yugo a que se les tenía sujetos, la
holgura relativa de sus recreos, les daba tiempo de conspirar
tranquilamente, sobre todo en sus centros de reunión, los días de
descanso, centros a los que era raro que concurriese hombre alguno.

</section>
<section epub:type="chapter" role="doc-chapter">

# III

¿Cuáles fueron las causas determinantes de esta cuarta revolución, la
última ---así lo espero--- de las que han ensangrentado el planeta? En tesis
general, las mismas que ocasionaron la Revolución social, las mismas que
han ocasionado, puede decirse, todas las revoluciones: viejas hambres,
viejos odios hereditarios, la tendencia a igualdad de prerrogativas y de
derechos y la aspiración a lo mejor, latente en el alma de todos los
seres…

Los animales no podían quejarse, por cierto: el hombre era para ellos
paternal, muy más paternal de lo que lo fueron para el proletario los
grandes señores después de la Revolución francesa. Obligábalos a
desempeñar tareas relativamente rudas, es cierto; porque él, por lo
excelente de su naturaleza, se dedicaba de preferencia a la
contemplación; mas un intercambio noble, y aun magnánimo, recompensaba
estos trabajos con relativas comodidades y placeres. Empero, por una
parte el odio atávico de que hablamos, acumulado en tantos siglos de
malos tratamientos, y por otra el anhelo, quizá justo ya, de reposo y de
mando, determinaban aquella lucha que iba a hacer época en los anales
del mundo.

Para que los que oyen esta historia puedan darse una cuenta más exacta y
más gráfica, si vale la palabra, de los hechos que precedieron a la
revolución, a la rebelión debiéramos decir, de los animales contra el
hombre, vamos a hacerles asistir a una de tantas asambleas secretas que
se convocaban para definir el programa de la tremenda pugna, asamblea
efectuada en México, uno de los grandes focos directores, y que,
cumpliendo la profecía de un viejo sabio del siglo +++XIX+++, llamado Eliseo
Reclus, se había convertido, por su posición geográfica en la medianía
de América y entre los dos grandes océanos, en el centro del mundo.

Había en la falda del Ajusco, adonde llegaban los últimos barrios de la
ciudad, un gimnasio para mamíferos, en el que estos se reunían los días
de fiesta y casi pegado al gimnasio un gran salón de conciertos, muy
frecuentado por los mismos. En este salón, de condiciones acústicas
perfectas y de amplitud considerable, se efectuó el domingo 3 de agosto
de 5532 ---de la nueva era--- la asamblea en cuestión.

Presidía Equs Robertis, un caballo muy hermoso, por cierto; y el primer
orador designado era un propagandista célebre en aquel entonces, Can
Canis, perro de una inteligencia notable, aunque muy exaltado. Debo
advertir que en todas partes del mundo repercutiría, como si dijéramos,
el discurso en cuestión, merced a emisores especiales que registraban
toda vibración y la transmitían solo a aquellos que tenían los
receptores correspondientes, utilizando ciertas corrientes magnéticas;
aparatos estos ya hoy en desuso por poco prácticos.

Cuando Can Canis se puso en pie para dirigir la palabra al auditorio,
oyéronse por todas partes rumores de aprobación.

</section>
<section epub:type="chapter" role="doc-chapter">

# IV

---Mis queridos hermanos ---empezó Can Canis----:

»La hora de nuestra definitiva liberación está próxima. A un signo
nuestro, centenares de millares de hermanos se levantarán como una sola
masa y caerán sobre los hombres, sobre los tiranos, con la rapidez de
una centella. El hombre desaparecerá del haz del planeta y hasta su
huella se desvanecerá con él. Entonces seremos nosotros dueños de la
tierra, volveremos a serlo, mejor dicho, pues que primero que nadie lo
fuimos, en el albor de los milenarios, antes de que el antropoide
apareciese en las florestas vírgenes y de que su aullido de terror
repercutiese en las cavernas ancestrales. ¡Ah!, todos llevamos en los
glóbulos de nuestra sangre el recuerdo orgánico, si la frase se me
permite, de aquellos tiempos benditos en que fuimos los reyes del mundo.
Entonces, el sol enmarañado aún de llamas a la simple vista, enorme y
tórrido, calentaba la tierra con amor en toda su superficie, y de los
bosques, de los mares, de los barrancos, de los collados, se exhalaba un
vaho espeso y tibio que convidaba a la pereza y a la beatitud. El mar
divino fraguaba y desbarataba aún sus archipiélagos inconsistentes,
tejidos de algas y de madréporas; la cordillera lejana humeaba por las
mil bocas de sus volcanes, y en las noches una zona ardiente, de un rojo
vivo, le prestaba una gloria extraña y temerosa. La luna, todavía joven
y lozana, estremecida por el continuo bombardeo de sus cráteres,
aparecía enorme y roja en el espacio, y a su luz misteriosa surgía
formidable de su caverna el león _saepelius_; el uro erguía su testa
poderosa entre las breñas, y el mastodonte contemplaba el perfil de las
montañas, que, según la expresión de un poeta árabe, le fingían la
silueta de un abuelo gigantesco. Los saurios volantes de las primeras
épocas, los iguanodontes de breves cabezas y cuerpos colosales, los
_Megatheriums_ torpes y lentos, no sentían turbado su reposo más que por el
rumor sonoro del mar genésico, que fraguaba en sus entrañas el porvenir
del mundo.

»¡Cuán felices fueron nuestros padres en el nido caliente y piadoso de la
tierra de entonces, envuelta en la suave cabellera de esmeralda de sus
vegetaciones inmensas, como una virgen que sale del baño!… ¡Cuán
felices!… A sus rugidos, a sus gritos inarticulados, respondían solo
los ecos de las montañas… Pero un día vieron aparecer con curiosidad,
entre las mil variedades de cuadrúmanos que poblaban los bosques y los
llenaban con sus chillidos desapacibles, una especie de monos rubios
que, más frecuentemente que los otros, se enderezaban y mantenían en
posición vertical, cuyo vello era menos áspero, cuyas mandíbulas eran
menos toscas, cuyos movimientos eran más suaves, más cadenciosos, más
ondulantes, y en cuyos ojos grandes y rizados ardía una chispa extraña y
enigmática que nuestros padres no habían visto en otros ojos en la
tierra. Aquellos monos eran débiles y miserables… ¡Cuán fácil hubiera
sido para nuestros abuelos gigantescos exterminarlos para siempre!… Y
de hecho, ¡cuántas veces cuando la horda dormía en medio de la noche,
protegida por el claror parpadeante de sus hogueras, una manada de
mastodontes, espantada por algún cataclismo, rompía la débil valla de
lumbre y pasaba de largo triturando huesos y aplastando vidas; o bien
una turba de felinos que acechaba la extinción de las hogueras, una vez
que su fuego custodio desaparecía, entraba al campamento y se ofrecía un
festín de suculencia memorable!… A pesar de tales catástrofes,
aquellos cuadrúmanos, aquellas bestezuelas frágiles, de ojos
misteriosos, que sabían encender el fuego, se multiplicaban; y un día,
día nefasto para nosotros, a un macho de la horda se le ocurrió, para
defenderse, echar mano de una rama de árbol, como hacían los gorilas, y
aguzarla con una piedra, como los gorilas nunca soñaron hacerlo. Desde
aquel día nuestro destino quedó fijado en la existencia: el hombre había
inventado la máquina, y aquella estaca puntiaguda fue su cetro, el cetro
de rey que le daba la naturaleza… ¿A qué recordar nuestros largos
milenarios de esclavitud, de dolor y de muerte?… El hombre, no
contento con destinarnos a las más rudas faenas, recompensadas con malos
tratamientos, hacía de muchos de nosotros su manjar habitual, nos
condenaba a la vivisección y a martirios análogos, y las hecatombes
seguían a las hecatombes sin una protesta, sin un movimiento de
piedad… La naturaleza, empero, nos reservaba para más altos destinos
que el de ser comidos a perpetuidad por nuestros tiranos. El progreso,
que es la condición de todo lo que alienta, no nos exceptuaba de su ley;
y a través de los siglos, algo divino que había en nuestros espíritus
rudimentarios, un germen luminoso de intelectualidad, de humanidad
futura, que a veces fulguraba dulcemente en los ojos de mi abuelo el
perro, a quien un sabio llamaba en el siglo +++XVIII+++ ---_post_ +++J.C.+++--- un
candidato a la humanidad; en las pupilas del caballo, del elefante o del
mono, se iba desarrollando en los senos más íntimos de nuestro ser,
hasta que, pasados siglos y siglos floreció en indecibles
manifestaciones de vida cerebral… El idioma surgió monosilábico,
rudo, tímido, imperfecto, de nuestros labios; el pensamiento se abrió
como una celeste flor en nuestras cabezas, y un día pudo decirse que
había ya nuevos dioses sobre la tierra; por segunda vez en el curso de
los tiempos el Creador pronunció un “_Fiat, et homo factus fuit_”.

»No vieron ellos con buenos ojos este paulatino surgimiento de humanidad;
mas hubieron de aceptar los hechos consumados, y no pudiendo
extinguirla, optaron por utilizarla… Nuestra esclavitud continuó,
pues, y ha continuado bajo otra forma: ya no se nos come, se nos trata
con aparente dulzura y consideración, se nos abriga, se nos aloja, se
nos llama a participar, en una palabra, de todas las ventajas de la vida
social; pero el hombre continúa siendo nuestro tutor, nos mide
escrupulosamente nuestros derechos… y deja para nosotros la parte más
ruda y penosa de todas las labores de la vida. No somos libres, no somos
amos, y queremos ser amos y libres… Por eso nos reunimos aquí hace
mucho tiempo, por eso pensamos y maquinamos hace muchos siglos nuestra
emancipación, y por eso muy pronto la última revolución del planeta, el
grito de rebelión de los animales contra el hombre, estallará, llenando
de pavor el universo y definiendo la igualdad de todos los mamíferos que
pueblan la tierra»…

Así habló Can Canis, y este fue, según todas las probabilidades, el
último discurso pronunciado antes de la espantosa conflagración que
relatamos.

</section>
<section epub:type="chapter" role="doc-chapter">

# V

El mundo, he dicho, había olvidado ya su historia de dolor y de muerte;
sus armamentos se orinecían en los museos, se encontraba en la época
luminosa de la serenidad y de la paz; pero aquella guerra que duró diez
años, como el sitio de Troya, aquella guerra que no había tenido ni
semejante ni paralelo por lo espantosa, aquella guerra en la que se
emplearon máquinas terribles, comparadas con las cuales los proyectiles
eléctricos, las granadas henchidas de gases, los espantosos efectos del
radio utilizado de mil maneras para dar muerte, las corrientes
formidables de aire, los dardos inyectores de microbios, los choques
telepáticos…, todos los factores de combate, en fin, de que la
humanidad se servía en los antiguos tiempos, eran risibles juegos de
niños; aquella guerra, decimos, constituyó un inopinado, nuevo,
inenarrable aprendizaje de sangre…

Los hombres, a pesar de su astucia, fuimos sorprendidos en todos los
ámbitos del orbe, y el movimiento de los agresores tuvo un carácter tan
unánime, tan certero, tan hábil, tan formidable, que no hubo en ningún
espíritu siquiera la posibilidad de prevenirlo…

Los animales manejaban las máquinas de todos géneros que proveían a las
necesidades de los elegidos; la química era para ellos eminentemente
familiar, pues que a diario utilizaban sus secretos: ellos poseían
además y vigilaban todos los almacenes de provisiones, ellos dirigían y
utilizaban todos los vehículos… Imagínese, por tanto, lo que debió
ser aquella pugna, que se libró en la tierra, en el mar y en el aire…
La humanidad estuvo a punto de perecer por completo; su fin absoluto
llegó a creerse seguro ---seguro lo creemos aún---… y a la hora en que
yo, uno de los pocos hombres que quedan en el mundo, pienso ante el
fonotelerradiógrafo estas líneas, que no sé si concluiré, este relato
incoherente que quizá mañana constituirá un utilísimo pedazo de
historia… para los humanizados del porvenir, apenas si moramos sobre
el haz del planeta unos centenares de sobrevivientes, esclavos de
nuestro destino, desposeídos ya de todo lo que fue nuestro prestigio,
nuestra fuerza y nuestra gloria, incapaces por nuestro escaso número y a
pesar del incalculable poder de nuestro espíritu, de reconquistar el
cetro perdido, y llenos del secreto instinto que confirma asaz la
conducta cautelosa y enigmática de nuestros vencedores, de que estamos
llamados a morir todos, hasta el último, de un modo misterioso, pues que
ellos temen que un arbitrio propio de nuestros soberanos recursos
mentales nos lleve otra vez, a pesar de nuestro escaso número, al trono
de donde hemos sido despeñados… Estaba escrito así… Los autóctonos
de Europa desaparecieron ante el vigor latino; desapareció el vigor
latino ante el vigor sajón, que se enseñoreó del mundo… y el vigor
sajón desapareció ante la invasión eslava; esta, ante la invasión
amarilla, que a su vez fue arrollada por la invasión negra, y así, de
raza en raza, de hegemonía en hegemonía, de preeminencia en
preeminencia, de dominación en dominación, el hombre llegó perfecto y
augusto a los límites de la historia… Su misión se cifraba en
desaparecer, puesto que ya no era susceptible, por lo absoluto de su
perfección, de perfeccionarse más… ¿Quién podía sustituirlos en el
imperio del mundo? ¿Qué raza nueva y vigorosa podía reemplazarle en él?
Los primeros animales humanizados, a los cuales tocaba su turno en el
escenario de los tiempos… Vengan, pues, enhorabuena; a nosotros,
llegados a la divina serenidad de los espíritus completos y definitivos,
no nos queda más que morir dulcemente. Humanos son ellos y piadosos
serán para matarnos. Después, a su vez, perfeccionados y serenos,
morirán para dejar su puesto a nuevas razas que hoy fermentan en el seno
oscuro aún de la animalidad inferior, en el misterio de un génesis
activo e impenetrable… ¡Todo ello hasta que la vieja llama del sol se
extinga suavemente, hasta que su enorme globo, ya oscuro, girando
alrededor de una estrella de la constelación de Hércules, sea fecundado
por primera vez en el espacio, y de su seno inmenso surjan nuevas
humanidades… para que todo recomience!

</section>
