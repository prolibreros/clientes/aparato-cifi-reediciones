<section epub:type="chapter" role="doc-chapter">

# El Buque Negro

Corría el año de gracia de 1716. Era el mes de octubre,
y los padres de la misión de Nuestra Señora de
Loreto no recibían cartas ni víveres desde enero. La
carestía era inmensa. Todas las tardes se sentaban,
después de las preces públicas, a vigilar tristemente
el golfo de Cortés, con la esperanza de avistar el barco
protector que aguardaban hacía luengos meses.

Una de estas tardes, teniendo el reverendo padre
Juan María Salvatierra su largo rosario entre las manos,
interrumpió la piadosa devoción para señalar
con el dedo a sus compañeros, que no lejos de allí
rezaban, un punto negro y lejano que se percibía en
el horizonte. Este pecadillo de distracción, que el
santo jesuita lloró como un niño el resto de su vida,
escandalizó a los otros padres, los cuales no haciendo
caso de la señal del padre superior, continuaron
su rezo impasiblemente.

Cuando todos hubieron concluido, les pidió perdón
de su falta y que rogaran a Dios no fuese a hacer
sentir su justicia sobre la misión en castigo de
aquel pecado, cometido por el pastor de aquellas
ovejas, en quien ellas solo debían mirar ejemplos de
exactitud, perseverancia y santidad en las buenas
obras.

El punto avistado se acercaba a toda prisa. Indudablemente
debía de ser una embarcación; así lo
pensaban los padres y la gente que había acudido a
la playa a saber la buena nueva. Pero el caso es que
aquello no tenía velas, ni al parecer mástiles. Veíase
solo una masa negra que avanzaba rápidamente.
¿Sería un cetáceo? Inverosímilmente podía pensarse
esto; la historia natural de aquel tiempo era bastante
completa en lo relativo a monstruos marinos,
pues todos los mares del mundo habían sido ya explorados.

Fuese lo que fuese, en las buenas almas de Loreto
dominaba universal regocijo; solo el padre Salvatierra
parecía contristado como si temiese en el
arribo del barco enigmático la caída de una maldición
a su santa obra.

Acercose por fin la grandiosa mole, redonda como
el dorso de la ballena, menos en la proa, donde,
estrechándose y reentrando las convexidades opuestas,
degeneraban en dos planos verticales que unían
las líneas de sus extremos en un ángulo de setenta.

Carecía de arboladura y velamen. Desde la línea
de flotación podía medir de altura o puntal hasta siete
metros, y su largo o eslora vendría a ser como de
unos treinta y seis, con manga proporcionada a estas
dimensiones. Por las lucanas o los ventanillos salía
un fulgor verdoso y vivísimo. Su color o pintura era
negra, sin brillo ninguno, y su cubierta estaba coronada
por tripulantes negros también. Eran las seis
menos cuarto cuando fondeó sin ruido ninguno, a
cincuenta brazadas de la playa.

El asombro hizo enmudecer a la colonia. Esta
se componía entonces de algunas tres mil almas, y
la piedad que los misioneros habían inculcado en
todas, no menos que la frecuente escasez en que vivían
hasta de lo indispensable para la vida, las habían
acostumbrado a recurrir a la oración, en los casos
apurados y a confiar sus destinos tranquilamente
a la Providencia. Los más de los presentes a esta
escena pensaban que Dios había escuchado las preces
públicas que a la sazón habían ordenado los padres,
así que, si bien no se explicaban aquella embarcación
nunca vista, hallándola del todo diferente
del pequeño bastimento San Jaime, único barquillo
que por entonces los proveía, esperaban no obstante
que la llegada del buque sería el fin de la carestía.
Recibieron, pues, al desconocido barco entonando
desde la playa regocijadas alabanzas, levantando
las manos al cielo y saludando a la tripulación
negra con vítores y honores de bienvenida.

Los jesuitas no las tenían todas consigo. Su superior
ilustración les hacía rechazar de plano cualquier
teoría de navegación no fundada en los aparejos veleros,
único sistema conocido hasta entonces; y no
teniendo noticia de que se hubiese ensayado siquiera
otro medio de locomoción por el mar, distinto del
viento y del remo, a punto estuvieron de calificar de
diabólico artificio la aparición del Buque Negro.

Su asombro no tuvo límites cuando vieron que
cuatro negrazos horribles descolgaban desde la borda
un batelillo color de hollín, y que por una escala de
cuerda se deslizaba un hombre blanco, vestido a la
usanza de los hijodalgos españoles, y que parecía ser
el jefe de aquellos atezados tripulantes.

Sentose el caballero en el largo escaño de madera
que flanqueaba el esquife, a su vez hicieron lo
mismo los cuatro negrazos y se dirigieron al puerto
a todo remo. El blanco llamábase Don Veremundo
de la Garza y Contreras, natural de Villamadera, en
el reino de Navarra; tenía veinticinco años y era hermano
menor del duque de Torre la Mora. Esto rezaba
un pasaporte en toda regla que presentó al padre
superior, simultáneamente pastor espiritual y representante
del virrey en la colonia. La estatura mediana,
la barba finísima, bien poblada y lustrosa, la nariz
grande y graciosamente corva, la boca plegada
en dos leves arrugas hacia las comisuras de los labios
tiernísimos, buena la sonrisa y astuta la mirada,
despedida por dos ojos de un verde espléndido, como
la barba y el pelo; tal es, en pocas palabras, el retrato
del héroe de mi historia.

Con aire señorial, aunque realzado por una conveniente
modestia, con palabra fácil y persuasiva y
con maneras de una cortesanía nada afectada, habló
el personaje con los padres y los colonos de cuanto
fue oportuno en aquella ocasión: del mar, de España,
del rey, del Nuevo Mundo, de los largos viajes,
de la temperatura, de las misiones.

Pero con prudentes reticencias y salvedades discretamente
diplomáticas, se dejó en el coleto la explicación
del enigma del barco negro, dando a entender
que aplazaba la revelación del misterio para otro día;
día que ---sea dicho de una sola vez--- no llegó jamás;
porque ni en las crónicas, ni en el archivo de la misión,
ni en los papeles particulares de los jesuitas, se
ha encontrado la clave de este singularísimo suceso.

Y como para abreviar a sus interlocutores del
purito de inquisición y examen a que parecía comenzaban
a someterle, se apresuró a ponderar el inmenso
cargamento de víveres y socorros que traía
para la colonia, pidiendo el auxilio de gente y canoas
a fin de abreviar la descarga. Esta noticia despertó
en la misión el más extraordinario entusiasmo:
canoas iban, canoas venían, y sobre la playa se
apilaba en colosales balumbas enorme porción de
sacos, valijas, cajas, barriles y fardos y bultos de toda
clase. Semillas, frutas, carnes saladas, mantas,
sombreros, muebles, útiles de labranza, cerdos,
ovejas, toros y vacas… de todo ello quedaba la misión
abastecida para muy largo tiempo. La descarga
duró cerca de tres días, durante los cuales a los colonos
los tuvo sin cuidado el problema náutico del
barco sin velamen ni arboladura, atendiéndose prácticamente
a la solución en alto grado gastronómico,
indumentaria y agrícola que les deparaba el botín
enorme. Concluida la descarga, a las primeras sombras
de la noche del dieciocho de octubre, se alejó
el Buque Negro, sin viento ni remos, con el mismo
silencio de su arribo, y dejándose en la misión al hijodalgo
don Veremundo de la Garza y Contreras,
muy agasajado de la colonia, en la cual había adquirido
una popularidad que rayaba en veneración: cosa
que nada tiene de extraordinario ni en Loreto ni
en el resto del mundo.

Al padre Salvatierra le supo muy amargo todo
aquello aunque fuese su huésped navarro y hermano
de un duque de la corte de España. El recién llegado
no traía entre los infinitos artículos de su cargamento,
ni un solo paquete de rosarios, ni un lote de catecismos,
ni un mal ornamento para iglesia, ni siquiera
una estampa de santos; su devoción, por otra parte,
era un tanto problemática, pues desde su venida no
había visitado ni una sola vez el templo de la misión,
para dar gracias por el buen suceso de su viaje. A
efecto de tener el corazón de aquel impío, ordenó el
padre un _Te Deum_ solemne, en acción de gracias por
los socorros recibidos en el Buque Negro. El señor
Veremundo concurrió al acto como todo hijo de vecino,
sin distinguirse de los demás por otra particularidad,
sino porque no hizo la señal de la cruz ni antes
ni después del piadoso ejercicio; en lo cual nadie paró
mientes. Pero he aquí que, al concluir el cántico
religioso y al volverse de frente a sus neófitos el buen
padre para bendecirlos, sintió tan grande inmovilidad
en el brazo derecho, que apenas pudo levantarlo, y
sin poder trazar en el aire la sacrosanta enseña, dejó
caer la mano sobre el muslo con la pesantez del plomo
y sin poder evitarlo.

Lleváronle de allí en brazos; porque era presa de
tenacísima fiebre. Algunos días después, convaleciente
y siempre triste, embarcose para la Nueva Galicia
en busca de salud y reposo, y no pasó mucho tiempo
sin que exhalase en Guadalajara el último suspiro. En
las supremas ansias de la agonía, dirigiendo la mortecina
vista hacia el occidente, intentó bendecir de nuevo,
aunque fuese desde tan lejos, la misión de Loreto,
y sintió otra vez rebeldes sus nervios y pesada la mano,
falleciendo sin derramar sobre sus catecúmenos el
postrer sentimiento de su vida.

Pero volvamos a Loreto. Don Veremundo, con
las simpatías que le había conquistado su desmedida
generosidad, con su despejado y siempre listo cacumen
y con la fortuna que le acariciaba notoriamente
desde su llegada a aquellas playas, comenzó a prosperar
en grande en todas las empresas que acometía
su audaz y nunca dormido carácter. Expediciones de
buceo, plantíos de cereales, cabotaje por su cuenta
en el golfo, exportación de vinos y frutas: cuanto intentaba
le colmaba de riquezas, al inaudito extremo
de que a fines de 1718, sus tesoros eran incalculables.
De cada valva sacaba una perla, de cada semilla
un mundo de semillas.

No sé si mis lectores estarán de acuerdo conmigo
en que no hay en este planeta cosa alguna que
más despierte la envidia de los mortales, que ver que
el prójimo se hace rico. Lo cierto es que las gentes
de la misión comenzaron a murmurar de don Veremundo,
cosas maravillosas y nunca oídas. Decíase
que su riqueza era dádiva demoniaca. Que un papel
trazado de gruesas líneas negras, que a nadie había
dado a leer don Veremundo, pero que este ojeaba de
vez en cuando sentado en la playa, contenía el convenio,
firmado de puño y letra de ambos contratantes,
mediante el cual este transfería a Satanás el dominio
de su alma, con exclusión de los derechos de
Dios y a cambio de riquezas; y para confirmar este
dicho añadían que a la fin o a la postre, el Buque
Negro se lo había de llevar en cuerpo y alma. Finalmente,
que la decadencia de la misión no tenía otra
data que el arribo de Garza, a quien debía atribuirse
asimismo la parálisis aguda del brazo del P. Salvatierra,
así como su inesperada y prematura muerte.

Y en estas y otras semejantes pláticas, esparcidas
primero _sotto voce_ y transmitidas después de
padres a hijos ya con mayor libertad y garrulerías,
porque don Veremundo iba envejeciendo y tornándose
en débil estantigua, transcurrieron hasta cincuenta
años, sin que por lo demás, en el lapso de este
tiempo, dejasen los buenos feligreses de Loreto
de solicitar y percibir en pingües demostraciones
constantes y sonantes, los desbordamientos de la liberalidad
siempre pródiga del hijodalgo. Y esto
prueba otra sencillísima observación que me ocurre,
Si a mis lectores no incomoda, y digo me ocurre, no
porque sea nueva, sino porque viene a cuento, y es
que nada hay en este bajo mundo que armonice mejor
las voluntades y trueque en servidores obsequiosos
a los malquerientes, como la generosidad y largueza
en las dádivas; y así, don Veremundo, aunque
visto con desconfianza y antipatía, no tuvo en torno
suyo más que atenciones, servicios y alabanzas. Solo
le abandonaron sus convecinos cuando cayó en
cama, atacado de extraña dolencia que nadie diagnosticó
ni pudo curar en la colonia.

A pocos días de estar enfermo don Veremundo,
volvió a avistarse el Buque Negro desde las playas
de Loreto. Con rapidez inusitada en embarcaciones
comunes, este se acercó al puerto silenciosamente,
sin velamen, ni arboladura ni jarcias, lleno de una
intensa luz rojiza que se veía a través de los vidrios
de las lucanas y lumbreras, y movido por no sé qué
fuerzas misteriosas. Salieron a cubierta cuatro negrazos,
descolgaron un esquife, se metieron en él,
remaron hasta atracar en el desembarcadero, saltaron
tres de ellos en tierra y se dirigieron a la casa de
Garza y Contreras, lo levantaron en brazos y, envuelto
en sus ropas de cama, lo embarcaron en el batel
negruzco, volvieron a remar hacia el Buque Negro,
adonde subieron con el moribundo, y zarparon
sin rumor y con rapidez, perdiéndose bien pronto de
vista el barco maravilloso en las lejanías ensombrecidas
de la mar, que ya empezaba a obscurecerse con
el capuz de la noche.

</section>
