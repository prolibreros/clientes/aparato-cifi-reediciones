<section epub:type="introduction" role="doc-introduction">

# Introducción

La selección de textos que presentamos a continuación incluye
textos escritos entre el río Bravo y el río Suchiate, entre 1692
y 1947. Los textos pertenecen al género de la ciencia ficción,
prefiguran su surgimiento o problematizan de manera interesante
su definición más convencional. Entendemos a la ciencia ficción
como un «género especulativo que relata acontecimientos posibles
desarrollados en un marco imaginario, cuya verosimilitud se construye
a partir de las ciencias físicas, naturales y sociales».@note
No deja de llamar nuestra atención lo difícil que es encontrar
textos escritos por mujeres en el periodo que comprende esta
selección y que concluye con la llegada de la Segunda Guerra
Mundial. Esta selección de textos, sin embargo, permanece abierta
y en busca de esas voces femeninas que los archivos no han registrado.
Al mismo tiempo comienza su recorrido con un texto de Sor Juana
Inés de la Cruz, haciendo eco de la re-lectura feminista con
la que puede abordarse la tradición literaria de la ciencia ficción
y en específico los textos que aquí presentamos. Nuestra selección
además quiere invitar al lector a que se acerque a la versión
completa de las piezas que presentamos, las cuales se pueden
descargar en nuestro sitio: [aparatocifi.press](http://www.aparatocifi.press/).
El riesgo de seleccionar ciertos fragmentos, y dejar fuera algunos
otros, busca generar procesos de apropiación literaria que lean,
escuchen y jueguen con los fragmentos que presentamos en esta
antología.

Nuestra selección quiere ser interesante en términos narrativos.
Los cortes se han realizado _in media res_. Es decir, se ha buscado
que los fragmentos que se presentan inmiscuyan al lector en momentos
específicos en que se describe el funcionamiento de una máquina,
se llega al momento culmen de un viaje o se viven procesos sociales
como la eugenesia, el triunfo del fascismo o la revolución política
de los animales. No pretendemos ocultarlo: nuestra selección
es arbitraria, simplemente busca compartir una lectura e invitar
a que muchas más se desprendan de la nuestra. Ahora bien, nuestro
punto de partida fue el trabajo que otros han hecho para volver visibles
los textos que presentamos. Para armar esta selección fueron claves: la
página «Ciencia Ficción Mexicana» ([cfm.mx](http://cfm.mx/)),
la cronología de textos titulada _La ciencia ficción en México
(hasta el año 2002)_ de Gonzálo Martré y la antología _El futuro
en llamas_ de Gabriel Trujillo Muñoz. Al ser este un compendio
de autoras y autores que escribieron en los siglos que nos preceden,
nuestra selección también es una invitación a escuchar y a re-escribir
las palabras de estas voces del pasado para dialogar con ellas
y re-inventar nuestro presente. Con este fin, los fragmentos
que aquí compartimos comienzan con una introducción que busca
darle al lector una sinopsis de lo que acontece en la versión
integra del texto, así como algunos datos que puedan enriquecer
su lectura.

Siguiendo a Ursula K. Le Guin, consideramos importante poner
en entredicho la dicotomía entre una supuesta ciencia ficción
dura, en la que la racionalidad de los argumentos científicos
es central para la construcción de la trama, y una ciencia ficción
blanda más cercana al género fantástico. Sin duda nos interesan
las explicaciones científicas y técnicas que articulan la diversidad
de relatos que presentamos a continuación. Pero también queremos
observar las huidas a lo fantástico como síntomas del _shock_
que la tecnología produce en los usuarios que ven transformadas
sus vidas con la llegada de distintos aparatos. Finalmente, lo
que más nos interesa, siguiendo a Le Guin, es observar cómo los
relatos que presentamos especularon acerca de las posibilidades
y los efectos que tendría la ciencia y la tecnología, en la vida
de los seres humanos que vivirían entre el río Bravo y el Suchiate,
y los procesos sociales que pondrían en marcha.

Hemos preferido no usar el término «ciencia ficción mexicana»,
primero, porque algunos de los textos en esta antología fueron
escritos cuando eso que llamamos México no existía. Pero también
hemos querido señalar los dos ríos entre los cuales se escribieron
estos textos, para subrayar ---en estas épocas de cambio climático---
que es un entorno vivo el que permitió que surgieran estas distintas
formas de escritura.

Juan Pablo Anaya \
Mayra Roffe {.espacio-arriba1 .derecha}

</section>
<section epub:type="chapter" role="doc-chapter">

# _Primero sueño_ (1692) de Sor Juana Inés de la Cruz

El _Primero sueño_ de Sor Juana Inés de la Cruz (1648-1695) refiere
un viaje, emprendido a la llegada de la noche, en el que el alma
abandona el cuerpo. Este último se encuentra dormido, es insensible
a los estímulos de su entorno pero se encuentra estimulado por
la digestión. Lo anterior da pie a un sueño en el que el alma
se eleva para ver el todo y busca ir más allá de lo concreto.
En el texto, esta ambición es comparada con la trágica empresa
de Faetón, quien al convencer a su padre Helios que lo deje conducir
su carroza, el Sol, acaba calcinado. Cuando el estímulo que producen
los vapores del estómago termina, el cuerpo despierta. Llega
el día, triunfa sobre la noche y el poema concluye.

Comenzamos nuestra selección con el _Primero sueño_ de Sor Juana
Inés de la Cruz por dos razones. La primera tiene que ver con
un juicio que se suele hacer al respecto de la ciencia ficción
escrita entre el río Bravo y el río Suchiate. Se dice que la
ciencia ficción en esta parte del mundo tiende a confundirse
o incluso es parte del género fantástico. Como comenta la académica
Lilia Granillo Vázquez, un señalamiento de este tipo, en principio,
lo que busca subrayar es que la literatura escrita en estos lugares
se caracteriza por un «escaso, cuando no nulo, conocimiento científico-tecnológico».
Supuestamente, para subsanar esta falta se busca el refugio en
la «fantasía» (Granillo: 12). El _Primero sueño_ de Sor Juan
es precisamente un testimonio de lo contrario. Como lo señala
Antonio Alatorre, en su «Invitación a la lectura del Sueño de
Sor Juana», en las imágenes poéticas del texto hay varias de
carácter científico. Por ejemplo, la caracterización de la noche,
al inicio del poema, mediante la imagen «Piramidal, funesta,
de la tierra / nacida sombra, al cielo encaminaba» (157) es en
parte una descripción científica de «la sombra» en forma de
pirámide «que la Tierra proyecta» durante la noche. La sombra
«nace» de la Tierra misma «hacia» lo que para el espectador
son los cielos «cuando el Sol» ilumina la cara opuesta del
planeta (Alatorre: 126). En este sentido, seguimos a la escritura
Gabriela Damián cuando afirma que en el poema de Sor Juana se
encuentra «la semilla» de un cierto tipo de ejercicio de la «imaginación»
pero creemos que esta no solo es fantástica, sino también científica
y que hace posible la escritura por venir, como trataremos de
dar cuenta en nuestra selección de textos.

La segunda razón por la que incluimos el _Primero sueño_ como
punto de partida es por la manera en que retrata al cuerpo como
una máquina cuyas fantasías no están separadas del deseo de conocimiento.
El texto concibe al cuerpo humano como una máquina barroca y
describe cómo opera, al momento de dormir, cuando la imaginación
y la fantasía quedan libres y son estimuladas al punto en que
engendran una forma de delirio. En Sor Juana este delirio no
está separado del deseo de conocimiento. Es cierto que Sor Juana
no imagina una máquina del futuro y en ese sentido es polémico
situarla como una escritora de ciencia ficción. Sin embargo,
mediante la ciencia de su época, el _Primero sueño_ sí nos deja
ver los mecanismos que hacen posible el ejercicio de la imaginación
en distintos relatos por venir. Por ello es que coincidimos con
aquellos textos (Damián, 2018; Lepori, 2011) que ven en este
texto un ejercicio de «proto ciencia ficción».

El fragmento que presentamos a continuación pone en escena precisamente
los mecanismos que en el cuerpo hacen posible el ejercicio de
una forma de imaginación fantástica. En el mundo barroco de Sor
Juan hay tres facultades interiores. La estimativa es aquella
que recibe los mensajes de los sentidos. Esta los transmite a
la imaginativa, facultad que los ordena y se los manda a la memoria.
Como lo explica Antonio Alatorre, según el poema, cuando estamos
dormidos no reciben las facultades interiores «ninguna información
de los (sentidos) exteriores, que están inactivos». El «vapor
sutilísimo» que va de la digestión en el estómago al cerebro
«impulsa sin estorbos a la imaginativa para ir más allá de las
experiencias reales y concretas, o sea para fantasear». Dormidos,
«a solas con nosotros mismos, la fantasía queda liberada de compromisos
y se pone a inventar, a crear imágenes» (Alatorre: 131). Como
esperamos mostrar en esta antología creemos encontrar ecos de
los mecanismos para la fantasía de este cuerpo máquina en textos
posteriores.

# Fragmento

[…] {.espacio-arriba1 .sin-sangria}

así pues, de profundo {.espacio-arriba1}

sueño dulce los miembros ocupados, {.sin-sangria}

quedaron los sentidos

del que ejercicio tienen ordinario {.sin-sangria}

(trabajo en fin, pero trabajo amado, {.sin-sangria}

si hay amable trabajo),

si privados no, al menos suspendidos, {.sin-sangria}

y cediendo al retrato del contrario {.sin-sangria}

de la vida, que, lentamente armado, {.sin-sangria}

cobarde embiste y vence perezoso {.sin-sangria}

con armas soñolientas,

desde el cayado humilde al cetro altivo {.sin-sangria}

sin que haya distintivo

que el sayal de la púrpura discierna, {.sin-sangria}

pues su nivel, en todo poderoso, {.sin-sangria}

gradúa por exentas

a ningunas personas,

desde la de a quien tres forman coronas {.sin-sangria}

soberana tiara

hasta la que pajiza vive choza; {.sin-sangria}

desde la que el Danubio undoso dora, {.sin-sangria}

a la que junco humilde, humilde mora; {.sin-sangria}

y con siempre igual vara

(como, en efecto, imagen poderosa {.sin-sangria}

de la muerte) Morfeo

el sayal mide igual con el brocado. {.sin-sangria}

El alma, pues, suspensa

del exterior gobierno ---en que, ocupada {.sin-sangria}

en material empleo,

o bien o mal da el día por gastado---, {.sin-sangria}

solamente dispensa

remota, si del todo separada {.sin-sangria}

no, a los de muerte temporal opresos, {.sin-sangria}

lánguidos miembros, sosegados huesos, {.sin-sangria}

los gajes del calor vegetativo, {.sin-sangria}

el cuerpo siendo, en sosegada calma, {.sin-sangria}

un cadáver con alma,

muerto a la vida y a la muerte vivo, {.sin-sangria}

de lo segundo dando tardas señas {.sin-sangria}

el de reloj humano

vital volante que, sino con mano, {.sin-sangria}

con arterial concierto, unas pequeñas {.sin-sangria}

muestras, pulsando, manifiesta lento {.sin-sangria}

de su bien regulado movimiento. {.sin-sangria}

Este, pues, miembro rey y centro vivo {.sin-sangria}

de espíritus vitales,

con su asociado respirante fuelle {.sin-sangria}

---pulmón, que imán del viento es atractivo, {.sin-sangria}

que en movimientos nunca desiguales, {.sin-sangria}

o comprimiendo ya, o ya dilatando {.sin-sangria}

el musculoso, claro, arcaduz blando, {.sin-sangria}

hace que en él resuelle

el que le circunscribe fresco ambiente {.sin-sangria}

que impele ya caliente,

y él venga su expulsión haciendo, activo, {.sin-sangria}

pequeños robos al calor nativo, {.sin-sangria}

algún tiempo llorados,

nunca recuperados,

si ahora no sentidos de su dueño, {.sin-sangria}

(que, repetido, no hay robo pequeño)---; {.sin-sangria}

estos, pues, de mayor, como ya digo, {.sin-sangria}

excepción, uno y otro fiel testigo, {.sin-sangria}

la vida aseguraban,

mientras con mudas voces impugnaban {.sin-sangria}

la información, callados, los sentidos, {.sin-sangria}

con no replicar solo defendidos; {.sin-sangria}

y la lengua que, torpe, enmudecía, {.sin-sangria}

con no poder hablar los desmentía. {.sin-sangria}

Y aquella del calor más competente {.sin-sangria}

científica oficina,

próvida de los miembros despensera, {.sin-sangria}

que avara nunca y siempre diligente, {.sin-sangria}

ni a la parte prefiere más vecina {.sin-sangria}

ni olvida a la remota,

y en ajustado natural cuadrante, {.sin-sangria}

las cuantidades nota

que a cada cual tocarle considera, {.sin-sangria}

del que alambicó quilo el incesante {.sin-sangria}

calor, en el manjar que, medianero {.sin-sangria}

piadoso, entre él y el húmedo interpuso {.sin-sangria}

su inocente sustancia,

pagando por entero

la que, ya piedad sea, o ya arrogancia, {.sin-sangria}

al contrario voraz, necio, la expuso {.sin-sangria}

(merecido castigo, aunque se excuse, {.sin-sangria}

al que en pendencia ajena se introduce); {.sin-sangria}

esta, pues, si no fragua de Vulcano, {.sin-sangria}

templada hoguera del calor humano, {.sin-sangria}

al cerebro enviaba

húmedos, mas tan claros, los vapores {.sin-sangria}

de los atemperados cuatro humores, {.sin-sangria}

que con ellos no solo empañaba {.sin-sangria}

los simulacros que la estimativa {.sin-sangria}

dio a la imaginativa

y aquesta, por custodia más segura, {.sin-sangria}

en forma ya más pura

entregó a la memoria (que, oficiosa, {.sin-sangria}

grabó tenaz y guarda cuidadosa), {.sin-sangria}

sino que daban a la fantasía {.sin-sangria}

lugar de que formase

imágenes diversas. {.sin-sangria}

Y del modo {.sin-sangria}

que en tersa superficie, que de Faro {.sin-sangria}

cristalino portento, asilo raro {.sin-sangria}

fue, en distancia longísima se vían, {.sin-sangria}

sin que esta le estorbase,

del reino casi de Neptuno todo, {.sin-sangria}

las que distantes le surcaban naves, {.sin-sangria}

viéndose claramente,

en su azogada luna

el número, el tamaño y la fortuna {.sin-sangria}

que en la instable campaña transparente {.sin-sangria}

arresgadas tenían,

mientras aguas y vientos dividían {.sin-sangria}

sus velas leves y sus quillas graves: {.sin-sangria}

así ella, sosegada, iba copiando {.sin-sangria}

las imágenes todas de las cosas, {.sin-sangria}

y el pincel invisible iba formando {.sin-sangria}

de mentales, sin luz, siempre vistosas {.sin-sangria}

colores, las figuras

no solo ya de todas las criaturas {.sin-sangria}

sublunares, mas aun también de aquellas {.sin-sangria}

que intelectuales claras son estrellas, {.sin-sangria}

y en el modo posible

que concebirse puede lo invisible, {.sin-sangria}

en sí, mañosa, las representaba {.sin-sangria}

y al alma las mostraba.

# _Primero sueño_ (1692) de Sor Juana Inés de la Cruz @ignore

</section>
<section epub:type="chapter" role="doc-chapter">

# _Sizigias y cuadraturas lunares_ (1775) de Manuel Antonio de Rivas

En lo que hoy es Mérida, Yucatán, en 1775, el fraile franciscano,
Manuel Antonio de Rivas (fecha de nacimiento y muerte desconocidos),
escribió un texto satírico que narra la llegada a la Luna de
Onésimo Dutalón, un científico francés, abordo de un «carro
volante». El texto nunca fue publicado. A Rivas lo denunciaron
otros franciscanos ante la Inquisición, entre otras razones,
precisamente por escribir _Sizigias y cuadraturas lunares_ (el
título abreviado del texto) el cual incluía «algunos temas urticantes
para la iglesia» (Depetris: 11). Rivas fue absuelto, pero su
texto nunca vio la luz. En 1958, casi dos siglos después, Pablo
González Casanova lo encontró en los legajos del Santo oficio
en el Archivo General de la Nación. En el capítulo «Fantasía
y realidad» de su libro _La literatura perseguida en la crisis
de la Colonia_ se encuentra una de las primeras referencias a
este texto. Al día de hoy _Sizigias y cuadraturas lunares_ es
considerado «el primer cuento fantástico escrito en hispanoamérica»
(Depetris: 16).

El título completo del manuscrito es _Sizigias y cuadraturas
lunares ajustadas al meridiano de Mérida de Yucatán por un antíctona
o habitador de la luna y dirigidas al bachiller Don Ambrosio
de Echeverría, entonador que ha sido de kyries funerales en la
Parroquia del Jeśus de dicha ciudad y al presente profesor de
logarítmica en el pueblo de Mama de la península de Yucatán,
para el año del señor 1775_. Como sucede en los textos la época,
el título incluye además otros elementos como son el lugar, el
año de publicación y los supuestos destinatarios del mismo. Sin
embargo, estos datos están inscritos dentro del propio registro
satírico del texto. _Sizigias y cuadraturas lunares_ es, en principio,
una carta escrita por algunos habitantes de la luna, aquellos
que conforman el Ateneo lunar, en respuesta a una primera misiva
recibida de la tierra. La carta recibida de la Tierra fue supuestamente
escrita por alguien que se hace llamar «el atisbador» u observador
«de los movimientos lunares». En ella expone sus observaciones
respecto a los distintos momentos en que se habían alineado el
Sol, la Tierra y la Luna ---las llamadas sizigias---; así como las
fechas en que, debido a su posición, la Luna, la Tierra y el
Sol habían trazado un ángulo recto ---cada cuerpo celeste sería
un punto; la línea que se formaría al unir estos puntos nos daría
una L, en las cuadraturas orientales, y una L invertida en las
cuadraturas occidentales---. Rivas no solo se inscribe dentro de
una tradición que, tras el giro copernicano y la llegada del
heliocentrismo, especula sobre la existencia de habitantes en
otros planetas (como es el caso de Copérnico, Cyrano de Bergerac
y Voltaire, entre otros), sino que abre también la posibilidad
de fabular una perspectiva no humana para ejercer la sátira y
mira a la Tierra desde otros mundos.

En _Sizigias_, la relevancia de las observaciones recibidas en
la carta hace que los selenitas quieran retribuir el gesto y
convoquen a un congreso con los «mejores computistas» en la
luna «versados en la historia del globo terráqueo» (Rivas:
38). Por ello se congrega al Ateneo lunar, y su presidente
pide a su secretario, de nombre Remeltoin, escriba una relatoría
del encuentro para mandarla a la Tierra. Lo primero que se destaca
en la epístola del congreso dirigida a la Tierra, fechada en
el 1775 de la cronología cristiana, es la diversidad de registros
calendáricos puestos en juego. La carta recibida de la Tierra,
por ejemplo, esta fechada, mediante el calendario de la antigua
Mesopotamia, en el «5 del mes epifi del año de Nabonasar 2510»
(Rivas: 37). A su vez, las observaciones astronómicas recibidas
y algunos otros datos discutidos en _Sizigias_ también están
fechados mediante otros calendarios distintos al cristiano, como
el del Islam, el judío o el chino. Rivas muestra su conocimiento
de todas estas cronologías y afina su sátira cuando señala que
el año cero de los selenitas, similar al año cero de los cristianos,
se estableció cuando Faetón, el hijo de Helios, convenció a su
padre que lo dejara manejar su carro, el Sol, y trastornó la
vida en la Tierra y quemó la Luna ---calcinándose él mismo con
ella---.

Durante el congreso del Ateneo lunar tienen lugar dos incidentes
que son claves para el relato. En su camino al Sol, unos demonios
se detienen en la luna y charlan con los miembros del Ateneo.
Los demonios llevan al Sol a un materialista ---muy probablemente
el propio Rivas--- pues al parecer Satanás no lo aceptó en el infierno
terrestre. Entonces comienza una discusión sobre el lugar en
que se ubica el averno, si en el subsuelo terrícola o en el Sol.
Este debate será uno de los argumentos que los compañeros de
Rivas presentarán ante el Santo Oficio para iniciar la investigación
en su contra. En el relato se le pregunta al materialista llegado
a la Luna si acaso conoce a un «atisbador de movimientos lunares».
Afirma entonces que solo conoce a un «almanaquista» que se
dedica a esos asuntos. Este último, según dice, mantiene una
relación epistolar con «el bachiller Ambrosio de Echeverría»
(Rivas: 45). Por eso la carta, como señala el título, va dirigida
a Echeverría y al profesor de «logarítmica» u observador de
movimientos lunares que les envío la misiva.

El fragmento que presentamos a continuación busca acercar al
lector al viaje de Onésimo Dutalón a la Luna y exponer la relación
y las opiniones que tienen los selenitas acerca de los habitantes
de Mérida. Según se lee en el fragmento, el movimiento acelerado
que padecen los habitantes de la península de Yucatán, debido
a la rotación de la Tierra, afecta su estado de ánimo en un sentido
que el lector está pronto a descubrir. Este asunto es otro de
los que llevarán a Rivas a tener que defenderse ante la Inquisición.
Vale la pena observar, finalmente, que tanto en el juego con
los registros calendáricos como en el viaje de Dutalón, es posible
ver el gusto de Rivas por una literatura informada por la ciencia.
Dutalón es un científico que construye una máquina voladora.
En su camino se dedica a probar ---y a demostrar que es falsa---
la teoría de Descartes sobre los torbellinos con la que este
último explicaba los movimientos del sistema solar. Es cierto
que el método que utiliza este viajero para llegar a la luna
no está presentado en base a algún conocimiento duro. Sin
embargo, en sus distintas exploraciones, el relato no se queda
en la fantasía, sino que muestra a menudo intereses y argumentaciones
propias de la ciencia de su época. En este sentido es que puede
considerársele también un texto de protociencia ficción, emparentado
con _Historia cómica de los estados e imperios de la Luna_ (1657)
de Cyrano de Bergerac y con el _Primero sueño_ (1692) de Sor
Juana. Las relaciones con este último texto están no solo en
la referencia al mito de Faetón, sino también en la manera
en que el texto de Rivas propone «que cualquier terrícola
durmiendo» puede también viajar a la luna (Rivas: 43).

# Fragmento

[…] Estando para disolverse el congreso a que yo asistí como Secretario
y computista vimos, como a distancia de dos millas y media (¡quién
lo pensara!) un carro o vajel volante instruido de dos alas y
un timón puesto donde debe estar, que venía rompiendo nuestra
atmósfera con una celeridad increíble. Al principio pensamos
que todo era ilusión pues no hay memoria ni tradición de haberse
visto jamás en nuestro orbe hombre alguno en cuerpo y alma. Salimos
a conducirle a nuestro Ateneo y después de haber hecho el arráez
una profunda reverencia, dio cuenta muy por menor de su viaje
y destino de que nosotros sólo podremos hacer un extracto muy
diminuto y él allá de vuelta podrá explayarse cuanto quiera.
Monsieures, dijo, yo me llamo Onésimo Dutalón: nací en un pequeño
lugar del Bayliage d'Stampe,@note en la Francia. Hice mis primeros
estudios en mi patria, más viendo que la filosofía de la escuela
era inútil y que no podía hacer docto chico ni grande, pasé a
París en donde me entregué con aplicación infatigable al estudio
de la física experimental, que es la verdadera. Y con esta ocasión,
después de una meditación pausada en las obras de aquel espíritu
de primer orden del suelo británico, el incomparable Isaac Newton,
me hice dueño de los más profundos arcanos de la geometría. Vuelto
a mi patria, cultivé la comunicación y amistad de un eclesiástico
llamado monsieur Desforges, hombre que sabe apreciar el mérito
de los sabios sin respeto a facultades, autoridad, ni poder.
Como nuestra amistad se iba estrechando cada día, quise darle
una prueba de confianza comunicándole el empeño en que estaba
de fabricar una máquina volante cual es la que véis. Después
de una infinita repugnancia instruí a monsieur Desforges, porque
así lo pedía, en todas las reglas que podían dirigir la práctica
del secreto comunicado. Yo no podré deciros, monsieures, en qué
paró la instrucción. Por lo que a mí toca, previniendo que al
vérseme discurrir por el aire se encendería una hoguera para
ser quemado públicamente en la plaza como mágico, tuve por conveniente,
para hacer algunos ensayos, antes de remontarme a las esferas
salvarme en una de las islas Calaminas en la Libia, flotantes
o nadantes en la superficie del agua, de que hacen mención Plinio
libro 2, capítulo 95; y Séneca libro 3, capítulo 25. Retirado
pues a una de estas islas, hice el primer ensayo lustrando toda
la África. En el segundo, picado de una curiosidad geográfica,
quise examinar por mí mismo si había alguna comunicación por
la parte del norte entre nuestro continente y el americano y
hallé que los dividía un euripo@note del mar glacial. En el tercero,
levantando un poco más el vuelo, hice asiento en la eminencia
de los dos montes más altos de la tierra, el de Tenerife en una
de las Canarias y el de Pichincha en el Perú. En la cumbre de
este último cerro tuve el gusto de experimentar que el agua regia
o fuente, libre de la gravitación y presión del aire, no disolvía
el oro poco ni mucho, como también por esta misma causa no tenían
gusto alguno sensible los cuerpos picantes y mordaces como la
pimienta, la sal, el acíbar,@note etcétera. Sobre la elasticidad
o resorte del aire, también hice algunos experimentos que ahora
no importa referir. Después de dos meses y medio volví a la isla
flotante de mi residencia y mirándome en una disposición ventajosa
para emprender un viaje literario a este planeta, me embarqué
en mi carro volante encomendándome a mi buena o mala suerte,
hallándose la Luna dicótoma@note respecto de quien la observaba
de la tierra, de cuyo centro distaba según su paralaje@note 59 semidiámetros
terrestres. Como yo en mi viaje no me apartaba del plano de la
equinoccial,@note corridas 273 leguas de atmósfera tuve la curiosidad
de arrojar al fluido que navegaba una cuartilla de papel de China
y observé con grande admiración mía que el papel seguía hacia
el Oriente la rotación que llevaba la atmósfera con el globo
terráqueo. Antes de salir de esta región hacía un frío incomparablemente
más intenso que el que sentí en la Estotilandia@note en mi segundo
ensayo sobre [el] que hice una reflexión digna de atención pública
en oportunidad favorable, para esforzar la opinión de cierto
filósofo moderno en orden a la causa del frío en sitios elevadísimos
sobre el nivel del mar. Tenía yo andadas bien seguramente 25
mil leguas cuando tuve bastante que reír acordándome del turbillón
terrestre de monsieur Descartes,@note quien por un rapto de imaginación
extravagante hace dar vuelta a la Luna alrededor de la Tierra
en fuerza de su turbillón, de la que no encontré el menor vestigio.
Y para asegurarme más bien, tiré al fluido una pipa llena de
agua del río Leteo,@note que perseveró inmóvil en aquel éter purísimo.
Y también vine en pensar que si allí se construyese una torre
cien mil veces más alta que la de Babel, se mantuviera eternamente
sin vaivén, sin movimiento, sin desunión de sus partes ni inclinación
o propensión a centro alguno.

Yo (digo la verdad) en medio de aquella materia celeste no sentí
frío ni calor, aún herido de los rayos directos del sol que congregué
en el foco de un exquisito espejo cáustico@note y no inflamaron
ni licuaron varias materias puestas a conveniente distancia sin
duda por falta del aire heterogéneo, de que concluí que la catóptrica@note
con sus demostraciones no tiene qué hacer en aquel éter sutilísimo
y homogéneo. En fin, monsieures, dijo el maquinario Dutalón,
después de los auxilios precautorios que tomé para el uso de
la inspiración y respiración en un espacio en donde no puede
haberle por su raridad e improporción, no tenéis por qué preguntarme
cuando me véis que sin pérdida de la vida he arribado velozmente
a este orbe. Yo os certifico que cualquiera terrícola durmiendo
[puede?] hacer el mismo viaje con la misma felicidad. Yo le continué
observando y filosofando y después de todo me hallo con la satisfacción
de haberme deshecho de una infinidad de preocupaciones, habiendo
registrado las claras fuentes en que deben beberse las noticias
experimentales, que es lo que aconseja Marcial en el epigrama
102 del libro 9.

> _Multum, crede mihi, refert, a fonte bibatur,_ \
> _qui fluit, an pigro, qui stupet unda, lacu._@note

Aquí iba a hablar el Presidente del Ateneo cuando distrajo nuestra
atención una tropa de ministros infernales que entrándose en
la asamblea, el jefe, que era de muy mala catadura, sin hacer
cortesía se explicó de este modo: Nosotros de orden de nuestro
príncipe vamos muy lejos de aquí cuanto de aquí dista el globo
solar. Conducimos el alma de un materialista, que en el punto
de la separación del cuerpo fue arrastrada a la puerta del infierno
en donde no quiso recibirle Luzbel diciendo que estaba informado
por sus esbirros que rodean toda la Tierra que es un espíritu
inquieto, turbulento, enemigo de la sociedad racional y de la
espiritualidad del alma. Que en su opinión la madre que le parió
no era de mejor condición que el zorro, el puerco espín, el escarabajo
y otro cualquiera vil insecto de la tierra cuya alma muere con
el cuerpo. Que no quería aumentar el desorden, la confusión y
el horror, que eternamente habita en su república, tal cual ella
es, con el establecimiento de un impío. Y que luego luego escoltado
por un destacamento de cuatrocientos demonios, fuese llevado
a aquel gran pirofilacio, el sol. ¿Al sol, dijo el Presidente
del Ateneo, en donde el Altísimo colocó (Salmo 18) su trono y
pabellón? Sí monsieur, al sol, repuso Dutalón, porque en el sol
colocó el infierno un anglicano, natural de Londres, llamado
Svvidin,@note que en una disertación, con los dos versículos 8 y
9 del capítulo 16 del Apocalipsis, pretende persuadir que el
lugar de los condenados está en medio del sol, en donde el demonio
fijó su trono (actas de los eruditos al mes de marzo, 1745) y
que ésta es la razón por que tantas naciones en el orbe terráqueo
hayan adorado al sol como Dios. Según eso, dijo el Presidente
del Ateneo, ese fatuo Svvidin también pudo con el mismo derecho
haber colocado el infierno en este orbe lunar, pues es constante
en nuestras memorias que la Luna ha tenido en la tierra sus adoradores.
Por ventura monsieur Dutalón, prosiguió el Presidente, ¿hay todavía
por allá altares consagrados a nuestro culto? Yo no sé, respondió
monsieur Dutalón, que se haya renovado las víctimas y holocaustos
de aquellos remotos siglos después del hecatombe que ofreció
el fundador de la escuela itálica, Pitágoras,@note en Crotón, noble
población al fondo del seno tarrentino en la Calabria, provincia
del Procurrentes de Italia, en acción de gracias por haber hallado
la proposición 47 del libro 1° [de] Euclides, con que enriqueció
las matemáticas. Y vos materialista, dijo el Presidente encarando
hacia él, ¿habéis estado en el quersoneso de Yucatán y tratado
o conocido por ventura allí un atisbador de movimientos lunares?
Yo Señor, respondió el materialista, he paseado todo aquel país
y conocido un sinnúmero de atisbadores de vidas ajenas, pero
de movimientos lunares sólo he oído hablar de un almanaquista
que ocupa el tiempo en esas bagatelas pudiendo emplearlo más
útilmente en formalidades forenses como: dar traslado a la parte,
en vista de autos, escrito de bien probado, acusar la rebeldía,
girar los autos, etcétera; que es ciencia de notarios y se hizo
ya de la moda, a que pudiera añadir el leve trabajo de registrar
índices de libros de consultas en romance o en latín tan claro
como el canon de la misa, para hacerse espectable en el vulgo
por este camino ya que no puede por otro. También hoy decía que
el almanaquista mantiene comunicación epistolar con el Bachiller
Don Ambrosio de Echeverría, residente en el pueblo de Mama, hombre
de un juicio sólido, muy práctico en los primores de la música
moderna y en el manejo del canon trigonométrico, de quien podréis
informaros en cuanto deseáis saber. Dicho esto, le arrebataron
los demonios siguiendo su derrota a aquel océano de fuego.

Ido el destacamento infernal, monsieur Dutalón pidió con un modo
muy obligante se le diera una instrucción para correr todo este
hemisferio y su opuesto y notar lo más excelente que encontrase
en el orbe lunar.

[…] {.espacio-arriba1 .sin-sangria}

Monsieur Dutalón se entró en su carro volante tomando
el rumbo del sudueste y dado el buen viaje, nos mantuvimos en
el Ateneo hasta su vuelta. {.espacio-arriba1 .sin-sangria}

Entretanto nosotros tomamos la gustosa diversión de colocar la
ciudad de Mérida de Yucatán debajo del meridiano inmóvil de un
globo geográfico que aquí dejó monsieur Dutalón y hallamos que
su latitud septentrional es 20 grados 20 minutos, lo mismo que
teníamos observado, como también su situación a la mitad del
tercer clima, cuyo día máximo del año debe ser de 13 horas 15
minutos. Y como desde aquí vemos que gira la tierra de poniente
a levante sobre su propio eje a proporción del movimiento de
la equinoccial terrestre, le corresponde a esta península, según
su paralelo, cuatro leguas españolas en un minuto de tiempo.
Verdaderamente es un milagro continuado de la Omnipotencia que
todos sus habitadores no sean lanzados por esos aires con un
movimiento muchísimo más impetuoso que el que a la piedra da
la honda pastoril por la tangente de su círculo. En esta consideración
debéis padecer un vértigo o desvanecimiento de cabeza permanente
que impida las funciones y reflexiones de una alma racional dandóos,
como gente sin un adarme de seso, a todo género de profanidades,
al lujo, a la farándula, al dolo, a la perfidia, a la alevosía,
a la simulación profunda, a la codicia sórdida, a la ambición
violenta hasta pisar descaradamente lo sagrado, una adulación
fastidiosa hasta el abatimiento, una calumnia detestable hasta
el más alto grado de malicia, una discordia perpetua entre la
lengua y el corazón, una sensualidad más que brutal que sólo
con la muerte acaba, una mendacidad por herencia, una volubilidad
o inconstancia por temperamento y otras torpezas indignas de
la naturaleza racional que pueden llenar de borrones más papel
que conduce una flota al puerto de la Veracruz. De intento hemos
formado este panegírico o llámese inventiva si así lo queréis,
en despique de los chistes que nos comunica el atisbador en su
carta de 5 del mes epifi, en que dice que los pocos terrícolas
que allá están por nuestra existencia dicen que sí, que somos
gente, pero ¿qué gente? Una gente sin palabra, sin vergüenza,
sin seso, unos tramposos, inconstantes, lunáticos. ¡¡Miren quiénes
hablan!!

Vuelto monsieur Dutalón de su viaje en que gastó cerca de cuatro
meses celestes, nos manifestó el placer de que estaba penetrado
de haber corrido todo nuestro orbe lunar. Monsieures, dijo, en
todo el universo no puede darse lugar más cómodo, más ameno ni
más delicioso para habitación de vivientes que adoren y alaben
al Creador. Yo apuesto que si hubiera discurrido por todas estas
regiones cualquiera de los que condenan como absurda la opinión
de colocar en la Luna el paraíso de donde fue empujado el buen
padre Adán por dar gusto a una mujer (¡ojalá no se hubiera derivado
a su posteridad esta fácil condescendencia!) acaso moderara su
sentir. ¡Qué maravillas y bellezas de naturaleza que aquí pasan
por ordinario y no pueden contemplarse sin estupor y asombro!
¡Qué gobierno tan dulce y acomodado a la temperie de los anctítonas!
Ciertamente allá nuestro globo terráqueo, por su constitución,
ha menester distinción de clases, en donde la suerte de los que
gobiernan es la más infeliz porque si el superior gobierna mal,
a todos desagrada; si gobierna bien, a pocos podrá agradar, siendo
muy pocos los amantes de la justicia y equidad. En fin, monsieures,
ya se acerca el tiempo de subir al globo de donde vine y retirarme
a mi amada isla flotante a trazar la obra que os dije, de que
a otro viaje prometo daros un ejemplar que podréis añadir a vuestros
registros o memorias.

El Presidente del Ateneo suplicó a monsieur Dutalón se sirviera
pasar por la península de Yucatán y poner en mano propia del
Bachiller Don Ambrosio de Echeverría, residente en el pueblo
de Mama, este escrito que será bien recibido por estar grabado
en láminas de plata. Y monsieur Dutalón respondió que todo ejecutaría
con buena voluntad y añadió que a otro viaje se venía con el
Bachiller Echeverría, de quien recibiera órdenes para el globo
de la Luna porque quedamos muy obligados. Y a mí, el presente
Secretario, mandó el Presidente del Ateneo lunar diera fe de
todo lo dicho y obrado y lo rubricara de mi nombre, lo que hago
hoy 7 del mes dydimón de nuestro año del incendio lunar 7.914.522.

Señor Bachiller \
Por mandado del Presidente del Ateneo lunar \
Remeltoín Secretario {.espacio-arriba1 .derecha}

# _Sizigias y cuadraturas lunares_ (1775) de Manuel Antonio de Rivas @ignore

</section>
<section epub:type="chapter" role="doc-chapter">

# _Un viaje celeste_ (1870) de Pedro Castera

Lejos del lenguaje barroco de Sor Juana y del registro satírico
de Rivas, _Un viaje celeste_, de Pedro Castera (1846-1906),
sintetiza varios rasgos presentes en el _Primero sueño_, y en
_Sizigias y cuadraturas lunares_ pero en un contexto que ya es
el de la modernidad. Por un lado, como en el caso de Sor Juana,
es un viaje realizado «con los ojos del alma», pero en el que
el «delirio» propio del sueño tiene lugar al momento mismo
de la escritura. Como en _Sizigias_, el viaje sobrepasa las fronteras
del planeta Tierra; sin embargo, va mucho más allá de la luna
pues, tal como lo deseaba el texto de Sor Juana, termina por
elevarse hasta estrellas lejanas. El viajero describe otros planetas,
como Júpiter, que presenta como habitados. En su camino hace
un compendio de los conocimientos astronómicos de su época: se
refiere a los planetas y estrellas del sistema solar, al tamaño
que tienen con respecto a la Tierra, a la distancia que hay entre
ellos y a la Vía Láctea, pero también a los átomos, a las moléculas
y a la velocidad de la luz. Un dios creador está presente a lo
largo de todo el texto. Lo acompaña una sensación de asombro
y pavor, propia de la concepción de lo sublime en el siglo +++XIX+++,
consecuencia de lidiar con un espacio infinito. Por último, un
vocabulario que hace pensar en el establecimiento del Estado
nación mexicano en esas décadas, con palabras como «patria» o
«ciudadanía», aparece en el texto. Pero se ve desbordado por
el viaje que se narra, al punto en que el texto afirma: «el
Universo es la patria de la humanidad y el hombre ciudadano del
cielo».

Pedro Castera es además el autor de la primera novela de ciencia
ficción en México de título _Querens,_ en la que un científico
experimenta con la hipnosis para implantarle recuerdos a una
mujer. En el fragmento de _Un viaje celeste_ que presentamos
a continuación transcribimos buena parte del texto original.
En él puede leerse el viaje que lleva a cabo el alma, así como
el asombro y el miedo que experimenta.

# Fragmento

_El hombre es el ciudadano del cielo._ \
++Flammarion++ {.epigrafe}

Mis ojos no podían desprenderse de esta línea, cuyos
caracteres brillaban con mágica luz. Recordaba que
Sócrates dijo: «El hombre es el ciudadano del mundo».
Pero como esta raquítica esfera es importante
para calmar nuestras aspiraciones, ilustre astrónomo
ha procurado con frase sublime nuestra legítima ambición.
Es cierto que el cielo no basta para llenar el
alma; pero el infinito es el velo con que se cubre
Dios, y tarde o temprano el Supremo Ideal habrá satisfecho
el anhelo de nuestro espíritu.

Mi absorción era completa; pero a poco iba olvidándolo
todo; mis ojos fueron perdiendo la percepción;
caí lentamente en una especie de sonambulismo
espontáneo. Mis sentidos se entorpecieron, pero
mi inteligencia no estaba embotada; con los ojos del
alma lo veía todo, comprendía lo que me estaba pasando;
pero aquel éxtasis, compuesto de no sé qué
voluptuosidades extrañas, era tan dulce, había en él
una mezcla tan indefinible de ideas, de delirios, de
fruiciones desconocidas, que en lugar de resistirme,
me dejaba arrastrar por aquella languidez llena de encanto
y también de vida. ¡Oh, yo quisiera estar siempre
así!

Mi alma se fue desprendiendo de mi cuerpo como
si fuese un vapor, un éter, un perfume; la veía, es
decir, me veía a mí mismo, como si estuviese formado
de gasa o de crespón aparente, y sin embargo
real, pero con todas aquellas ondulaciones, ligerezas
y flexibilidades que tiene lo intangible.

Aquello era maravilloso; la sorpresa que me
causaba mi nuevo estado no me dejaba ya lugar a la
reflexión; mi pobre cuerpo yacía exánime, sin movimiento,
en una postración absoluta. Comencé a creer
que había muerto, pero de una manera tan dulce, tan
bella, que no me arrepentía; antes bien estaba resuelto
a principiar nuevamente. Algunos momentos después
me hallaba convencido hasta la opresión de mi
nuevo estado, y con una gratitud inmensa al Creador
que había cortado con tanta dulzura el hilo de mi
triste vida.

¡Cosa rara!, mi vista adquirió una penetración y
un alcance admirable; las paredes de la habitación
las veía transparentes como si fuesen de cristal; la
materia toda diáfana, límpida, incolora y clara como
el agua pura; veía infinidad de animalículos pequeñísimos
habitándolo todo; los átomos flotantes
del aire estaban poblados de seres; las moléculas
más imperceptibles palpitaban bajo el soplo omnipotente
de la vida y del amor… Mis demás sentidos
se habían desarrollado en la misma proporción, y
me sentía feliz, os lo aseguro; intensamente feliz.

Al verme dotado con tan bellas facultades, mi
vacilación fue muy corta; levanté la mirada… y caí
anonadado al contemplar la magnificencia de los
cielos.

Oré un instante, y con la rapidez del pensamiento,
me lancé a vagar por el bellísimo jardín de la creación.
En mi estado normal veo a las estrellas, melancólicas
pupilas, fijas sobre la Tierra; rubíes, brillantes,
topacios, esmeraldas y amatistas, incrustadas en
un espléndido zafiro, pero entonces… ¡Oh!… entonces
voy a referiros con más calma lo que vi.

Es preciso que ordene algo mis ideas.

Comenzaré, pues, por deciros que me bastaba
pensar para que siguiese al pensamiento la más rápida
ejecución, y por lo mismo, la idea que había tenido
de ascender por los espacios me alejó de la Tierra
a una distancia inmensa.

A lo lejos veía una esfera colosal ---un millón quinientas
mil veces mayor que la Tierra---, incandescente
como el ojo sangriento de una fiera, roja como el
fuego, volaba con velocidad, arrastrando en aquella
carrera una multitud de esferas, entre las cuales había
algunas algo aplanadas por dos puntos, pero todas
de mucho menores dimensiones, pues si hubieran
podido reunirse no igualarían con su volumen al
hermosísimo disco de fuego; a pesar de que se encontraban
algo lejanas, las percibía con una claridad
extraordinaria, capaz de permitirme examinar hasta
sus menores detalles.

Figuraos mi asombro: aquella antorcha encendida
en medio de los cielos era nuestro Sol, y sus
acompañantes, su familia de planetas.

Pero no era todo, no: lo que me dejaba mudo,
absorto, enajenado, era que todas aquellas masas
enormes eran ¡mundos! más o menos semejantes al
nuestro, pero todos ellos, sin excepción, mundos
habitados.

Sí, sí, yo veía las manchas blancas de las nieves
polares, las nubes cruzando sus atmósferas, las unas
densas, cargadas de brumas, las otras purísimas y tenues,
los mares brillaban como líquida plata, y los
continentes parecían inmensas aves que se recostaban
cansadas de volar.

Allí hay seres, me decía yo, seres humanos, habitantes,
hombres tal vez, y ángeles como los que
habitan la Tierra con nombres de mujeres, porque si
no fuera así, esos mundos serían horribles; allí estarán
mis hermanas, mis padres, mi familia…

¡Oh Dios mío, cómo a la vista de esos mundos se
despliega tu soberana omnipotencia!

Entonces busqué a Júpiter, que de los planetas
de nuestro sistema es el mayor y el más bello; la Tierra
la veía como la 1/126 parte del brillante astro, que
me deslumbró por su hermosura; esto en cuanto a
superficie.

Sus montañas tienen una inclinación muy suave,
sus llanuras son perfectamente planas, los mares
tranquilos; nada de nieve; la eterna primavera bordando
sus campos, flores divinas embriagando con
sus deliciosos aromas a esos felices habitantes, aves
de pintados colores cruzando en todas direcciones,
y cuatro magníficas lunas que deben producir en
sus serenas y apacibles noches unos juegos de luz
admirables.

Multitud de ciudades diseminadas sobre su superficie,
pero por más que lo procuré no puede distinguir
los habitantes; tal vez serán de una belleza
deslumbradora, que después me hubieran hecho despreciar
los de la Tierra, y por eso la Providencia me
evitó el verlos. Júpiter es un mundo en el cual el dolor
no es conocido, es un verdadero Edén.

Mercurio y Venus no llamaban mi atención, la
Tierra me daba cólera por orgullosa, Marte tiene tantos
cataclismos y cambios que tampoco me agradaba,
los asteroides me parecían muy pequeños, olvidé
a Saturno, a Urano, y después de mi hermoso Júpiter,
mi futura patria, pensé en Neptuno, que según
la mitología representa al dios de las aguas.

Aquello fue un salto peligroso; en menos de un
segundo atravesé centenares de millones de leguas y
me encontré a una distancia regular del astro que por
hoy limita nuestro sistema. Entonces no comprendí
muy bien lo que me pasaba: el Sol lo veía del tamaño
de una lenteja, Saturno enorme, como de un volumen
de setecientas treinta y cuatro veces mayor
que la Tierra, y yo me hallaba en una penumbra indefinible.

La naturaleza, como la obra de Dios, es admirable;
apenas pude distinguir que aquel mundo, como
los otros, estaba habitado; pero previendo la lejanía
del Sol, los seres que allí viven tienen la facultad de
desprender luz, están rodeados de una aureola luminosa,
tan bella, que fascinado no podía apartar de
ellos mi vista embelesada con su contemplación.

Me fue imposible fijarme en más detalles, porque
en un momento me sentí arrastrado por una
fuerza extraña; observé lo que era: la cauda de un
cometa me envolvía, me encontraba en una línea de
atracción del astro errante, que sacudía su magnífica
cabellera en la inmensidad.

El vehículo celeste era cómodo y bello; me dejé
llevar sin oponer resistencia. La velocidad de mi
tren expreso iba aumentando cada vez más; cruzábamos
los abismos dejando a nuestros pies infinitas
miríadas de mundos.

Repentinamente observé que una estrella doble,
púrpura y oro, crecía a mi vista de una manera espantosa;
en algunos segundos adquirió proporciones
gigantescas, como de unas diez veces más que nuestro
Sol; sentí una atmósfera de fuego, y abandonando
mi solitario compañero me lancé huyendo en dirección
opuesta.

Os he dicho ya que volaba por los cielos con la
velocidad del pensamiento; los soles de colores se
multiplicaban a mi vista, ya rojos o violados, amarillos
o verdes, blancos o azules, y alrededor de cada
uno de ellos flotaban infinidad de mundos en los
cuales palpitaba también la vida y el amor.

Yo seguía corriendo, volando con una rapidez
vertiginosa, atravesaba las inmensas llanuras celestes
bordadas de flores, me sentía arrastrado por lo
invisible, y trémulo y palpitante, yo balbuceaba una
oración.

Aquello no terminaba nunca, nunca… La alfombra
de soles que Dios tiene a sus pies se prolongaba
hasta lo infinito… se pasaron instantes o siglos, no lo
sé; yo seguía con mayor velocidad que la luz, que la
Chispa eléctrica, que el pensamiento, y aquella magnífica
contemplación seguía también… soles inmensos
de todos colores, mundos colosos girando a su
derredor, y todo… todo lleno de vida, de seres, de almas
que bendecían a Dios. Los soles cantando con
voz luminosa y los mundos elevando sus himnos
formaban el concierto sublime, grandioso, divino de
la armonía universal.

Atravesaba los desiertos del espacio cruzando
de una nebulosa a otra; la extensión seguía; atravesaba
multitud de vías lácteas en todas direcciones, y
volaba… seguía… y la inmensidad seguía también.

Estaba jadeante, rendido, abrumado; oraba con
fervor y me sentía arrastrar por una fuerza irresistible:
los abismos, los espacios, las nebulosas, los soles
y los mundos se sucedían sin interrupción, se
mezclaban, se agitaban en turbiones armónicos sobre
mi frente humillada, abatida ante tanta magnificencia,
ante tan deslumbrante esplendidez. Yo estaba
ciego, loco, casi no existía ya; pequeño átomo
perdido en aquella inmensidad, apenas me atrevía a
murmurar conmovido, temblando, admirado ante la
manifestación divina de la Omnipotente Causa
Creadora, ¡Dios mío! ¡Dios mío!

De pronto mi carrera cesó… Dios escuchaba al
átomo.

Tardé algún tiempo en reponerme; perdido en la
extensión sideral, busqué en vano la Tierra; nada, no
se veía; quise encontrar nuestro Sol, pero imposible;
tampoco lo veía.

Apenas allá a lo lejos, a una distancia incalculable,
perdida en los abismos sin límites de la eternidad,
pude ver nuestra Vía Láctea, que parecía una
pequeña cinta de plata formando un círculo de dimensiones
como el de una oblea, que volaba con
una velocidad inapreciable en la profundidad divina
de las regiones infinitas. Ligero y veloz me lancé hacia
ella; pronto llegué, sin saber cómo; pero entre
sus setenta millones de soles no podía encontrar el
nuestro. Pensé entonces que con la velocidad de la
luz tardaría quince mil años en dar una vuelta a
nuestra pequeña Vía Láctea, y abrumado por aquel
cálculo, sin poder comprenderlo, oprimido por semejante
idea, me detuve lleno de terror. ¿Qué hacer?
¿Cómo hallar la miserable chispa que llamamos
Sol? ¿Cómo encontrar la Tierra, átomo mezquino,
molécula despreciable, excrecencia diminuta de
aquel sol que no podía hallar por su pequeñez? ¡Oh!
Entonces mi alma, desfallecida, ansiosa, anhelante,
se dirigió a Dios.

¡Oh Tú, espléndido sol de los soles, Supremo
Ideal de las almas, Espíritu de Luz y de Vida, Amor
Infinito de la Inmensidad de la Creación, del Universo!…
¡Oh, Tú, mi Dios, vuélveme a mi átomo y perdona
mi loco orgullo, vuélveme a la Tierra, Dios
mío, porque allí está lo que yo amo!

Mi carrera comenzó de nuevo terrible, frenética,
espantosa; sentía vértigo, un ansia atroz, algo como
el frío de la muerte; corría, volaba y… en ese momento
Manuel de Olaguíbel me sacudió fuertemente por
el brazo; yo me encontraba sentado en mi escritorio,
con el pelo algo quemado, las manos convulsas, multitud
de papeles en desorden, y escritas las anteriores
líneas.

# _Un viaje celeste_ (1870) de Pedro Castera @ignore

</section>
<section epub:type="chapter" role="doc-chapter">

# _La última guerra_ (1906) de Amado Nervo

El cuento _La última guerra_, de Amado Nervo (1870-1919), narra
una revolución llevada a cabo por los animales. El relato comienza
exponiendo una secuencia histórica: «Tres habían sido las grandes
revoluciones de que se tenía noticia: la que pudiéramos llamar
Revolución cristiana, que en modo tal modificó la sociedad y
la vida en todo el haz del planeta; la Revolución francesa, que,
eminentemente justiciera, vino, a cercén de guillotina, a igualar
derechos y cabezas, y la Revolución socialista, la más reciente
de todas, aunque remontaba al año 2030 de la era cristiana»
(187). Las acciones en el texto, por tanto, distinguen entre
dos momentos históricos, el antes y después de la «Revolución
social». Así, al igual que el nacimiento de Cristo, la Revolución
social establece un nuevo comienzo o año cero en el calendario
humano. Algo hay aquí de las cronologías y los calendarios de
distintas culturas con los que juega de Manuel de Rivas, pero
ahora despojados de su tono satírico. A penas transcurridos seis
años del siglo +++XX+++, en 1906, Nervo se refiere ya a una Revolución
socialista y a una «última guerra» por la igualdad que vendría
después. Se anticipa así a la Revolución rusa de 1917. Sin embargo,
en el relato la Revolución social sucede a una escala planetaria,
se entrelaza con la teoría de la evolución y la idea de progreso,
que aún tenía credibilidad antes de las dos guerras mundiales.

En los días anteriores a la Revolución social, según el cuento,
los cuerpos, principalmente los del proletariado, habían mutado.
Los trabajadores manuales tenían seis dedos en la mano derecha,
mientras que a quienes laboraban en la conducción de vehículos
se les habían atrofiado completamente las piernas. Las dos revoluciones
que Nervo sitúa en el futuro se entrelazan con mutaciones genéticas
y es por ello que entre una y otra transcurren largos periodos
de tiempo. Según el texto, la Revolución social planetaria sucedió
en el año 2030 ---tan cercano a nosotros---, pero la voz
que narra el cuento se sitúa «en el año 3502» D.R.s.
(«Después de la Revolución social», es decir, en
el año «5532 de la era cristiana»).
Para esta fecha, gracias a las condiciones de igualdad, las mutaciones
que el trabajo había producido en el proletariado finalmente
han desaparecido. La evolución, sin embargo, propició que los
animales comenzaran a desarrollar su propio lenguaje. Ante este
hecho, los seres humanos reaccionaron incorporándolos a su vida
social forzándolos a realizar las labores del viejo proletariado.

En el cuento, la sociedad del futuro se encuentra altamente tecnificada.
Hay aparatos como el «fonotelerradiógrafo», en el que «las
vibraciones del cerebro al pensar» se comunican «directamente
a un registrador especial, que a su vez» las transmite a su destino
(Nervo: 189). Además, esta sociedad se encuentra fuertemente estratificada.
Los seres humanos son, según el texto, una élite ociosa, mientras
los animales que realizan el trabajo tienen todos los conocimientos
necesarios para usar la ciencia y las máquinas de esta época.
«Por su posición geográfica en la medianía de América y entre
los dos grandes océanos», México se ha posicionado como «el
centro del mundo» (Nervo: 191). En el fragmento que presentamos
a continuación se observa cómo es en este país que se anuncia
el comienzo de la llamada «última guerra». El lenguaje de los
seres humanos, según el cuento, es uno de los testimonios de
que la evolución de las especies es un camino hacia el progreso.
En el relato, sin embargo, este progreso no se realiza exclusivamente
en los seres humanos y, sobre todo, es indiferente a su supervivencia.

# Fragmento

## III {.centrado}

[…] {.espacio-arriba1 .sin-sangria}

Había en la falda del Ajusco, adonde llegaban los últimos barrios de la
ciudad, un gimnasio para mamíferos, en el que estos se reunían los días
de fiesta y casi pegado al gimnasio un gran salón de conciertos, muy
frecuentado por los mismos. En este salón, de condiciones acústicas
perfectas y de amplitud considerable, se efectuó el domingo 3 de agosto
de 5532 ---de la nueva era--- la asamblea en cuestión. {.espacio-arriba1 .sin-sangria}

Presidía Equs Robertis, un caballo muy hermoso, por cierto; y el primer
orador designado era un propagandista célebre en aquel entonces, Can
Canis, perro de una inteligencia notable, aunque muy exaltado. Debo
advertir que en todas partes del mundo repercutiría, como si dijéramos,
el discurso en cuestión, merced a emisores especiales que registraban
toda vibración y la transmitían solo a aquellos que tenían los
receptores correspondientes, utilizando ciertas corrientes magnéticas;
aparatos estos ya hoy en desuso por poco prácticos.

Cuando Can Canis se puso en pie para dirigir la palabra al auditorio,
oyéronse por todas partes rumores de aprobación.

## IV {.centrado}

---Mis queridos hermanos ---empezó Can Canis----:

»La hora de nuestra definitiva liberación está próxima. A un signo
nuestro, centenares de millares de hermanos se levantarán como una sola
masa y caerán sobre los hombres, sobre los tiranos, con la rapidez de
una centella. El hombre desaparecerá del haz del planeta y hasta su
huella se desvanecerá con él. Entonces seremos nosotros dueños de la
tierra, volveremos a serlo, mejor dicho, pues que primero que nadie lo
fuimos, en el albor de los milenarios, antes de que el antropoide
apareciese en las florestas vírgenes y de que su aullido de terror
repercutiese en las cavernas ancestrales. ¡Ah!, todos llevamos en los
glóbulos de nuestra sangre el recuerdo orgánico, si la frase se me
permite, de aquellos tiempos benditos en que fuimos los reyes del mundo.
Entonces, el sol enmarañado aún de llamas a la simple vista, enorme y
tórrido, calentaba la tierra con amor en toda su superficie, y de los
bosques, de los mares, de los barrancos, de los collados, se exhalaba un
vaho espeso y tibio que convidaba a la pereza y a la beatitud. El mar
divino fraguaba y desbarataba aún sus archipiélagos inconsistentes,
tejidos de algas y de madréporas; la cordillera lejana humeaba por las
mil bocas de sus volcanes, y en las noches una zona ardiente, de un rojo
vivo, le prestaba una gloria extraña y temerosa. La luna, todavía joven
y lozana, estremecida por el continuo bombardeo de sus cráteres,
aparecía enorme y roja en el espacio, y a su luz misteriosa surgía
formidable de su caverna el león _saepelius_; el uro erguía su testa
poderosa entre las breñas, y el mastodonte contemplaba el perfil de las
montañas, que, según la expresión de un poeta árabe, le fingían la
silueta de un abuelo gigantesco. Los saurios volantes de las primeras
épocas, los iguanodontes de breves cabezas y cuerpos colosales, los
_Megatheriums_ torpes y lentos, no sentían turbado su reposo más que por el
rumor sonoro del mar genésico, que fraguaba en sus entrañas el porvenir
del mundo.

»¡Cuán felices fueron nuestros padres en el nido caliente y piadoso de la
tierra de entonces, envuelta en la suave cabellera de esmeralda de sus
vegetaciones inmensas, como una virgen que sale del baño!… ¡Cuán
felices!… A sus rugidos, a sus gritos inarticulados, respondían solo
los ecos de las montañas… Pero un día vieron aparecer con curiosidad,
entre las mil variedades de cuadrúmanos que poblaban los bosques y los
llenaban con sus chillidos desapacibles, una especie de monos rubios
que, más frecuentemente que los otros, se enderezaban y mantenían en
posición vertical, cuyo vello era menos áspero, cuyas mandíbulas eran
menos toscas, cuyos movimientos eran más suaves, más cadenciosos, más
ondulantes, y en cuyos ojos grandes y rizados ardía una chispa extraña y
enigmática que nuestros padres no habían visto en otros ojos en la
tierra. Aquellos monos eran débiles y miserables… ¡Cuán fácil hubiera
sido para nuestros abuelos gigantescos exterminarlos para siempre!… Y
de hecho, ¡cuántas veces cuando la horda dormía en medio de la noche,
protegida por el claror parpadeante de sus hogueras, una manada de
mastodontes, espantada por algún cataclismo, rompía la débil valla de
lumbre y pasaba de largo triturando huesos y aplastando vidas; o bien
una turba de felinos que acechaba la extinción de las hogueras, una vez
que su fuego custodio desaparecía, entraba al campamento y se ofrecía un
festín de suculencia memorable!… A pesar de tales catástrofes,
aquellos cuadrúmanos, aquellas bestezuelas frágiles, de ojos
misteriosos, que sabían encender el fuego, se multiplicaban; y un día,
día nefasto para nosotros, a un macho de la horda se le ocurrió, para
defenderse, echar mano de una rama de árbol, como hacían los gorilas, y
aguzarla con una piedra, como los gorilas nunca soñaron hacerlo. Desde
aquel día nuestro destino quedó fijado en la existencia: el hombre había
inventado la máquina, y aquella estaca puntiaguda fue su cetro, el cetro
de rey que le daba la naturaleza… ¿A qué recordar nuestros largos
milenarios de esclavitud, de dolor y de muerte?… El hombre, no
contento con destinarnos a las más rudas faenas, recompensadas con malos
tratamientos, hacía de muchos de nosotros su manjar habitual, nos
condenaba a la vivisección y a martirios análogos, y las hecatombes
seguían a las hecatombes sin una protesta, sin un movimiento de
piedad… La naturaleza, empero, nos reservaba para más altos destinos
que el de ser comidos a perpetuidad por nuestros tiranos. El progreso,
que es la condición de todo lo que alienta, no nos exceptuaba de su ley;
y a través de los siglos, algo divino que había en nuestros espíritus
rudimentarios, un germen luminoso de intelectualidad, de humanidad
futura, que a veces fulguraba dulcemente en los ojos de mi abuelo el
perro, a quien un sabio llamaba en el siglo +++XVIII+++ ---_post_ +++J.C.+++--- un
candidato a la humanidad; en las pupilas del caballo, del elefante o del
mono, se iba desarrollando en los senos más íntimos de nuestro ser,
hasta que, pasados siglos y siglos floreció en indecibles
manifestaciones de vida cerebral… El idioma surgió monosilábico,
rudo, tímido, imperfecto, de nuestros labios; el pensamiento se abrió
como una celeste flor en nuestras cabezas, y un día pudo decirse que
había ya nuevos dioses sobre la tierra; por segunda vez en el curso de
los tiempos el Creador pronunció un “_Fiat, et homo factus fuit_”.

»No vieron ellos con buenos ojos este paulatino surgimiento de humanidad;
mas hubieron de aceptar los hechos consumados, y no pudiendo
extinguirla, optaron por utilizarla… Nuestra esclavitud continuó,
pues, y ha continuado bajo otra forma: ya no se nos come, se nos trata
con aparente dulzura y consideración, se nos abriga, se nos aloja, se
nos llama a participar, en una palabra, de todas las ventajas de la vida
social; pero el hombre continúa siendo nuestro tutor, nos mide
escrupulosamente nuestros derechos… y deja para nosotros la parte más
ruda y penosa de todas las labores de la vida. No somos libres, no somos
amos, y queremos ser amos y libres… Por eso nos reunimos aquí hace
mucho tiempo, por eso pensamos y maquinamos hace muchos siglos nuestra
emancipación, y por eso muy pronto la última revolución del planeta, el
grito de rebelión de los animales contra el hombre, estallará, llenando
de pavor el universo y definiendo la igualdad de todos los mamíferos que
pueblan la tierra»…

Así habló Can Canis, y este fue, según todas las probabilidades, el
último discurso pronunciado antes de la espantosa conflagración que
relatamos.

## V {.centrado}

El mundo, he dicho, había olvidado ya su historia de dolor y de muerte;
sus armamentos se orinecían en los museos, se encontraba en la época
luminosa de la serenidad y de la paz; pero aquella guerra que duró diez
años, como el sitio de Troya, aquella guerra que no había tenido ni
semejante ni paralelo por lo espantosa, aquella guerra en la que se
emplearon máquinas terribles, comparadas con las cuales los proyectiles
eléctricos, las granadas henchidas de gases, los espantosos efectos del
radio utilizado de mil maneras para dar muerte, las corrientes
formidables de aire, los dardos inyectores de microbios, los choques
telepáticos…, todos los factores de combate, en fin, de que la
humanidad se servía en los antiguos tiempos, eran risibles juegos de
niños; aquella guerra, decimos, constituyó un inopinado, nuevo,
inenarrable aprendizaje de sangre…

Los hombres, a pesar de su astucia, fuimos sorprendidos en todos los
ámbitos del orbe, y el movimiento de los agresores tuvo un carácter tan
unánime, tan certero, tan hábil, tan formidable, que no hubo en ningún
espíritu siquiera la posibilidad de prevenirlo…

Los animales manejaban las máquinas de todos géneros que proveían a las
necesidades de los elegidos; la química era para ellos eminentemente
familiar, pues que a diario utilizaban sus secretos: ellos poseían
además y vigilaban todos los almacenes de provisiones, ellos dirigían y
utilizaban todos los vehículos… Imagínese, por tanto, lo que debió
ser aquella pugna, que se libró en la tierra, en el mar y en el aire…
La humanidad estuvo a punto de perecer por completo; su fin absoluto
llegó a creerse seguro ---seguro lo creemos aún---… y a la hora en que
yo, uno de los pocos hombres que quedan en el mundo, pienso ante el
fonotelerradiógrafo estas líneas, que no sé si concluiré, este relato
incoherente que quizá mañana constituirá un utilísimo pedazo de
historia… para los humanizados del porvenir, apenas si moramos sobre
el haz del planeta unos centenares de sobrevivientes, esclavos de
nuestro destino, desposeídos ya de todo lo que fue nuestro prestigio,
nuestra fuerza y nuestra gloria, incapaces por nuestro escaso número y a
pesar del incalculable poder de nuestro espíritu, de reconquistar el
cetro perdido, y llenos del secreto instinto que confirma asaz la
conducta cautelosa y enigmática de nuestros vencedores, de que estamos
llamados a morir todos, hasta el último, de un modo misterioso, pues que
ellos temen que un arbitrio propio de nuestros soberanos recursos
mentales nos lleve otra vez, a pesar de nuestro escaso número, al trono
de donde hemos sido despeñados… Estaba escrito así… Los autóctonos
de Europa desaparecieron ante el vigor latino; desapareció el vigor
latino ante el vigor sajón, que se enseñoreó del mundo… y el vigor
sajón desapareció ante la invasión eslava; esta, ante la invasión
amarilla, que a su vez fue arrollada por la invasión negra, y así, de
raza en raza, de hegemonía en hegemonía, de preeminencia en
preeminencia, de dominación en dominación, el hombre llegó perfecto y
augusto a los límites de la historia… Su misión se cifraba en
desaparecer, puesto que ya no era susceptible, por lo absoluto de su
perfección, de perfeccionarse más… ¿Quién podía sustituirlos en el
imperio del mundo? ¿Qué raza nueva y vigorosa podía reemplazarle en él?
Los primeros animales humanizados, a los cuales tocaba su turno en el
escenario de los tiempos… Vengan, pues, enhorabuena; a nosotros,
llegados a la divina serenidad de los espíritus completos y definitivos,
no nos queda más que morir dulcemente. Humanos son ellos y piadosos
serán para matarnos. Después, a su vez, perfeccionados y serenos,
morirán para dejar su puesto a nuevas razas que hoy fermentan en el seno
oscuro aún de la animalidad inferior, en el misterio de un génesis
activo e impenetrable… ¡Todo ello hasta que la vieja llama del sol se
extinga suavemente, hasta que su enorme globo, ya oscuro, girando
alrededor de una estrella de la constelación de Hércules, sea fecundado
por primera vez en el espacio, y de su seno inmenso surjan nuevas
humanidades… para que todo recomience!

# _La última guerra_ (1906) de Amado Nervo @ignore

</section>
<section epub:type="chapter" role="doc-chapter">

# _Eugenia_ (1919) de Eduardo Urzaiz

Como el título deja entrever, _Eugenia_, de Eduardo Urzaiz (1876-1955),
imagina un mundo futuro organizado bajo los preceptos de la eugenesia;
un conjunto de teorías para el supuesto mejoramiento de la especie
humana, a través de modalidades de intervención biomédica reguladas
por el Estado. La eugenesia fue popular en los círculos médicos
del México posrevolucionario. Eduardo Urzaiz estudió medicina
y se especializó en psiquiatría. Fue, durante casi tres décadas,
el director del asilo Ayala, un hospital psiquiátrico inaugurado
por Porfirio Díaz en 1906 y considerado como uno de los más innovadores
del país. Urzaiz era una voz activa en los debates de la época
sobre contracepción y derechos políticos de las mujeres, así
como defensor de la educación pública mixta. Aunque se le conocen
varios ensayos sobre temas de antropología, medicina, arte e
historia, _Eugenia_ es su única obra de ficción.

La novela se desarrolla durante el siglo +++XXIII+++ en la ciudad
de Villautopia, capital de la Subconfederación de la América Central.
El relato se centra en los personajes de Celiana y Ernesto que
han sido amantes por muchos años y forman, junto con otros tres
personajes, un «grupo familiar» ---en Villautopia no existe el
matrimonio y la familia es un grupo de personas a quienes uno
elige por afinidad---. Celiana es una académica reconocida que,
como muchas otras niñas, fue esterilizada antes de alcanzar la
pubertad por considerarse que sus tendencias hacia la vida intelectual
la hacían poco apta para la reproducción. El conflicto de la
novela se desencadena cuando Ernesto, que se distingue por su
supuesta belleza y salud, recibe un nombramiento como reproductor
oficial del Estado. El relato sigue a Ernesto mientras descubre
cómo se gestiona la reproducción en Villautopia y se cuestiona
sobre el futuro de su relación con Celiana, ahora que estará
llamado a tener relaciones sexuales con otras mujeres.

Algunos de los estudios críticos que existen sobre la novela
describen a _Eugenía_ como una distopía (véase por ejemplo Dziubinskyj,
2007; Peniche, 2006). En efecto, el desenlace del relato muestra
como los personajes, a pesar de vivir en esta sociedad que promueve
el amor libre, se sorprenden a sí mismos dominados por sus apegos.
La novela incluye, sin embargo, numerosos pasajes donde se describe
sin ironía una sociedad próspera, donde la riqueza heredada se
ha abolido, el Estado administra equitativamente los bienes y
las mujeres están liberadas del trabajo de la maternidad y las
tareas domésticas. Cabe leer la novela con la siguiente pregunta
en mente, ¿es esto una distopía o una utopía? O aún mejor, ¿es
necesario escoger entre ambas? Ursula K. Le Guin título a una
de sus mejores novelas _Los desposeídos. Una utopía ambigua_.
Quizá _Eugenia_ pueda leerse en esta clave, en la que un relato
futurista puede darse a la tarea de mostrar matices, ambigüedades
y dejar que el lector saque sus propias conclusiones.

_Eugenia_ forma parte de una larga tradición literaria que especula
con temas como el matrimonio regulado o la reproducción selectiva
y gestionada por el estado, la cual es tan vieja como la _República_
de Platón. Según Dziubinskyj, muchos de los relatos eugenésicos
comparten rasgos como: «la desaparición del estado nación para
dar lugar a una forma de gobierno de tipo confederado […],
una actitud de adoración hacia las mujeres, una tecnociencia
altamente avanzada y una denigración de las poblaciones no caucásicas»
---afroamericanos, indígenas, etc.--- (464). En el fragmento que
presentamos a continuación es posible ubicar estas características.
En él, el Dr. Remigio Pérez Serrato describe a Ernesto y a dos
médicos africanos visitantes el funcionamiento del Bureau de Eugenética
de Villautopia.

# Fragmento

## V {.centrado}

El doctor don Remigio Pérez Serrato, presidente del Bureau
de Eugenética de Villautopia, era uno de aquellos habladores
incoercibles que, cuando no tienen auditorio, hablan solos
o con los muebles de su despacho. {.espacio-arriba1 .sin-sangria}

Como el raudal inagotable de su instructiva elocuencia
tenía propiedades hipnóticas, el hallazgo de un oyente
atento era para él el más sabroso regalo.

Aquel día reventaba de satisfacción, pues la suerte,
propicia, habíale deparado selecto auditorio en las importantes
personas de dos médicos hotentotes que, en misión científica
que su gobierno les confiara, venían a estudiar la manera
de implantar en su país las medidas conducentes a evitar el
estancamiento evolutivo de su raza.

Cuando Ernesto, provisto de una tarjeta de presentación
de Miguel, llegó a la oficina del doctor para manifestarle
que aceptaba el nombramiento recibido y enterarse de sus
obligaciones, ya estaban allí los africanos galenos. Encantado
don Remigio, hizo las presentaciones de rúbrica:

---El señor Ernesto del Lazo, uno de nuestros más distinguidos
reproductores; los sabios doctores Booker T. Kuzubé y Lincoln
Mandínguez, dignos representantes de la ciencia médica en la Hotentocia…

Ernesto saludó inclinándose; los negros al sonreír descubrieron
el teclado de sus formidables dentaduras de caníbales.
Joven el uno y viejo el otro, los dos eran feos y
bembones y tenían un aire muy cómico de asustada curiosidad;
se expresaban correctamente en inglés y sus ademanes
eran afectados. Llevaban largas levitas negras y unos
sombreros muy largos en forma de cono truncado, que conservaban
hundidos hasta las orejas en señal de respeto; el viejo,
con su collar de barba blanca, parecía un chimpancé
domesticado.

Como de sesenta años, alto y grueso, el doctor Pérez
Serrato usaba la blanca y lacia cabellera larga y peinada hacia
atrás. Su cara redonda y pálida, totalmente afeitada, sus cejas
negras y muy espesas sobre unos ojos bovinos, le hacían
parecerse bastante a otro médico ilustre de la antigüedad, al
célebre Charcot.

La bata de seda morada, ceñida a la convexidad prócer
del abdomen, y una gorrila de terciopelo del mismo color,
le daban un aspecto episcopal y subrayaban la majestuosa
prestancia de su fachada. Sobre el pecho ostentaba el
tradicional botón rojo de la Legión de Honor; insignia que, por
no ser menos seguramente, lucían también los africanos en
la solapa de sus respectivos levitones.

Para documentar debidamente a sus visitantes antes de
mostrarles las dependencias de la vasta institución que
estaba a su cargo y, más que nada, para sacar de su auditorio
todo el partido posible, don Remigio inició una plática
preliminar, procurando tomar el hilo del asunto desde los más
remotos orígenes de la Eugenética; a ser posible, hubiese
arrancado desde los tiempos prehistóricos.

---Vosotros debéis recordar, distinguidos colegas
---comenzó diciendo--- que hasta mediados del siglo +++XX+++, aunque
creían haber llegado al _summum_ de la civilización, los hombres
seguían reproduciéndose exactamente lo mismo que los
demás mamíferos. Hace cerca de trescientos años, un ilustre
coterráneo nuestro, cuya es la estatua que habréis visto a
la entrada de este edificio y de quien no es necesario repetir
el nombre, por ser conocido en el mundo entero, demostró
experimentalmente que el óvulo de los mamíferos, una vez
fecundado, puede desarrollarse en la cavidad peritoneal de
otro individuo de la misma especie, aun de sexo masculino,
Él partió de la observación de las gestaciones ectópicas y,
naturalmente, hizo sus primeros ensayos en los animales de
laboratorio. Toda la dificultad estaba en modificar, en
feminizar por decirlo así, el organismo del animal macho, y
cuando esto se logró, merced a las inyecciones intravenosas
e intraperitoneales de extractos ováricos, el ingente
problema estuvo prácticamente resuelto. El mundo científico
entero se conmovió de admiración cuando, en el gran Instituto
Rockefeller de Nueva York y en presencia de las notabilidades
médicas de diversos países, nuestro sabio compatriota
seccionó el vientre de un conejo de Indias, macho; y
extrajo de él cinco conejillos perfectamente desarrollados
y viables.

»Más tarde se realizaron análogas experiencias en la especie humana,
con éxito brillante; pero pasó mucho tiempo sin que tales
ensayos perdieran el carácter de curiosidades científicas.
Prejuicios seculares se oponían a la generalización del
procedimiento, y el genial innovador, clasificado
hoy entre los más grandes benefactores de la humanidad,
murió casi olvidado y sin haber visto la completa y radical
transformación que su descubrimiento ha determinado en
las costumbres de las sociedades civilizadas, variando los
fundamentos de la moral, las condiciones económicas de
los pueblos y las relaciones de los gobiernos para con ellos.
Gracias a él, nos sentimos hoy completamente distintos del
resto de los seres y muy por encima de la animalidad fisiológica
de nuestros antepasados.

»Mas llegose al fin un día en que los gobiernos tuvieron
que recurrir a estos medios de reproducción artificial y
establecer instituciones especiales para practicarlos en gran
escala, como el único medio de detener la despoblación de la
tierra, que hubiese llegado a ser completa, a poco que se
hubiese prolongado el estado social de los siglos que nos precedieron.

»Claro es que en el resultado obtenido, intervinieron numerosas
circunstancias que, por su excesiva complejidad,
no puedo ni siquiera señalar en estos momentos. Pero a la
vista están las ventajas más salientes del feliz estado de cosas
de las sociedades contemporáneas: reglamentada por los
gobiernos la producción de hijos, de modo que no exceda
nunca a los recursos naturales del suelo, sostiénese el
equilibrio económico, realízase de una manera eficiente la
selección científica de la especie humana y evítase toda
posibilidad de degeneración».

Y viendo Serrato aparecer aquí un nuevo aspecto del asunto,
no quiso desperdiciarlo, y claro es que procuró también
tomarlo desde la más lejos posible. Satisfecho de la paciencia
con que sus oyentes habían soportado su primer chaparrón oratorio,
se dignó concederles la gracia de una breve pausa; se
frotó las manos, tosió ligeramente y continuó:

---Por las condiciones intrínsecas de la deficiente e incompleta
civilización de aquellos siglos que hoy estamos
autorizados a considerar como semibárbaros, la especie humana
se había sustraído voluntariamente a la selección natural
que, en las otras especies animales, se realiza mediante
la lucha por la vida y el triunfo del más fuerte o del mejor
adaptado al medio. En las sociedades de antaño, triunfaban
los individuos más inteligentes, los más astutos o los más
ricos, que por lo general eran los peor dotados físicamente,
por lo que la especie degeneraba a pasos agigantados.

»Es cierto que las naciones más adelantadas de aquel tiempo
trataron de realizar en lo posible una selección artificial. De
tales intentos nació la Eugenética, pero esta ciencia, que hoy,
perfectamente reglamentada, ha alcanzado su total desenvolvimiento
y constituye la principal preocupación de los gobiernos,
tenía que limitarse entonces a medidas meramente
paliativas, y sus resultados eran punto menos que irrisorios».

Ernesto, comprendiendo que tenía conferencia para rato,
hacía esfuerzos sobrehumanos para no bostezar, pues todo
aquello le interesaba muy poco; los negros parecían
hipnotizados. El doctor siguió diciendo:

---Los progresos de la cirugía aséptica han permitido
hacer la esterilización de los hombres y de las mujeres, sin
alterar en lo más mínimo la complicada sinergia de las
secreciones internas ni el dinamismo humoral. Empezose
por practicar esta operación, salvadora de la especie, a los
criminales natos o reincidentes, a los locos y desequilibrados
mentales y a ciertos enfermos incurables, como los epilépticos
y los tuberculosos.

»Más tarde, algunos individuos de uno y otro sexos, comenzaron
a hacerse esterilizar voluntariamente para huir de
las cargas económicas de la paternidad o de las fisiológicas
de la maternidad. Hoy que la paternidad ha dejado de ser
una carga para el hombre, pobre o rico, y que la maternidad
no pasa en la mujer más allá de la concepción, el gobierno
tiene bajo su inmediato cuidado y vigilancia la reproducción
de la especie; hace esterilizar a todo individuo física o
mentalmente inferior o deficiente, y solo deja en la plenitud
de sus facultades genéticas a los ejemplares perfectos y
aptos para dar productos ideales. Y no hay que olvidar que
esta distinción implica para ellos el deber de dar a la
comunidad cierto número de hijos, deber que ha venido a ser hoy
tan ineludible como lo fueran en otros tiempos el servicio
militar, el desempeño de los cargos de elección popular o el
ejercicio del sufragio.

»La selección la empezamos desde la escuela primaria.
Antes de la pubertad y después de un detenido estudio,
tanto médico como psicológico, se decide qué niños deben ser
esterilizados y cuáles no. Preferimos a los de tipo muscular
puro y desechamos sistemáticamente a los cerebrales de
ambos sexos, pues la experiencia ha demostrado que son
pésimos reproductores; en caso de escasez, puede utilizarse
a los varones de tipo respiratorio, a condición de cruzarlos
luego con mujeres de tipo digestivo.

»Si a ustedes les parece, empezaremos nuestra visita por
la sección de estadística. Allí podrán convencerse de que
año con año disminuye el número de los niños esterilizados;
día ha de llegar en que solo se practique la operación cuando
el exceso de habitantes obligue a restringir el número de
nacimientos.

»Como estas medidas han puesto un dique a la degeneración
de la humanidad, la población de las cárceles, los manicomios
y los hospitales de incurables se ha reducido casi a
cero. Y si tomamos en cuenta además que las condiciones
económicas de las clases proletarias han mejorado notablemente
y que hoy no se vacila en aplicar la eutanasia a los
seres condenados a pasar toda su vida o una gran parte de
ella en la inconsciencia o entre sufrimientos irremediables,
fácil será comprender que tales instituciones, que antaño
constituían una carga pesadísima para el Estado, han dejado de
serlo hoy y que los cuantiosos fondos que consumían se aplican
ahora ventajosamente a más urgentes necesidades».

Harto Ernesto de las prolijas disertaciones del doctor,
no esperó a que este repitiera la invitación y se puso de pie
como disponiéndose a comenzar la visita. Los negros lo
imitaron; dada la facilidad que los de su raza tienen para
dormirse en cualquier parte y a cualquier hora, demasiado
habían hecho los pobrecillos con permanecer despiertos.
Pérez Serrato no tuvo más remedio que resignarse y pasó
delante de ellos para guiarlos; ya tendría oportunidad para
desquitarse en cada departamento que visitaran.

Al cruzar la secretaría, contigua al despacho del
presidente, el secretario general ---un pulcro y amojamado viejecillo---
y más de treinta empleados subalternos se pusieron
en pie al ver a su jefe. Mientras las jóvenes mecanógrafas
cambiaban entre sí sonrisas y alegres comentarios sobre la
exótica indumentaria de los morenos, el ilustre cicerone
explicaba el complicado funcionamiento de aquella oficina,
pormenorizando cómo eran clasificados los asuntos, distribuido
el trabajo, estudiadas las solicitudes, archivadas y
contestadas las comunicaciones recibidas de las distintas
instituciones con que aquella se relacionaba.

La sección de Estadística, a que llegaron enseguida,
ocupaba tres vastos salones de techo alto y excelente iluminación;
trabajaban allí más de cien empleados, mujeres en
su mayoría. Los visitantes fueron presentados a una dama
de mediana edad, alta, seca y pelada como un hombre, que
ejercía las funciones de jefe de aquel importante departamento.
Correctísima, dio informes prolijos sobre la marcha
de los asuntos que estaban a su cargo.

En los libros, muy bien ordenados y llevados minuciosamente
al día, era fácil comprobar la verdad y exactitud de
las anteriores aseveraciones del director: en lo que iba
transcurrido de aquel año, solo se había esterilizado a tres mil
quinientos niños y cerca de dos mil niñas, apenas la décima
parte de los que alcanzaban la edad requerida para la operación,
y menos de la vigésima de la población escolar de
Villautopia. Datos eran estos que auguraban, para dentro
de pocos años, una espléndida cosecha de reproductores.
Salieron de aquel departamento muy complacidos y provistos
de algunos números del _Boletín de Estadística_, órgano de la sección.

Después de hacerlos cruzar un hermoso jardín, el doctor
Pérez Serrato detuvo a sus visitantes frente a una sala de
cirugía, a la sazón desierta por no ser día de operaciones.

---Este pabellón ---explicó--- se destina únicamente a
la esterilización de los muchachos; las otras operaciones se
practican en pabellones especiales que visitaremos después.
Los miércoles y sábados por la mañana tenemos operación:
si os dignáis asistir a ellas, queridos colegas, tendréis oportunidad
de admirar la destreza de nuestros cirujanos.

Y a continuación se enfrascó el digno presidente en
minuciosos detalles de carácter técnico (en chino para Ernesto
era todo aquello), relativos a los procedimientos operatorios
y a las precauciones antisépticas, gracias a las cuales la operación
era tan inocua y segura, que en muchos años no se
había registrado un solo accidente.

Dejando a los pacientes y sufridos hotentotes por carnada
la locuacidad inagotable del doctor, Ernesto se desentendía
de ella y recogía íntegra la profunda sensación casi
medrosa que emanaba de aquella enorme sala circular, toda
blanca, con las paredes y el piso de lustrosa porcelana, y en
la que podían trabajar simultáneamente diez cirujanos; el
inmenso anfiteatro, en forma de herradura, tenía capacidad
para más de tres mil estudiantes. Por la cúpula de cristal
que hacía de techo, entraba la luz a raudales, haciendo relucir
las barras niqueladas de las mesas de operaciones, que a
Ernesto, como a todo extraño a la profesión médica, se le
antojaban máquinas infernales de tortura.

Antes de abandonar aquel pabellón, visitaron sus dependencias:
la sala de anestesias, el espléndido y bien provisto
arsenal, el servicio de agua esterilizada y los grandes
autoclaves para la desinfección de los instrumentos y del
material de curaciones.

Hubo que cruzar otro trozo de jardín para llegar a las
salas donde los niños recién operados permanecían en reposo
durante cinco días a lo sumo. Mucha luz, mucha asepsia
y mucha ventilación; el aspecto de las blancas camitas
alineadas era alegre y tranquilizador. Era la hora de la
primera colación y las enfermeras, vestidas de blanco,
circulaban presurosas, sirviéndola entre la impaciente algazara de
los chicuelos. Había cerca de quinientos, entre varones y
hembras, distribuidos en seis grandes pabellones; en el hermoso
parque contiguo, los convalecientes paseaban por grupos o
jugaban a la sombra de árboles frondosos y floridas
enredaderas. Supieron con sorpresa los visitantes que en
menos de quince días, aquellos niños quedaban en aptitud
de volver a sus clases.

Tras la inevitable presentación, unióse al grupo el interno
de guardia en aquel departamento; era el doctor Suárez,
un joven muy simpático, de barba negra y mirada inteligente.
Permaneció con ellos durante el resto de la visita, la cual
continuaba su curso sin que amainase por un momento la
terrible verbosidad del sabio presidente de la institución, que
todo lo explicaba con lujo de detalles superfluos y citas de
una erudición pesadísima e inoportuna. Por ratos se hacía
agresivo: tomaba a uno cualquiera de sus interlocutores
---el que más a mano le cayese---, lo sujetaba un rato por
las solapas o lo arrimaba a una pared y lo monopolizaba y
abrumaba bajo un chaparrón de comentarios. Ahora daba a
conocer el destino de otro pabellón a que habían llegado,
también desierto a aquella hora. Constaba de tres piezas: un
saloncillo como de espera, coquetamente amueblado y con
entrada independiente por la calle, y dos pequeñas salas de
operaciones, con sus mesas respectivas y separadas por una
mampara de cristales opacos. Todo él respiraba cierto aire
de discreto misterio, de galantería y tapujo, que intrigó
bastante a Ernesto.

---Aquí ---se apresuró a decir Pérez Serrato--- practicamos
la toma de los óvulos, la _prise_, como decimos nosotros,
y el injerto de los mismos. Las damas vienen por sí solas en
el momento oportuno, que ya ellas saben conocer perfectamente,
y se vuelven enseguida. La operación, aunque delicada,
es sencillísima; se reduce a tomar delicadamente el óvulo
fecundado cuando empieza a hacer su nido en la mucosa del
útero; para ello empleamos esta ingeniosa cucharilla. ---Y
mostró una que extrajo de una vitrina.

---En la sala contigua ---siguió explicando--- espera ya
el _gestador_, previamente feminizado, y al cual otro cirujano
le ha hecho ya una pequeña incisión en el abdomen. El óvulo
es depositado en la cavidad peritoneal, como un grano de
trigo en el surco y, si la operación es fructuosa ---lo cual en
la actualidad rara vez deja de suceder---, a los doscientos
ochenta y un días exactos, hacemos una laparotomía y extraemos
un niño perfectamente desarrollado y viable. Con
los progresos de la cirugía aséptica, los peligros de estas
secciones cesáreas han venido a ser casi nulos; gestador tenemos
que ha sido operado con éxito diez o doce veces.
Debo advertiros que, durante la toma del óvulo y su injerto
o siembra, es indispensable conservar en la sala una
temperatura constante y aproximadamente igual a la del cuerpo
humano, para que los elementos no sufran el menor cambio
o alteración; esto hace que la labor de los operadores sea
bastante penosa y ruda, y nos obliga a tener un número
suficiente de cirujanos y a turnarlos de modo que ninguno
trabaje dos días seguidos.

»Ahora ---continuó diciendo--- voy a enseñaros el departamento
más curioso de nuestra institución: las salas que
ocupan los _gestadores_ y el pabellón donde se practican las
laparotomías o alumbramientos quirúrgicos. Si aún no están
ustedes cansados, visitaremos después las salas de lactancia
y el departamento de infancia».

Rodeado de un bello y extenso parque, que hubo también
que atravesar para llegar a él, componíase el edificio
destinado a los gestadores de varios dormitorios con grandes
ventanas y las camas en filas como en un hospital. Había además
un gran salón de reuniones, un magnífico balneario, comedor,
biblioteca y billares; en un extremo del parque, se
alzaba un pequeño y elegante teatro. No era llegada aún la
hora del almuerzo y los abnegados incubadores de la humanidad
futura, en número de seiscientos aproximadamente,
discurrían por las distintas dependencias. Algunos leían en
la biblioteca, novelas y periódicos, pues las lecturas serias
les estaban prohibidas; alguien tocaba el piano y otros jugaban
al tresillo, al ajedrez o al billar. No pocos paseaban por
las avenidas del parque, leían o conversaban a la sombra de
los árboles o se dedicaban a deportes poco violentos, compatibles
con su estado y muy favorables a una buena gestación; no
les estaba permitido fumar. Los más viejos ---¡quién lo creyera!---
bordaban, tejían croché o cosían diminutas camisitas y
primorosos gorros. Las edades de estos sujetos oscilaban entre
los dieciocho y los cuarenta y cinco años y todos eran gordos, lucios, colorados y
con un aire de beatífica satisfacción en la mirada.

Acogido por ellos con muestras de cariñosa adhesión,
el doctor sonreía a todos y bromeaba con algunos.

Al ver el cómico ademán con que los más avanzados cruzaban
las manos sobre la esfera abdominal, el más joven de los
negros no pudo contener un inoportuno acceso de hilaridad
que le hizo derramar lágrimas como garbanzos y a poco más
lo ahoga entre borbotones de risa. Corriose un tanto el doctor
Serrato, y Ernesto necesitó toda su fuerza de voluntad para no
hacer dúo al moreno. Al fin logró este calmarse, gracias a las
severas miradas y enérgicos ademanes de su compañero.

---¿Y no cree usted, doctor ---preguntó Ernesto al interno---
que la condición de estos infelices no es menos triste
y dura de lo que antaño fuera la de la mujer, y que el estado
interesante artificial no viene a ser algo así como una afrenta
a su condición de varones y aun a la dignidad humana?

---Cada siglo tiene su ética, amigo mío ---repuso el joven
galeno---; por lo demás, el Estado recompensa espléndidamente
los servicios de estos dignos sujetos; la vida que
aquí llevan no puede ser más cómoda ni más regalada y,
como antes dijo mi digno jefe, la operación final carece de
peligros.

---Pueden ustedes estar convencidos ---terció el presidente,
que no cedía a nadie la palabra por mucho tiempo--- de
que el puesto de _gestador_ es en la actualidad uno de los que
mejor se remuneran y, por consiguiente, uno de los más codiciados.
Tenemos siempre más solicitudes de las que necesitamos,
y conste que no podemos aceptar a cualquiera. El
gestador ha de ser un sujeto perfectamente sano y equilibrado,
en lo físico y en lo mental; ha de ser de tipo digestivo
puro, de excelente carácter y de buenas costumbres, pues no
ha de fumar ni beber alcohol; es preciso también conocer y
analizar sus antecedentes hereditarios.

»Por supuesto que desde pequeños han sido nulificados
como reproductores activos y, antes de cada injerto, hay que
aplicarles una serie de inyecciones intravenosas e intraperitoneales
de extractos ováricos para modificar el dinamismo
de sus secreciones internas y sus condiciones humorales. Así
se hacen aptos para el desarrollo de los óvulos, se feminizan,
en una palabra; todo impulso erótico desaparece en ellos durante
la gestación y, con el tiempo, su efectividad y sus inclinaciones
llegan a cambiar definitivamente; acaban por aficionarse a
los pasatiempos y ocupaciones femeniles.

»Raro es el que no llega a tomarle gusto al oficio, y tenemos
nuestros veteranos. Hace pocos días, precisamente, perdimos
al decano de todos, después de doce laparotomías
felices. Antes de su decimatercera gestación, quisimos
jubilarlo, cosa a que tenía derecho, pues había cumplido ya
cuarenta y ocho años de edad y veinte de servicios; además, estaba obeso y
tenía un principio de adiposis cardiaca. Pero él, obstinado
y mimoso, nos pedía en tono suplicante servir, “siquiera por
última vez”… Tuvimos la imperdonable debilidad de ceder a
sus súplicas y, a la hora del alumbramiento, se nos quedó en
la anestesia… ¡Pobre Manuelón! ¡Pobre y abnegado amigo!
La humanidad debe estarte agradecida y conservar con cariño
tu nombre de héroe oscuro e ignorado. Por cierto, señores,
que no se desperdició su producto póstumo, que es un hermoso varón».

La _nursery_ o departamento de infancia, que visitaron al
salir del de los _gestadores_, presentaba un aspecto
verdaderamente encantador y capaz de infundir ideas de optimismo
en el ánimo más recalcitrante. Más de dos mil pequeñuelos
repartíanse por edades en tres salas que, en condiciones de
limpieza, luz y ventilación, no dejaban nada que desear. La
primera, destinada a los recién nacidos, lucía en un ángulo
la gran báscula pesabebés y en el otro el moderno autoclave
con los biberones listos para servir, numerados y dispuestos
en hileras. En las cunas, primorosas y blancas, los rorros
aprestaban los puños, y sus caras, sumidas en la inconsciencia del
primer sueño, tenían la redondez y el encendido tinte de las
manzanas maduras. Junto a cada cuna, veíase la hoja clínica
con el nombre del niño, un número de orden y las curvas que
señalan gráficamente el aumento de peso y estatura. Las amas
vigilaban, atentas a satisfacer las necesidades de los críos,
solícitas y cariñosas como verdaderas madres.

En la segunda sala, niños de tres a seis meses eran paseados
en brazos o reposaban en las cunas, agitando sonajas
y mostrando al reír las encías sonrosadas en las que a
veces se engreían solitarios los primeros incisivos. Los de
la tercera sala gateaban o daban los primeros pasos entre
risas y caídas; en el primoroso jardín que rodeaba al
pabellón, corrían y jugaban ruidosamente los mayorcitos, al
cuidado de las niñeras, casi todas jóvenes y de agradable
aspecto.

¡Qué alegría tan sana en las adorables caras infantiles!
¡Cuánta solicitud maternal en las niñeras! Aquel espléndido
florecimiento de vida y salud bastaba por sí solo para
justificar cuanto de violento o inmoral pudiese haber en las
medidas a que la humanidad se había visto obligada a recurrir
para detener su degeneración y acabamiento y seguir con
paso firme su marcha evolutiva hacia un ideal de perfección.
Ni uno solo de los pequeños ofrecía el triste espectáculo
de la atrepsia o el encanijamiento, tan frecuente en
los pasados siglos.

---Hoy que el nacimiento de un niño ---dijo el doctor
Suárez--- es el resultado de una deliberación científica y viene
precedido de una rigurosa selección, hoy que no es como
antaño, el fruto, rara vez deseado, de un instinto irreflexivo,
todo lo que nace llega a su completo y total desenvolvimiento.
Puede decirse que la mortalidad infantil, aquella horrible
cosa absurda que fue la desesperación de nuestros
antepasados, ha desaparecido por completo.

Faltaba poco para las dos de la tarde cuando salieron
del departamento de infancia; el doctor Pérez Serrato no se
resignaba a soltar su presa, y como lo hablador no quita lo
cortés, se empeñó en que Ernesto, los dos negros y el interno
almorzasen con él en su espléndida residencia particular,
situada dentro de los terrenos del Instituto. No era posible
rehusar sin pecar de incorrección; por otra parte, no dejaba
de ser halagadora la perspectiva de un buen almuerzo, en
aquel momento tan oportuno, con lo lejos que estaban de la
ciudad y con el ejercicio que habían hecho. Más tarde continuarían
la visita; que aún quedaban por ver muchas de las
dependencias del gran Instituto de Eugenética, legítimo orgullo
de Villautopia.

# _Eugenia_ (1919) de Eduardo Urzaiz @ignore

</section>
<section epub:type="chapter" role="doc-chapter">

# _El réferi cuenta nueve_ (1943) de Diego Cañedo

_El réferi cuenta nueve_, de Diego Cañedo (Guillermo Zarraga
Argüelles, 1892-1978), presenta un mundo en el que las potencias
del eje ganan la Segunda Guerra Mundial e invaden México. Un
régimen de ocupación se establece y extiende durante algunos
años, hasta que los gobernantes nazis son expulsados del país
gracias a una estrategia de sabotaje y guerrilla. Este triunfo
da lugar a una refundación del Estado mexicano donde los ideales
de la Revolución, que habían sido traicionados por las élites
gobernantes y sus burocracias parasitarias, pueden al fin realizarse.
Se consolida entonces un nuevo gobierno caracterizado por el
bienestar económico generalizado, la disolución de la burocracia
y una relación equilibrada con Estados Unidos. Diego Cañedo fue
el seudónimo utilizado por Guillermo Zarraga Argüelles,
quien después de completar sus estudios como arquitecto, ocupó
varios cargos públicos en las décadas de los treinta a los cincuenta. Se
le atribuyen alrededor de veinte obras, incluyendo novelas, relatos
breves y un ensayo. Ni el propio texto ni las pocas reseñas y
estudios críticos que hemos localizado parecen revelar por qué
el autor escogió el título _El réferi cuenta nueve_.

En su reseña de esta novela, Alfonso Reyes señala la coexistencia
de dos narraciones: «La costumbrista, bastante lograda aunque
de escaso alcance por la naturaleza misma del género, y cuyos
aspectos pudieron abreviarse un tanto, y la política, de mucha
mayor trascendencia, en que se revisan y exponen con rara sinceridad
y honradez las vicisitudes de la vida pública mexicana, en el
pasado inmediato, en el presente y hasta en un porvenir utópicamente
forjado mediante la invención literaria» (338). Como lo señala
Reyes, la dimensión costumbrista ocupa una buena parte del relato.
Decenas de páginas narran con nostalgia la vida cotidiana de
un San Miguel de Allende profundamente católico y reproducen
con detalle charlas de sobremesa, así como sermones de padres
preocupados. La novela, sin embargo, no solo plantea el retrato
del nazismo en México sino también la aparición esporádica de
algunos elementos futuristas: carros voladores con hélices o
molinos mecánicos gigantes de cuyas aspas salen disparados decenas
de soldados nazis.

Podría afirmarse que la novela de Cañedo se anticipó a obras
como _El hombre en el Castillo_ (1962), de Philip K. Dick, que
imaginaron también un mundo dominado por los nazis. Los relatos
como el de Dick que responden a la pregunta «¿qué hubiera pasado
si…?» se conocen como ucronías. Muchas veces las ucronías parten
de un suceso histórico y juegan a cambiar su desenlace. Sin embargo,
a diferencia de _El hombre en el castillo_ que se escribió años
después de la Segunda Guerra, _El réferi cuenta nueve_ fue publicado
en 1943 cuando aún se desconocía quiénes serían los ganadores.
Es decir que, si bien visto desde el presente el relato es ucrónico,
al momento de su publicación se trataba de una narración futurista.
Muy posiblemente fue también el llamado de atención de un autor
preocupado por la presencia de sectores de la sociedad mexicana
que simpatizaban con el nazismo y lo percibían como una alternativa
frente al imperialismo yanqui.

El pasaje seleccionado narra el ingreso de las tropas nazis a
la Ciudad de México. Los ejércitos de Alemania y Japón, aprovechando
el apoyo de las células profascistas que han cultivado en América,
emprenden una ocupación que inicia en las costas de Brasil y
se extiende hacia el norte.

# Fragmento

## VI {.centrado}

[…] {.espacio-arriba1 .sin-sangria}

Los sucesos posteriores pertenecen a la Historia. Una flotilla
enorme de submarinos ---se calcula que de varios miles---, atacó
las costas brasileñas. A pesar de la tenaz resistencia de sus
habitantes, la sorpresa los aplastó. La invasión derramóse hasta
Panamá y la destrucción del canal por un gigantesco torpedo que
cargaba miles de toneladas de explosivos y que según se dijo
era guiado por una tripulación suicida, fue un golpe que parecía
mortal para las naciones unidas. El Tío Sam se replegó violentamente
a su territorio y comenzó a prepararse febrilmente para la defensa.
{.espacio-arriba1 .sin-sangria}

Los alemanes invadieron Centro América hasta la frontera de Guatemala
con Honduras y Salvador, donde tomaron unos días de tregua. Aquello
recordaba la primavera de 1940, cuando ocuparon los Países Bajos
y Francia.

El 9 de febrero de 1946 los habitantes de la metrópoli que vivían
presas del sobresalto y la curiosidad, comenzaron a escuchar
en la madrugada un ruido sordo de bombardeos. Muchos abandonaron
el lecho y subieron a las azoteas; en el aire frío y claro el
estrépito de las explosiones se hacía más retumbante y llenaba
el espacio; como a las siete se escuchó el ronronear lejano de
aviones. El conserje del edificio ocupado entonces por los ferrocarriles
en la esquina del Paseo de la Reforma y la calle del Ejido, trepó
hasta la terraza más alta y pudo contemplar centenares de máquinas
que llegaban por el sur; procuró contarlas, pero su vista no
podía agruparlas para hacer un cálculo rápido. Indudablemente
pasaban de quinientas. Cuando se hicieron perfectamente visibles
empezaron a soltar de sus barrigas bultitos oscuros que se precipitaban
al vacío y que de pronto flotaban colgados de sombrillas que
se abrían como gigantescas flores blancas. Poco a poco esos bultos
iban adquiriendo forma humana y se discernían, en la claridad
del ambiente, las cabezas, las piernas y los brazos. Mientras
tanto de casas desparramadas en muchos rumbos de la población
salían grupos muy numerosos con toda clase de armas y se dirigían
hacia el Palacio Nacional y hacia los cuarteles. En las afueras,
donde los paracaidistas aterrizaban, muchos vehículos estaban
en su espera para transportarlos al centro. Las oficinas de los
partidos de oposición se erizaron de ametralladoras y las bocacalles
se llenaron de patrullas armadas.

Simultáneamente comenzaron a escucharse tiroteos por rumbos muy
diversos. El vecindario temeroso y expectante al mismo tiempo
no sabía a ciencia cierta lo que pasaba; pero como a las ocho
una columna de populacho, de cuatro o cinco mil hombres, recorrió
el Paseo de la Reforma gritando vivas al ingeniero Cabral y mueras
al gobierno; otros grupos llevaban grandes cartelones con esta
sola palabra: «Paz».

Los elementos leales organizaron la resistencia en diversos sitios:
en el cuartel de Peralvillo, en el palacio nacional, en la ciudadela,
en Chapultepec y comenzó la lucha por posesionarse de las avenidas
más estratégicas.

En el aeródromo de Balbuena unos treinta o cuarenta aviones se
elevaron para librar una batalla suicida y heroica; pero uno
a uno fueron cayendo. Hicieron, sin embargo, bastantes bajas
entre los enemigos y uno de los bombarderos invasores cayó panza
arriba en la Alameda, mostrando las alas con las insignias nazis
y provocando un gran incendio de árboles que crepitaban con el
fuego.

Ese mismo día, en las primeras horas de la tarde, una radio gobiernista,
que seguía transmitiendo, anunció que un transporte enemigo había
logrado atracar en Veracruz rodeado por centenares de submarinos
que lo escoltaban como delfines gigantescos. El pueblo y los
muchachos conscriptos del puerto trataron de hacer resistencia,
pero no existía una sola pieza ni un asomo de preparación y todo
terminó en una carnicería de héroes. Entonces los invasores organizaron
columnas de tanques y hombres motorizados que se dirigieron sobre
México. Al día siguiente, que era domingo, estaban ya sobre el
camino a sesenta kilómetros de las goteras.

Los grupos facciosos se apoderaron fácilmente de Tampico y Acapulco.
En esta última bahía desembarcó también una fuerte columna alemana.
Varios transportes cayeron sobre Puerto México y los invasores
avanzaron a través del Istmo; traían plétora de cañones, tanques,
locomotoras y carros. En esa ruta las tropas del gobierno se
batieron con bravura ejemplar, pero nada podían contra hombres
veteranos de la guerra en Europa, equipados con el armamento
más moderno.

Mientras tanto las fuerzas nazis habían atravesado Guatemala,
donde se les hizo una oposición desesperada, y se internaban
por nuestro territorio.

Los japoneses que juraron lealtad al país se convirtieron de
pronto en una fuerte columna que se avalanzó contra Guadalajara.
Otros saltaron desde las costas de Sonora, a través del Golfo
de Cortés, sobre la Baja California, mientras varios centenares
de submarinos nipones hacían un desembarco por la costa del Pacífico.

Estos hechos no forman propiamente parte de mi relato, que debería
ceñirse a unas cuantas gentes unidas por una trama casi familiar.
Pero no puedo desprender a mis protagonistas del cuadro de la
invasión y de la guerra que dominaba y regía los intereses, las
pasiones y hasta las circunstancias más nimias en las vidas de
cada quien.

El recuerdo que guardo de esas semanas es que los vecinos se
sentían poseídos por el pánico y la sorpresa. Mis padres se encerraron
en nuestra pequeña vivienda y a mí me encerraron con ellos mientras
en las calles ocurrían escaramuzas y sobre nuestras cabezas se
escuchaba el zumbido de los aviones. Los periódicos dejaron de
publicarse y durante muchos días conocíamos lo ocurrido por los
rumores que se propalaban de una calle a otra. Nos dábamos cuenta
de los progresos nazis por las radiodifusoras que al principio
transmitían mensajes llenos de esperanza para después irse callando
una a una. Entonces comenzaron a difundir discursos hablando
de la caída del gobierno y de la formación de una asamblea que
debería elegir a un presidente provisional.

El Presidente legítimo, con su ministro de la guerra y unos tres
o cuatro mil hombres se defendían obstinadamente en el viejo
convento del Carmen en San Angel. Finalmente, ante el empuje
enemigo, tuvieron que echarse a pie de noche por los pedregales
buscando una salida hacia la serranía del Ajusco; fueron alcanzados
y conminados para rendirse, pero ellos contestaron con el fuego
de sus ametralladoras. Las tropas de invasores los cercaron al
fin y los exterminaron uno a uno, sin hacer un solo prisionero.
Así murió un presidente que aunque escaló el poder por el fraude
electoral en contra de la opinión casi unánime del país, se hizo
respetable después por sus virtudes de hombre de bien, tan escasas
entonces entre los políticos. Todavía ahora los textos de historia
hablan de él llamándolo El Bueno, y con este adjetivo, en el
cual no hay ni una sombra de ironía, rinden un tributo a su memoria.
Fue un hombre bueno y murió como un valiente y un patriota. ¡Cuántos
habrían querido hacer otro tanto!

Lo anterior ocurrió con la rapidez de un relámpago; era en realidad
la misma _blitzkrieg_ de Bélgica, de Francia, de Grecia. Para
el 19 de febrero las tropas que se llamaban a sí mismas del Nuevo
Gobierno, pero que en realidad estaban formadas por nazis invasores,
desfilaron por la capital. Cantidades fantásticas de armamentos,
que parecían haber bajado del cielo por milagro, llegaban en
flotillas interminables de camiones que como líneas puntuadas
rayaban los caminos por distancias inmensas. Para mediados de
marzo los nazis habían consolidado sus posiciones hasta el norte
de la república. Los japoneses dominaban todo el Occidente. Algunos
jefes militares, aplastados por la rudeza del ataque, se rindieron;
unos cuantos, nazistas de corazón, hicieron proclamas y alegando
que el bien del país lo representaba el gobierno recién constituído,
entraron a colaborar con el Nuevo Orden.

El resto de nuestro ejército, entre ellos un gran número de muchachos
que se batieron con denuedo, se salvó replegándose hasta los
jirones del territorio que el enemigo no pudo invadir. Entre
esos hombres se conservó siempre intacta la esperanza de recuperar
un día la patria perdida.

Los alemanes obraron con gran sagacidad y cuando se presentaron
en la metrópoli con lujo de aeroplanos, paracaidistas, tanques
y ametralladoras, hacían la comedia de que venían a prestar su
ayuda a un movimiento popular que restauraba la dignidad del
país sojuzgado por el poderío americano. Así, la lucha en México
se enmascaró con la careta de la guerra civil.

Al principio muchos hombres de buena fe, que ante el conflicto
habían perdido el sentido de la proporción, cayeron en esa trampa
hábilmente tendida. Más tarde, cuando el lobo disfrazado con
la piel del cordero empezó a gruñir y a mostrar los colmillos,
casi todos esos hombres dieron un paso atrás, arrostrando el
peligro y aún la muerte, y se unieron a los que luchaban por
arrojar al nazi. La invasión fue una prueba dura que a la postre
tuvimos que bendecir. Bajo la bota nazi México inició su primera
hora de pueblo libre y unido. Desde ese momento las gentes comenzaron
a luchar y a morir por algo más alto que las engañifas de los
líderes.

Se reunió una gran asamblea y fue electo presidente provisional
Onésimo Cabañas. Existía el plan de convocar a una elección definitiva;
pero estos sueños nunca llegaron a realizarse. Cabañas fue el
primero y el último de la Dinastía de los _Quislings_ Mexicanos.

# _El réferi cuenta nueve_ (1943) de Diego Cañedo @ignore

</section>
<section epub:type="chapter" role="doc-chapter">

# _La noche anuncia el día_ (1947) de Diego Cañedo

_La noche anuncia el día_ (1947), de Diego Cañedo (Guillermo
Zarraga Argüelles, 1892-1978), cuenta la historia de una máquina
para captar y proyectar visualmente los pensamientos, con la
que Don Antonio Cutiño influye y controla las decisiones de
un tirano, el General Camargo, que gobierna en la imaginaria
república de La Paz. Cutiño, quien se vuelve comerciante durante
el periodo revolucionario, se dedica a vender todo tipo de objetos
a los soldados de un caudillo apodado el Chueco Farias. Aparece
entonces en el relato Vrevsky, un científico ruso que, en su
camino a La paz, es capturado por el Chueco. Cutiño convence
al Chueco que no mate al científico. En recompensa, en su lecho
de muerte, Vrevsky le da los planos y las instrucciones para
que construya la máquina que ha inventado y que permite conocer
los pensamientos de otros. Terminada la revolución, el Chueco
adquiere un puesto político y se desvincula de Antonio Cutiño,
quien sigue en su labor de comerciante, se fascina por la radio
y comienza a construir la máquina.

El artefacto funciona, Cutiño lo instala en la sala de su casa.
Diego Cañedo imagina un mecanismo que integra en su funcionamiento
la recepción de ondas hertzianas, como lo hacen la radio y la
televisión, y con ellas imprime una película como la del cinematógrafo.
Sin embargo, a la manera del fonotelerradiógrafo de Nervo, la
máquina de _La noche anuncia el día_ capta ondas peculiares:
las que supuestamente producen los pensamientos involuntarios
y no verbalizados de las personas, aquellos que expresan sus
más íntimos deseos y fantasías. Con la ayuda de su secretario,
José Mendieta, quien en la novela cuenta la historia al narrador,
Cutiño comienza a celebrar fiestas, haciendo que la élite política
asista a esos eventos para captar sus pensamientos y comunicarlos
al General Camargo, argumentando un supuesto poder para leer
la mente. Así, Cutiño logra prevenir al dictador de una intriga
en su contra y de una conspiración para darle un golpe de estado.
Al final de la novela, la máquina es destruida por el nuevo general
que sube al poder.

En las reflexiones del personaje de Mendieta, esta máquina para
conocer los pensamientos es presentada como un fascinante instrumento
para conocer la verdad que cae en manos equivocadas. En la novela,
este artefacto científico permitirá exponer las verdaderas fantasías
de aquellos quienes practican «el culto a los principios socialistas
con la fe del granjero que exclama “Hágase la voluntad de Dios
en la sementera de mi vecino”» (Cañedo: 147). Así mismo, el
texto de Cañedo no deja de denunciar «la furia agrarista» que
vino después de la revolución a la República de La paz. Lo anterior
invita a leer la novela como una crítica a los gobiernos que
siguieron a la Revolución mexicana.

Desde nuestra perspectiva, la manera en que la novela explora
los efectos de un dispositivo capaz de violentar la privacidad
de las personas, dada la manera en que capta las supuestas ondas
que produce el pensamiento, la aleja de cualquier pretensión
de ciencia ficción dura al tiempo que le brinda actualidad.

En el fragmento que presentamos a continuación Mendieta le cuenta
al narrador cómo fue que Antonio Cutiño la mostró la máquina
y su funcionamiento.

# Fragmento

## IV \
   _Los mecanismos_ {.centrado}

En un principio ---prosiguió Mendieta---, cuando comencé
a trabajar con él, no me di cuenta del asunto. Tenía en su despacho,
y en medio de unas estanterías, un gran armario; al abrirlo aparecían
varios tableros con carátulas graduadas; había también una serie
de diminutos focos y varios amperímetros y galvanómetros. En
la parte superior existía algo así como un hacinamiento desordenado
de accesorios eléctricos: bulbos, espirales de cobre, carretes
y otras cosas cuyos nombres fui conociendo poco a poco. Eran
moduladores, filtros, amplificadores, fotoceldillas. Y como partes
a las que Cutiño algunas ocasiones se refería como muy esenciales,
había unas bombillas, que a mí eso me parecían, y que después
supe eran oscilógrafos de rayos catódicos. {.espacio-arriba1 .sin-sangria}

Recuerdo también que en una especie de torno giraban varios cilindros
de vidrio revestidos con un esmalte especial aparentemente opaco,
que en la oscuridad mostraba miles de diminutos puntos fosforescentes,
los cuales cambiaban de posición a cada fracción de segundo.

Varios motores muy pequeños accionaban unos discos de una sustancia
traslúcida de color verdoso, que giraban con rapidez alrededor
de su centro y simultáneamente y con gran lentitud alrededor
de sus ejes verticales. Estaban montados en un mecanismo especial
que coordinaba sus movimientos haciendo que los planos de unos
y otros formaran determinados ángulos, variables, según decía
Cutiño, de acuerdo con las emisiones electrónicas. La parte alta
del armario estaba ocupado con aparatos ópticos, a juzgar por
los espejos y lentes que entraban en su estructura.

Fuera, en la parte superior, existían unos círculos opalinos
que se iluminaban con luces tenues y violadas, como si fuesen
de mercurio o neón. Parece que hacían un papel un poco semejante
al de los micrófonos en la trasmisión de la voz. Después, cuando
Cutiño perfeccionó sus mecanismos, estos círculos se multiplicaron
por toda la casa y su objeto se disimulaba dándoles, sobre las
consolas, en las mesitas de fumar y en los libreros, un carácter
ornamental.

El conjunto se veía bien que era algo improvisado; armado con
elementos adquiridos en fábrica e importados de Estados Unidos,
Alemania o Suiza; pero sujetos a tablas de madera blanca, o fijos
a láminas galvanizadas, como si solo se hubiese querido atender
a la precisión técnica sin fijarse en el aspecto que tanto seduce
al profano.

Interrumpí a Mendieta para preguntarle:

---¿Pero nunca se le ocurrió tomar una fotografía de aquellos
mecanismos?

---Aun haciendo a un lado mi increíble abandono ---me contestó---
tal maniobra hubiese pugnado abiertamente con mis resoluciones;
su apariencia me habría condenado. Cutiño llegó a depositar en
mí una confianza tal, que por nada me habría atrevido a despertar
una sospecha.

En un principio aquello me parecían maquinarias de radio, pues
pronto supe que don Antonio era un radiófilo. ---Un aficionado,
decía con cierto orgullo--- como otros pueden cifrarlo en declararse
miembros de hermandades secretas. Pero me di cuenta de que todas
las instalaciones que servían para satisfacer este capricho de
generoso desinterés estaban en un rincón de la casa, dentro de
un cuarto cuyas paredes las tenía materialmente tapizadas con
tarjetas postales que contenían informes de todas partes del
mundo y con el techo perforado para dar paso a una antena. Allí
se encerraba a veces don Antonio y se pasaba las horas muertas
buscando comunicaciones con ignorados amigos de Australia, Argentina
y hasta con un Rajá indio. Esto halagaba su pueril espíritu aventurero.

Algún día le hice, ingenuamente y con mi conciencia limpia, algunas
preguntas sobre el misterioso contenido del armario de su biblioteca.

---Le voy a enseñar ---me contestó--- algo curioso; más tarde
o más temprano tendrá que saberlo.

Abrió un cajón dentro del cual vi un pequeño cofre metálico cerrado
con una pequeña combinación. En este último estaban arregladas
en compartimientos varias docenas de cintas cinematográficas.
Tomó una diciéndome:

---He aquí un ejemplo típico.

Fuimos a un cuarto oscuro en donde escasamente cabíamos los dos
y había instalado un pequeño proyector y una pantalla. La cinta
comenzó a correr; al principio tenía solo una fecha y una hora:
de las nueve a nueve y media de la noche, se leía. Después una
referencia que más tarde supe era una especie de cifra para designar
a los distintos personajes. En seguida el *film* mostró cosas confusas;
finalmente se precisó un paisaje y en un banco una pareja besándose
apasionadamente; la imagen de la mujer se fue aclarando y amplificando
hasta eliminar el resto. Aunque la fisonomía era un poco borrosa
no pude menos de exclamar:

---¡Esta es la esposa del licenciado Carrano!

Aquel rostro se fundió en otras formas y más pequeña, apareció
varias veces la imagen, desnuda, de la misma mujer. Sería imposible
recordar ahora los detalles de aquella cinta cinematográfica
de un género desconocido hasta entonces para mí; pero sí rememoro
que más tarde se detalló, hasta ser reconocible, la figura del
mismo Carrano con su aspecto bonachón y sus bigotes blancos.
Y a los pocos minutos apareció el mismo hombre en diferentes
trances de muerte: tomando un brebaje o cayendo herido de un
balazo o con un estilete hundido en un costado. Estas escenas
se mezclaban a veces confusamente con las de la misma mujer,
unida a un hombre imposible de ser identificado, cuando menos
por mí.

Al terminar, don Antonio enrolló su película, llevóla parsimoniosamente
al mismo cofrecito, lo cerró y sentóse esperando mi interrogatorio.

Yo estaba atónito, sin saber qué decir. Aquello me parecía un
pasatiempo sin sentido. Finalmente me decidí a hablar:

---Si no hubiera usted despertado mi curiosidad mis preguntas
serían indiscretas; pero me creo autorizado para pedirle me diga
qué significa eso.

---Eso ---me contestó--- es lo que dan o fabrican o reproducen
los aparatos del armario sobre los cuales usted me ha interrogado.
Acaba de asistir a la revelación de un pequeño secreto, vulgar
en sí mismo, pero interesante por las personas envueltas en él.
En ese *film* ha visto usted cómo piensa uno de nuestros políticos
más encumbrados, amigo íntimo e inseparable de Carrano, lo cual
no ha sido obstáculo para que lo engañe con su mujer, de la que
está bestialmente enamorado, hasta el extremo de que su mente
alimenta proyectos homicidas para hacer desaparecer al pobre
marido.

Después supe que el asesino potencial que entregó sus intimidades
ante las maquinarias de don Antonio, era nada menos que el Ministro
don Juan José Paullada.

Cuando la confianza que a Cutiño y a mí nos ligó fue completa,
pude pasar muchas otras películas interesantes. Vi con mis ojos
el pensamiento descarnado de la mujer adúltera de Carrano, corroborando
la primera con lujo de detalles. En fin ---continuó---, a medida
que los procedimientos se fueron afinando tuve ocasión de contemplar
cosas tremendas. Comencé a acostumbrarme a ver al desnudo las
almas de las gentes que formaban ese mundo revuelto.

# _La noche anuncia el día_ (1947) de Diego Cañedo @ignore

</section>
<section epub:type="chapter" role="doc-chapter">

# _Su nombre era Muerte_ (1947) de Rafael Bernal

_Su nombre era Muerte_, de Rafael Bernal (1915-1972), cuenta
la historia de un misántropo que huye a la selva, entabla amistad
con los lacandones y se convierte en su dios tras descifrar la
lengua que hablan los moscos. Después de hacer contacto con estos
insectos, el también narrador se enterará del plan que tiene
«El consejo superior» de los zancudos para subyugar a los seres
humanos y de su importante papel en él. «El Consejo Superior»
guía la vida social de los moscos, se dedica a pensar y a administrar,
como un zángano, el trabajo de las capas más bajas de la pirámide
social mosquil y presume haber logrado prever todas las situaciones
posibles que podrían desencadenarse en el futuro ---por lo que
al parecer ahora se dedica únicamente a administrar el trabajo
de los demás---. El Consejo conforma así una élite, mezcla de
los cerdos que toman la granja en _Animal Farm_ (1945) y el gran
hermano de _1984_ (1949) de George Orwell, que ha inoculado en
los moscos una férrea creencia: el moscocentrismo. «Nosotros
los moscos somos los dueños absolutos del Universo y toda criatura
en él debe pagarnos tributo de sangre» (Bernal: 88).

Tal megalomanía está vinculada con un sentido de la propiedad,
los moscos reclaman ser los dueños del territorio que compone
al planeta Tierra, por lo que le cobran al resto de las especies
el tributo que extraen de sus venas. Según los moscos, todos
los demás animales gozan de una cierta libertad que se tolera
mientras paguen lo que se les exige sin quejarse. La excepción
es el ser humano, al que planean esclavizar para disfrutar de
su sangre. Para comunicarse con los seres humanos y llevar a
cabo su plan, los moscos necesitan aliados: un gobierno de hombres
que se sujete a sus designios. El misántropo, protagonista de
la novela aceptará ser su cómplice en un inicio. Se convertirá,
entonces, en uno de los jinetes del Apocalipsis. Aquel de quien
se dice en la Biblia que «tenía por nombre Muerte, y el infierno
le iba siguiendo».

El protagonista se enfrentara entonces ante una disyuntiva: dar
paso al imperio de los moscos, cuyos líderes han suprimido la
creencia en Dios, o bien volver a su antigua fe cristiana que
curiosamente en la novela promueve la igualdad entre todas las
criaturas, incluidos los insectos. Se debate así entre dos posibilidades,
ser un jinete del apocalipsis y esclavizar al género humano,
o convertirse en el cristo de los moscos subyugados y liberarlos,
evitando así el ataque contra los hombres. Escogerá esta última
posibilidad motivado por su anhelo de salvar a la mujer que
desea.

El oído absoluto del protagonista, capaz de ubicar la nota y
la altura de cualquier sonido, es central en su trabajo de reconstrucción
del lenguaje mosquil: «Para anotar los zumbidos desarrollé un
sistema donde incluía las intermitencias […] y la nota musical
en la que se emitían» (Bernal: 31). Descubre entonces que en
el zumbido de los moscos hay, como en la voz humana, cuatro tesituras
principales, cada una con un significado distinto: «deduje que
el verbo en idioma mosquil tiene siempre en voz de bajo un sentido
afirmativo, en voz de barítono, negativo, en voz de soprano interrogativo
y en voz muy aguda o de niño, suplicativo o exclamativo» (33).
A diferencia de la revolución de los animales de Nervo, aquí
no son los mamíferos los personajes principales, y mucho menos
hay un solo lenguaje para todo el reino animal. La revolución
ahora está en manos de una parte de los insectos que desde siempre
han tenido su peculiar forma de comunicarse.

En el capítulo que reproducimos a continuación el narrador se
comunica por primera vez con los moscos que habitan en la Selva
Lacandona. Creemos que es de los pasajes que más invitan a llevar
a cabo un proceso de re-escritura. Como lo observará el lector,
un instrumento peculiar le permite al protagonista entablar su
primera charla con uno de los insectos. Es este invento el que
vuelve a la novela claramente un relato de ciencia ficción. Su
tecnología, sin embargo, es muy distinta de los imaginarios _high
tech_ de nuestros tiempos.

# Fragmento

## IV {.centrado}

Cuatro días de marcha nos llevaron hasta el lugar que había
escogido Pajarito Amarillo en las márgenes cenagosas
del Metasboc para instalar su caribal y el de su tribu. Era
el sitio un pequeño cerro junto al pantano, donde hacía
muchos años la tribu había acampado ya en una ocasión
y aún se veían huellas de los claros que hicieron entonces
para sus siembras. Por lo demás, era un pedazo de selva
como cualquier otro. Los lacandones construyeron su caribal
en la punta del cerro y yo junto al lago, donde hubiera
moscos, pues ya lo único que me interesaba en la vida
era el estudio del idioma y costumbres de estos animales
y el bien de mis amigos los lacandones. Mucho insistieron
estos para que hiciera mi enramada junto a su caribal, pero
yo no quise y les dije que debía estar lejos y en un lugar
solitario, para tener libre contacto con los balames de los
vientos, pero que podían ir a mi casa cuando quisiesen.
{.espacio-arriba1 .sin-sangria}

Ya instalado, repasé mis notas y me volví a entregar
al estudio, temeroso de que el idioma de los moscos no
fuera el mismo aquí que en el Lacantún; pero desde la
primera noche observé con inmenso júbilo que era el
mismo y pude repetir mis experimentos, reconociendo
todos los zumbidos.

Durante seis meses me dediqué al estudio, apenas interrumpido
de vez en cuando por Pajarito Amarillo o
por algún otro miembro de la tribu, que venían a contarme
lo que habían sabido ---nunca pude enterarme cómo---,
acerca de los madereros que se aproximaban al
Lacantún y a nuestros antiguos hogares. Creo que ese
fue el tiempo más feliz de mi vida, el que he vivido con
mayor tranquilidad, teniendo tan solo en el alma deseos
de hacer el bien, de construir, de levantar. En ese tiempo
mi única ambición era la de hacer el bien a mis amigos
los lacandones. Soñaba yo con juntar tres o cuatro
tribus dispersas en un solo gran caribal, allí mismo, en
las márgenes fértiles del Metasboc, y enseñarles a sembrar
la tierra debidamente, a cuidar el ganado, que conseguiría
yo para ellos. Tenía la cabeza llena de proyectos
buenos, y si seguía estudiando el idioma de los
moscos era tan solo por un afán científico. Pensaba en
salir algún día de la selva, con un libro estupendo sobre
los moscos, publicarlo, y con el dinero que ganara traer
las cosas que necesitaran más mis amigos. Puedo asegurar
con toda verdad, con la misma verdad con la que he
contado mis delirios y mis maldades, que en ese tiempo
tenía el alma llena de bondad, que había llegado casi al
extremo de no odiar a los hombres de mi raza; tan solo
a temerles por el mal que les pudieran causar a mis amigos.
Vagamente recordaba mis lecturas sobre las misiones
de los jesuitas y de los franciscanos en el Uruguay y
en la California y soñaba con hacer algo parecido. Ya
tenía adelantado lo más difícil del camino, o sea, el ganarme
la confianza de los indios, y todo lo demás me parecía
fácil.

Para ayudar a mis amigos empecé a interesarme por
los niños, todos raquíticos y enfermos. En mi choza les
daba más comida y los divertía con cuentos que inventaba,
tratando de despertarles la dormida imaginación, pero
no hablándoles nunca del mundo de fuera de la selva,
para que no sintieran el deseo de irse con los hombres
blancos. Otras veces, en los días de calor sofocante, nos
bañábamos en el lago y les hacía barquitos de papel para
que jugaran. Esta diversión les encantó también a los
grandes y pronto todos me pedían barquitos para echarlos
al lago o a algún caño. Yo les contaba que en los barquitos
aquellos se iban todos los malos espíritus y creo
que ya consideran el pasatiempo como un rito.

Sí, ese fue el tiempo más feliz de mi vida. Ahora lo comprendo
y lloro por haber destrozado todo aquello, lloro
porque pudo más en mí la loca ambición del poder que la
bondad que empezaba a vivir en mi corazón, que ese amor
nuevo y maravilloso, sin egoísmos, que había puesto en mi
alma la bondad de los lacandones. Ahora, cerca ya de la
muerte, cuando escribo, aún lleno de odio e impulsado tan
solo por el temor de la aniquilación total, el libro que debía
de hacer tan solo con el interés de ayudar a mis amigos,
comprendo que ese tiempo de pobreza, de nulidad, ha
sido el único feliz de mi vida; pero ya es tarde, no puedo
volver atrás y los arrepentimientos son estériles.

Por lo que se refiere a mis estudios, llegué a entender
muchas de las frases usadas por los moscos y perfeccioné
el oído para distinguir los más sutiles cambios de tonos
y semitonos. Así mis noches eran divertidas, las pasaba
escuchando la charla de miles de moscos, oyendo
las órdenes que daban los que parecían ser los jefes y precaviéndome
de ellas, cuando se referían a mí o a mi sangre,
de la mejor manera posible.

Ocho meses empleé en hacer el diccionario del idioma
mosquil, que dejo en un cuaderno junto a este, para
que a los hombres que vengan les sea fácil interpretar el
idioma de los moscos y pactar entre ellos. Este diccionario,
que pensé destruir para que no cayera en manos de
ningún hombre, se lo entrego ahora al género humano
para demostrarle que le he perdonado todo el mal que
me hizo a condición de que nunca me olvide. Porque si
los hombres logran pactar con los moscos, cosa que será
fácil, se abrirá ante las nuevas generaciones todo un
mundo nuevo de cooperación con lo que llamamos animales
inferiores, un mundo exento de gran cantidad de
enfermedades y lleno de maravillas. Un mundo que yo
conozco y que yo les doy, que me deben a mí.

Cuando ya pude entender todo lo que decían los moscos,
se me ocurrió que tal vez pudiera imitar sus sonidos
y, por lo tanto, hablarles en su idioma y entenderme con
ellos. La cosa no era tan fácil como parece. El hecho de
hablar en música, en las cuatro escalas, haciendo distingos
de semitonos, presentaba para mí, con una cultura
musical muy mediocre, un gran problema. Muchos días
los pasé ensayando y ensayando las frases más simples, pero
mis sonidos en nada se parecían a los que emitían los
moscos y estaba seguro de no ser entendido. Traté entonces
de emitirlo con algún instrumento y busqué, en el
caribal de Mapache Nocturno que estaba a unas cuatro
leguas del nuestro, a Florentino Kimbol, que decían era
sabio en hacer flautas de barro y silbatos de carrizo.
Llegué al caribal a eso del mediodía y encontré a Florentino
en su hamaca, reposando. Al verme se levantó y
me preguntó:

---¿Está bien tu corazón?

---Utz ---le contesté.

Y nos sentamos el uno junto al otro en silencio. La
tribu de Mapache Nocturno me conocía bien, sabían todos
los miembros de ella que yo era amigo de los de su
raza y me estimaban. Al cabo de un rato me dijo:

---Los de tu raza van adelantando y pronto llegarán
al Lacantún. Buscan puna y aquí tenemos mucha. Mira
este tronco en el que estamos sentados.

---Es caoba ---le dije.

---Utz ---me contestó---. Y hay mucha en la selva.
Hay grandes árboles y otros que producen chicle.

Diciendo esto, sacó un gran puro de tabaco negro y me
lo ofreció. Yo lo acepté y lo encendí en una brasa del fogón
que nos trajo Petronila, su mujer, y seguí en silencio.

---¿Has andado por la selva? ---me preguntó.

---Sí ---le contesté---, he venido a verte porque mi corazón
desea decirte algunas palabras.

---Si tuviera aguardiente te ofrecería ---me dijo---.
Pero nos hemos alejado de los hombres de tu raza y ellos
traen el aguardiente.

---No quiero aguardiente ---le dije---, porque sé que
los hombres de mi raza esconden a los espíritus del mal
en él, para acabar con todos ustedes. Por eso convencí a
Pajarito Amarillo de que mudara su caribal hasta estas
regiones, para no tener tratos con los blancos.

---Mi padre, Mapache Nocturno, también quiso venirse
tras de Pajarito Amarillo porque te aprecia y su corazón
te necesita ---dijo Florentino con tristeza---, y
ahora no tengo aguardiente que ofrecerte.

---No te afanes por eso; yo ya no lo tomo, porque sé
que es malo ---le dije, y en el fondo de mi alma sentí algo
agradable. No tan solo Pajarito Amarillo me apreciaba;
también Mapache Nocturno había seguido con su tribu
mis pisadas. Pronto se podría hacer la unión de estas
dos tribus y empezarse la civilización de mis amigos.

Mientras yo pensaba en estas cosas, Florentino fumaba
en silencio, escupiendo de vez en cuando. Por fin habló:

---Tú eres sabio ---me dijo--- y nosotros te comparamos
con el tecolote que todo lo ve y nunca cierra los ojos.
Pero en tu corazón hay tristeza porque tienes odio a los
hombres de tu raza y nos alejas de ellos.

---Si pretendo alejarlos de ellos es porque los conozco,
porque sé el mal que acarrean. Pero no vine a hablarte
de esas cosas, Florentino: vine porque quiero usar
tus manos y tu ciencia para hacer una flauta.

Con su acostumbrada delicadeza, Florentino no preguntó
para qué la quería. Tal vez imaginó que trataba
yo, con ella, invocar a mis dioses. Tan solo escuchó atentamente
mis ideas y se puso a trabajar con un carrizo delgado.

Después de varios ensayos, logró una flauta que emitiera
todos los sonidos que buscaba, en un tono muy parecido
al zumbar de los moscos, y con ella, ya entrada la
noche, regresé al caribal de Pajarito Amarillo y a mi choza.

Esa misma noche ensayé con mi flauta y zumbé la palabra:

---Ven.

Un mosco que revoloteaba sobre mi cabeza se detuvo
un momento y salió huyendo, gritándoles a sus compañeros
que había escuchado una voz que lo llamaba.
Ese experimento me llenó de entusiasmo, pues con él ya
estaba seguro de que los moscos habrían de entender lo
que les dijera o zumbara, con lo que seguí practicando
con más empeño. Entre más me adentraba por los diferentes
aspectos del idioma, más difícil se me hacía el dar
todos los tonos y semitonos requeridos, con la debida
exactitud.

En otro cuaderno que dejo junto a este y junto al que
contiene el diccionario, he anotado todo lo indispensable
para el buen uso del idioma mosquil, o sea, todas las
reglas gramaticales más importantes. Hay que hacer notar
que en este idioma nunca hay excepciones a las reglas,
lo cual hace el idioma más civilizado del que he oído
hablar.

Después de muchos ensayos, como la constancia y la
práctica todo lo vencen, me creí con los conocimientos
y la habilidad suficientes para entablar una conversación
con los moscos, y la inicié, una noche, con uno que revoloteaba
cantando sobre mi cabeza una tonadilla que
parecía estar muy de moda entre ellos:

---Ven ---le dije en tono grave de mando; y luego en
tono agudo de súplica---: No te vayas, quiero hablarte.

El mosco se llenó de asombro y soltó tres o cuatro interjecciones
en tono agudo, buscando a su alrededor para
ver si había otro mosco que le hablara.

---Escúchame ---le volví a decir en tono grave de
mando---. Soy yo quien te habla, el hombre a quien atormentas.

Entonces el mosco se detuvo un momento sobre una
de las cuerdas de la hamaca.

---Has aprendido, por lo que veo, nuestro idioma
---me dijo---; y debo obedecerte y acatarte, como lo manda
el Gran Consejo que nos rige y cuyo nombre no puedo
pronunciar. Ordena lo que quieras y yo te obedezco.

Algo confuso me quedé, no sabiendo qué ordenar ni cómo
iniciar la charla. Además, para ser franco, la emoción
me impedía casi el emitir un solo zumbido con mi flauta.

---Si nada quieres de mí, ¿para qué me has llamado?
---me preguntó el mosco, turbado también ante mi indecisión.

---Nada tengo que ordenarte ---repuse---. Tan solo te
he llamado para conversar contigo.

El mosco pareció dudar un momento y por fin zumbó:

---Perdona mi duda, pero no creo ser yo quien deba
hablar contigo, porque soy de clase baja: no soy más que
explorador. En este caso no sé qué hacer y debo avisar
inmediatamente a mi superior, el cual avisará a su superior,
hasta que llegue la voz al Gran Consejo de aquí; pero
me da miedo hacerlo, ya que nunca se había oído decir
que otro ser de la creación hablara y temo que todo
esto sea tan solo un sueño mío.

---No es sueño. Durante muchos años he estudiado el
idioma de ustedes y ahora…

---Tú fuiste entonces quien le habló a un compañero
mío hace tiempo, que dijo haber escuchado voces y fue
sentenciado a morir por ello.

---Sí, yo fui el que le habló: le dije «Ven» y él se alejó
lleno de temor gritando. Siento mucho su muerte…

---La muerte no tiene importancia ---me contestó---.
Lo que importa es cumplir con la misión, llevar adelante
el proyecto y creo que el hecho de que tú hayas aprendido
nuestro idioma y por primera vez podamos entendernos
con otro ser de la creación, es algo de gran
importancia; así que llamaré a mi superior, a quien te
ruego le hables, para que no me cueste la vida.

---Llámalo ---le dije--- y no temas.

Así lo hizo, zumbando fuertemente, y al poco tiempo
se presentó el superior, un anófeles perfecto que pareció
muy indignado cuando supo por qué lo habían llamado.
Entonces le hablé yo:

---No castigues a tu inferior ---le dije---. Él no ha hecho
más que decirte la verdad y yo le he rogado que te
llame para que decidas lo que se debe hacer. He aprendido
tu idioma, durante años lo he estudiado y quiero hablar
con ustedes y ser su amigo en lugar de su enemigo.

---Nunca has sido nuestro enemigo ---me contestó el
superior---. Nosotros los moscos, los dueños de todo, no
tenemos enemigos. Tú has servido como fuente de sangre
para alimentar al Gran Consejo, que no puedo nombrar
porque su nombre es demasiado alto para que lo
pronuncie yo…

---Pero es que he matado a muchos de ustedes.

---Nada importa la muerte de unos cuantos cuerpos.
Tu sangre nos era necesaria y la hemos tomado y, aunque
hubieras matado cien veces más, la hubiéramos tomado.

---Me alegra oír eso ---le contesté con mucha finura
aunque sin mucha verdad---. Yo quiero ser amigo vuestro…

---Nosotros no tenemos amigos ni enemigos, pero ya
que has aprendido nuestro idioma y podemos hablar
contigo, tal vez te podamos utilizar en algo. ---Luego,
volviéndose hacia el que primero habló conmigo, le dijo---:
Has hecho muy bien en llamarme. Te recomendaré
para que se te nombre guardián del Gran Tesoro.

---Me alegro de oír eso ---intervine---. No me hubiera
parecido bien que este pobre sufriera un castigo injusto,
como el otro.

---Tú le das demasiada importancia a la muerte, que
para nosotros no es nada. Pero es bueno que siga hablando
contigo. Convocaré a todos mis superiores para que
ellos decidan cómo se debe llevar este asunto ante el
Gran Consejo y lo que se debe hacer.

Y diciendo esto, zumbó con gran fuerza y aparecieron
esos moscos grandes que yo ya conocía. Aparecieron
varios centenares de ellos, que, después de escuchar las
palabras del superior, llenaron el espacio con tal zumbadero,
que creí oportuno hablarles para calmarlos y, tomando
mi flauta, les dije:

---Señores míos, este capitán ha dicho la verdad. Yo
he aprendido el idioma para poder charlar con ustedes.
No creo que esto sea causa para tanto alboroto.

---No sabes lo que dices ---me contestó uno de los
moscos grandes---. Esto es lo más importante que ha sucedido
en nuestra historia, que abarca cientos de miles
de años. Tu venida puede ser providencial, pero yo no
debo hablar de estas cosas, sino avisar inmediatamente
al Gran Consejo de aquí, para que él, a su vez, avise a
todos los Grandes Consejos del mundo y se reúna el
Consejo Superior, que no se ha reunido hace quinientos
ochenta y seis mil años, siete meses y catorce días. Eso
es lo que debo hacer…

Y diciendo esto, salió seguido de todos los moscos,
dejando mi choza vacía, tan vacía que el murmullo de la
selva penetraba entre las hojas de palma, como queriendo
llegar hasta mí. Mientras, yo esperaba.

# _Su nombre era Muerte_ (1947) de Rafael Bernal @ignore

</section>
<section epub:type="bibliography" role="doc-bibliography">

# Bibliografía

Alatorre, Antonio, «Invitación a la lectura del _Sueño_ de sor
Juana» en _Soledades / Primero sueño,_ Ciudad de México: +++FCE+++,
2010, págs. 123-154. {.frances}

Cañedo, Diego (Guillermo Zarraga Argüelles), _La noche anuncia
el día,_ Ciudad de México: editorial Stylo, 1947. {.frances}

Castera, Pedro, «Un viaje celeste» en _El futuro en llamas. Cuentos
clásicos de la ciencia ficción mexicana_, Ciudad de México: Grupo
Editorial Vid, 1997. {.frances}

«Ciencia Ficción Mexicana», página web: [cfm.mx](http://www.cfm.mx/)
(última consulta, martes 21 de mayo del 2019). {.frances}

Damián Miravete, Gabriela, «La mano izquierda de la ciencia ficción»,
_Letras Libres_, 1 de octubre 2018. En [alturl.com/uaxz9](http://alturl.com/uaxz9)
(última consulta, martes 21 de mayo del 2019). {.frances}

Depetris, Carolina, «Viaje fantástico y escolástica inquisitorial:
el derrotero lunar del fraile Maunel Antonio de Rivas» en _Sizigias
y cuadraturas lunares ajustadas al meridiano de Mérida de Yucatán
por un antíctona o habitador de la luna y dirigidas al bachiller
Don Ambrosio de Echeverría, entonador que ha sido de_ kyries
_funerales en la parroquia del Jesús de dicha ciudad y al presente
profesor de logarítmica en el pueblo de Mama de la península
de Yucatán, para el año del señor 1775. Seguido de fragmentos
del proceso inquisitorial_, Mérida: +++UNAM+++, 2009. {.frances}

Dziubinskyj, Aaron, «Eduardo Urzaiz's “Eugenia”: Eugenics, Gender,
and Dystopian Society in Twenty-Third-Century Mexico», _Science
Fiction Studies_, vol. 34, núm. 3, noviembre 2007, pp. 463-472. {.frances}

González Casanova, Pablo, _La literatura perseguida en la crisis
de la Colonia_, Ciudad de México: +++FCE+++, 1958. {.frances}

Granillo Vázquez, Lilia e Isaí Mejía Villareal, «Mujeres en la
ciencia ficción mexicana: Blanca, Martha, Brenda y otras más»,
_Revista internacional de culturas y literaturas_, núm. 1, 2015,
págs. 59-85. {.frances}

Inés de la Cruz, Sor Juana en _Soledades / Primero sueño_, Ciudad
de México: +++FCE+++, 2010. {.frances}

Le Guin, Ursula K., «A Rant About “Technology”» en [alturl.com/56jtv](http://alturl.com/56jtv)
(última consulta, martes 21 de mayo del 2019). {.frances}

Lepori, Roberto, «Sor Juan y la ciencia ficción o las consecuencias
de una crítica paranoica», _Revista virtual de estudios literarios
y culturales centroamericano,_, núm. 23, julio-diciembre 2011. En
[alturl.com/acvqj](http://alturl.com/acvqj). {.frances}

Martré, Gonzálo, _La ciencia ficción en México (hasta el año
2002)_, Ciudad de México: Instituto Politécnico Nacional, 2004. {.frances}

Nervo, Amado, «La última guerra» en _El castillo del inconsciente_,
Ciudad de México: +++D.G.P. CONACULTA+++, 2000. {.frances}

Peniche Ponce, Carlos, «Introducción» en _Eugenia. Esbozo novelesco
de costumbres futuras_, Ciudad de México: +++UNAM+++, 2006. {.frances}

Rivas, Manuel Antonio de, _Sizigias y cuadraturas lunares ajustadas
al meridiano de Mérida de Yucatán por un antíctona o habitador
de la luna y dirigidas al bachiller Don Ambrosio de Echeverría,
entonador que ha sido de_ kyries _funerales en la parroquia del
Jesús de dicha ciudad y al presente profesor de logarítmica en
el pueblo de Mama de la península de Yucatán, para el año del
señor 1775. Seguido de fragmentos del proceso inquisitorial_,
Mérida: +++UNAM+++, 2009. {.frances}

Trujillo Muñoz, Gabriel (compilador), _El futuro en llamas. Cuentos
clásicos de la ciencia ficción mexicana_, Ciudad de México: Grupo
Editorial Vid, 1997. {.frances}

</section>
