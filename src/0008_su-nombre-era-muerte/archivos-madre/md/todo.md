<section epub:type="dedication" role="doc-dedication">

# Dedicatoria @ignore

A Pilar {.espacio-arriba3 .epigrafe}

</section>
<section epub:type="epigraph">

# Epígrafe @ignore

_Y he ahí un caballo pálido, cuyo jinete tenía por
nombre Muerte, y el infierno le iba siguiendo, y diósele
poder sobre las cuatro partes de la tierra para matar_
+++a los hombres+++ _a cuchillo, con hambre, con
mortandad y fieras de la tierra._ \
++Ap vi, 8.++ {.espacio-arriba3 .epigrafe}

</section>
<section epub:type="chapter" role="doc-chapter">

# I

Tal vez ya mi trabajo resulte inútil y sea demasiado tarde
para empezar estas memorias; la muerte me rodea y
no sé cuánto tiempo me quede. Ahora comprendo que
debí empezar antes, cuando tenía poder para mandar sobre
la misma muerte, pero estas memorias las escribo para
los hombres, para el bien del género humano, y cuando
tuve tiempo de escribirlas no lo hubiera querido: no
me consideraba miembro de tan absurda organización.
Más bien me consideraba como un ser superior, enemigo,
ofendido, lleno del deseo de venganza y con el poder
bastante para realizarla.

Ahora, abandonado por todo aquello que me dio mi
poder inmenso, me vuelvo a sentir hombre, un hombre
como cualquier otro, lleno de miedo ante la aniquilación
que me espera y con un deseo insensato de ser inmortal,
de vivir más allá de este pobre barro. Por eso escribo mis
memorias, aunque sé que no me han de servir para nada
en el breve plazo que me queda de vida. Tan solo pretendo
con ellas vivir más allá de mi muerte en la memoria
de los hombres a quienes hago el bien.

En verdad, yo no le he hecho mal alguno a los hombres;
tan solo los he ofendido con el pensamiento y con
el deseo y, si les hice el mal, fue por omisión, al no entregarles
el bien más grande que hombre alguno pueda
entregarles. Porque yo pude ser algo como un «SuperPasteur»,
pero mi odio, que ahora comprendo era insensato,
y mi ambición de poder, me llevaron por otros caminos.
Es cierto que jamás recibí de los hombres bien
alguno, y aun ahora, con la muerte irremediable cerca de
mí, no sé si obré bien o mal y no siento arrepentimiento
por haber hecho lo que hice. Cuando me llegó el momento
de decidir entre la caridad y mi ambición, me sentí
impulsado por el odio a la sociedad, por la angustia
que llevaba dentro, por el recuerdo de todas las amarguras,
de todos los egoísmos que había yo visto y sufrido
entre los hombres. Aun ahora, cerca ya de la muerte,
abandonado de todo mi poder, sintiéndome de nuevo
humano, no puedo decir que amo a los hombres. No encuentro
en todo mi pasado causa alguna por la que deba
amarlos, aunque pertenezco a ellos y soy de su mundo.
Ahora me siento aquí, a esperar la muerte, a la luz de
una lámpara miserable, mientras afuera se agita y aúlla
la selva; y escribo estas memorias, pero yo mismo comprendo
que lo hago tan solo por un innato sentido de solidaridad
humana y por un deseo imperioso de inmortalizarme
en la memoria de los hombres que he despreciado.

Si mi único interés fue hacerle el bien a los hombres,
contaría escuetamente mi aventura, señalaría el peligro
que pesa sobre el género humano, sugeriría el remedio y
no diría nada de mi vida particular. Pero es que no escribo
esto impulsado por la caridad: escribo porque quiero
ser conocido, no ser olvidado nunca; escribo para que mi
nombre, el que he puesto en la primera página de este libro,
viva mientras haya mundo. Esta seguridad de una
larga sobrevivencia en mi obra, me calma en algo el miedo
terrible que me invade mientras espero a la muerte.

La parte de mi vida que tiene importancia es muy breve:
tan solo cuatro años, desde los cuarenta y cinco hasta
los cuarenta y nueve que tengo ahora. Todo lo anterior
fue tan solo una preparación para la amargura; y esta
amargura es la razón, es el móvil de todas mis acciones
en los cuatro años importantes. Pero no quiero contar lo
anterior. Ya en la portada de este cuaderno doy mi nombre
y mi patria, y bien pueden los eruditos reconstruir
mi historia hasta que se pierda en los antros de la selva.
Básteme decir que en los cuarenta y cuatro años de mi
vida anterior, solo coseché amargura, amargura y odio.

La maldad de los hombres y el asco que me producía
su contacto me arrojaron de las grandes ciudades, rumbo
a las orillas de la civilización, hasta llegar a Chiapas.
Un tiempo viví con los chamulas de las márgenes limpias
del Grijalva; allí viví en mi soledad interna, rota de vez
en cuando por la maldad humana. Pero mi espíritu buscaba
mayor soledad y cada contacto con los hombres era
un tormento insoportable; con cada palabra que me decían,
renacía en mí todo lo pasado y mi alma se llenaba
de una angustia sorda que me apretaba la garganta, y
sentía el deseo de herir, de matar, de hacer un daño irreparable.
La absurda necesidad de ganarme la vida me
obligaba a tratar con los hombres, y ahora que recuerdo
esos tratos los odio más que nunca, Cuando conseguía
algo de dinero, hacía lo posible por olvidar y bebía hasta
quedar tirado en las calles sin que nadie tuviera la misericordia
de recogerme. Para el mundo era yo un borrachín
despreciable, objeto de una risa estúpida, pero, si
era yo un borracho, se lo debía al mundo y me consideraba
más como el ofendido que como el ofensor.

Emigré buscando una soledad más completa y crucé la
sierra hasta llegar a las márgenes perdidas del Usumacinta,
a San Quintín, lugar de odio y de muerte, acorralado por
la selva, donde los hombres enfermos y podridos buscan
con ansia las maderas preciosas y el chicle que los ha de llevar
a las ciudades, también enfermas y podridas, y es que
ellos llevan la podredumbre en sus almas y, dondequiera
que estén, no encontrarán más que lo que llevan dentro.

Allí conocí la verdadera selva y me adentré en sus espesuras,
remontando los caños cenagosos en mi canoa solitaria,
buscando algún medio de vida que me permitiera
conservar mi soledad, que me salvara del horripilante
contacto con los hombres. Allí aprendí lo que es la selva
destructora, la selva enemiga, la selva que suda muerte,
pero mejor que las ciudades, más benigna, más dulce. La
selva, cobijadora de desgracias, ocultadora de amarguras
y de odios, la selva bendita. Quien quiere vivir en ella,
puede hacerlo si se sujeta a sus leyes y se conforma con
ser un adorador miserable entre tanta magnificencia, si
se conforma con dejar a un lado su orgullo de hombre,
para ser tan solo un arrimado, un advenedizo sin derechos,
que vegeta a la sombra de la bondad de la selva.

Así viví en las márgenes del Usumacinta perezoso, en
una miserable choza de palma, sin más bienes que una
hamaca, una carabina y unos cuantos libros viejos y carcomidos
por la humedad y el tiempo. A veces llegaba yo
hasta los pueblos a vender las pieles de tigre y las plumas
de garza que había cobrado en mis correrías, y con
el producto compraba algunos cartuchos y unos botes
de aguardiente, con lo que me retiraba por algún caño
hasta mi choza y me dedicaba a beber y a olvidar mi lamentable
condición de hombre. La vida así era soportable.
Mi único tormento eran los moscos que por la noche
se cebaban mi sangre y que de día me acechaban
entre las sombras de los árboles, para hacerme huir rumbo
a los playones descubiertos, donde el sol, a plomo sobre
la espalda, dolía como un inmenso latigazo. En la
cabeza, durante las eternas noches de insomnio, me
martillaban los versos de no sé qué poeta, que también
ha de haber padecido este incansable tormento, poeta
ducho en la amargura de la selva, en su horror y en su
muerte lenta:

> No sé dónde aprendió la selva \
> el arte de llorar; \
> yo supe de esa angustia, \
> de la impotencia del machete \
> ante el asesinato fértil de la tierra \
> y yo velé la noche sin estrellas \
> echado junto al río, \
> bajo el toldo sonoro de los moscos.

Quizá la mejor solución hubiera sido la muerte. Recuerdo
que un día, mientras trataba de despertar de una borrachera
en las calles de Tumbalá, un hombre, empujándome
con el pie, dijo:

---Para vivir como vive, mejor haría este en morirse.

La frase se me había quedado pegada dentro, era parte
mía y me asaltaba por las noches, cuando los moscos
me atormentaban y no tenía junto a mí la fuerza consoladora
del aguardiente. Muchas veces me acerqué a las
márgenes del río y medité en la sencillez de caminar
aguas adentro, hasta que la corriente me recogiera en sus
brazos fríos, Pero el coleteo de algún caimán, que tal vez
había adivinado mis pensamientos, me llenaba de tal pavor
que corría a refugiarme a mi hamaca, donde el alba
me encontraba sollozando. Otras veces meditaba en lo
rápido que es un disparo, pero el miedo, el terrible miedo
al no ser, al no haber sido nunca, detuvo mi mano,
puesta ya sobre el gatillo de la carabina.

A través de mis tragedias, de las que no he de hablar,
de mis borracheras, de mi odio y de mi amargura, he conservado
este deseo terrible de ser, que ahora me hace tomar
la pluma para entregarle al mundo mi secreto. Cuando
joven soñé primero con la gloria que se adquiere en las batallas
y los sueños de mi niñez estuvieron llenos de heroísmo.
Luego quise un nombre y un lugar en el mundo de las
letras, pero todas las puertas se me cerraron, aunque bien
sé que en mí alienta el espíritu de un hombre que puede
escribir cosas maravillosas. Es raro que ahora, después de
tantos años, de tantas tragedias y desastres, de tanta grandeza
y poder, aquí, en las márgenes cenagosas del Metasboc,
con la muerte pisándome los talones ---«puesto ya el
pie en el estribo», pudiera decir con Cervantes---y le vuelva
a confiar mi inmortalidad a la pluma.

Cinco años viví la vida del cazador, al cabo de los cuales
había perdido ya hasta el deseo de dejar la selva, no
porque la amara, no porque me hubiera acostumbrado a
su vida oscura, ni porque me hubiera resignado a esta
muerte lenta, sino porque el aguardiente y la angustia,
el paludismo y la miseria moral, me habían arrebatado
todo poder para desear y actuar. Era yo como las aguas
de los caños que van lentamente hacia el río, podridas,
muertas, sin voluntad.

Vagué cinco años por la selva del Usumacinta, me
adentré en las espesuras, llegué a lugares desconocidos
para el hombre blanco, pasé más allá del misterioso
Metasboc, hasta las charcas del Petén, para volver luego
hacia los pueblos, con mi cargamento de pieles, plumas
y miserias.

A veces construí mi enramada junto al caribal de alguna
tribu de lacandones miserables, indios olvidados en
el centro de la selva, que no conocen de la civilización
más que el aguardiente asesino y el robo eterno de los
comerciantes. Allí conocí a Pajarito Amarillo y a
Mapache Nocturno, dos venerables jefes de tribu, grandes
sacerdotes de no sé qué dioses olvidados ya, los únicos
amigos verdaderos que he tenido.

Aprendida la lengua de los lacandones, me fue fácil
entenderme con ellos y los estimé más que a los hombres
de mi raza. Viven en una semibestialidad agradable al
hombre que busca la soledad y que a veces necesita de
brazos que lo ayuden. Poco a poco dejé de aparecer en
los pueblos y todo mi comercio era con los lacandones,
sobre todo con la tribu de Pajarito Amarillo, que constaba
de nueve hombres y cinco mujeres codiciadas, madres
de once niños barrigones y sucios. En los últimos
tres años de esos cinco de vida nómada y sin importancia,
mi choza estuvo siempre junto al caribal de la tribu.
Yo les daba la carne de los animales que mataba y los defendía
de las garras de los tigres y de los comerciantes
blancos. Ellos, en cambio, me daban un poco de maíz o
de yuca y su silenciosa amistad.

Ya no hay para qué hablar de esos cinco años de vagancia
por los caños y las charcas pestilentes. Se deben
perder con mis años anteriores, sin importancia. El
tiempo es corto y más vale hablar de lo que importa.

</section>
<section epub:type="chapter" role="doc-chapter">

# II

Lo que voy a relatar sucedió hará unos cuatro años, más o
menos. Era el tiempo en que empiezan las aguas y toda la
tierra está ya inundada, juntándose un caño con el otro,
haciendo de la selva un solo pantano impasable. Yo vivía
en un pequeño claro, junto al Lacantún de aguas rojizas, a
escasas doscientas varas de las enramadas de Pajarito
Amarillo. Desde hacía más de dos meses me venían repitiendo
constantemente las calenturas y no salía para nada
de mi choza, pasándome el día dormitando en la hamaca
y la noche espantando moscos, matándolos entre mis manos
y apilando sus cadáveres en un cajón que me servía de
mesa. Hay un cierto gusto en matar entre las manos a un
mosco, en sentir en las palmas húmedas su cadáver, tomarlo
con los dedos y ponerlo junto a otros muchos cadáveres
en una cazuela, para regocijarme con el espectáculo a la
mañana siguiente.

Había una de las mujeres indias, que tenía algo de hechicera,
a quien llamaban Hormiga Negra. Esta solía atenderme
cuando estaba enfermo, me preparaba el maíz y me
traía el agua. Viendo todas las mañanas la cazuela con todos
los cadáveres de los moscos, le entró un extraño empeño
en que yo también era hechicero y que fabricaba medicina
con moscos muertos. Nunca la pude convencer de
que se trataba tan solo de un pasatiempo inocente, nacido
del odio implacable que despertaba en mi alma el zumbido
absurdo. Por más que traté de persuadirla, siguió adelante
con su creencia, la infundió a los demás miembros de
la tribu y pronto todos me vieron con cierto temor y mucho
más respeto.

Muchos de ellos venían a pedirme medicinas para las
calenturas y yo les daba un poco de quinina, con lo que
sentían alivio, Esto hacía que mi fama creciera y la de
Hormiga Negra menguara, así que, temiendo sus iras y
deseando sobre todo conservar la paz, me asocié con ella.
Los enfermos me veían a mí, yo les recomendaba que vieran
a la Chimán, a la cual le daba los polvos blancos, que
ella revolvía con mil porquerías, como panzas de arañas
de agua y otras cosas, y se los daba a los enfermos entre
sahumerios y cantos. Yo creo que Hormiga Negra es una
de las mujeres más inteligentes que he conocido. Había
enterrado ya a tres maridos, y ahora, a los sesenta años,
estaba casada con un muchacho de veinte, que se consideraba
feliz de ser el marido de tan ilustre hechicera.

Los moscos, mientras tanto, seguían atormentándome
en mi choza. Desde hacía mucho tiempo ya no sentía sus
piquetes, pero su constante zumbar alrededor de mi cabeza
me exasperaba y me llenaba de odio y de rabia.
Llegué a odiar a los moscos tanto como a los hombres.

En mis mocedades había yo estudiado algo, y ahora,
en el ocio obligado del tiempo de aguas, con poco aguardiente
para distraer mis días, me dediqué a estudiar a los
moscos, hasta que pude conocer más de cien clases de la
especie del anófeles. Por falta de libros no pude catalogarlos
ni saber si había encontrado especies nuevas, lo
que seguramente hice. No creo que toda la multitud de
moscos haya sido ya catalogada y estudiada por los sabios,
y seguramente que alguna nueva murió bajo el golpe
justiciero de mis manos.

Los estudios sobre los moscos me interesaban bastante
y traté de penetrar en su organización social. Observé que
durante el tiempo en que hay mucha fruta madura en los
árboles aparecen más moscos, cosa que no me explicaba,
pues tenía entendido que su nacimiento, desde el momento
en que la hembra pone los huevecillos en las charcas,
hasta que el mosco ya hábil para volar deja su capullo, es
de unas tres semanas, así que, si las hembras, como fuera
lógico, pusieran más huevos cuando hay bastante fruta en
los árboles para mantenerse, las crías nacerían hasta tres
semanas más tarde y entonces se notaría el aumento. Pero
siendo el aumento exactamente en el tiempo de la fruta,
pensé que debía haber un cerebro previsor y que las hembras
ponen sus huevos con tiempo, para que las crías nazcan
cuando hay un alimento bueno y seguro. Más tarde entendí
la razón de todo esto, pero al principio andaba yo a
tientas, suponiendo una organización, pero no llegando a
imaginar jamás lo completa y maravillosa que es.

Observé también que primero entraban solamente a mi
choza dos o tres moscos, me picaban y al poco tiempo empezaban
a llegar muchos más. Esto me hizo pensar que había
una forma de comunicación entre ellos y que se llamaban
cuando encontraban un ser _ad-hoc_ para extraerle la
sangre. Como las hembras son las que pican y beben sangre,
mi choza se llenaba de ellas y tan solo, de vez en cuando,
aparecía un macho. En cambio, si dejaba yo fruta sobre
la mesa, llegaban muchos machos a chupar del dulce, sobre
todo si se trataba de mangos y de chicozapote y si
estaban muy maduros. Pero si existía un sistema de comunicación,
¿cómo sería este? Tal vez se comunicaban por
ondas pequeñísimas que captaban con antenas, como las
hormigas y algunos otros insectos, o les serviría de idioma
ese constante zumbar. Otra cosa me preocupaba: si se podían
comunicar entre sí, ¿cómo es que se seguían acercando
a mí, cuando mataba a tantos cada noche, y no mejor
atacaban a los animales que como el venado, son presa fácil?
Pero me atacaban a mí y atacaban al sapo, que los mataba
también sin misericordia y los devoraba.

Como ya dije, los piquetes ya no me molestaban ni le
temía a las calenturas, que eran mis eternas compañeras.
Lo que aún me desesperaba era ese constante zumbar, ese
ir y venir sobre mi cabeza, dejando un rastro sonoro y desesperante.
Un día se me ocurrió que ese zumbar fuera el
sistema de comunicación que tenían entre ellos, ya que estaba
yo plenamente convencido de que podían comunicarse
tan fácilmente como lo hacen los hombres por medio
de la palabra, o mejor aún. Con esta idea en la cabeza,
empecé a estudiar los zumbidos y noté que no todos eran
iguales, que los había más largos, más agudos, más graves,
unos intermitentes y otros continuos, y no era solo la diferencia
del tono de voz de cada mosco, sino que cada animal
emitía sonidos en distintos tonos. Al principio se me
ocurrió que los zumbidos fueran como señales de telégrafo
y que las intermitencias señalaban letras o palabras, tal
vez ideas completas. Pero si así fuera, con toda seguridad
los zumbidos serían todos iguales en cuanto a su gravedad
o agudeza, ya que tan solo las intermitencias serán factores
para entender lo zumbado o dicho.

Las observaciones sobre los moscos, su vida y su posible
idioma me divirtieron de mis tristezas durante todo
este tiempo de aguas de hace cuatro años. Ya no le
temía a la noche como antes, sino que la anhelaba, para
pasarla despierto, estudiando los zumbidos y reteniéndolos
en la memoria, tratando de entenderlos antes de
formar una teoría acerca de ellos.

Pero de pronto me entró la amargura y fui presa de la
desesperación. No sé qué oscuro camino siguió dentro
del alma hasta llegarme a la garganta, como en los tiempos
antiguos. Una noche necesité del consuelo del aguardiente,
lo necesité más que nunca, con una ansia loca,
irrefrenable. Grité yo solo en mi choza, clamé contra todo,
contra los hombres aborrecidos y contra la selva amada,
contra los ríos perezosos, sucios como mi vida. Sin
poderme contener fui a las casas de Pajarito Amarillo y
robé el aguardiente salvador; me escondí en un playón y
bebí, bebí hasta revolcarme en la arena, gritando mi odio
y mi desesperación, mientras a mi alrededor callaba la selva
magnánima, horrorizada ante la pequeñez del hombre.

A la mañana siguiente, la vergüenza y el odio me obligaron
a dejar mi choza y retirarme selva adentro, lejos
de todo contacto humano, donde beber a solas, sin el reproche
mudo de mis amigos los lacandones.

Dos meses me duró el infierno del aguardiente.
Escondido en el vientre de la selva, bebía y maldecía de
los hombres que me arrojaron a tal bajeza. A veces, durante
días enteros, no comía, me los pasaba tumbado en
algún playón bebiendo y odiando, odiando siempre. Yo
sabía que este camino me había de llevar a la muerte, pero
yo deseaba la muerte redentora, la muerte que es reposo
sin odio y sin amargura. No sé cuándo perdí el conocimiento
de mí mismo. Me sumí en la bruma del alcohol
y la selva me acogió entre sus brazos como una madre
buena o como una querida peligrosa. Tampoco sé cuánto
tiempo estuve sin conciencia, muerto a todo, si no era
al fuego que llevaba dentro. Tal vez vagué por los ríos y
por los caños; los lacandones cuentan que disparaba yo
al aire y que mis carcajadas, mi llanto y mis gritos desgarraban
los más hondos rincones de los caños. Que
atravesaba yo por la selva, sin machete, abriéndome paso
con las manos, con los dientes, en una ansia inacabable
de llegar a algún sitio, de romper, de destrozar. La
selva reía a mi alrededor, reía de mi pequeñez, de la pobreza
de mi triste alma podrida que arrastraba yo por los
caños, también podridos. No sé cuánto tiempo duró mi
delirio, no recuerdo nada de lo que hice, ni las palabras
que dije, las palabras amargas que me nacían de adentro.

Cuando desperté era una mañana clara. Estaba yo
tendido en mi hamaca, lastimado en el cuerpo y en el alma.
Pajarito Amarillo se inclinó sobre mí y me dijo:

---Hombre blanco, amigo nuestro, mío y de mi tribu:
ya te ha dejado el demonio que se esconde en el alcohol,
gracias a Hormiga Negra. Hemos invocado a los cuatro
balames buenos y a la cruz de los hombres de tu raza;
hemos ofrecido miel nueva en sacrificio y he aquí que
los espíritus nos han escuchado y han alejado de ti a los
demonios que te atormentaban, porque los cuatro vientos
buenos han soplado sobre tu cara y estás limpio. No
pienses que guardamos ofensa contra ti. Antes bien te
pedimos que para siempre te quedes a vivir con nosotros,
pues nos gusta tu compañía; tú sabes alejar los demonios
de las calenturas y de los fríos y por tu boca hablan
muchas veces los espíritus buenos. Quédate con
nosotros, porque tú eres como el tecolote, que sabe muchas
cosas.

En efecto, los demonios de la amargura y de la desesperación
me habían dejado y me quedé junto al caribal
de Pajarito Amarillo. Yo creo que la crisis brutal, expresada
en las locuras odiosas del delirio, me había
purgado el alma de toda amargura. Ya no sentía odio ni
angustia, el nudo que me apretó por tanto tiempo la garganta
se había deshecho y solo me quedaba una terrible
laxitud. Las palabras de Pajarito Amarillo, las primeras
palabras de alabanza y de esperanza que oía en mi vida,
me habían reconfortado el alma. Por fin alguien creía en
mí, en mi ciencia de la vida, en mi bondad de hombre.

Así me entregué a la pereza de la selva y durante toda
esa temporada de secas no tronó mi rifle en los caños
ni vi coletear al caimán impotente, ni aceché al tigre malicioso.
Pajarito Amarillo y su gente se ocuparon de mi
manutención y yo los aconsejaba, los aconsejaba siempre
en lo que creía que era el bien, al odio a los hombres de
mi raza.

</section>
<section epub:type="chapter" role="doc-chapter">

# III

Apenas terminó mi delirio, empecé de nuevo a estudiar
a los moscos, tratando de acordarme de todo aquello que
había observado y anotando por primera vez mis ideas.
Seguía fascinado con la teoría de que los moscos pudieran
tener un medio de comunicarse entre sí y de que ese
método estaba basado en los zumbidos, para el hombre
tan impertinentes y tan sin razón de ser.

Imaginar esto es imaginar que los moscos tienen una
inteligencia humana y una organización probablemente
similar a la de las hormigas y del comején, pero mucho
más amplia, no sujeta a un absurdo agujero en la tierra o
en el tronco de un árbol. Si esa organización existiera,
debería ser más perfecta que la de las hormigas, aunque
a primera vista pareciera menos exacta. Pero querer estudiar
a los moscos y su organización, sin comprender
primero su método de comunicarse, es como el querer estudiar
a un país extraño, del que solo se conocen unos
cuantos habitantes, con los que nunca se ha podido hablar.
Ante todo había que aprender el idioma mosquil para
entender y estudiar a los moscos y muchas otras cosas
de su vida y sus costumbres que no lograba yo entender.
Por ejemplo: si existía en ellos una inteligencia, ¿cómo es
que se pasaban la noche tratando de beber mi sangre,
cuando yo mataba a tantos de ellos? Cuidadosamente llevé
una estadística de los moscos que lograban picarme y
de los que mataba yo y al cabo de un mes vi que de cada
cien moscos que tomaban mi sangre, tan solo once escapaban
con vida y muchos morían antes de poder picarme.
En un solo mes maté tres mil seiscientos cuarenta y
nueve moscos, de los cuales solo dos mil trescientos veinticuatro
me habían succionado sangre, lo cual indica que
mil trescientos veinticinco murieron antes de lograr su
fin. Calculo, pues no pude contarlos, que en este mes estuvieron
en mi choza más de quince mil moscos, de los
cuales solo doscientos cincuenta y siete lograron escapar
con vida después de picarme. Muy cierto es que había adquirido
una práctica extraordinaria en el arte de matar
moscos, y que cualquier otro hombre solo hubiera matado
una mínima parte de los que yo maté, pero de todas
maneras, para animales inteligentes como suponía yo a
los moscos, eran demasiadas las bajas padecidas para que
les conviniera picar a un hombre.

De esto deduje que si los moscos, como lo deberían
ser sin duda alguna, eran animales inteligentes, tenían
con toda seguridad alguna razón tan poderosa para tomar
mi sangre, que no les importaban las bajas causadas
con tal de lograr su objeto, así como un general que tiene
que tomar una posición, que es la clave de su victoria,
sacrifica para lograrla cuantos hombres sean necesarios.
Al principio me imaginé que los moscos estaban
esclavizados o sujetos en alguna forma a los gérmenes palúdicos
y que estos los obligaban a picar al hombre para
poder vivir ellos, sin importarles la muerte de su vehículo,
como el soldado moderno, que nada le importa perder
su carro blindado, o lo que lleve con tal de alcanzar
su objetivo. Pero esta teoría no me satisfizo. Sabía que
algunos moscos llevan el germen palúdico y otros no, pero
que todos atacan al hombre; así que no era de suponerse
que todos fuesen esclavos del microbio de la malaria.
Más bien, lo probable era que la malaria fuese un
arma de los moscos, un cuerpo especial en sus ejércitos,
así como la artillería en los ejércitos de los hombres.

Todos estos problemas me traían desvelado y estudioso,
seguro de nunca poder resolverlos hasta lograr entender
el idioma. Con este pensamiento me dediqué en cuerpo
y alma a catalogar los zumbidos que escuchaba en las
noches, anotando la ocasión en que habían sido emitidos
y lo que yo suponía que pudiera significar. Para anotar
los zumbidos desarrollé un sistema donde incluía las
intermitencias, lo agudo o lo grave del sonido, lo prolongado
de los intervalos y la nota musical en la que se emitían.
Pronto vi, en lo que se refiere a las notas, que los
moscos usan semitonos de la escala, así que en cada tono
hay doce sonidos más o menos agudos. Al principio
me fue difícil el percibirlos, pero poco a poco fui acostumbrando
a mi oído a ellos, para lo que valió mucho la
educación musical que recibí siendo pequeño.

Después de varios meses de estudios me di cuenta de
que cada frase va emitida en uno de los cuatro tonos que
tiene la voz humana y de que toda frase se emite en ese mismo
tono, aunque la siguiente o la anterior estén en otro.
Por ejemplo: un mosco zumbaba un momento, digamos
ocho o diez intermitencias, en voz de bajo, para luego tomar
el tono del soprano o del barítono, o, tal vez, cosa poco
frecuente, el de la voz del niño. Claro está que el tono
de los sonidos no es exactamente igual al de los hombres,
pero tiene una gran semejanza y se puede uno guiar por él.

Ya observada y comprobada esta igualdad de tono en
cada frase, no quise teorizar sobre ella, sino que esperé
hasta encontrar otros datos y me ocupé en anotar fielmente
los sonidos que escuchaba y la ocasión en que habían
sido emitidos. Observé que había un cierto sonido, un setenta y siete
en voz de bajo repetido dos veces, con una breve intermitencia.
Este sonido se repetía frecuentemente en las noches
y pude darme cuenta de que al producirlo un mosco,
acudían otros, de lo que deduje que se trataba de un llamado.
Luego observé que este mismo sonido, pero emitido
en voz de barítono, producía los efectos contrarios y
que los moscos que estaban cerca de quien lo emitía se ausentaban.
Era emitido generalmente cuando mataba yo a
alguno de ellos y los espantaba. Con esos datos ya me atreví
a formular la teoría de que en idioma mosquil el tono
del zumbido cambia el sentido del verbo y de que si «mi,
intermitencia, mi» en voz baja significa «Venid», en voz
de barítono significará «No vengas». En una ocasión dejé
a un mosco herido sobre la cama y noté que zumbaba
constantemente en voz aguda de niño. Todas sus frases
empezaban con «mi, intermitencia, mi», pero ningún otro
mosco le hacía caso, hasta que vino otro un poco más
grande y se paró junto a él. El mosco herido, aún en voz
de niño muy aguda, zumbó varias notas y el grande le contestó
en voz de bajo: «do largo, re, sol, mi, do, re largo»,
todo con intermitencias breves, exceptuando el re y el sol
y, acabando de zumbar, lo mató, dejando el cadáver sobre
mi camastro. Varias veces hice el mismo experimento
y observé que el herido emitía los mismos sonidos, que
aparecía el mosco grande, el cual, después de escuchar al
herido, a veces contestaba lo mismo que en el caso anterior
y remataba a quien lo había llamado, pero otras contestaba
una frase en voz de soprano, a la cual replicaba el
herido en voz de bajo. Entonces llegaban otros moscos y
se lo llevaban sin matarlo.

De estas y otras observaciones, deduje que el verbo
en idioma mosquil tiene siempre en voz de bajo un sentido
afirmativo, en voz de barítono, negativo, en voz de
soprano interrogativo y en voz muy aguda o de niño, suplicativo
o exclamativo. También había observado ya
que siempre el verbo es un compuesto de la nota mi, y
que cuando se trata del verbo en singular se usa el semitono
más grave y cuando es plural el más agudo: así,
«voy» es «mi intermitencia, mi bajo, semitono grave», y
«vamos» es «mi intermitencia, mi bajo, semitono agudo».
Para mi sistema de anotar los sonidos, numeré los
doce semitonos del uno al doce, las cuatro voces «G» bajo
o grave, «S» soprano, «B» barítono y «A» voz aguda
o de niño. Las intermitencias las señalaba con la «I» si
eran breves y con una «L» si eran largas, poniendo siempre
al principio de cada frase el tono de la voz. Así, la
palabra «Voy» quedaría expresada en esta forma: «G 5
I 5». Hay que tener en cuenta que las intermitencias, en
la mayor parte de los moscos, sobre todo cuando hablan
apresuradamente, no son silencios completos, sino solo
breves intervalos en que bajan la voz, sin dejar de zumbar,
por lo que al oído inexperto le parece que toda la
frase es un constante zumbido.

Cada vez más entusiasmado con mis estudios, dejé por
completo el aguardiente y todo tráfico de comercio y cacería.
Los lacandones me seguían manteniendo, y, al verme
ocupado con mis cuadernos de notas, no se atrevían
a interrumpirme más que cuando lo consideraban una
gran urgencia. Un día se me acercó Pajarito Amarillo, estuvo
un rato de lejos observando mis ocupaciones y por
fin se atrevió a hablar:

---Tecolote Sabio ---me dijo---, hemos sabido que los
hombres de tu raza se acercan a estas riberas en busca de
las maderas buenas que tiene la selva y nosotros, los lacandones,
no queremos tratos con ellos, así que hemos
decidido que tú nos aconsejes si debemos marcharnos
rumbo al sagrado Metasboc, donde no llega nunca el
blanco, o quedarnos a trabajar con ellos, que nos darán
mantas, cuchillos y aguardiente. Habla tú, Tecolote
Sabio, pues toda la tribu espera tus palabras y sabe que
tus consejos están inspirados por los espíritus buenos que
apresas en los cuadernos con los trazos oscuros.

Siempre, a mi parecer, Pajarito Amarillo fue un poco.
pomposo en su manera de hablar y me daba la impresión
de que había leído las novelas de Fenimore Cooper y de
que trataba de imitar a los indios que hablan en ellas.
Muy cierto es que cuando se trata de mandar y, sobre todo,
de regañar, olvida su elegancia, y revolviendo su idioma
con el castellano, logra frases por demás expresivas.
De todos modos, siempre me ha dado un poco de risa su
pomposidad cuando me habla de cosas serias. Esta vez,
guardando la risa, le contesté:

---Te agradezco, Pajarito Amarillo, la confianza que
tú y tu tribu han puesto en mí y en los espíritus que me
guían. Tú sabes que la tribu es mi familia, que los hombres
de mi raza no son nada para mí y yo nada para ellos,
así que te voy a aconsejar con palabras buenas, con las
palabras que se le dicen a un hermano. Emprendamos
sin tardanza la marcha rumbo al Metasboc sagrado, huyamos
de los hombres de mi raza que, si te dan aguardiente
bueno para tu tribu, en ese aguardiente han encerrado
los espíritus malos y pronto todos serán esclavos
de ellos y ya no reinará la paz entre tu gente.

Noté que mis palabras no le agradaron, ya que había
pensado que yo trataría de quedarme para ver a los hombres
de mi raza y él quería convencer a la tribu de que
eso era lo mejor. Le gustaba bastante el aguardiente y
bien sabía que los hombres blancos lo dan con facilidad
a cambio de trabajo. Viendo su indecisión y queriendo
salvarlo del contacto de los traficantes blancos, le dije:

---Las palabras que te he dicho me han nacido del corazón,
pero si no quieres escucharlas, no las escuches y
quédate con tu tribu, aunque desde ahora te digo que los
hombres de mi raza no te han de traer ningún bien y
sí muchos males. Yo, de todas maneras, me retiraré al
Metasboc hasta que lleguen las aguas y se hayan ido los
que vienen.

---Si te vas ---me contestó--- la tribu se va contigo.
No quieren dejarte porque tú nos libras de muchos males
y creen que eres un dios bueno, aunque tengas el aspecto
de los hombres blancos. Y tal vez eres un balam
más de los cuatro balames de los vientos y el quinto de
la cruz de los de tu raza. Así que si decides irte, mudaremos
el caribal a un lugar cercano al de tu casa.

Las palabras de Pajarito Amarillo me causaron una
extraña emoción. Yo, el hombre desechado por los de
mi raza, el que mejor haría en morirse, el borrachín asqueroso,
era para esos hombres limpios del Lacantún un
nuevo balam, un profeta, casi un dios. Y eso sería yo para
ellos, un espíritu bueno que los llenaría de beneficios.
Les enseñaría algún arte, los educaría en la vida sedentaria,
con ellos formaría un pueblo limpio, higiénico,
donde los niños pudieran vivir. Sería yo un nuevo
Kukulcán que renovara las razas mayas y les devolviera
su antiguo esplendor.

Al día siguiente desmantelamos las casas y emprendimos
el camino rumbo al Metasboc. Las mujeres cargaban
los utensilios y las jícaras de comida y los bultos de
maíz y yuca, los hombres llevaban las armas y abrían el
paso entre la selva, Pajarito Amarillo las cosas del culto a
los dioses, los incensarios y el copal, y yo mis cuadernos,
mi carabina y mi deseo de una vida nueva y grande.

</section>
<section epub:type="chapter" role="doc-chapter">

# IV

Cuatro días de marcha nos llevaron hasta el lugar que había
escogido Pajarito Amarillo en las márgenes cenagosas
del Metasboc para instalar su caribal y el de su tribu. Era
el sitio un pequeño cerro junto al pantano, donde hacía
muchos años la tribu había acampado ya en una ocasión
y aún se veían huellas de los claros que hicieron entonces
para sus siembras. Por lo demás, era un pedazo de selva
como cualquier otro. Los lacandones construyeron su caribal
en la punta del cerro y yo junto al lago, donde hubiera
moscos, pues ya lo único que me interesaba en la vida
era el estudio del idioma y costumbres de estos animales
y el bien de mis amigos los lacandones. Mucho insistieron
estos para que hiciera mi enramada junto a su caribal, pero
yo no quise y les dije que debía estar lejos y en un lugar
solitario, para tener libre contacto con los balames de los
vientos, pero que podían ir a mi casa cuando quisiesen.

Ya instalado, repasé mis notas y me volví a entregar
al estudio, temeroso de que el idioma de los moscos no
fuera el mismo aquí que en el Lacantún; pero desde la
primera noche observé con inmenso júbilo que era el
mismo y pude repetir mis experimentos, reconociendo
todos los zumbidos.

Durante seis meses me dediqué al estudio, apenas interrumpido
de vez en cuando por Pajarito Amarillo o
por algún otro miembro de la tribu, que venían a contarme
lo que habían sabido ---nunca pude enterarme cómo---,
acerca de los madereros que se aproximaban al
Lacantún y a nuestros antiguos hogares. Creo que ese
fue el tiempo más feliz de mi vida, el que he vivido con
mayor tranquilidad, teniendo tan solo en el alma deseos
de hacer el bien, de construir, de levantar. En ese tiempo
mi única ambición era la de hacer el bien a mis amigos
los lacandones. Soñaba yo con juntar tres o cuatro
tribus dispersas en un solo gran caribal, allí mismo, en
las márgenes fértiles del Metasboc, y enseñarles a sembrar
la tierra debidamente, a cuidar el ganado, que conseguiría
yo para ellos. Tenía la cabeza llena de proyectos
buenos, y si seguía estudiando el idioma de los
moscos era tan solo por un afán científico. Pensaba en
salir algún día de la selva, con un libro estupendo sobre
los moscos, publicarlo, y con el dinero que ganara traer
las cosas que necesitaran más mis amigos. Puedo asegurar
con toda verdad, con la misma verdad con la que he
contado mis delirios y mis maldades, que en ese tiempo
tenía el alma llena de bondad, que había llegado casi al
extremo de no odiar a los hombres de mi raza; tan solo
a temerles por el mal que les pudieran causar a mis amigos.
Vagamente recordaba mis lecturas sobre las misiones
de los jesuitas y de los franciscanos en el Uruguay y
en la California y soñaba con hacer algo parecido. Ya
tenía adelantado lo más difícil del camino, o sea, el ganarme
la confianza de los indios, y todo lo demás me parecía
fácil.

Para ayudar a mis amigos empecé a interesarme por
los niños, todos raquíticos y enfermos. En mi choza les
daba más comida y los divertía con cuentos que inventaba,
tratando de despertarles la dormida imaginación, pero
no hablándoles nunca del mundo de fuera de la selva,
para que no sintieran el deseo de irse con los hombres
blancos. Otras veces, en los días de calor sofocante, nos
bañábamos en el lago y les hacía barquitos de papel para
que jugaran. Esta diversión les encantó también a los
grandes y pronto todos me pedían barquitos para echarlos
al lago o a algún caño. Yo les contaba que en los barquitos
aquellos se iban todos los malos espíritus y creo
que ya consideran el pasatiempo como un rito.

Sí, ese fue el tiempo más feliz de mi vida. Ahora lo comprendo
y lloro por haber destrozado todo aquello, lloro
porque pudo más en mí la loca ambición del poder que la
bondad que empezaba a vivir en mi corazón, que ese amor
nuevo y maravilloso, sin egoísmos, que había puesto en mi
alma la bondad de los lacandones. Ahora, cerca ya de la
muerte, cuando escribo, aún lleno de odio e impulsado tan
solo por el temor de la aniquilación total, el libro que debía
de hacer tan solo con el interés de ayudar a mis amigos,
comprendo que ese tiempo de pobreza, de nulidad, ha
sido el único feliz de mi vida; pero ya es tarde, no puedo
volver atrás y los arrepentimientos son estériles.

Por lo que se refiere a mis estudios, llegué a entender
muchas de las frases usadas por los moscos y perfeccioné
el oído para distinguir los más sutiles cambios de tonos
y semitonos. Así mis noches eran divertidas, las pasaba
escuchando la charla de miles de moscos, oyendo
las órdenes que daban los que parecían ser los jefes y precaviéndome
de ellas, cuando se referían a mí o a mi sangre,
de la mejor manera posible.

Ocho meses empleé en hacer el diccionario del idioma
mosquil, que dejo en un cuaderno junto a este, para
que a los hombres que vengan les sea fácil interpretar el
idioma de los moscos y pactar entre ellos. Este diccionario,
que pensé destruir para que no cayera en manos de
ningún hombre, se lo entrego ahora al género humano
para demostrarle que le he perdonado todo el mal que
me hizo a condición de que nunca me olvide. Porque si
los hombres logran pactar con los moscos, cosa que será
fácil, se abrirá ante las nuevas generaciones todo un
mundo nuevo de cooperación con lo que llamamos animales
inferiores, un mundo exento de gran cantidad de
enfermedades y lleno de maravillas. Un mundo que yo
conozco y que yo les doy, que me deben a mí.

Cuando ya pude entender todo lo que decían los moscos,
se me ocurrió que tal vez pudiera imitar sus sonidos
y, por lo tanto, hablarles en su idioma y entenderme con
ellos. La cosa no era tan fácil como parece. El hecho de
hablar en música, en las cuatro escalas, haciendo distingos
de semitonos, presentaba para mí, con una cultura
musical muy mediocre, un gran problema. Muchos días
los pasé ensayando y ensayando las frases más simples, pero
mis sonidos en nada se parecían a los que emitían los
moscos y estaba seguro de no ser entendido. Traté entonces
de emitirlo con algún instrumento y busqué, en el
caribal de Mapache Nocturno que estaba a unas cuatro
leguas del nuestro, a Florentino Kimbol, que decían era
sabio en hacer flautas de barro y silbatos de carrizo.
Llegué al caribal a eso del mediodía y encontré a Florentino
en su hamaca, reposando. Al verme se levantó y
me preguntó:

---¿Está bien tu corazón?

---Utz ---le contesté.

Y nos sentamos el uno junto al otro en silencio. La
tribu de Mapache Nocturno me conocía bien, sabían todos
los miembros de ella que yo era amigo de los de su
raza y me estimaban. Al cabo de un rato me dijo:

---Los de tu raza van adelantando y pronto llegarán
al Lacantún. Buscan puna y aquí tenemos mucha. Mira
este tronco en el que estamos sentados.

---Es caoba ---le dije.

---Utz ---me contestó---. Y hay mucha en la selva.
Hay grandes árboles y otros que producen chicle.

Diciendo esto, sacó un gran puro de tabaco negro y me
lo ofreció. Yo lo acepté y lo encendí en una brasa del fogón
que nos trajo Petronila, su mujer, y seguí en silencio.

---¿Has andado por la selva? ---me preguntó.

---Sí ---le contesté---, he venido a verte porque mi corazón
desea decirte algunas palabras.

---Si tuviera aguardiente te ofrecería ---me dijo---.
Pero nos hemos alejado de los hombres de tu raza y ellos
traen el aguardiente.

---No quiero aguardiente ---le dije---, porque sé que
los hombres de mi raza esconden a los espíritus del mal
en él, para acabar con todos ustedes. Por eso convencí a
Pajarito Amarillo de que mudara su caribal hasta estas
regiones, para no tener tratos con los blancos.

---Mi padre, Mapache Nocturno, también quiso venirse
tras de Pajarito Amarillo porque te aprecia y su corazón
te necesita ---dijo Florentino con tristeza---, y
ahora no tengo aguardiente que ofrecerte.

---No te afanes por eso; yo ya no lo tomo, porque sé
que es malo ---le dije, y en el fondo de mi alma sentí algo
agradable. No tan solo Pajarito Amarillo me apreciaba;
también Mapache Nocturno había seguido con su tribu
mis pisadas. Pronto se podría hacer la unión de estas
dos tribus y empezarse la civilización de mis amigos.

Mientras yo pensaba en estas cosas, Florentino fumaba
en silencio, escupiendo de vez en cuando. Por fin habló:

---Tú eres sabio ---me dijo--- y nosotros te comparamos
con el tecolote que todo lo ve y nunca cierra los ojos.
Pero en tu corazón hay tristeza porque tienes odio a los
hombres de tu raza y nos alejas de ellos.

---Si pretendo alejarlos de ellos es porque los conozco,
porque sé el mal que acarrean. Pero no vine a hablarte
de esas cosas, Florentino: vine porque quiero usar
tus manos y tu ciencia para hacer una flauta.

Con su acostumbrada delicadeza, Florentino no preguntó
para qué la quería. Tal vez imaginó que trataba
yo, con ella, invocar a mis dioses. Tan solo escuchó atentamente
mis ideas y se puso a trabajar con un carrizo delgado.

Después de varios ensayos, logró una flauta que emitiera
todos los sonidos que buscaba, en un tono muy parecido
al zumbar de los moscos, y con ella, ya entrada la
noche, regresé al caribal de Pajarito Amarillo y a mi choza.

Esa misma noche ensayé con mi flauta y zumbé la palabra:

---Ven.

Un mosco que revoloteaba sobre mi cabeza se detuvo
un momento y salió huyendo, gritándoles a sus compañeros
que había escuchado una voz que lo llamaba.
Ese experimento me llenó de entusiasmo, pues con él ya
estaba seguro de que los moscos habrían de entender lo
que les dijera o zumbara, con lo que seguí practicando
con más empeño. Entre más me adentraba por los diferentes
aspectos del idioma, más difícil se me hacía el dar
todos los tonos y semitonos requeridos, con la debida
exactitud.

En otro cuaderno que dejo junto a este y junto al que
contiene el diccionario, he anotado todo lo indispensable
para el buen uso del idioma mosquil, o sea, todas las
reglas gramaticales más importantes. Hay que hacer notar
que en este idioma nunca hay excepciones a las reglas,
lo cual hace el idioma más civilizado del que he oído
hablar.

Después de muchos ensayos, como la constancia y la
práctica todo lo vencen, me creí con los conocimientos
y la habilidad suficientes para entablar una conversación
con los moscos, y la inicié, una noche, con uno que revoloteaba
cantando sobre mi cabeza una tonadilla que
parecía estar muy de moda entre ellos:

---Ven ---le dije en tono grave de mando; y luego en
tono agudo de súplica---: No te vayas, quiero hablarte.

El mosco se llenó de asombro y soltó tres o cuatro interjecciones
en tono agudo, buscando a su alrededor para
ver si había otro mosco que le hablara.

---Escúchame ---le volví a decir en tono grave de
mando---. Soy yo quien te habla, el hombre a quien atormentas.

Entonces el mosco se detuvo un momento sobre una
de las cuerdas de la hamaca.

---Has aprendido, por lo que veo, nuestro idioma
---me dijo---; y debo obedecerte y acatarte, como lo manda
el Gran Consejo que nos rige y cuyo nombre no puedo
pronunciar. Ordena lo que quieras y yo te obedezco.

Algo confuso me quedé, no sabiendo qué ordenar ni cómo
iniciar la charla. Además, para ser franco, la emoción
me impedía casi el emitir un solo zumbido con mi flauta.

---Si nada quieres de mí, ¿para qué me has llamado?
---me preguntó el mosco, turbado también ante mi indecisión.

---Nada tengo que ordenarte ---repuse---. Tan solo te
he llamado para conversar contigo.

El mosco pareció dudar un momento y por fin zumbó:

---Perdona mi duda, pero no creo ser yo quien deba
hablar contigo, porque soy de clase baja: no soy más que
explorador. En este caso no sé qué hacer y debo avisar
inmediatamente a mi superior, el cual avisará a su superior,
hasta que llegue la voz al Gran Consejo de aquí; pero
me da miedo hacerlo, ya que nunca se había oído decir
que otro ser de la creación hablara y temo que todo
esto sea tan solo un sueño mío.

---No es sueño. Durante muchos años he estudiado el
idioma de ustedes y ahora…

---Tú fuiste entonces quien le habló a un compañero
mío hace tiempo, que dijo haber escuchado voces y fue
sentenciado a morir por ello.

---Sí, yo fui el que le habló: le dije «Ven» y él se alejó
lleno de temor gritando. Siento mucho su muerte…

---La muerte no tiene importancia ---me contestó---.
Lo que importa es cumplir con la misión, llevar adelante
el proyecto y creo que el hecho de que tú hayas aprendido
nuestro idioma y por primera vez podamos entendernos
con otro ser de la creación, es algo de gran
importancia; así que llamaré a mi superior, a quien te
ruego le hables, para que no me cueste la vida.

---Llámalo ---le dije--- y no temas.

Así lo hizo, zumbando fuertemente, y al poco tiempo
se presentó el superior, un anófeles perfecto que pareció
muy indignado cuando supo por qué lo habían llamado.
Entonces le hablé yo:

---No castigues a tu inferior ---le dije---. Él no ha hecho
más que decirte la verdad y yo le he rogado que te
llame para que decidas lo que se debe hacer. He aprendido
tu idioma, durante años lo he estudiado y quiero hablar
con ustedes y ser su amigo en lugar de su enemigo.

---Nunca has sido nuestro enemigo ---me contestó el
superior---. Nosotros los moscos, los dueños de todo, no
tenemos enemigos. Tú has servido como fuente de sangre
para alimentar al Gran Consejo, que no puedo nombrar
porque su nombre es demasiado alto para que lo
pronuncie yo…

---Pero es que he matado a muchos de ustedes.

---Nada importa la muerte de unos cuantos cuerpos.
Tu sangre nos era necesaria y la hemos tomado y, aunque
hubieras matado cien veces más, la hubiéramos tomado.

---Me alegra oír eso ---le contesté con mucha finura
aunque sin mucha verdad---. Yo quiero ser amigo vuestro…

---Nosotros no tenemos amigos ni enemigos, pero ya
que has aprendido nuestro idioma y podemos hablar
contigo, tal vez te podamos utilizar en algo. ---Luego,
volviéndose hacia el que primero habló conmigo, le dijo---:
Has hecho muy bien en llamarme. Te recomendaré
para que se te nombre guardián del Gran Tesoro.

---Me alegro de oír eso ---intervine---. No me hubiera
parecido bien que este pobre sufriera un castigo injusto,
como el otro.

---Tú le das demasiada importancia a la muerte, que
para nosotros no es nada. Pero es bueno que siga hablando
contigo. Convocaré a todos mis superiores para que
ellos decidan cómo se debe llevar este asunto ante el
Gran Consejo y lo que se debe hacer.

Y diciendo esto, zumbó con gran fuerza y aparecieron
esos moscos grandes que yo ya conocía. Aparecieron
varios centenares de ellos, que, después de escuchar las
palabras del superior, llenaron el espacio con tal zumbadero,
que creí oportuno hablarles para calmarlos y, tomando
mi flauta, les dije:

---Señores míos, este capitán ha dicho la verdad. Yo
he aprendido el idioma para poder charlar con ustedes.
No creo que esto sea causa para tanto alboroto.

---No sabes lo que dices ---me contestó uno de los
moscos grandes---. Esto es lo más importante que ha sucedido
en nuestra historia, que abarca cientos de miles
de años. Tu venida puede ser providencial, pero yo no
debo hablar de estas cosas, sino avisar inmediatamente
al Gran Consejo de aquí, para que él, a su vez, avise a
todos los Grandes Consejos del mundo y se reúna el
Consejo Superior, que no se ha reunido hace quinientos
ochenta y seis mil años, siete meses y catorce días. Eso
es lo que debo hacer…

Y diciendo esto, salió seguido de todos los moscos,
dejando mi choza vacía, tan vacía que el murmullo de la
selva penetraba entre las hojas de palma, como queriendo
llegar hasta mí. Mientras, yo esperaba.

</section>
<section epub:type="chapter" role="doc-chapter">

# V

Al alba llegaron los niños lacandones queriendo irse a bañar
conmigo y echar barquitos de papel a la corriente del
río. Fui con ellos y las aguas frescas y turbias me despejaron
la cabeza y pude meditar serenamente en lo que había
acontecido la noche anterior. Era indudable que había
logrado algo que ningún hombre había podido
hacer, algo tan extraordinario que me haría famoso en todas
las generaciones futuras, y mi nombre jamás sería olvidado.
Ahora ya podría regresar al mundo y escupir en
la cara de todos mi desprecio. Yo, el borrachín que mejor
estaría muerto, había logrado más que todos los hombres
de ciencia, que todos los poderosos, que todos los sabios,
los biólogos, los estudiantes de las razas inferiores. Tal vez
fundara un instituto, que llevara naturalmente mi nombre,
para estudiar los idiomas de todos los animales.

Pero aún me parecía un sueño todo lo de la noche anterior.
Había que estar seguro, hablar con los moscos a la luz
clara del día, aprender sus costumbres, su vida, su historia,
tratar de celebrar con ellos un pacto de alianza, para que no
molestaran más al hombre y llevar este pacto como un don
inapreciable para el género humano. ¿Pero me agradecerían
los hombres lo que hacía por ellos? A los grandes hombres
siempre los habían despreciado; lo habían hecho mil
veces, lo seguirían haciendo, pero no conmigo. Mi descubrimiento
me daría tal poder que todos tendrían que respetarme
y yo sabría imponerme y hacer que me trataran como
era debido. Tal vez llevara algunos escuadrones de
moscos a mis órdenes para que me sirvieran de escolta y
probaran lo que yo había logrado. Se iniciaría el estudio de
una nueva ciencia, el idioma de los animales, a la cual le
pondría un nombre griego que en ese momento no pude
imaginar cuál debería ser, algo así como zoofonología.

De pronto me di cuenta de que por pensar en esas cosas
tan grandes había olvidado a los niños que me llamaban
a gritos para que les hiciera más barquitos. Levantándome
de la arena, donde había estado recostado, me
acerqué al agua y eché los barquitos, entre los gritos de júbilo
de la chiquillada. Entonces decidí no abandonar nunca
a mis amigos lacandones, hacerlos gente civilizada, pero
conservando su candor, su bondad, su sencillez, y, para
lograrlo, emplearía el dinero que me produjera mi descubrimiento.

Mientras los niños perseguían a los barquitos, yo me
recosté de nuevo sobre la arena, buscando la sombra de
un caulote que allí había. De pronto me llamó la atención
el zumbido de un mosco y quise, con reacción instintiva,
buscarlo para aplastarlo, pero, sin saber cómo,
empecé a entender lo que me decía:

---Unos miembros del Gran Consejo, cuyo nombre no
podemos decir, me envían a saludarte y a decirte que quisieran
hablar contigo a través de uno de sus embajadores.

---¿Y por qué no me hablan directamente? ---pregunté
con mi flauta:

---No sabes lo que has dicho. El Gran Consejo, cuyo
nombre no nos es permitido pronunciar y que pocos sabemos,
nunca habla más que con el que es cabeza de los
transmisores o embajadores. Él, a su vez, comisiona a alguno
de sus subordinados para que este sea quien escoja
al que ha de llevar la palabra del Gran Consejo, y esta
palabra la aprenden unos subordinados, que se llaman
recordadores, los cuales la repiten siempre para que nunca
se olvide.

---¿Pero en un caso tan importante como este? ---pregunté
por conocer más a fondo la organización mosquil.

---Este es el caso más importante que ha habido en
nuestra historia, desde hace más de quinientos mil años,
pero aún así el Gran Consejo no hablará contigo. Ya los
recordadores han repetido las palabras que el Consejo
creyó necesarias para este caso y todos hemos escuchado
esas palabras y por ellas sabemos que han de suceder
grandes cosas. Creo que el embajador o transmisor quiere
darte a conocer sus palabras.

---Pues que venga en buena hora ---dije.

El mosco emprendió el vuelo, perdiéndose entre los
árboles, y a los pocos segundos llegaron varios moscos
grandes, zumbando fuertemente y tras de ellos un anófeles
macho. Este se detuvo en una varita que estaba
junto a mi cabeza y yo preparé mi flauta.

---El Gran Consejo ---dijo---, cuyo nombre no he de
pronunciar, me envía a ti para desearte prosperidad
entre tus semejantes y gloria para que en tu muerte no
seas olvidado y tu nombre se repita en las generaciones
futuras hasta el fin de los fines.

---Gracias ---le contesté---. Agradécele de mi parte al
Gran Consejo sus buenos deseos. Yo no sé qué pueda
desearle ni cuál fórmula sea la debida para saludarlo, pero
sí quiero, antes que me digas lo que te han encomendado,
hablar unas cuantas palabras. Creo que esta es la
primera vez que un hombre habla con ustedes y que logran
entenderse mutuamente la raza mosquil y la humana.
De este entendimiento quiero que surja la paz entre
ambas razas, que dejéis vosotros de perseguir al hombre
y que el hombre deje de matar moscos con los muchos
medios que para ello ha inventado.

No bien hube acabado de hablar, varios de los moscos
grandes que habían precedido al transmisor empezaron
a zumbar, repitiendo lo por mí dicho, y, cuando terminaron,
se alejaron entre las sombras de la selva. El
transmisor tomó entonces la palabra:

---El Gran Consejo, cuyo nombre no puedo proferir,
me ha dicho además que nombra desde ahora recordadores
especiales para que ninguna de tus palabras se vaya
a perder. Esta es la primera vez que hace tal honor y
debes sentirte orgulloso. Ahora los recordadores que
vienen conmigo te dirán las frases que el Gran Consejo
ha escogido de entre todas las que existen. Diciendo esto
calló y uno de los recordadores dijo:

---¡Oíd la palabra del Gran Consejo, la palabra que me
ha sido encomendada!: siendo pocos los animales racionales
que hay en el universo, el reino de los moscos necesita
la alianza con alguno de ellos para lograr sus fines. Esto ha
dicho el Gran Consejo hace cuatrocientos treinta y dos mil,
seiscientos cincuenta y nueve años, tres meses y dos días.

Apenas terminó, dijo otro:

---¡Oíd la palabra del Gran Consejo, la palabra que
me ha sido encomendada! De entre todas las razas existentes
en el mundo, creemos que la humana es la más razonable,
y debemos buscar alianza con ella cuando se haya
organizado lo bastante. Esto ha dicho el Gran Consejo,
hace cuatro mil setecientos veintidós años, cinco meses
y diecinueve días.

Otro dijo, apenas terminó este:

---¡Oíd la palabra del Gran Consejo, la palabra que
me ha sido encomendada! El hombre ha progresado y
aprendido mucho. Hay que vigilarlo, ya que puede sernos
útil. Esto ha dicho el Gran Consejo, hace trescientos
veinticuatro años, nueve meses y un día.

Como ningún otro llevaba trazas de decir nada, tomé
mi flauta y dije:

---Con inmenso gusto veo que el Gran Consejo desea,
desde hace tan innumerables años, una alianza con los
hombres. Yo creo que puedo ser el intermediario para
esa alianza y, si me dan poderes bastantes, puedo lograr
un entendimiento completo.

---Ya el Gran Consejo ---me contestó el transmisor---
lo ha pensado todo y sabe todo lo que es necesario saber
en esta ocasión. El Gran Consejo no hace más que pensar
y no hay situación posible para la que no tenga ya resuelto
algo. Pero antes de tratar nada contigo y los de tu
raza, quiere que estudies nuestra vida y nuestro gobierno,
que te empapes en su perfección para que así puedas
llevar nuestro mensaje a los hombres.

---Me parece muy bien ---le contesté---. Nada me
puede dar más gusto que el enterarme de vuestra organización
social.

---Llevarán los recordadores tus palabras al Gran
Consejo y yo sé que les van a agradar. Aquí tienes a estos
compañeros, nombrados especialmente para decirte
todo lo que tienes que saber. Que tu nombre se conserve
en las palabras de las generaciones venideras.

Y diciendo esto, desapareció con su turba de recordadores
que zumbaban de lo lindo, recordando mis palabras.
Quedaron tan solo veinte moscos grandes; y uno
de ellos, el que parecía ser el jefe, me dijo:

---Mi nombre es fácil de recordarse, me llamo Sol
Bueno; y el Consejo me ha nombrado para que te instruya,
junto con estos mis compañeros de quienes soy el jefe.
Todos pertenecemos a la rama de los lógicos y nuestro
oficio consiste en saberlo todo, para poder ligar así
los conocimientos dispersos que traen los exploradores
y los que conservan los recordadores, sacando de todo
ello las conclusiones que debe saber el Consejo.

---Por lo que veo ---le dije---, hay entre ustedes muchas
diferentes categorías, cada una con su ocupación fija.

---Así es ---me contestó---. Somos muchas ramas diversas,
unidas en un solo cuerpo, que es el Gran Centro que
domina el Consejo. Cada quien tiene su ocupación, hereda
los conocimientos, los amplía, vive y muere en ella.
Empezando por lo más bajo, hay la rama de los exploradores,
cuya misión es buscar el sitio donde haya alimento para
el Consejo y llevar allá a las proveedoras, que son hembras
dispensadas de la cría de larvas y que forman parte de
la rama más baja también. Exploradores y proveedoras
pueden llegar a ocupar un sitio en la guardia del tesoro, del
que hablaremos más tarde. Sobre ellos está la rama de los
capitanes que se encargan de revisar el trabajo hecho por
los subalternos, de recoger o rematar a los heridos, según
tengan o no sangre, y de buscar lugares seguros para el tesoro.
Hay también otra rama que se encarga de la contabilidad.
Estos tienen que saber cuántos miembros tiene cada
cuerpo, cuántos han fallecido y cuántos se necesitan.
Por ejemplo, cuando tú matabas exploradores y proveedoras,
los contadores lo sabían inmediatamente, por noticias
de los capitanes, y así podían reemplazar a esos sujetos, de
manera que los cuadros siempre estuvieran cabales y pudieran
dominar y vigilar toda la selva, encomendada a
nuestro cuerpo.

---¿Entonces hay otros cuerpos que no sean ustedes?
---pregunté.

---Sí, hay en el mundo unos trescientos mil cuerpos diferentes,
obedeciendo todos a un consejo superior al Gran
Consejo del que ya has oído hablar. Este Consejo Superior
se forma por un miembro de cada Gran Consejo, los cuales
a su vez eligen ciento diez miembros, y ellos a once,
que son los que dictaminan y su fallo es inapelable. Pero
volviendo a las ramas bajas, debo advertirte que hay otras
que se encargan Únicamente de recoger la sangre que
portan las proveedoras y llevarla ante el Gran Consejo,
que se alimenta de ella exclusivamente. Para lograrlo, la
rama de los alimentadores toma a las proveedoras, las
mata, les succiona la sangre y la pasa a los Miembros
Innombrables…

Me parece una crueldad matar así a las proveedoras
---interrumpí.

---No veo por qué. El nombre de la que matan se le da
a otra y así no mueren. Además, su cadáver queda en el
gran tesoro para cuando se necesite. Hay además otra rama,
que es el ejército de ataque. Cuando algún ser nos molesta,
lo atacamos con ese ejército, que tiene diferentes armas.
Unos instilan un veneno que causa hinchazón y
comezón, otros transmiten la malaria, el vómito negro o la
oncocercosis, según el caso. Otros más llevan enfermedades
que aún no conoce el hombre. Mediante estos cuerpos
hemos logrado tener inmensas áreas libres para nosotros,
dónde cuidar el tesoro y sostener al Gran Consejo.

---El hombre ---intervine yo--- en muchos lugares ha
logrado rechazar esos ejércitos y hacerlos inofensivos.

---Cierto es que hemos sufrido derrotas, pero la gran
batalla aún no se libra y en ella saldremos victoriosos.
Pero de eso ya hablaremos más tarde. Ahora medita en
lo que te he dicho y guárdalo en tu memoria.

Y diciendo esto desapareció. Los niños lacandones regresaban
por el playón, tristes porque sus barquitos de
papel habían naufragado. Hicimos otros que corrieron
igual suerte y me fui a mi choza a descansar.

</section>
<section epub:type="chapter" role="doc-chapter">

# VI

En la misma forma, a través de los transmisores y recordadores,
fui conociendo poco a poco el complicado mundo
de los moscos. Aparte de la inmensa organización en
ramas diferentes hay dos dependencias que se llaman «El
Gran Tesoro» y «El Arsenal». El Gran Tesoro está escondido
en unas cuevas y, según entendí, todos los cuerpos
de moscos diseminados por el Universo tienen su tesoro
en la misma forma. Consta este de un incalculable número
de larvas que se guardan allí en condiciones favorables
para poderse convertir en moscos en el momento en que
se necesitan. En el tesoro del cuerpo de moscos del
Metasboc, con el que yo estaba en contacto, había más de
cien billones de larvas listas para convertirse en moscos,
de la especie que se necesitasen. Aparte de eso, las ponedoras
estaban fabricando un millón diario, más o menos.
El cuidado de ese tesoro estaba a cargo de una rama especial
y tenía su cuerpo de contadores que repetían constantemente
el número de larvas en existencia. Nunca me
fue permitido ver ese tesoro ni se me dijo dónde estaban
las cuevas, pero entiendo que se encuentran en las márgenes
del Metasboc, junto a unas grandes rocas, cerca del
lugar en que habita el Gran Consejo.

El Arsenal es otro sitio, generalmente en alguna laguna,
donde los moscos que forman la rama del ejército
crían y guardan los microbios que les sirven de armas.
Allí llegan a cargarse y van a repartirlos a donde se les
indica. Hay una rama especial que se ocupa en criar y
alimentar esos gérmenes y una parte de la sangre que
chupan las proveedoras se destina a ese fin. Según entendí,
en el arsenal del Metasboc hay bastantes microbios
de malaria y vómito negro para acabar con más de
doscientos millones de hombres, y pueden producir bastantes
microbios como para matar un millón de hombres diariamente.
Otros microbios, como la oncocercosis, los
crían en los árboles especiales que tienen para ello, principalmente
en los cafetos, pero los usan poco en comparación
con el de la malaria, que parece ser el predilecto.

Para la cría de este microbio tienen arsenales vivientes,
como los llaman ellos. Son estos nada menos que la
sangre de algún hombre o animal vivo. Un mosco inyecta
allí los gérmenes y deja que se reproduzcan, marcando
al hombre o al animal con signos que tan solo ellos
conocen. Si el hombre se va a otras partes, los moscos
de ese lugar lo reconocen como un arsenal vivo de malaria
y lo usan debidamente.

Con sus terribles armas, los moscos no pretenden matar
al hombre y por eso usan poco el mortal vómito negro
y prefieren con mucho la malaria, que tan solo debilita.
El hombre así debilitado, con la sangre llena de
gérmenes, es un arsenal ambulante y su sangre un alimento
estupendo para el Gran Consejo.

Algunas veces los hombres han logrado acabar con el
arsenal o con el tesoro de algún cuerpo de moscos, como
les pasó a los tres cuerpos de Panamá, a los que siempre
pone el Gran Consejo del Metasboc como ejemplo.
Estos cuerpos han tenido que pedir ayuda a otros para
rehacer su tesoro y su arsenal, que lentamente van reedificando
en lugares más apartados.

Por cuanto a la estructura interior de los cuerpos se refiere,
pude averiguar que cada uno de ellos es como un ser
completo y los moscos que lo integran son algo así como las
células del cuerpo humano, de las cuales cada una tiene su
oficio. El Gran Consejo es el cerebro de ese cuerpo, los
transmisores son los nervios, los recordadores la memoria.
El Gran Tesoro es el sistema de reproducción, las proveedoras
son las células que recogen el alimento en el intestino
del hombre y el ejército es lo que en medicina se llama,
según creo, antitoxinas, ya que su misión especial es defender
al cuerpo, especialmente al Gran Consejo. Por esto, la
muerte tiene para los moscos la importancia que puede tener
para un hombre la muerte de una de las células de su
cuerpo. En verdad cada unidad de moscos es un ser, como
el humano, pero con la gran ventaja de que cada célula tiene
su vida propia y sin que estén circunscritas al espacio
que ocupa un solo cuerpo, sino que se pueden diseminar
por donde quieran. Con esto ganan mucho, pues para ir a
buscar comida no tienen que arrastrar todo el cuerpo, sino
que solo van las células necesarias, lo mismo que para
cualquier otra actividad. Así han logrado, con un solo cuerpo,
ocuparse a un tiempo de todas las cosas necesarias.

Unidades como esta en el Universo hay unas trescientas
mil, y cada una de ellas abarca un radio de mayor
o menor magnitud, según su fuerza. Algunas hay,
según me dijo Sol Bueno, que abarcan centenares de kilómetros
cuadrados de selva, y otras, las que están en los
países fríos y solo viven en el verano y no tienen arsenales,
abarcan lugares mucho más pequeños algunos tan
solo la superficie de una charca.

Entre todos estos cuerpos o unidades hay una liga y
se gobiernan por un consejo superior, que ya dejé explicado.
Este consejo no se había reunido en los últimos
quinientos mil años, desde que se logró la paz entre los
cuerpos de moscos del mundo, pero para entender esto
es bueno hacer un poco de historia.

Hubo un tiempo en que cada cuerpo era una unidad y
las células estaban unidas entre sí, como las del cuerpo humano.
Pero estos monstruos vivían en continua guerra y
toda su inventiva se reducía a buscar armas ofensivas y defensivas.
Uno de aquellos individuos descubrió la forma de
usar como arma los gérmenes dañinos, causando con esto
tal mortandad entre sus semejantes, que los dominó a todos
y los gobernó con gran tiranía durante miles de años.
Por fin uno de los oprimidos logró eliminar la mayor parte
de su cuerpo, dispersando sus células y dándoles una vida
propia, logrando así que tan solo enfermara una sola célula,
a la cual mataba inmediatamente. Siguiendo adelante
con estos descubrimientos, lograron constituir sus cuerpos
como los tienen ahora, cuerpos inmortales, pero que no
pueden reproducirse en nuevos cuerpos, así que su número
ha quedado limitado al ya dicho. Habiendo diseminado
sus cuerpos, pronto derrocaron al tirano, pero siguieron
guerreando entre sí, hasta hace unos quinientos mil años,
cuando comprendieron que era absurdo el seguir peleando
en esa forma y que lo mejor era repartirse el mundo.
Entonces se juntó el Gran Consejo para hacer esa repartición
y se redactó un estatuto, en el que se decía que cada
cuerpo de moscos, cualquiera que fuera su tamaño, tenía
derecho a un lugar en el mundo y que los lugares se sortearían.
Inmediatamente los cuerpos más grandes se opusieron
y alegaron que a ellos les correspondía el escoger antes
que nadie sus lugares; y como apoyaron su proposición
en la fuerza, el Consejo la aprobó. Así, los cuerpos más
grandes tomaron los lugares que les convenían, especialmente
los lugares calientes, donde encontraban más alimento
y no padecían frío. Los cuerpos chicos se fueron
acomodando donde pudieron y algunos de ellos se remontaron
muy al norte, hasta Alaska, en busca de lagunas donde
vivir en paz.

Desde entonces nunca se había vuelto a juntar el
Consejo Superior y cada cuerpo vive en el lugar señalado,
con un régimen interno libre. Pero todos los Grandes
Consejos se comunican entre sí, por medio de emisarios
que recorren el mundo, supongo que dejándose llevar de
un continente al otro por las corrientes de aire, de las que
son grandes conocedores.

Ahora la mayor parte de los cuerpos ya no caben en
sus zonas y se han visto obligados, por falta de espacio y
de alimentos para tantos, a reducir el número de larvas
que convierten en moscos, guardando así una gran cantidad
de sus tesoros. A esta escasez de espacio y de alimentos
ha cooperado en parte el hombre, talando grandes extensiones
de selva, acabando casi la fauna salvaje y
saneando muchos lugares. Cuerpos hay que han perdido
casi todo su territorio y viven miserablemente, con un tesoro
monumental y un cuerpo raquítico.

Cuando un cuerpo decide ampliarse, sus contadores
ven el número de individuos que se necesitan de cada especie,
van al tesoro y sacan las larvas necesarias y las
crían. Cuando pretenden, por lo contrario, reducirse,
dejan que se mueran las células que sobran y todas las
larvas que ponen las hembras se destinan al tesoro. Cada
año los cuerpos aumentan y disminuyen, según las estaciones
en los climas fríos y según la alimentación en los
calientes. En el del Metasboc, que es por cierto uno de
los más importantes que hay en el mundo, tres semanas
antes que empiece el tiempo de la fruta, los contadores
van al tesoro y sacan larvas de varios millones de hembras.
Cuando nacen, estas pueden alimentarse bien y ponen
gran cantidad de huevos, que pasan al tesoro.
Cuando mueren estas ponedoras, no son reemplazadas.

Hay plazas que siguen existiendo siempre, como son
los tres mil miembros del Gran Consejo, los cuales se
ocupan tan solo de pensar. Como llevan quinientos mil
años pensando, ya han imaginado una respuesta para
cualquier situación que se pueda presentar, por más extraña
que sea, y todas esas respuestas las guardan en la
memoria los recordadores, que tampoco dejan de existir
nunca. Como el Gran Consejo ya ha pensado todas las
respuestas a todas las situaciones y no se les ocurre situación
nueva, creo que ahora se dedican a la pereza en
sus cuevas y no hacen casi nada, más que autorizar las
cuentas de los contadores.

Todo eso me lo enseñaron Sol Bueno y sus recordadores,
que me repetían las palabras que el Consejo les había
encomendado. Con Sol Bueno primero y los Soles
Buenos que iban tomando su lugar conforme morían,
pues un mosco vive poco y los que lo reemplazan toman
su nombre y todo lo que él era, hice muy buena amistad;
y, mientras él me enseñaba la vida y costumbres de los
moscos, yo le enseñaba las de los hombres y lo admiraba
con los inventos que han hecho para combatir contra
toda clase de insectos. Supe que mi amigo era un miembro
importante del cuerpo, ya que era la cabeza o jefe de
los lógicos.

Mientras yo andaba ocupado en estos estudios, mis
amigos los lacandones nunca se atrevieron a interrumpirme,
creyendo, al verme hablar solo y tocar mi flauta constantemente,
que estaba yo en comunicación con los espíritus
buenos. Algunas veces los niños venían a bañarse
conmigo, pero ya no había entre ellos y yo la antigua camaradería
y apenas si se atrevían a pedirme que les hiciera
barquitos de papel, por lo que les enseñé a hacerlos y les
daba hojas de mis cuadernos. A diario, en las mañanas,
Hormiga Negra me llevaba mi pozol de masa de maíz en
agua, limpiaba mi choza y se iba sin decirme una sola palabra,
pero yo sentía en sus ojos la pregunta constante:

---¿Por qué ya no mataba moscos para hacer medicina?

Un día le dije:

---Hormiga Negra, hermana mía, veo que durante muchos
días has venido a mi casa y no me preguntas lo que
quisieras saber.

---Es cierto ---me respondió---. Tú lo sabes todo, Tecolote
Sabio. Contesta, pues, a mi pregunta.

---Tú quieres saber por qué yo ya no mato moscos en
las noches, como lo hacía antes. Te lo voy a decir. Los
moscos ya no son mis enemigos, ahora son mis amigos y
quiero que sean los amigos de ustedes, como lo son míos.
Yo hablaré con ellos y así podrán ustedes acostarse en
cualquier lugar sin que los moscos molesten; y se irán las
calenturas y los fríos, y no tendrán que llenar sus chozas
de humo, ni comprarles a los blancos los velos que libran
de los moscos.

Hormiga Negra se me quedó viendo algo extrañada y
se alejó sin decir una sola palabra. Inmediatamente saqué
mi flauta y me puse al habla con Sol Bueno.

---Nunca le he pedido al Gran Consejo un favor ---le
dije---. Ahora quiero que me concedan uno.

---No es costumbre del Gran Consejo, cuyo nombre
no puedo pronunciar, el conceder favores ---me contestó---.
Nunca hemos oído de nadie que los pida, pero si
ya tienen pensada una respuesta para el favor que vas a
pedir, se te concederá.

---Lo que quiero pedir es fácil y creo que el Consejo,
si no ha pensado respuesta para este caso, bien puede
pensarla.

---No sabes lo que dices ---me interrumpió Sol
Bueno---. El Gran Consejo no piensa ya respuestas nuevas,
más que en los casos de mucha urgencia.

---¿Entonces para qué sirve? ---pregunté.

---Es el Gran Consejo ---me dijo---. Pregunta lo que
quieras y oirás la respuesta.

---Lo que quiero es fácil. Tan solo pretendo que dejen
ustedes de atormentar a este pequeño grupo de hombres
que vive junto a mí y que son mis amigos. Quiero
que no los piquen, que no los enfermen y que no los molesten
con sus zumbidos.

---Llevaré tu pregunta ante el Consejo, pero ten en
cuenta que tal vez les concedan el librarlos de la servidumbre
del tributo de sangre que nos deben, ya que los
toleramos en nuestro territorio, pero que no les darán la
protección que te han acordado a ti.

Hasta entonces supe que el Gran Consejo se había ocupado
de poner varios escuadrones de moscos que me cuidaran,
alejando a todos los animales que me pudieran dañar.

Al poco tiempo regresó Sol Bueno con un recordador
que zumbó al punto:

---¡Oíd la palabra del Gran Consejo, la palabra que
me ha sido encomendada! Cuando algún otro ser de la
creación aprenda nuestro idioma y se pueda comunicar
con nosotros, lo protegeremos hasta ver si nos es útil y
protegeremos a aquellos a quienes nos indique, siempre
que sean en número limitado. Esto ha dicho el Gran
Consejo hace cuatrocientos veintidós años, nueve meses
y dos días.

---Has oído ---me dijo Sol Bueno---. Tus amigos ya
no pagarán el tributo de sangre que nos deben y no serán
molestados.

Como pude le di las gracias, aunque algo molesto por
eso del tributo de sangre y que me habían de proteger tan
solo mientras les fuera útil. ¿Útil para qué? ---pensaba
yo---. ¿Para hacer una alianza con los hombres?

Esa noche no hubo un solo mosco que molestara a mis
amigos los lacandones en su caribal. Al día siguiente
Hormiga Negra ya no se atrevió a entrar a mi choza y me
dejó la comida en la puerta, haciéndome profundas reverencias.
Poco después llegó Pajarito Amarillo y puso sobre
la mesa los jarros que eran sus dioses, con la banda roja de
la cabeza que era su signo de gran sacerdote. Después de
murmurar algunos rezos, me sahumó con copal y puesto
de rodillas me dijo:

---Sabio Tecolote, que todo lo ves y todo lo sabes, por
Hormiga Negra hemos sabido de tu poder, de tu grandeza,
de tu bondad, cómo has logrado dominar a los espíritus
malos que nos atormentaban por las noches. Por eso
yo creo que tu eres Kukulcán, el blanco, el barbado, que
ha vuelto entre su pueblo y te traigo a nuestros dioses para
que te acompañen. Solo me queda el rogarte que te quedes
para siempre en mi caribal y me enseñes el arte de hacer
felices a los míos.

Asombrado me quedé con tal discurso, que me deificaba
y, antes que pudiera yo contestar, ya Pajarito
Amarillo había salido de mi choza, sin darme nunca la
espalda, dejando allí a sus dioses y el copal humeante.

</section>
<section epub:type="chapter" role="doc-chapter">

# VII

Dos meses, más o menos, pasé en charlas con mi amigo
Sol Bueno, en los cuales aprendí la organización política
del reino de los moscos y me perfeccioné en el idioma.
No tomaba yo notas de todo aquello, por no tener
tiempo de hacerlo y estar demasiado admirado y suspenso
con todo lo que oía, para ocuparme en apuntarlo, pero
lo recuerdo tan bien como si hubiera sido ayer.

Sol Bueno me comunicaba también los hechos diarios
de los del Gran Consejo; y aunque varias veces insinué
que quisiera hablar directamente con ellos, nunca se llegó
a tomar en cuenta mi petición ni Sol Bueno se dio por
enterado, con lo cual ya no quise insistir.

El Gran Consejo había convocado al Consejo Superior
y mandado emisarios a todos los reinos o cuerpos de moscos
dispersos por el mundo, para que supieran la noticia;
y según me dijo Sol Bueno, el Consejo Superior había
acordado reunirse en algún lugar y tratar mi asunto, aunque
sin decirme exactamente qué asunto era ese.

Yo suponía que se trataba del pacto con los hombres,
pero había oído cosas que no me gustaban mucho, como
eso de que los lacandones pagaban un tributo de sangre;
y la superioridad con la que siempre hablaban me daba
la impresión de que consideraban a los hombres como
seres inferiores, por más que yo daba a entender que los
inferiores eran los moscos.

Por lo pronto lo único que yo hacía era charlar con Sol
Bueno y escuchar a los recordadores para ver si mis palabras
habían sido bien entendidas. Dos o tres veces, andando
por la selva, observé cómo unos escuadrones de moscos
me seguían y precedían, espantando a todos los animales
que me pudieran dañar, especialmente a las serpientes y
otras sabandijas. Una noche rugía un tigre cerca de la puerta
de mi casa y aparecieron varios escuadrones de moscos
y lo atormentaron en tal forma, que el pobre salió huyendo.

Una mañana platicaba yo con Sol Bueno, por medio
de mi flauta, cuando aparecieron en mi puerta Mapache
Nocturno y Florentino. Se detuvieron en el umbral y,
viéndome tocar la flauta, cayeron de rodillas. Me levanté
para hacerlos que se pusieran de pie, pero no lo logré
hasta que me hubieron besado las manos y depositado
una ofrenda de carne, miel y frutas a mis pies.

---Perdónanos, ¡oh, Tecolote Sabio, Balam Bueno! ---dijo
Mapache Nocturno---, por haber llegado hasta tus
puertas sin gritar desde lejos como es costumbre entre
nosotros, pero como sabemos ahora que eres Kukulcán
y que todo lo sabes, no creímos necesario gritar. Te rogamos
que aceptes esta ofrenda humilde.

Y diciendo esto volvió a caer de rodillas. Yo me adelanté
y lo tomé de los brazos para levantarlo, dejando mi
flauta en la mesa, junto a los jarros que eran los dioses
de Pajarito Amarillo y que no había yo conseguido que
se volviera a llevar, con lo cual Florentino cayó de rodillas
a su vez, adorando la flauta que había hecho con sus
propias manos.

Después de otro rato de cortesías divinas y humanas
pude tenerlos a los dos de pie, frente a mí, y habló Mapache
Nocturno:

---Venimos a hacerte una súplica, ¡oh, Kukulcán
Blanco!

Antes que se pudiera volver a poner de rodillas, le dije:

---Pide lo que quieras y, si puedo hacerlo, se hará porque
tú y tu gente han sido mis amigos y están cerca de
mi corazón.

---Queremos que nos permitas hacer nuestro caribal
junto a tu choza, en el lugar que tú digas, para poder, junto
con Pajarito Amarillo y los suyos, cuidar de tu persona
y alimentarte.

Florentino, con la cabeza, asentía a todo lo que hablaba
su padre, con tal fuerza que casi se desnucaba. Yo estuve
un rato pensativo, imaginando cómo le caería a Pajarito
Amarillo la vecindad de esta tribu; y, por fin, les dije:

---Yo quisiera que hicieran su Caribal junto al de
Pajarito Amarillo y que fundaran una sola tribu. Si lo hacen
así, tendrán mi protección, como la tienen los de
Pajarito Amarillo, y yo los libraré de los malos espíritus.
Así que yo quiero…

---Tú no quieres, ¡oh, Kukulcán!, tú mandas ---interrumpió
Florentino.

---Bueno ---dije---. Lo que yo mando es que hagan su
caribal en la falda del cerro junto al de Pajarito Amarillo y
que vivan todos unidos como hermanos. Anda, Florentino,
hermano mío, y llama a Pajarito Amarillo, porque
quiero decirle también a él lo que he resuelto…

Florentino salió a escape y volvió al punto con el jefe,
mi amigo, quien según creo estaba cerca espiando lo que
hacían Mapache Nocturno y Florentino en mi choza.
Cuando le hube explicado lo que pretendían los visitantes,
no puso muy buena cara y quedó un rato en silencio.

---¿Qué dice tu corazón? ¿Está bien lo que he propuesto?

---Utz ---me contestó---. Todo lo que tú dices está bien
porque tú eres Kukulcán, el blanco, el barbado, el bueno.

---Pues quiero ---le dije--- que las dos tribus se fusionen
en una sola y todos se vean como hermanos.

Hubo un rato de silencio, mientras los dos jefes cavilaban
en lo que debían decir. Por fin habló Pajarito
Amarillo:

---¿Quién, ¡oh, Balam Bueno!, va a gobernar esa gran
tribu?

---¿Quién crees tú que debe gobernarla? ---le pregunté.

---Yo ---me contestó---. Nosotros nos establecimos
aquí antes, este es nuestro lugar, nosotros hemos sido
tus amigos…

---Pero yo soy más viejo que tú, Pajarito Amarillo
---interrumpió Mapache Nocturno---. A mí me corresponde
el mando.

---Sí ---dijo Florentino---, le corresponde a mi padre
porque es el mayor y el más rico.

Antes de empezar la unión ya los había separado el
afán del mando. Tenía yo que tomar una actitud enérgica,
o todos mis sueños de unir a los lacandones y civilizarlos
caerían por tierra; así que, poniéndome de pie,
dije:

---El mando de la tribu estará a cargo de Pajarito
Amarillo. Eso es lo que yo mando y eso es lo que me han
dicho los espíritus buenos con quienes hablo por medio
de mi flauta. Mapache Nocturno será el segundo en el
mando; y, cuando salga del caribal Pajarito Amarillo,
Mapache Nocturno será el jefe.

Los tres lacandones quedaron en silencio, Pajarito
Amarillo sonriente y los otros dos molestos y a punto de
rebelarse y regresar a sus casas. Para evitar esto y, recordando
que Pajarito no tenía hijos, dije:

---Cuando hayan pasado con sus mayores los dos jefes,
Florentino será el jefe. Si aceptan lo que les propongo,
dicen los espíritus que los harán poderosos y serán
padres de grandes naciones. Si no lo aceptan, volverán
a su antiguo estado y los perseguirán muchos más espíritus
malos que antes.

Hubo un silencio largo, al cabo del cual dijo Mapache
Nocturno, entre vacilaciones:

---Yo soy más viejo, soy más rico y a mí me corresponde
el mando. Si no me lo dan no puedo aceptar la
unión de las tribus, porque mi gente se reiría de mí.

---Yo tengo sobrinos que deben heredar el mando
---dijo Pajarito Amarillo---. No es justo que se les quite
el mando a mi muerte para darlo a estos que llegan.

Durante dos horas alegamos, sin llegar a ningún acuerdo,
por más que quise hacer valer mi autoridad. Por fin,
Pajarito Amarillo ya había convenido en que a su muerte
heredara el mando Florentino, junto con uno de sus
hermanos o sus sobrinos si estos morían, pero Mapache
Nocturno no aceptaba nada. Se sentía el jefe nato y no
quería dar su brazo a torcer. Por fin, enojado, los despaché
a sus casas amenazándolos con mil plagas de moscos
y de malos espíritus.

Cuando se hubieron ido, le conté a Sol Bueno lo que
pasaba, cosa que le dio mucha risa, y me hizo notar inmediatamente
cómo la organización de los hombres era
defectuosa, ya que el mando lo tenía uno igual a los otros
y que la única forma de que ese mando fuera intachable
era que quienes lo ejercieran fueran más fuertes que la
comunidad junta o tan necesarios que esta no pudiera
pasarse sin ellos. Algo molesto por la escena con los lacandones
y por las burlas del mosco, le contesté:

---Tú dices todo eso, porque nada sabes de la organización
de los hombres. Entre nosotros cualquiera puede
llegar a ser poderoso, puede subir todos los escalones.
No somos como ustedes, que están sujetos a un consejo
decrépito, que ya nada piensa porque lo ha pensado todo
y que harían bien en disolverlo para hacerse libres y
fuertes.

Sin contestar nada, Sol Bueno salió riendo y yo me
quedé solo con mi cólera impotente, pensando cómo haría
para cumplir mi amenaza a los lacandones, no muy
seguro de que los moscos me concedieran otro favor.

</section>
<section epub:type="chapter" role="doc-chapter">

# VIII

Por la noche, Sol Bueno llegó, como de costumbre, a
visitarme. Tomé mi flauta y empecé a platicar con él, sin
saber cómo abordar el tema de mis amigos los lacandones.
Quería yo que varios escuadrones de moscos atacaran
ambos caribales esa misma noche y les dieran tal zarandeada
a los indios que no les quedara duda de que mi
maldición se cumplía. Sol Bueno parecía también nervioso
y distraído: su charla era inconexa y parecía pensar
en cosas muy distantes, totalmente ajenas a mis amigos
los lacandones y las clases que me daba sobre la
organización mosquil. De pronto me dijo, interrumpiendo
otra charla:

---Sabe, hombre, que hoy en la noche se reúne el
Consejo Superior, cosa que no había sucedido en quinientos
mil años.

---Ha de ser algo muy interesante ---le dije.

---Sí ---me contestó---. El Gran Consejo lo ha arreglado
todo. Por mejor decir, arregló todo hace trescientos
cuarenta y tantos mil años, según oí decir a uno de
los recordadores…

---Como siempre, el Consejo ya todo lo tiene resuelto
---interrumpí---. Yo creo que con los recordadores
bastaba y sobraba. El objeto del Consejo era pensar y ya
lo pensó todo, luego sale sobrando…

---No hablemos de eso ---me interrumpió Sol Bueno---.
Antes bien ha nacido una situación para la cual el Gran
Consejo no tiene respuesta.

---¡Vaya! ---exclamé---. ¿Y ahora qué van a hacer?

---Tú puedes ayudarnos. La situación es esta: el
Consejo Superior, el innombrable, consta de más de dos
millones de miembros de los que se mantienen con sangre.
A nosotros nos toca, por las leyes de la hospitalidad,
el proveerlos, porque ellos viajan solos, sin proveedoras
y sin ejércitos. La antigua tradición que ha consultado el
Gran Consejo dice que solían, al principio, los cuerpos
o unidades convocar al Consejo Superior a cada paso. En
aquellos tiempos cada miembro del Consejo Superior
viajaba con gran séquito y buscaba su manutención como
podía, hasta que una vez acordaron que el cuerpo
que solicitara la reunión proveyera la alimentación del
Consejo Superior, cuyos miembros no traerían proveedoras.
Desde que se dio esta ley, ningún cuerpo había
convocado al Consejo Superior, tal vez por ahorrarse el
gasto y el trabajo.

---Sabia ley ---comenté yo---. Si los hombres se reunieran
menos a tratar los asuntos del mundo, las cosas
andarían mucho mejor.

---Es que los hombres tienen poca experiencia ---me
dijo---. Tal vez algún día alcancen nuestro grado de perfección
y comprenderán todas estas cosas. Por lo pronto,
según entiendo, es tal el afán que tiene cada individuo
en mandar y poseer, que todos se desvelan y comen
las manos tras de los puestos de gobierno y esto hace su
vida en común insufrible. Pero no hay que desesperar,
ya aprenderán. Confío para no entristecerme.

---A todo esto no me has dicho en qué puedo ayudarlos
---le dije para apartarlo de la crítica justa al régimen
humano de gobierno.

---El Gran Consejo ordenó hace días a los contadores
que sacaran del tesoro las larvas de cien millones de proveedoras
y un millón de exploradores y las criaran, para
poder alimentar a los miembros del Consejo Superior; así
lo han hecho y ya están trabajando…

---Sabia precaución del Gran Consejo ---interrumpí yo.

---Pero han tropezado con dificultades para realizar
su trabajo. Nuestra principal fuente de abasto iba a ser
la sangre de tus amigos los indios y te hemos prometido
no tocarlos. Y la sangre de los animales y de los hombres
que viven a unas cuatro leguas de aquí no alcanza.

Estuvo un rato en silencio, mientras yo pensaba cómo
esto venía a ayudarme grandemente en mis planes;
pero quería que la solicitud de atacar a los lacandones
proviniera de él y no de mí. Por fin me dijo:

---El Consejo Superior se va a reunir por causa tuya
y es bueno que nos ayudes en esta dificultad. Debes permitirnos
que tomemos un poco de sangre de tus amigos.

Era lo que había estado esperando. Con esto podría
castigar a los lacandones y doblegarlos a mi autoridad.
Así que le contesté, después de fingir un momento de
meditación:

---Mi principal interés es el ayudar y ser amigo de ustedes.
Además, a esos hombres les haría provecho entregar
un poco de sangre. Puedes decirle al Gran Consejo
que le doy mi autorización para que ataque a los lacandones
de aquí…

---Será un ataque como nunca han visto ---me interrumpió
Sol Bueno---: caerán sobre ellos millones de proveedoras…

---Lo único que te ruego ---interrumpí--- es que no los
infecten de enfermedades. Que los moscos que vayan a
atacarlos no lleven gérmenes nocivos para el hombre.

---No los llevarán ---me repuso---. Y en nombre del
Gran Consejo, te agradezco lo que has hecho y no se olvidará.
Ahora me voy a dar las órdenes necesarias…

---Espera un momento ---le dije---.Yo también quiero
cooperar con ustedes para proveer de sangre a los miembros
del Consejo Superior. Voy a matar un venado o cualquier
otro animal grande y os entregaré la sangre.

---Te lo agradecemos ---me dijo---. Pero ten en cuenta
que la sangre debe ser viva, porque ya muerta no nos
sirve.

Entonces se me ocurrió un acto de crueldad, del que
aún me arrepiento.

---Heriré a un venado ---le dije--- y lo traeré aquí para
que puedan tomar su sangre conforme va escurriendo.

---Eso puede servirnos de mucho ---me contestó---.
Adiós, volveré cuando tengas el venado.

Salí cuando ya se acercaba la media noche y me puse
en un sitio por el que yo sabía que bajaban a beber los
venados a una charca grande. Al poco rato pasó uno junto
a mí, alcancé la carabina y le disparé, tratando de herirlo
en las patas traseras.

Cayó el animal, gimiendo como hacen los venados,
con un _pathos_ humano, y me abalancé sobre él, me lo
eché en los hombros y lo llevé a la puerta de mi casa, por
más que pretendía patalear desesperadamente.

Al pasar cerca del caribal de Pajarito Amarillo, noté
que habían encendido grandes fogatas humeantes para
espantar a los moscos y que nadie dormía. Sonriendo llegué
a mi casa y puse el venado herido frente a la puerta.
Ya me esperaba allí Sol Bueno y, atrás de él, una gran
cohorte de proveedoras. Por la herida escurría un hilo
de sangre constante y sobre ella se lanzaron, ennegreciéndola
en un minuto. Apenas se llenaban las proveedoras
la panza, llegaban otras. El pobre venado pataleaba
y trataba de levantarse, gimiendo de vez en cuando.
Sol Bueno, me dijo:

---Amárrale las patas y las manos. Así acabaremos
más aprisa. ---Sin responder hice lo que se me mandaba. La
luna era llena y había luz bastante para ver lo que se
hacía. De pronto dejó de correr la sangre.

---¿Ha muerto? ---me preguntó Sol Bueno.

---No ---le contesté---. Es que se ha coagulado la sangre
y ya no corre.

---Haz que corra--- me dijo.

Tomé el cuchillo y abrí un poco más la herida. El venado
me veía con sus grandes ojos tristes, como suplicando
la muerte, pero en su mirada no había ni rabia ni odio: había
tan solo tristeza, una tristeza tan honda que no quise
verlo más y le cubrí la cabeza con un trapo. Mientras tanto,
seguían llegando millones de moscos a chupar la sangre;
y, apenas llenos, se iban. De pronto dijo Sol Bueno:

---Con esto tenemos bastante. Consérvalo vivo, que
lo vamos a necesitar mañana en la noche.

---No puedo ---le dije---: el verlo sufrir me horroriza.
Quisiera matarlo ahora mismo…

---Es que eres hombre y sufres por las cosas que ves.
¿Cómo no te horrorizaba el matar a tantas proveedoras
cuando eras nuestro enemigo?

---No era lo mismo ---le dije.

---Lo que pasa es que no veías la cara de sufrimiento
que hacían. Pero haz lo que quieras. Mañana vamos a
necesitar más sangre.

Diciendo esto se fue, seguido de sus cohortes ya llenas
de sangre. Yo tomé el cuchillo y maté al venado, le
quité el pellejo y colgué la carne, tendiéndome luego a
dormir.

No amanecía aún cuando llegó Pajarito Amarillo seguido
de toda la tribu. Se veía que no habían podido
dormir en toda la noche y tenían los ojos hinchados. Los
niños lloraban. Se detuvieron todos frente a mi puerta
y se pusieron de rodillas, Pajarito Amarillo fue el primero
que habló:

---Te hemos desobedecido, ¡oh, Tecolote Sabio!, a ti,
que eres nuestro amigo, que eres un balam bueno que
nos visita, que eres Ku-kul-cán, el sabio, el bueno, el
barbado, el blanco. Frente a ti, Mapache Nocturno y yo
hemos pronunciado palabras altas y no hemos respetado
tu autoridad.

---Levántate, Pajarito Amarillo ---le dije---. Ya sé el
castigo que ha caído sobre ustedes y me duele. Los espíritus
están enojados y me han dicho que noche a noche
los perseguirán y que perseguirán también a la tribu de
Mapache Nocturno.

---¡Apiádate de nosotros! ---gritó todo el pueblo---
¡Apiádate de nosotros, oh, Kukulcán, oh, Tecolote Sabio!

---¡Apiádate de nosotros! ---lloraba Pajarito Amarillo---.
¡Apiádate de nosotros, quítanos esta plaga terrible
y haremos lo que mandes!

---Hablaré con los espíritus ---les dije; y me metí a mi
choza, cerrando la puerta. Tomé mi flauta y llamé a Sol
Bueno, pero tan solo vino un mosco insignificante.

---Sol Bueno ---me dijo--- está ocupado y no puede
venir y te pide que lo perdones. Dice que vendrá en la
noche por la sangre.

---Está bien ---le dije.

Acababa de irse el mosco cuando oí fuera un gran
vocerío y gritos pidiendo la muerte. Salí inmediatamente
y vi que acababa de llegar toda la tribu de
Mapache Nocturno, sobre la cual recaían las iras, en
forma de palos y de piedras, de la tribu de Pajarito
Amarillo. Mi sola presencia bastó para calmar a todos;
y Mapache Nocturno se arrojó a mis pies, llorando y lamentándose.

---¡Tecolote Sabio, Kukulcán, anoche cayó sobre mi
tribu una nube innumerable de moscos y se han cebado
en nuestra sangre y nos han quitado el sueño! ¡No podemos
ya vivir así!

Toda su tribu lloraba con él y se lamentaba, las mujeres
retorciéndose las manos y los hombres dejando que
las lágrimas corrieran por sus caras. La tribu de Pajarito
Amarillo se unió inmediatamente al llanto y volvieron
a sus ruegos. Parado en el quicio de mi puerta, quedaba
yo un poco en alto y aproveché el lugar para hablarles:

---Pajarito Amarillo, Mapache Nocturno, amigos míos
todos ---les dije---. Han desobedecido ustedes las voces
de los espíritus buenos que hablan por mi boca y han sufrido
el castigo. Yo puedo invocar a esos espíritus y lograr
que los perdonen, pero tienen que hacer lo que les
mando.

---Haremos lo que digas ---gritaron varios.

---Deben unirse en una sola tribu y será jefe quien yo
diga.

---Sé tú el jefe ---gritó uno y todos aprobaron.

---No ---les dije---. Yo seré tan solo su consejero y su
amigo. El jefe va a ser Pajarito Amarillo.

Mapache Nocturno agachó la cabeza, aceptando en
esa forma.

---Pajarito Amarillo va a ser el jefe y, cuando él muera,
lo será uno elegido entre ustedes. Para elegirlo se juntarán
todos, después de enterrar al jefe muerto, y verán
quién es el más digno para ser jefe y a él elegirán y respetarán.
Y si no lo hacen así, caerá sobre ustedes la plaga
que vieron anoche.

En silencio aprobaron todos lo que yo había dicho.
Mapache Nocturno ofreció traer ese mismo día a toda
su gente con sus casas y sus muebles.

---Por lo pronto ---les dije---, para calmar a los espíritus
y darles algo en cambio de la sangre de ustedes, cada
noche, hasta que yo les diga, saldrán y herirán un venado,
no lo matarán, lo dejarán tan solo herido y lo
traerán aquí, frente a mi casa, cuidando que le escurra
sangre fresca de la herida, pero sin que muera, y esto lo
harán cada noche, hasta que yo les diga.

Asintieron todos y se dispersaron, cada uno a sus ocupaciones.
Yo salí de mi choza en busca de Sol Bueno.

</section>
<section epub:type="chapter" role="doc-chapter">

# IX

Me adentré por la selva, llamando con mi flauta a algún
mosco que pudiera oírme. Sabía que siempre que andaba
por la selva me seguían y precedían varios escuadrones
para protegerme, así que confiaba en que algún capitán
me oyera y se acercara a hablar conmigo. Por fin
apareció uno de ellos.

---Es urgente que vea yo a Sol Bueno ---le dije---.
Tengo que verlo ahora mismo.

---Está ocupado con el Consejo Superior ---me contestó---,
pero lo llamaré. Espera en el playón chico, donde
sueles hablar con Sol Bueno, y allí te llevaré la razón.

El mosco se fue y yo me dirigí hacia donde se me había
indicado. A los pocos minutos de esperar, apareció
Sol Bueno y me dijo:

---También yo iba en tu busca. El Consejo Superior
ha terminado sus deliberaciones y ha enviado a varios
mensajeros para que te hablen y te digan lo que ha resuelto.

---Antes quiero decirte una cosa ---le dije---. Mis amigos
los indios han sido muy castigados por ustedes…

---Te avisé que el ataque iba a ser terrible ---me repuso.

---Sí ---le contesté---. Y no te culpo por ello, pues yo
di mi consentimiento. Pero ahora se me ha ocurrido otra
idea mejor. He visto anoche con qué facilidad las proveedoras
tomaban la sangre del venado herido. Cada noche
tendrás un venado así en la puerta de mi casa para que
tomen su sangre…

---Me pareció que anoche te había dado mucha lástima
el venado ---me dijo un poco burlón.

---Más lástima me dan los lacandones.

---Está bien: esta noche iremos por la sangre, pero sin
falta, o tendremos que tomarla de tus amigos.

---Tengo que decirte otra cosa ---le dije---. Resulta
que los que vivían en el caribal a cuatro leguas de aquí,
se han mudado para estar todos juntos, cerca de mí…

---Ya entiendo ---me interrumpió---, quieres que
tampoco a ellos los molesten. No tengas cuidado: mientras
haya bastante sangre, no tocaremos a tus amigos.
Ahora vete a tu casa y oye lo que tienen que decirte los
recordadores del Consejo Superior.

Me encaminé a mi choza y la encontré llena de moscos.
Afuera me esperaban casi todos los lacandones y tuve que
despacharlos, prometiéndoles que si traían el venado herido
y no muerto, como se los había yo dicho, no los volverían
a molestar los moscos ni los malos espíritus. Pajarito
A marillo me dijo que querían algunas hojas de papel para
hacer barquitos y echarlos al río, de manera que en ellos
se fueran los malos espíritus. Se las di y se fueron.

Apenas entré a mi choza, uno de los recordadores se
adelantó para decirme:

---¡Oíd la palabra del Consejo Superior, la palabra
que me ha sido encomendada! Salud al hombre que ha
aprendido nuestro idioma y que se doblega a nuestros
deseos. Esto ha dicho el Consejo Superior hace una hora
y treinta y cinco minutos.

---¡Oíd la palabra del Consejo Superior ---dijo otro---,
la palabra que me ha sido encomendada! El hombre que
ha aprendido nuestro idioma hablará con nosotros, los
del Consejo Superior, el día este, en la noche, en el lugar
que se le señalaré. Para señalárselo se nombra a Sol
Bueno, jefe de los lógicos de este cuerpo del Metasboc,
que nos ha convocado. Esto ha dicho el Consejo
Superior hace una hora y dieciséis minutos.

Callaron los recordadores y comprendí que era el momento
en que debería hablar yo; así que, tomando mi
flauta, dije:

---Salud al Consejo Superior. He escuchado sus palabras
y estaré en el sitio que se me indique, a la hora debida.

Inmediatamente algunos recordadores repitieron mis
palabras y se alejaron todos, dejándome solo. Tan solo
oía yo afuera a la guardia que canturreaba.

Ya empezaba a caer la tarde cuando llegó Sol Bueno,
seguido de varios recordadores.

---Hombre ---me dijo---, debes ir, apenas caiga la noche,
a la roca grande que está junto al lago, al norte del
playón. Debes ir solo…

---Sí ---le dije---. Ya los recordadores…

---Escucha y no interrumpas. Llegarás ahí y te pararás
viendo hacia el lago, hasta que tu nombre sea pronunciado.
Entonces podrás volverte y escucharás lo que debe
decirte el Consejo Superior. Tal vez lo veas, pero debes
conservarte inmóvil y contestar tan solo cuando te
pregunten. Cuando te despidan, volverás directamente
acá para que me des la sangre que me tienes prometida.

---Está bien ---le dije---. Pero no veo la razón de tanto
misterio ni de tanta alharaca, ya que tú y yo somos
amigos, Sol Bueno…

---No te incumbe a ti el explorar estos misterios. El
Consejo Superior y el Gran Consejo lo tienen todo pensado
y saben lo que se debe hacer, desde hace muchos
años. A ti no te corresponde más que obedecer.

---Yo creí que lo que se iba a tratar era un pacto de
alianza entre los hombres y los moscos, pero como tú lo
presentas más parece que los moscos van a imponer sus
condiciones…

---Eso yo no lo sé. Se te dirán palabras que han sido
pensadas hace muchos miles de años. Tú no tienes más
que obedecer.

Y diciendo esto se alejó, sin el acostumbrado saludo.
Yo quedé meditabundo. Mis amigos los moscos no eran
mis amigos: eran más bien mis dueños, que me ordenaban
lo que debía hacer. Así me parecía difícil llegar a un
acuerdo para un pacto con los hombres. Tal vez lo mejor
era el no acudir a la cita y demostrarles que con un
hombre no se juega. Pero, después de todo, ¿me interesaba
en algo el que se hiciera un pacto entre los hombres
y los moscos? ¿Me lo iba a agradecer alguien si se hacía?
Ya sabía yo que los hombres olvidan pronto los favores.
No, mi interés no era resueltamente el bien de la raza
humana: mi interés era el estudiar la organización y la
vida de los moscos, escribir sobre ello un libro, ganar dinero
y ayudar a mis amigos los lacandones.

Para lograr ese fin lo mejor era acudir a la cita del
Consejo Superior y ver lo más posible, aunque no les hiciera
caso a lo que propusieran. No podía yo perder esa
oportunidad de ver cosas que ningún hombre había visto
jamás.

Anochecía cuando salí de mi choza rumbo al sitio indicado.
Observé que el caribal estaba vacío, seguramente
porque mis amigos los lacandones habían ido a cazar
el venado que les pedí. Caminando por la orilla del lago,
con algunos trabajos por las muchas lianas y bejucos que
cerraban el paso, llegué al playón, lo atravesé rápidamente
y volví a internarme en la selva. Ya casi estaba oscuro,
pero pude ver que una verdadera nube de moscos me
seguía y otra me precedía, espantando a mi paso a todos
los animales. Caminé cosa de un kilómetro entre la selva
y llegué por fin al pie de la roca que se me había indicado.
Allí me detuve viendo hacia las aguas. Sobre ellas se
había formado una nube de moscos, como yo nunca había
visto en tamaño. Había capa sobre capa, todos sosteniéndose
en el aire en silencio, casi inmóviles. Atrás de
mí escuchaba zumbidos y voces de otra multitud, pero
me mantenía inmóvil, como se me había indicado, viendo
hacia las aguas. Observé que entre los moscos que flotaban
en el aire, sobre el lago, había unos grandes que yo
nunca había observado y otros pequeñísimos, también
desconocidos. Poco a poco, a fuerza de mucho ver, pude
observar que había cierto orden entre ellos y que estaban
colocados de acuerdo a sus categorías. La capa más baja
estaba formada por proveedoras, que yo conocía tan
bien; luego, más arriba estaban los exploradores; seguían
los recordadores, que por primera vez veía yo en silencio.
Arriba estaban las ponedoras, y, encima de ellas, el
ejército. Más arriba estaban los lógicos y, seguramente,
entre ellos, mi amigo Sol Bueno. Arriba quedaban esos
moscos grandes que no sabía yo qué fuesen, y, hasta encima,
los pequeños. De pronto la nube aquella se puso en
movimiento y se acercó a la orilla, hasta quedar a un metro
de distancia de mí. Yo tenía en la boca mi flauta y
pensé hablarles, pero algo había en el silencio absoluto
que reinó en la selva, algo sofocante y terrible, que me
obligó a callar. Observé que la selva estaba muda; no gritaban
ya las aves en busca de sus nidos, ni se oía el susurro
de animales que se arrastraran, ni siquiera el temblor
de una hoja, ni el chillido de algún mono. Todo era silencio
sofocante. Por primera vez tuve miedo y ganas de
huir. Di un paso hacia la derecha y la nube de moscos
frente a mí se extendió para cerrarme la salida. Lo mismo
sucedió a la izquierda, con lo que resolví quedar inmóvil.
Atrás volvió el zumbido y los ruidos breves. De pronto
un zumbido me dijo claramente:

---Vuélvete hacia nosotros, hombre.

</section>
<section epub:type="chapter" role="doc-chapter">

# X

Apenas acabó de sonar la voz, me volví, quedando de espaldas
al lago, de frente a unos matorrales grandes, entre
los que sobresalían un inmenso caobo y un palo mulato
rojizo. Al principio no vi nada, tan solo era una masa negra
impenetrable a la vista, en la cual se destacaban unos
puntos blancos, inmóviles, que me dieron la impresión de
ojos que me miraban fijamente. Poco a poco acostumbré
la vista y pude distinguir que esa inmensa masa negra estaba
formada por grandes moscos, entre los cuales había
unos blancos. Alguno de ellos empezó a zumbar:

---Hombre que has aprendido nuestro idioma, vamos a
hablar contigo; nosotros somos los que pensamos, y ya lo
hemos pensado todo en el mundo. Queremos, ante todo,
que estés completamente inmóvil y en silencio mientras no
se te diga que puedes hablar. ¿Has entendido esto?

---Sí ---contesté.

---Bien. Ahora escucha atentamente, y que estas palabras
no se pierdan de tu cabeza y que las conserves para
siempre. Cuando tú diste muestras de haber aprendido
nuestro idioma, el Cuerpo del Metasboc, que domina
estos territorios, avisó inmediatamente a todos los cuerpos
que vivimos diseminados en el mundo; y así nos hemos
reunido en un Consejo Superior, en el cual están representados
doscientos ochenta y tres mil quinientos
veinticuatro cuerpos, o sea, casi la totalidad de los cuerpos
existentes. Los que no han venido no tienen importancia:
son los que viven en zonas frías y están poco desarrollados.
Por estas palabras que te he dicho, debes
entender la importancia que tiene este Consejo Superior
y debes acatar fielmente sus órdenes. ¿Has entendido?

---Sí ---contesté de nuevo, aunque no me gustaba mucho
esto de las órdenes.

---¡Bien! ---prosiguió el mosco que hablaba---. Ahora
te diremos lo que hemos acordado en este Consejo
Superior. Ya todo estaba pensado desde mucho antes y
hemos, tan solo, oído lo que se había pensado en los diferentes
cuerpos; y lo hemos resumido para que tu inteligencia,
poco capacitada, pueda comprenderlo fácilmente.
Es esto lo que queremos decirte: nosotros los
moscos somos los dueños absolutos del Universo y toda
criatura en él debe pagarnos tributo de sangre que nos
es necesaria para vivir. Tú ya conoces nuestra organización
y sabes la necesidad que tenemos de sangre. Ahora
bien, todos los animales, más o menos, se han sometido
a nosotros y los aprovechamos cuando queremos, dejándoles
cierta libertad, que a ellos les parece completa. Tan
solo nos dan su tributo y no nos metemos en asuntos internos
para nada. Esto hacen todos los animales, menos
los hombres, que, o por más adelantados que el resto de
los animales o por más orgullosos, no quieren darnos
apaciblemente su tributo de sangre y han inventado medios
para destruirnos, lo cual ha provocado guerras parciales.
¿Entiendes esto?

---Sí ---le contesté---. Y en estas guerras parciales, como
en el caso de Panamá y en otros, habéis sido derrotados…

---Nunca hemos sido derrotados. Algunos de nuestros
cuerpos se han replegado en espera del gran momento
que tú no puedes ni debes conocer; pero en ese gran momento
será tal el combate, que, si así lo queremos, no
quedará en el mundo memoria de los hombres. Ahora
bien, esto no es lo que pretendemos. El hombre tiene
buena sangre que nos sirve, así que podemos, ya que has
aprendido nuestro idioma, proponerle un tratado de
alianza y amistad. Las cláusulas de ese tratado son fáciles
de recordar y queremos que las lleves ante los hombres.
Primera: los hombres dejarán de usar para siempre
las armas que han venido usando con el absurdo propósito
de destruirnos. Segunda: los hombres comprenderán
que nos deben un tributo de sangre y lo pagarán. Tercera:
para pagarlo pondrán a un número determinado de los de
su especie en los lugares que se les señalaren, durante un
tiempo definido, de manera que haya constantemente
tres millones de personas dispuestas en los dichos lugares,
las cuales estarán desnudas y se dejarán succionar toda
la sangre que creamos necesaria. Cuarta: en otros lugares
que se les señalarán, no entrarán los hombres por
ningún motivo, bajo pena de muerte. Quinta: en ningún
caso matará un hombre a ninguno de nosotros y el que lo
hiciere será puesto en un lugar indicado en forma que le
podamos succionar toda la sangre. A cambio de estas condiciones,
les ofrecemos a los hombres no transmitirles enfermedades,
no molestarlos en los lugares que queden
fuera de los señalados y defenderlos contra cualquier animal
que los quiera atacar ¿Qué dices a esto, hombre?

---Me parece ---empecé--- que difícilmente aceptarán
los hombres ese arreglo. Primero tropezaremos con la dificultad
de que los hombres se creen dueños del mundo
y los consideran a ustedes como una de las especies inferiores.
Tropezaremos además con la dificultad del tributo
que los hombres se negarán a pagar. Deben ustedes
tener en cuenta que el hombre es el más inteligente
de los animales de la creación…

---Eso creen ustedes ---me interrumpió---. Pero creo
haberte demostrado ya que somos nosotros, los moscos,
los más inteligentes y los que hemos logrado más adelantos.
Hemos logrado dispersar nuestras células de manera
que podemos ocupar muchos espacios a la vez…

---Es cierto eso ---le repuse---. Pero ustedes han perdido
el poder de procrearse y no pueden hacer un nuevo
cuerpo.

---No lo necesitamos. Ya nos hemos repartido el mundo
y estamos conformes con ello. Fíjate qué poco pedimos.
Hay entre los hombres más de dos mil millones de
individuos y pedimos tan solo tres millones para apaciguar
nuestra necesidad de sangre. Esos tres millones de
individuos no morirán y serán reemplazados, cuando ya
estén débiles, por otros. Toma el ejemplo de nosotros que
sacrificamos un número infinito de proveedoras…

---El caso no es el mismo ---le interrumpí---. Ustedes
no sacrifican más que células de su cuerpo que reponen
fácilmente, pero entre nosotros cada hombre es un ser
completo, con inteligencia, con memoria, iniciativa, voluntad.
Cada hombre es como todo un cuerpo de ustedes,
sobre el cual los demás no pueden o no deben ejercer
más que una limitada influencia, porque en lo
sustancial todos los hombres son iguales entre sí…

---Hay entre ustedes ---me interrumpió--- unos que
son poderosos y otros que no lo son. Bien pueden los poderosos
obligar a los que no son poderosos a que den el
tributo de sangre. El poder, entre ustedes los hombres,
es una gran cosa, por la cual luchan desesperadamente,
porque no han alcanzado nuestro grado de perfección en
el que ya todo está distribuido.

---Ese grado de perfección ---le dije--- tiene sus inconvenientes.

---¿Y cuáles son ellos? ---me preguntó algo burlón.

---Es tan perfecta su organización que ya no hay iniciativa
entre ustedes. Todo se les va en cuidar de esa organización
y en buscar el alimento necesario sin imaginar
nunca ninguna cosa nueva, porque ya todo lo han
pensado y ya no saben pensar, sino que tan solo saben
resumir y darle vuelta a esos pensamientos viejos.

---Es interesante tu tesis ---me repuso---, pero tiene el
defecto de abogar por la anarquía. Si ya hemos encontrado
el régimen de vida perfecto, ¿para qué hemos de cambiar?

---El régimen es perfecto para las células que son parte
del cerebro, para ustedes los del Consejo, pero para
los otros no es tan perfecto; y tal vez ellos quieran cambiar
algún día, si algún día sienten en sí, como lo han
sentido los hombres, la necesidad de ser libres.

---Esas son tonterías. ¿Para qué quisieran ser libres
las proveedoras? No tienen cerebro ni pensamiento.
Tienen tan solo el necesario para ejecutar nuestras órdenes.
Pero volvamos a nuestro asunto. Me parece que no
te ha gustado lo que te hemos propuesto para las relaciones
entre los hombres y nosotros aunque todas nuestras proposiciones
son de gran justicia, ya que somos los más
fuertes y podemos, con toda facilidad, aniquilar al hombre
de sobre la faz de la tierra. Tú dices que los hombres
son libres entre sí, que tienen en sí todo. Eso mismo es
lo que los hace tan vulnerables y tan fáciles de destruir,
si nos lo proponemos. Hay que cambiar en ellos esa organización
y darles una semejante a la nuestra.

---Eso es imposible ---le dije---. Ningún hombre llegaría
a la abyección que tienen entre ustedes las proveedoras.
Ya les he explicado que todos son iguales…

---Y ya te he contestado que eso no es cierto. Pero no
discutamos el punto. Hemos pensado demasiado y estamos
cansados de hacerlo. Puedes retirarte.

La mancha negra al pie del caobo desapareció casi en
un abrir y cerrar de ojos, y la nube que quedaba a mis
espaldas, sobre el lago, se disolvió como por encanto. Yo
me volví lentamente rumbo a mi choza en cuya puerta
me encontré a Pajarito Amarillo que sostenía entre sus
brazos a un venado herido, del que manaba bastante
sangre. Le dije que lo dejara en el suelo y se fuera a casa,
sin volver la cara. Apenas se hubo ido me vi rodeado
de gran cantidad de proveedoras y empezó de nuevo
la terrible escena de la noche anterior. Tres veces, por
indicaciones de Sol Bueno, tuve que abrir la herida del
venado. Estaba yo inclinado sobre él, cuando una de las
proveedoras me dijo en voz baja, suplicante:

---¿Tú crees, como le dijiste al Consejo, que hay esperanza
para nosotros, que podemos llegar a más?

Iba yo a contestarle, cuando nos interrumpió Sol
Bueno, reprendiendo a la proveedora por perder el tiempo.
Esta se alejó rápidamente a entregar la sangre que
llevaba y la vida.

Cuando se hubieron ido todos, rematé al venado y me
metí en mi choza, donde me puse a pensar.

</section>
<section epub:type="chapter" role="doc-chapter">

# XI

En la tarde del día siguiente recibí un nuevo recado del
Consejo Superior, citándome en el mismo sitio. Esta vez
no me trajo el recado Sol Bueno ni se presentó para nada,
cosa que me dio que pensar. Había que romper de alguna
manera ese orgullo de los moscos. ¿Cómo se iban a
querer comparar con los hombres? Entre más pensaba yo
en ello me parecía más absurdo, pero por otro lado pensaba
yo que a los moscos también les debería parecer absurdo
el que los hombres se igualaran con ellos. Tal vez
fuera bueno averiguar algo en relación a su espiritualidad:
si tenían alma como la nuestra, si creían en Dios, si
lo conocían como nosotros los hombres o si nunca habían
llegado a ese conocimiento por medio de la fe o del raciocinio.
Me daba yo cuenta de que los Consejos o cerebros
ya habían pensado todo para el futuro, se habían
puesto en todas las situaciones imaginables; pero nunca
vi que hubieran tratado de relacionar el pasado con el futuro,
que trataran de buscar las causas de los efectos.

Pensando en estas cosas me dirigí al lugar de la cita,
donde se repitió la escena del día anterior. Yo creo que
el mismo mosco fue el que me hablo por lo menos su
voz me sonó igual.

---Hombre ---me dijo---. ¿Has pensado en lo que te
propusimos anoche?

---Sí ---le repuse---. Lo he pensado y siento decirles
que los hombres no pueden aceptar eso. Las únicas cláusulas
que aceptarían serían las relativas a suspender la
guerra, ya que a los hombres los anima siempre, en todas
sus decisiones, el espíritu de la paz…

---Por eso ---me interrumpió--- siempre están en guerra
entre ellos mismo.

---Eso es otra cosa ---dije.

---Así vivíamos nosotros en tiempos muy lejanos que
ya pasaron. Ahora hemos progresado y ya acabaron las
guerras inútiles. Pero sigue hablando.

---Digo que no podemos aceptar eso, no podemos
considerarnos como tributarios de ustedes ni darles esa
sangre que tanto piden. Tal vez pudiéramos, a cambio
de vuestra amistad, daros sangre de animales, de los que
sacrificamos para nuestra manutención, y señalar ciertas
zonas en las que…

---No te hemos dicho ---me interrumpió--- que nos
propongas un nuevo tratado. Tan solo queremos saber
si aceptas llevar al conocimiento de los hombres lo que
te propusimos anoche. Nosotros te ayudaríamos para
que los hombres te creyesen, podrías enseñarles nuestro
idioma a varios de entre ellos y los convencerías…

---Es inútil ---le dije---. Los hombres nunca aceptarían
eso.

---¿Y nos contestas sin consultarlo con ellos? ¿Estás
facultado para hablar a nombre de todos?

---No estoy facultado, pero sé que, si llevo esta proposición,
se me reirían en la cara. Los hombres no les temen
a ustedes y tienen armas con que destruirlos.

---Piénsalo bien. ¿Es esa tu respuesta?

---Sí.

---Pues entonces no nos quedan más que dos caminos
y los dos se basan en el uso de la fuerza. Uno de ellos
consiste en exterminar prontamente a todos los hombres.
Para ello tenemos gérmenes de enfermedades que
aún no hemos usado, si no es como experimento, y que
son infalibles. El otro camino es el de sujetar a los hombres
por la fuerza y gobernarlos nosotros, como ustedes
hacen con sus ganados. Para eso te necesitamos.

---Pero ---le interrumpí--- yo no…

---Calla y no hables hasta que te lo digamos. Ya has
entendido las dos maneras que tenemos para eliminar el
problema, hombre de entre nosotros, y creo que el segundo
es el más conveniente para todos. Entre la esclavitud
y la muerte muchos dicen que prefieren la muerte, pero
en general es preferible la esclavitud, sobre todo la que
impondríamos, que sería suave, ya que no pretendemos
destruir al género humano, que nos puede ser útil, así
como ustedes no pretenderían acabar con sus ganados
porque una vez un animal pueda molestar o matar a un
hombre. Hemos sabido, por lo que tú has hablado con
Sol Bueno, que muchos de los hombres dicen preferir la
muerte, que los poetas que tienen ustedes alaban a los
pocos que han muerto en busca de libertad; pero sabemos
también que la mayoría de los hombres viven en una
especie de esclavitud, a veces más dura que la que les impondríamos
nosotros. Ten en cuenta que con nuestro régimen
acabarían riquezas y pobrezas, ya que alimentaríamos
y vestiríamos a todos por igual, limitando el
número de personas a las que puedan mantenerse bien
en cada región. Para esto necesitamos un gobierno de
hombres que se sujete a nosotros, y allí es donde tú,
hombre, puedes servirnos y debes hacerlo. Mira que te
haríamos el más poderoso de entre los hombres, que todos
estarían sujetos a tu mandato y a tu voluntad; tendrías
sobre ellos derecho de vida y muerte y te daríamos
una guardia impasable que te protegiera contra sus asechanzas.
Piensa, en esto, hombre.

Y yo pensaba en eso. Ahora comprendo que debí reaccionar
de otra manera, como hubiera reaccionado un héroe
de una novela o un santo, pero yo no era ni soy ninguna
de las dos cosas. Era tan solo un hombre perdido
en la selva, de quien los suyos renegaron, y me sentía humillado
y odiado. Nunca había yo recibido bien alguno
de los hombres, antes sí muchos males. En un instante
apareció ante mí la imagen de esos cuarenta años de
amargura, de tristeza, de humillación, y sentí un odio inmenso
que me subía a la garganta y me llenaba todo.
¡Aquí estaba mi oportunidad! ¡Yo podría ser el más
grande de los hombres y en forma tal que no me olvidaran
como olvidan siempre a sus grandes hombres! ¡Qué
me importaba que mi reino fuera sobre esclavos! Sería
yo el más poderoso y mi venganza sería completa, perfecta.

Mientras pensaba yo en todo esto, el silencio que reinaba
en la selva era terrible, casi se podía palpar. Los millones
de moscos presentes estaban inmóviles, no soplaba
brisa y los animales todos callaban. De pie, ante la
mancha negra que llenaba la base del caobo, yo pensaba
estos grandes pensamientos de venganza y mi alma se
sentía llena de un ansia inmensa de obrar, de empezar.
Por fin hablé:

---He medido tus razones y las comprendo. Bien se
ve que ustedes ya lo han pensado todo y tienen una respuesta
para todos los casos que se pueden presentar.
Estoy dispuesto a ayudarles a dominar a los hombres para
que puedan salvarse algunos y no perezca la especie…

---No pretendas engañarnos ni engañarte con palabras
vanas ---me interrumpió el mosco---. Bien sabes
que aceptas porque te hemos ofrecido el dominio completo
sobre los hombres y, como hombre que eres, ansías
con todas tus fuerzas el poder.

---Es cierto ---dije apenado---. La verdad es lo que has
dicho, pero hay algo más. Yo no amo a los hombres, de
quienes no he recibido más que ultrajes y humillaciones,
y por eso acepto servirles a ustedes. Quiero dominar a los
hombres, cambiarles su manera de vivir a alguna manera
más justa, en que todos tengan cierta igualdad…

---La tendrán ---me interrumpió el mosco que hablaba---.
Tendrán la igualdad que tienen los ganados entre
ustedes. Y ahora que ya has aceptado nuestro ofrecimiento,
como estábamos seguros que lo harías, viendo
los bienes que puede acarrearse a tu especie con los planes
que tenemos, pasaremos a hablar de ellos. El plan,
pensado en sus grandes rasgos hace muchos miles de
años, se divide en dos partes. La primera, transitoria, es
la guerra que vamos a hacer a los hombres para subyugarlos.
Para ello contamos con una cantidad incalculable
de elementos. Cuando nuestro programa esté listo, mandaremos
crear más de cien millones de moscos guerreros,
transmisores de enfermedades, entre las cuales se
cuentan las ya conocidas por el hombre, según has dicho
tú a Sol Bueno, como son el vómito negro, el paludismo
o malaria, el mal del sueño, la oncocercosis y otras menores
como la inflamación de la piel. Aparte podemos
usar de otras que no conocen los hombres. Tenemos varias
en reserva que ya hemos utilizado en casos especiales,
como experimentos, y que no quiero explicarte. Tan
solo, a manera de ejemplo, te hablaré de una de ellas.
Tenemos en algunos lugares unos gérmenes especiales
que, introducidos por medio del aguijón de los guerreros,
debajo de la piel de los hombres, provocan a las pocas
horas una gran ampolla, que crece en tal forma que
a los dos días todo el cuerpo está cubierto por ella y el
hombre muere entre horribles tormentos.

«Creemos que la guerra puede durar unos dos años,
al cabo de los cuales, por mediación tuya, los hombres
se rendirán y entonces llevaremos a cabo la segunda parte
de nuestro programa.

»Las cosas que come el hombre, exceptuando el azúcar,
que tomaremos en grandes cantidades, no nos sirven
para nuestra alimentación, más que cuando se han
transformado en sangre. Los hombres serán por lo tanto
quienes se encarguen de ese trabajo y habrá tan solo
los que necesitemos para nosotros. Los demás serán destruidos.
Los que queden, que se tratará de que sean fuertes
y sanos, trabajarán durante once meses cada año en
producir alimentos para sus congéneres y azúcar para
nosotros y el mes restante vendrán a los lugares que indicaremos
a que les chupemos la sangre. Las mujeres capaces
de tener hijos se ocuparán de ello y las ya viejas,
lo mismo que los ancianos inútiles, serán muertos irremisiblemente,
para que no consuman los alimentos necesarios.

»Para gobernar a los hombres, nombraremos a algunos
de entre ellos, que estarán exentos del impuesto de
sangre y tendrán una guardia nuestra. Tú serás el primer
gran gobernador; solo tú, por lo tanto, estarás exento del
impuesto o tributo. A tu muerte, nombraremos otro. Tú
podrás nombrar a los lugartenientes que creas necesarios,
pero esos cargos no los librarán del tributo. Tan solo
tú estarás libre de él»…

---Pero ---interrumpí--- me habían prometido inmunidad
para mis amigos los lacandones.

---Sí, la tendrán mientras vivan. Hemos dado nuestra
palabra y sabemos guardarla. Pero a ningún otro
hombre se le concederá tal favor en el mundo, más que
al que gobierne a los otros hombres. Naturalmente que
conservaremos un fuerte ejército para tener en sumisión
a los hombres, pero a los que usemos como productores
de sangre no los enfermaremos, excepto a algunos que
usaremos como criaderos vivos de gérmenes o como arsenales
ambulantes.

«Logrado esto, podremos extender nuestros cuerpos
hasta donde queramos, seguros del alimento necesario.
Nuestro único trabajo será el de tener en orden a los
hombres, para lo cual nos basta y sobra con un ejército
moderado y buenos armamentos de gérmenes. Cualquier
intento de rebelión será duramente castigado y mataremos
sin piedad a los que lo hayan hecho y a todos los
que vivan en los alrededores.

»Los hombres, según sabemos, tienen demasiadas comodidades
que tendrán que dejar, para ocuparse únicamente
en buscar sustento. Tan solo permitiremos fábricas
rudimentarias de azúcar y serán destruidas todas las otras.
Todos los hombres que conocen el manejo de ellas, los sabios,
los artistas que llaman ustedes, los que manejan máquinas,
serán muertos. Queremos que el hombre sea tan
solo agricultor y lo será. ¿Has entendido esto?».

---Sí ---repuse azorado ante la magnitud del plan---.
Lo he entendido.

---Pues bien, piensa en todo lo necesario para llevar
a cabo este plan y comunica el fruto de tus pensamientos
a los miembros de este Consejo que se quedarán aquí
para tratar contigo de todos los asuntos necesarios. Tan
solo quiero advertirte que nuestra ley es rigurosa y quien
la infringe, aunque sea en materia leve, es reo de muerte
inmediata, sin apelación posible.

Diciendo esto desapareció la inmensa mancha negra
y el lago se limpió de la nube de moscos que lo cubría.
Lentamente me dirigí a mi choza a preparar el venado
que seguramente ya habría dejado allí Pajarito Amarillo.
Debería yo haber ido contento: pronto sería yo el hombre
más poderoso del mundo; pero algo me pesaba en el
corazón.

Cuando llegué a mi choza, allí estaba Pajarito Amarillo
con el venado herido.

---Ya debes saber, ¡oh, Quetzalcóatl!, la nueva que te
traigo.

---Dila ---le dije---. Dila como la sepas.

---Por la selva avanzan unos hombres de tu raza. No
vienen cortando maderas ni buscando chicle. Vienen enseñando
estampas y averiguando todas nuestras cosas.
¿Qué debemos hacer?, ¡oh, Kukulcán divino, oh, serpiente
emplumada!

Inmediatamente imaginé que sería alguna expedición
científica que andaba en busca de los lacandones para estudiar
su vida y sus costumbres.

En esos momentos no me convenía la presencia de esa
expedición que pudiera entorpecer mis pláticas y arreglos
con los moscos, así que le pregunté a Pajarito
Amarillo:

---¿Ya están muy cerca?

---No, aún están a una jornada si caminaran derecho
hacia nosotros.

---Pues sal mañana con toda tu gente y borra todos
los rastros y huellas, de manera que no nos encuentren.
No deben vernos porque se podrían enojar los espíritus
malos contra ustedes, como sucedió hace dos noches.

Vi que mi orden no le había gustado mucho a Pajarito
Amarillo, pero tuvo que aguantarse por el miedo que le
causaban los moscos y aceptó hacer lo que le mandaba.
Temeroso de un engaño, le dije:

---Sí los blancos que avanzan por la selva nos encuentran
aquí, dejarás de ser jefe y los espíritus malos mandarán
sobre ti una plaga de moscos que te costará la vida.

Haciendo grandes caravanas, se retiró a su caribal y
yo me ocupé en dar la sangre necesaria. Varias proveedoras
me zumbaron al oído en secreto, mientras Sol
Bueno estaba ocupado en otras cosas:

---¿Crees que hay esperanza para nosotras? ¿Qué, en
verdad crees que dejaremos de ser esclavas si lo queremos?

Yo no me atreví a contestarles, temeroso de que me
oyera Sol Bueno y me acusara con el Consejo.

</section>
<section epub:type="chapter" role="doc-chapter">

# XII

Desde antes del amanecer ya estaban en mi choza varios
moscos presididos por Sol Bueno. Entre ellos había dos
de esos blancos que había yo visto en la masa negra bajo
el caobo y muchos de los muy pequeños que vi flotando
sobre el lago. Sol Bueno me informó que esa era la
comisión que había dejado el Consejo Superior para que
junto conmigo formara el plan de ataque sobre los hombres.
Los moscos blancos eran los portavoces del
Consejo Superior y de ellos dependían los mensajeros
que, cruzando todo el mundo, llevaban las palabras de
un Consejo Superior a otro. Los pequeños eran los contadores
de los grandes tesoros. El resto eran solamente
recordadores y lógicos. Sol Bueno empezó a hablar:

---Hombre ---me dijo---, el Consejo Superior, el que
no se puede nombrar, ha dispuesto que estos que me
acompañan sean los que formen la comisión. Queremos
hacer rápidamente, junto contigo, un plan de combate
perfecto para los fines que ayer en la noche te fueron expuestos
y que aceptaste. Mira, aquí tienes a este amigo,
blanco como el aire por el que navega; se llama Viento
Veloz y él puede informarnos de todo lo que pueden hacer
los cuerpos diseminados por el mundo. Este otro, pequeño,
es el que resume en sí las cuentas de todos los
cuerpos, sus tesoros y sus arsenales y se llama Memoria
Infinita. Cada uno de ellos, como ves, trae a sus asesores
y hay aquí bastante número de recordadores para que
nada de lo que digamos se pierda. Con esto podemos empezar
nuestras deliberaciones cuanto antes.

---Empecemos ahora mismo ---le dije, pensando en
los hombres blancos que avanzaban por la selva y que, a
pesar de las diligencias de Pajarito Amarillo, podrían encontrarnos
de un momento a otro. Por ningún motivo
me convenía su presencia y que se dieran cuenta de cómo
los lacandones me tenían por dios.

Inmediatamente pusimos manos a la obra y empezamos
a organizar el plan, que los recordadores repetían
constantemente, en voz baja. Al principio hicimos un
cálculo de las fuerzas con las que contábamos y yo les hice
conocer las armas que tenían los hombres para combatir.
El arma principal de los hombres es el petróleo,
por lo cual resolvimos ocupar, antes que nada, los campos
petroleros. Ya sin petróleo, los hombres estarían casi
paralizados, pero creímos oportuno destruir también
las plantas de luz, los ferrocarriles y los puertos de mar.
Para lograr esta destrucción se lanzarían escuadrones de
moscos con gérmenes que matarían a todos los hombres
que trabajaban allí.

Paralizado así el mundo, pero sin tocar los campos ni
los agricultores, calculamos que nos sería fácil el imponer
nuestras condiciones de paz y dedicarnos a reorganizar
la sociedad a nuestra manera.

La mayor parte de los cuerpos de moscos están cerca
del ecuador, entre este y los treinta grados de latitud
norte o sur, así que los ataques empezarían del ecuador
al norte y al sur simultáneamente. Cada cuerpo de moscos
buscaría una charca en algún lugar conveniente y en
ella incubaría varios millones de guerreros y los gérmenes
necesarios para armarlos. Mientras tanto, en los lugares
acostumbrados, se incubarían millones de ponedoras
que estuviesen listas para poner huevos a razón de
cien millones diarios por cuerpo. Los contadores dijeron
que todo aquello sería fácil de organizarse y no se necesitarán
arriba de veinticinco días.

Mientras se desarrollaban los combates, yo estaría
preparado, en algún lugar donde hubiera telégrafo, o, de
ser posible, radio, para dar al mundo la noticia de su esclavitud
y del nuevo gobierno que les esperaba a los hombres.
Cerca de mí estaría el cuartel general, donde se recibirían
todas las noticias de los diferentes ataques que
se iban a hacer en el mundo.

Los cuerpos que quedaban sumidos en las selvas amazónicas
y en otros lugares donde no hubiera casi hombres,
prestarían un gran contingente de guerreros y de
gérmenes que acarrearían a su tiempo, y servirían como
reservas en caso de que los hombres lograran destruir
algún cuerpo durante la lucha, cosa bastante improbable.

Todo el día lo pasamos en elaborar estos planes y tan
solo interrumpimos los trabajos cuando me llamó
Pajarito Amarillo desde afuera. Pude darle la grata nueva
de que esa noche ya no necesitaríamos venado herido,
pues con mis ruegos había yo logrado calmar a los espíritus
malos. Él me dijo que los hombres blancos estaban
más cerca del caribal y que caminaban directamente
hacia él, por más que les había borrado todas las huellas
y todos los pasos. Según él, ya los blancos sabían de la
existencia de ese caribal y lo buscaban. Como pude lo
despaché, ordenándole que no se pusiera en contacto
con los que avanzaban hasta que yo se lo dijera. Un rato
quedé en la puerta de mi choza meditando en lo que
haría yo, una vez convertido en gobernador general de
todo el mundo, con mis amigos los lacandones. Tal vez
lo mejor fuera ponerlos como jefes de grupos de los agricultores,
sobre todo de agricultores de hombres de mi
raza, para que supieran estos lo que es sentirse humillado
y despreciado, aunque de todos modos ya bastante lo
sentirían cuando fueran esclavos de los moscos.

Volviendo al interior de la choza, seguimos trabajando
en la elaboración del plan. Memoria Infinita puso una
objeción. Si queríamos criar gérmenes bastantes para
nuestro programa, necesitábamos sangre fresca y segura,
con lo que resolvimos que cada cuerpo apresara a un
número de hombres, para tomar de ellos la sangre y usarlos
como arsenales vivientes de gérmenes no mortales.
Allí supe que los moscos pueden, si así lo desean, librar
a un hombre de los gérmenes, inyectándole antitoxinas;
y esto es lo que habían hecho conmigo, para librarme de
las calenturas palúdicas. Acabado este punto, pasamos a
la organización del mundo ya ganada la guerra.

Por lo pronto, cada cuerpo se reservaba una zona en
la cual, bajo pena de muerte, no podía entrar ningún
hombre. Alrededor de estas zonas, donde se guardaría el
tesoro y se colocarían los Grandes Consejos, se designaría
otra en la cual deberían estar los hombres que iban a
servir de alimento a los cuerpos. Según el tamaño de cada
cuerpo, sería la cantidad de hombres que se necesitarían.
Los otros hombres estarían, mientras tanto, trabajando
en los campos, produciendo alimentos para ellos
mismos y azúcar para los moscos. Claro está que con el
alimento asegurado en forma tan fácil, cada cuerpo crecería
mucho, hasta llenar por completo su zona, y el número
de hombres que se permitiría sobre la faz de la tierra
sería de acuerdo con las necesidades de los moscos.

Se crearían zonas especiales, en climas fríos, poco
agradables para los moscos, donde se pondrían los lugares
necesarios para la reproducción de hombres, perfectamente
controlada, de manera que nunca hubiera más
de los necesarios. Mediante los cruzamientos debidos se
buscaría una raza fuerte con buena sangre, pero intelectualmente
del más bajo nivel posible, para evitar en esa
forma un levantamiento de los hombres que obligara a
los moscos a acabar con la especie. Naturalmente que no
habría escuelas ni nada parecido, se perdería la escritura,
se acabarían las artes y todo lo que conocemos como
cultura.

A los hombres se les permitiría tener ganados, casas,
algo de ropa hecha de pieles y telas burdas, fuego y útiles
de labranza rudimentarios, pero se cerrarían y destruirían
todas las fábricas, se acabarían los ferrocarriles,
se prohibiría el uso del petróleo, de la luz eléctrica y de
toda fuente de armas. Se repartirían en clases. Una, de
los productores de sangre para los gérmenes, que estarían
siempre en las zonas señaladas, alrededor de cada Gran
Consejo, hasta que murieran de la enfermedad que se les
inoculara. Otra rama sería la de los trabajadores manuales,
dedicados doce horas diarias a tareas agrícolas y a
producir todo lo necesario para la alimentación suya y
de los moscos. Otro grupo y las mujeres jóvenes estarían
destinados a la reproducción; y, por último, algunos al
gobierno y organización de todo esto. Los ancianos y ancianas
serían muertos apenas pasaran de los cincuenta
años, cualquiera que fuera su condición, exceptuando
los ocupados en el gobierno.

Un hombre que por cualquier motivo matara a un
mosco, pretendiera no cumplir con su tarea, desobedeciera,
pasara a una zona prohibida o tratara de inventar
algo para combatir a los moscos, sería inmediatamente
condenado a muerte y ejecutado. No había otra pena más
que la de muerte y esta se aplicaría invariablemente.

Acabado de elaborar este plan, los dos moscos grandes
se dedicaron, mediante unos mensajeros que tenían
en algún lugar cercano, a darlo a conocer a todos los
cuerpos del mundo, para ver si alguno de ellos tenía alguna
objeción que hacer.

Dos días pasamos en esto, dos días como de sueño,
donde me veía yo dominando al mundo, siendo el amo
y señor de vidas y haciendas. Este tiempo me parece como
un sueño lleno de pensamientos, de ambiciones y de
ansias. Esos dos días fueron los más grandes de mi vida.

Al tercero, temprano en la mañana, tocaron en la
puerta de mi choza; contesté en maya que entraran y la
puerta se abrió para dar paso a un hombre de mi raza y
a una mujer. Él era como de unos cincuenta años, de barba
rubia y cerrada, ojos pequeños e inquisitivos, pelo escaso
y cano. Vestía pantalón blanco de montar, botas altas,
camisola abierta y casco de corcho blanco. De una
cinta que le pasaba alrededor del cuello le colgaban unas
gafas de esas que montan a caballo sobre las narices. La
mujer era joven, tendría unos treinta años, rubia, de cara
fina e inteligente, cuerpo alto y esbelto, como acostumbrada
al ejercicio.

El hombre me habló en castellano:

---Soy el profesor Wassell. Los indios no han querido
hablar con nosotros y me han dirigido a su casa.

---Mucho gusto en conocerlo ---le contesté---. Le ruego
que pase, lo mismo que a la señora.

---Es mi secretaria, la señorita Johnes ---repuso el
hombre, entrando en mi choza. La mujer lo siguió. Yo
me senté en la cama y puse cara de expectación como
aguardando que me dijeran el motivo de su visita. El
hombre fue el que siguió hablando. La mujer apenas si se
había sonreído una vez y no había hablado para nada.

---Venimos en busca de los indios lacandones para
hacer unos estudios sobre ellos y ver qué posibilidades
hay de incorporarlos a la civilización. Estamos comisionados
por el Gobierno Federal y el Gobierno del
Estado. En San Quintín nos dijeron que tal vez lo encontraríamos
a usted, aunque nadie supo decirnos su
nombre.

---Aquí me llaman Tecolote Sabio ---le repuse.

La mujer sonrió, pero el profesor siguió hablando.

---Ninguno de los indios ha querido hablar con nosotros,
por más que nuestros intérpretes han tratado de
ponerse en contacto con ellos. Uno de nuestros guías dice
que ha encontrado rastros de que las veredas y pasos
han sido cegados para que no pudiéramos llegar al caribal.
¿Por qué han hecho eso?

---Porque yo se los mandé ---le repuse---. No queremos
aquí intromisiones de la mal llamada civilización; y
desde ahora lo prevengo: si se le ocurre a usted dar
aguardiente a los indios, o enseñarselos o tomarlo frente
a ellos, lo mato.

La mujer ahora me veía interesada. El profesor se levantó
digno.

---Nosotros no venimos a emborrachar a los indios
---me dijo cortante---. Claro está que traemos bebidas
para nuestro personal y para…

---Pues le ruego que ahora mismo me las entregue todas
para que sean destruidas. De no hacerlo así habrá
perdido su viaje, ya que ordenaré a los indios que no hablen
con usted, y ellos me obedecerán.

---Le digo que soy el representante del Gobierno..

---A mí ---le repuse lentamente--- todos los gobiernos
me salen sobrando. Aquí el gobierno soy yo, yo soy la
ley, yo soy los tres poderes y cuantos poderes se me dé
la gana ser. Así que ya lo entiende, y si no le gusta, puede
largarse.

El profesor se levantó y salió de mi choza dignamente.
La mujer, sentada en la mesa, con los pies columpiándose
en el aire, se me quedó viendo, entre sonriente y
admirada. Pareció que iba a decir algo, pero de pronto
saltó al suelo y salió tras del profesor. Yo me quedé en
la puerta viendo hacia dónde iban. Se dirigieron hacia el
caribal y vi, cerca de él, a los que formaban la caravana,
madereros y cargueros y otros dos hombres con cascos.

</section>
<section epub:type="chapter" role="doc-chapter">

# XIII

Sol Bueno me dijo que ya habían vuelto varios de los
mensajeros y que todos los cuerpos aprobaban nuestro
plan, fijando como fecha de iniciación de la guerra el
principio de la temporada de lluvias. Estábamos en enero,
así que faltaban por lo menos unos cuatro meses en
los que se podría preparar todo. A nuestro cuerpo, junto
con algunos otros de la zona de Chiapas y Tabasco,
nos había tocado el ataque a los campos petroleros de
Minatitlán y la paralización del ferrocarril y del puerto.
Debíamos luego trasladar nuestros tesoros de larvas a algún
lugar propicio del río Coatzacoalcos y avanzar hacia
el Norte, hasta despoblar Veracruz y unirnos, allí con los
cuerpos del Norte que combatirían en Tampico y toda
esa zona petrolera. Lograda la unión, avanzaríamos a la
meseta Central para atacar las grandes ciudades.

Simultáneamente sucedería lo mismo en todos los
países dentro de los trópicos, y creíamos que cuando ya
estos hubieran sido destruidos podríamos dictar nuestras
condiciones a todos los hombres del mundo. Si no las
aceptaban dejaríamos pasar el invierno, ocupando con
nuestros tesoros y arsenales los puestos más avanzados al
norte y sur para, con la primavera, atacar a los países del
norte, llevando, si resultaba necesario, arsenales vivientes.
La meta de todos los cuerpos de nuestro rumbo sería
Nueva York para el año entrante en el mes de julio. En
el territorio de los Estados Unidos hay varios cuerpos de
moscos, pero no son ricos ni poderosos, así que había que
ayudarlos. Mientras nosotros libráramos nuestras primeras
batallas, esos cuerpos se ocuparían tan solo en aumentar
sus tesoros y arsenales, para estar listos con el mayor
contingente posible en el momento necesario. Algunos de
los cuerpos del río Misisipi alegaron que bien podían
ellos solos destruir algunas ciudades y campos petroleros,
especialmente el puerto de Nueva Orleans, pero no se creyó
oportuno, por el temor de que siendo débiles fueran
derrotados y permitieran a los hombres estudiar anticipadamente
los gérmenes que se les iban inocular.

Para evitar que los hombres pudieran defenderse con
cortinas de insecticidas, se inventó una especie de máscara
que llevarían los moscos guerreros. Esta máscara la
fabricaba el mismo mosco con cierto líquido resinoso
que secreta, pero yo no me fiaba mucho en su eficacia.
Se había probado tan solo, y con buenos resultados, como
protección contra el humo que empleaban los indios.
Para probarla contra verdaderos insecticidas ideamos robar
una pequeña dosis del insecticida más activo a la expedición
que acababa de llegar y experimentar con algunos
guerreros.

Por la tarde salí de mi choza y fui al caribal. Los hombres
de la expedición no me saludaron, pero Pajarito
Amarillo se adelantó lleno de entusiasmo, haciendo
grandes caravanas. Inmediatamente me dijo cómo él no
era responsable de la llegada de los blancos, cómo había
cumplido todas mis órdenes y no había hablado con ellos
para nada.

Los expedicionarios habían levantado tiendas de campaña
junto al caribal. Las recorrí con la vista, pero no vi
por ningún lado al profesor ni a la señorita Johnes, con
lo que supuse que estaban en el interior de alguna de las
tiendas.

---Anda ---le dije a Pajarito Amarillo--- y dile al jefe
de los blancos que lo espero en mi choza para hablar con
él. Que vaya con la mujer que lo acompaña.

Pajarito Amarillo fue hacia una de las tiendas de campaña
y yo volví a mi casa. No sé por qué se me ocurrió
ordenar que fuera también la mujer, pero sentía yo dentro
de mí cierto gusto en verla y hablar con ella y sentir
la admiración que yo le causaba.

A los pocos momentos de haber llegado yo a mi choza,
llegó el profesor Wassell con su secretaria. Desde que
entró advertí que traía una pistola escondida bajo su saco
blanco ligero y me pareció observar que la mujer se
daba cuenta de que ya lo había yo notado, y se reía un
poco de ello.

---Me ha dicho el jefe de la tribu ---empezó el profesor---
que deseaba usted hablarme. Supongo que habrá
usted pensando un poco y quiere cambiar su opinión y
manera de proceder…

---No es eso exactamente lo que quería yo ---dije---.
Aún insisto en que antes de tratar con la tribu me entreguen
ustedes todo el aguardiente que tengan en su poder.

---No entiendo su actitud, señor… ---dijo el profesor
tratando de sacar mi nombre. Pero a mí no me convenía
aún que lo conociera; así que le dije:

---Por aquí me llaman Tecolote Sabio. Mi otro nombre
lo he olvidado o no quiero recordarlo.

---Bueno, llámese como se llame, le digo que no entiendo
su actitud. Nosotros traemos tan solo una misión científica.
Vengo yo, que soy miembro del Instituto Carnegie,
y otros dos compañeros, uno que es un filólogo y otro un
gran músico, que pretende recoger la música de estas tribus.
Nos acompaña la señorita Johnes, como secretaria, y
la señorita López, empleada del Gobierno del Estado. Fuera
de ellos no traemos más que algunos guías y cargadores.

---Todo eso está muy bien ---le interrumpí---; y yo no
me opongo a que hagan los estudios que quieran, aunque
me temo que de poco les van a servir. Lo único que
quiero es que me entreguen todo el aguardiente que traigan
y el insecticida que tengan.

---¡El insecticida! ---gritó el profesor---. Usted debe
estar loco, señor… Tecolote Sabio.

La mujer sonrió imperceptiblemente. Tal vez solo yo
noté que se había reído.

---Aquí no hay moscos. No necesitan ustedes el insecticida
que tengan ---le dije---. Yo lo necesito para cierto
experimento que estoy haciendo…

---¿Usted es también un investigador? ---preguntó la
mujer.

---Sí, señorita ---le contesté---. Pero lo que investigo
no lo podrían entender ustedes. Investigo a los moscos.

---Muy interesante ---interrumpió el profesor---.
¡Muy interesante! Yo entiendo algo de zoología y pensaba
hacer algunos estudios…

---Conmigo no los hará usted ---repuse cortante---. O
tal vez sí los hagamos juntos ---añadí con un nuevo pensamiento
en la cabeza---. Tal vez usted y su gente me
ayuden mucho para mis experimentos.

---¡Encantado! ---gritó casi el profesor---. Yo no sabía
que usted fuera un investigador. Puede usted contar
conmigo, pero yo quisiera que, en cambio de nuestra
ayuda para sus experimentos, nos permitiera…

---Cuando tenga yo en mi poder todo el aguardiente
---le repuse--- podrán ustedes hablar con los indios y hacer
lo que quieran, aunque me temo que pierden su tiempo…

La mujer rió de nuevo, con una risa suficiente y confiada
que me molestó. El profesor pensaba y dijo al fin:

---Bien pensado, señor… hum… señor Tecolote Sabio,
no creo que haya en verdad ningún inconveniente para
entregarle las bebidas que traemos, siempre y cuando nos
prometa usted guardarlas por si se presenta una enfermedad
que nos haga…

---Aceptado ---le dije---. Yo guardaré las bebidas.

El profesor se levantó y salió, prometiendo mandar
inmediatamente todas las botellas que tuviera en el campamento.
La mujer se quedó sentada en la mesa, viéndome
con una sonrisa rara. Cuando se hubo cerrado la
puerta tras de su jefe, me dijo:

---Dígame usted, Tecolote Sabio, ¿qué quiere decir
cuando afirma que nuestros estudios saldrán sobrando?

---Son cosas que yo sé ---le repuse--- pero que no deben
molestarla.

---No veo por qué nuestros estudios no han de servirnos
para nada. Yo creo que se sacará mucho provecho
de esta expedición: se conocerá mejor a las tribus que
habitan estos lugares…

---Sí, se les conocerá mejor, pero ya demasiado tarde.

---¿Por qué?

---Pues… porque siempre es tarde ya para conocer las
cosas.

---¿Lo dice usted porque considera que estos indios
se están acabando?

---Tal vez por eso ---le contesté---. O tal vez porque
considero que todo lo que hace el hombre es inútil…

---¿Y a pesar de eso usted investiga?

---Sí, investigo, pero cosas más serias.

La mujer me veía riendo con algo de burla. Esto me
obligó a decirle:

---Investigo cosas tan serias que de ellas depende, tal
vez, todo el porvenir de la humanidad.

---¿Sí? ---dijo ella, aún sonriendo.

---Sí ---le repuse---. Mis investigaciones es lo único útil
que se está haciendo en el mundo. Llegará un día en que
sepa usted qué es lo que investigo y no se reirá de mí…

---Pero si no me estoy riendo ---interrumpió---. Me
interesa usted mucho, señor don Tecolote Sabio, y creo
que en muchas cosas nuestros puntos de vista coinciden.

---¿En cuáles? Usted nada sabe de mí, ni de lo que
pienso, ni de lo que voy a hacer.

---Efectivamente, nada sé de eso. Pero he observado y
visto. Ha de ser muy divertido convertirse de pronto en
dios, aunque sea tan solo de unos cuantos indios salvajes.

---¿Cómo? No entiendo lo que dice ---le contesté, poniendo
cara de asombro, pero comprendiendo, con cierto
temor vago, que ya ella se había dado cuenta de mi
posición en relación a mis amigos los lacandones.

---Para ser dios, señor Tecolote Sabio, me parece que
entiende usted muy poco. Pero, con permiso, voy al río
a ver si encuentro un lugar donde bañarme.

Y diciendo esto salió de mi choza. Yo me quedé pensativo.
Quise pensar en mi futuro poder, pero de una
manera o de otra, siempre acababa pensando en la señorita
Johnes e imaginando toda clase de respuestas que la
humillaran y la obligaran a dejar de reírse de mí.

En esto entraron dos de los miembros de la expedición
cargando algunas cajas que contenían aguardiente
de diferentes especies. Les ordené que las pusieran en
un rincón y me fueran a traer todo el insecticida que tuvieran
en el campamento. Salieron, prometiendo volver
con lo pedido, no sin echar miradas curiosas hacia la mesa
donde reposaban los dioses de Pajarito Amarillo. Al
quedarme solo, llamé a Sol Bueno. Estaba este encaramado
en el techo y vino al instante:

---Ya tengo insecticida para probar las máscaras ---le dije---.
Fuera bueno que llamaras a varios guerreros para que
se pusieran sus máscaras y lo probáramos cuanto antes.

Casi a un tiempo entraron los cargadores con una bomba
esparcidora y dos botes de insecticida comercial, y Sol
Bueno seguido por unos cien guerreros ya con sus máscaras
puestas. Llené el recipiente de la bomba y ordené a los
guerreros que se pusieran en un sitio frente a mí, y a los
moscos que iban a presenciar el experimento que se colocaran
detrás, temeroso de matarlos también a ellos…

Los guerreros se amontonaron todos en un rincón,
donde no había corriente de aire, y empecé a rociarlos
con el insecticida. Cuando hube saturado bien la atmósfera,
dejé la bomba y me acerqué a los otros moscos para
esperar el resultado del experimento. De pronto, dos
o tres de los guerreros cayeron al suelo, pero los otros
seguían revoloteando. Recogí los cadáveres de los tres y los
puse frente a Sol Bueno para que los examinara:

---Las máscaras de estos estaban mal hechas ---me dijo---
y el gas les ha penetrado. Por eso han muerto y lo
merecían por ser tan descuidados. Ordenaré que sus
nombres se olviden para siempre.

Los otros guerreros del experimento seguían bien y
Sol Bueno les ordenó que se adelantaran. Uno de ellos
dijo:

---No hemos sentido nada pero creo que para perfeccionar
estas máscaras, será necesario que cubran todo el
cuerpo, pues cuando el líquido entra en contacto con la
piel, esta arde terriblemente, y creo que si fuera mucho
el líquido moriríamos.

---Id al arsenal ---les repuso Sol Bueno------ y haced las
pruebas necesarias.

En ese momento se abrió la puerta y entró la señorita
Johnes. Yo tenía aún la flauta en la boca.

---Se me olvidó preguntarle si no era peligroso bañarse
en el río ---dijo al entrar---. Pero veo que a usted le
gusta la música. Se lo diré al señor Godínez, que es músico
de la expedición.

---En el río puede usted bañarse cuanto quiera, con
tal que no se aleje mucho de la orilla, porque hay caimanes
---le repuse---. Además, yo no soy afecto a la música
ni tengo tiempo para esas simplezas. Esta flauta es
parte de mis investigaciones.

Mi tono cortante pareció no importarle gran cosa.
Tranquilamente entró a mi choza y se sentó a la mesa.

---Me interesa usted sobremanera, señor Tecolote
Sabio ---me dijo---. Le ha sonsacado usted al doctor
Wassell todo su insecticida, diciendo que aquí era inútil
y ahora veo que lo ha usado en su casa a profusión.
¿También eso será parte de sus investigaciones?

---Sí es, y le ruego que me deje, pues tengo mucho
trabajo por delante.

Ella, sin moverse, se rió abiertamente.

---Y yo que pensaba ---dijo--- que el encanto de llegar
a ser un dios era justamente que ya no habría necesidad
de trabajar. Sabe usted, don Tecolote: oí decir a unos indios
que usted era Kukulcán, o sea el Quetzalcóatl de los
nahuas, y eso me ha interesado mucho.

---Los indios no saben lo que están diciendo ---contesté
molesto---. Porque les he hecho ciertos favores insignificantes
es que me han tomado por un dios…

---Error que usted se ha olvidado de contradecir.
¿Verdad? Tal vez tenga razón…

---¿En qué?

---En no contradecirlo. No cualquiera llega a ser dios.
Además, los indios me han dicho que usted los ha librado
de los espíritus malos…

---Por lo que veo los indios han hablado mucho con
usted.

---Los hombres, no; pero con las mujeres es fácil. Me
meto en las casas a pedirles alguna cosa y entablo la charla.
Una de ellas me dijo que usted los había librado de
los moscos…

Bajé la cabeza sin querer contestar nada. No me convenía
que se empezara a hablar de ese tema tan peligroso
antes de que todo estuviera preparado. Ella siguió,
como divirtiéndose al ver mi pena:

---Me dijo que cuando usted tocaba su flauta convocaba
a los espíritus malos y a los buenos y les ordenaba
a los moscos que no molestaran a la tribu. Me había interesado
mucho, pero ahora veo que no es la flauta sino
el insecticida lo que usa usted para ahuyentar a los moscos
y eso le quita mucho de su encanto a la situación.
Pero, como mi charla no le divierte, me voy al río a bañarme.

Por un momento sentí un extraño impulso de detenerla,
pero la dejé salir sin decir una palabra. No sé por
qué, yo que he olvidado tantas cosas, recuerdo perfectamente
todas las palabras que me dijo ella y las sanciones
que tuve al recibir esas palabras dentro de mí.

</section>
<section epub:type="chapter" role="doc-chapter">

# XIV

Toda la noche medité en la conversación que había tenido
con la señorita Johnes, y de pronto comprendí que
la presencia de hombres blancos junto a mí volvía a turbar
mi tranquilidad. Tuve deseos inmoderados de probar
el aguardiente que me acechaba en el rincón de la
choza, pero el miedo de empezar de nuevo me tuvo inmóvil
en la hamaca. De pronto comprendí que en mi loca
carrera hacia el poder supremo entre los hombres, había
yo dejado de odiarlos como en los días en que sus
injusticias me impulsaron a perder mi existencia entre
las selvas acogedoras. Pero ahora, con los hombres de
nuevo frente a mí, volvía a sentir ese odio y el deseo
inaudito de venganza, que me daba fuerza para seguir
adelante con el plan que teníamos ya trazado.

«El mundo está en mis manos», pensaba yo. «Yo soy
el dueño de todo lo que tiene el hombre sobre la faz de
la tierra: sus bienes, sus vidas, sus mujeres».

Y pensando en las mujeres, renació la imagen de la señorita
Johnes, de esa mujer burlona, que parecía reír de todo
lo que decía yo y que a su vez, buscaba mi compañía.

Antes que amaneciera me levanté y salí a sentarme
junto al río. Tal vez si en ese momento, arrepentido, hubiera
buscado la ayuda de los hombres, no hubieran pasado
ninguna de estas tragedias. Tal vez debí dirigirme
a Dios, pero yo entonces, cercano a mi gran poder, no
creía en Dios. Él había creado al hombre y lo había hecho
amo y señor del mundo, amo y señor de todas las cosas
creadas; pero yo, con mi poder, iba a destruir la idea
de Dios, iba a reducir a los hombres, a la creación más
perfecta de Dios, a la calidad de ganado que se compra
y se vende. Pensando en esto no podía creer en Dios, un
Dios desconocido para los moscos, que iban a ser los
amos de la tierra. ¿O sería desconocido para ellos?
Nunca había yo tratado ese punto con Sol Bueno y más
me valía no tratarlo. Ni siquiera sabía yo qué palabra en
el idioma mosquil pudiera significar Dios.

Pero si Dios existía, no iba a permitir que el hombre
se extinguiera sobre la faz de la tierra antes de que
llegara el momento inevitable, pensado por Él. O tal
vez, aburrido de nuevo de las maldades de los hombres,
iba a usar de los inconscientes moscos y de mí, de mi vanidad,
de mi orgullo y de mi resentimiento, para castigar
al hombre y hacerlo volver a la senda del bien, a la
unión completa.

En ese momento, sentado junto al río, yo comprendí
que no debía pensar en esas cosas, pero pensaba en ellas
sin querer: me asaltaban, sentía un temor indescriptible,
un temor vago y a la vez concreto, frente a Dios. Y por
eso me aseguraba a mí mismo, diciéndome que Dios no
existía, que el mundo se había formado por la casualidad
y no sé cuántos desatinos inspirados en unos libros seudofilosóficos
que leí allá en mis mocedades. Y si el mundo
se había formado por la casualidad, esta ponía en mis
manos poderosas, en mi inteligencia, el poder de destruir
el orden reinante y crear otro. Entonces yo era tan solo
una fuerza ciega de la casualidad; y esto me rebelaba, hacía
temblar mi alma, llena de orgullo. Era mejor ser un
peón en el juego de ajedrez de un Dios inteligente, de un
Dios creador y poderoso; pero ahora era mejor aún lo que
me había enseñado mi madre: ser una entidad libre, aun
frente al Dios creador, al Dios que nos había creado a su
imagen y semejanza. Pero si esto era verdad, si la cándida
fe de mi madre era la cierta, por más que yo la hubiera
rechazado hacía mucho tiempo, entonces yo estaba no
tan solo traicionando a la raza humana, sino que también
a mi Dios, al Dios que me había creado; y me estaba vendiendo
a mí mismo a cambio de un poder sobre esclavos.

Creo que esa mañana fue cuando estuve más cerca del
arrepentimiento. El poder que iba a adquirir no me parecía
ya tan hermoso ni tan dulce, visto a través del
Señor. Recuerdo que por un momento estuve a punto de
levantarme y buscar al doctor Wassell para pedirle que
me llevara lejos de la selva, lejos de esa terrible tentación.
Pero a mi espalda sonaron voces y, volviéndome,
vi a uno de los expedicionarios, que luego supe que era
Godínez el músico, charlando con la señorita Johnes, a
quien llevaba abrazada rumbo al río. Rápidamente me
oculté en unos matorrales y los vi cómo se sentaban en
el mismo sitio en el que había estado sentado yo, y, las
cabezas casi unidas, charlaban.

Entonces, de nuevo invadido por una cólera que no
alcanzaba a comprender, fragüe la última parte del plan,
la más perversa. Ya conquistado el mundo, se le prohibiría
al hombre todo contacto con Dios, toda religión.
Había que explicarle al Consejo Superior el porqué de
esta medida. El Consejo tenía que comprender el peligro
que había para nuestros planes en esa comunicación
entre Dios y el hombre, en esa inspiración a Dios que
hace del hombre un ser libre y fuerte, cosa que había que
evitar.

Sol Bueno zumbó en mis oídos su salutación; y yo, tomando
mi flauta, que traía siempre atada al cuello, le
contesté y le expuse el nuevo artículo que había que incorporar
al plan de gobierno de los hombres.

---¿El Ser Superior? ---me dijo, usando el término
que había yo empleado para darle a entender mi idea---.
Nunca he oído hablar de él, no sé a qué te refieres.

---El hombre ---le expliqué--- cree en Dios, que es ese
Ser Superior, que lo creó y le dio todo cuanto tiene. Cree
que le dio, sobre todo, la libertad de pensar, de obrar,
de escoger entre el bien y el mal.

---Ya comprendo ---me dijo---. Dios es el Gran Consejo
de los hombres.

---No, Dios está sobre los grandes consejos y los consejos
superiores. Dios es infinito, es todo poderoso. Dios los
ha creado a ustedes y ha creado al hombre de la nada…

---No te entiendo ---me interrumpió---, pero si quieres
pasaré tus palabras a los representantes del Consejo
Superior, cuyo nombre no se puede decir, ya tal vez ellos
te entiendan. Pero si crees que nos puede perjudicar en
algo el que los hombres crean en eso que dices, les quitaremos
la creencia.

---Nunca los hombres serán verdaderamente esclavos
mientras crean en Dios ---le repuse---. El es el principio
más firme de la libertad, y no es tan fácil como crees el
quitarles esa creencia.

---Todo se puede hacer ---me dijo---. Llevaré tus palabras
y el Consejo ha de resolver algo.

Apenas se hubo ido Sol Bueno, una proveedora se
acercó y me zumbó en el oído:

---He oído tus palabras ---me dijo--- y escuché también
otras que pronunciaste frente al Innombrable.
Quiero que me expliques por qué los hombres son libres
y por qué nosotras, las proveedoras y los exploradores,
no somos libres. Si Dios nos creó a todos, todos debemos
ser libres.

---Sí ---le repuse---, todas podrían ser libres si creyeran
en Dios, si tuvieran un Dios, pero ni siquiera yo creo
ya en Él.

---¿Por qué?

---Pues, porque… por razones mías.

---Pero si el Gran Consejo cree en Dios, nosotras debemos
creer en Dios ---me dijo---. Nosotras creemos todo
lo que el Gran Consejo cree y nunca nos ha hablado
de Dios ni hemos oído a los recordadores que lo mencionen.
Si el Gran Consejo lo supiera, sin duda nos lo diría.
Pero adiós, me tengo que ir a tomar la sangre de esos
dos que están en la playa.

La señorita Johnes y Godínez se habían levantado y
caminaban lentamente río arriba, por toda la playa. Ella
había dejado su mano en la de él y hablaban en voz baja,
como se hablan los enamorados. Yo me quedé entre
las malezas, acechando sin saber por qué.

Algunos niños lacandones llegaron hasta la orilla del
agua y pronto me vieron, porque lo que para los de mi raza
está oculto en la selva, para los lacandones está a la vista.
Después de saludarme con grandes reverencias, me pidieron
unas hojas de papel. En lugar de dárselas, salí con
ellos de mi escondite y les hice sus barquitos y jugué
con ellos, haciendo tanto ruido que la señorita Johnes tuvo
que volver la cara y vernos, que era lo que yo quería.
Con su compañero se acercó a nosotros y contempló durante
un rato los barquitos y a los niños que jugueteaban con
ellos en el agua. Luego, sonriendo como siempre, me dijo:

---Me parece que no conoce usted al señor Godínez.
Es el músico de nuestra expedición. El señor Godínez, el
señor Tecolote Sabio.

Nos saludamos con la mano, pero yo noté en ella cierta
burla, probablemente como para indicarle a su compañero
lo cómico del nombre que me habían puesto y
que usaba por no saber otro. Godínez parecía verme
también con cierta burla. La señorita habló primero:

---Está despachando a los malos espíritus, según veo
---dijo---. Una de las mujeres indias me contó ese estupendo
sistema de despachar a los malos.

---Las mujeres indias le cuentan a usted muchas cosas
---le dije---. Esto es tan solo una diversión para los niños…

---Pues los grandes la han tomado muy en serio ---me
interrumpió.

---Y yo ¿qué culpa tengo de eso? ---grité ya casi enojado.

---Ninguna ---me dijo ella sonriendo, siempre sonriendo---.
Tan solo me pareció chistoso.

Sin contestar me retiré a encerrarme en mi choza a
pensar a solas. Pero no fue posible, porque se me presentó
el doctor Wassell con todo el resto de los expedicionarios
para presentármelos y decirme que iban a iniciar
sus trabajos. Estuve lo más amable que pude, le
enseñé dónde había guardado su aguardiente y los despaché.
Apenas se hubieron ido me habló Sol Bueno:

---Los representantes del Consejo Superior, que no
podemos nombrar, quieren hablar contigo para discutir
el proyecto nuevo que tienes. Así que te esperan junto
al caobo y fuera bueno que me acompañaras.

Los miembros representantes de los Consejos
Superiores, antes de iniciar las pláticas, hicieron que se
retiraran todos los moscos que estaban junto a ellos,
mandando que se pusiera una barrera de guerreros alrededor
de nosotros para que no nos interrumpiera nadie.

---Sol Bueno ---me dijo uno de ellos, cuando estuvimos
completamente solos--- nos ha dado tu mensaje. Por
tu culpa podemos vernos en grave peligro, pero no te
acusamos de ello, pues no sabías nada.

---No entiendo lo que me quieres decir ---le dije.

---Has hablado de Dios ---me dijo---. Y eso es peligroso.
Nosotros nunca hablamos de Él; el pueblo, la gente
menuda, no debe ni siquiera conocer su existencia,
porque sería peligroso para nuestra organización.

---Entonces ---les dije--- ustedes sí conocen a Dios.

---Sabemos de Él ---repuso el que hablaba---. Pero
tan solo lo sabe el Consejo Superior y nunca habla de
ello. Cuando estábamos unidos en un solo cuerpo lo adorábamos
como es debido. Eso fue mucho antes de que
apareciera el hombre sobre la tierra. Ahora lo adora el
Gran Consejo de cada cuerpo, pero las células dispersas
nada saben de ello ni los recordadores guardan esas palabras.
Si las proveedoras, por ejemplo, se enteraran de
la existencia de Dios, se creerían iguales a nosotros y se
acabaría nuestra organización tan perfecta.

---Ahora comprendo ---le dije---. No tengan cuidado,
no volveré a hablar sobre ello.

---Está bien. En cuanto a los hombres, fuera bueno
hacer por quitarles toda idea de Dios; y, para ello, diremos
a nuestros súbditos que los hombres creen en tonteras
y patrañas. Pero tú no vuelvas a hablar de eso.

---Así lo haré, no teman ---les contesté---. Ahora
quiero hablarles de otro asunto. Han llegado aquí varios
hombres y creo que la suerte nos los manda. En ellos tenemos
un perfecto arsenal viviente y sangre bastante para
alimentar a los gérmenes y a los miembros del Gran
Consejo mientras fraguan nuestros planes.

---Ya lo habíamos pensado ---me dijo el que hablaba---.
Y así se hará.

---Creo oportuno hacerlos prisioneros, ya que piensan
estarse poco tiempo ---les dije---, y para ello hay que
darles una muestra de nuestro poder y debo hablarles yo
antes. Posiblemente sea necesario matar a uno o dos de
ellos, pero con los que quedan nos basta para nuestros
fines.

---Haz como quieras y manda a Sol Bueno. Él te dará
inoculadores de gérmenes y todo lo que te sea necesario,
pero que no se vayan y, sobre todo, que no se escape
uno solo después de que hayas hablado con ellos,
pues aún no es oportuno que el mundo de los hombres
sepa de nuestros planes.

---Así lo haré ---le dije---. Ya había yo pensado en
ello.

Con esto me despedí de los representantes y me retiré
a mi choza a madurar el nuevo plan que se me había
ocurrido. La señorita Johnes ya no se iba a reír de mí.

La señorita Johnes fue a visitarme esa noche, cuando
ya había caído el sol. Como siempre, sonreía y parecía
estar como en su casa. Entró sin pedir permiso, se sentó
en la hamaca, columpiándose lentamente mientras me
miraba. Yo fingía no darme cuenta y seguía ocupado con
mis cuadernos de apuntes, pero con el rabo del ojo la observaba.
Vestía una enagua blanca corta y una blusa ligera;
no llevaba medias y tan solo unos guaraches extraños.
El pelo rubio le caía sobre los hombros como si no
se hubiera peinado, pero yo adivinaba que mucho cuidado
había puesto al acomodarlo para dar esa impresión y
sentía cierto gusto dentro de mí imaginando que todo
ese arreglo era para agradarme. Pero ¿por qué pretendía
agradarme esa mujer? Y, además, a mí nada me importaba
el que se preocupara por mí. Yo estaba ocupado
en grandes cosas, tan grandes que ella era incapaz de
entenderlas o adivinarlas.

De pronto me dijo:

---Los indios me han contado que habla usted con los
moscos, valiéndose de esa extraordinaria flauta.

Por un instante me quedé desconcertado. Luego pude
decirle:

---Los indios no saben lo que están diciendo. ¿Quién
ha logrado, alguna vez, hablar con un animal?

---Sabe usted, señor Tecolote…

---No me diga así ---le interrumpí---. Tengo mi nombre.

---Pues yo no lo sé y usted me dijo que así le dijera.

---Eso se lo dije al profesor Wassell ---le contesté---.
Pero usted debería llamarme por mi nombre.

Entonces le dije mi nombre, pero ella tan solo se rió
y me dijo que le gustaba más el de Tecolote Sabio.

---Sabe usted ---siguió diciendo---: yo he leído muchas
novelas de misterio. Tal vez por eso veo misterios
en todos lados y usted me parece un misterio. En primer
lugar, ¿qué hace aquí? No es usted uno de tantos explotadores
de indios y no veo que se afane por buscar oro
o maderas preciosas. En el mundo civilizado podría usted
llegar a algo…

---¡Eso creí yo también hace tiempo! ---exclamé.

---Pero no tuvo usted la bastante fuerza para sobreponerse
al mundo y se dejó vencer y vino a ocultar su desencanto
y su amargura en el fondo de esta selva maldita.

---Más o menos esa es la historia ---contesté. No sé
qué, dentro de mí, me impulsó a hablar; algo dentro de
mí me obligaba a contarle a esta mujer toda la amargura
que llevaba, todo el odio y el dolor. Tal vez ella me comprendiera,
tal vez ella me salvara---. Esa es la historia
---repetí---, la vulgar historia. Pero, como dice usted, yo
pude ser algo y lo voy a ser aún, voy a ser algo más de lo
que había soñado ser. Pero para llegar a esto he atravesado
por los caminos de la amargura y del odio…

---Ve usted cómo tengo razón ---me interrumpió riendo---:
hay en usted algo misterioso y los misterios me
fascinan.

No sé por qué su interrupción me molestó. Volví a
sentarme frente a la mesa y abrí de nuevo mi cuaderno.
Por fin me había yo decidido a abrirle el corazón a un ser
de mi raza, creyendo que me comprendería; pero esa interrupción
estúpida me cerraba las puertas y me dejaba
vacío como antes. Ella seguía hablando sin darse cuenta.

---Antonio, el señor Godínez, va a venir dentro de un
momento por mí. Me convidó a dar un paseo por la playita,
a falta de otra distracción.

---Y entonces ---le pregunté---, ¿para qué ha venido
usted a mi casa? ¿Por qué no salió con él del campamento?

---El profesor Wassell me pretende y no quiero ofenderlo.

---Pero usted sale con el músico a escondidas.

---No a escondidas ---replicó ella poniéndose seria---.
Pero no quiero que el profesor se dé cuenta ya de que el
señor Godínez y yo somos novios. Le tengo mucho cariño
y mucho respeto al profesor y me dolería mucho que
se molestara. Poco a poco quiero ir metiéndole en la cabeza
la idea de que nunca me he de casar con él. Pero
eso a usted no le interesa. Yo vine a su casa porque me
gusta su charla misteriosa. Como le digo, yo…

---¿Y piensa usted casarse con Godínez? ---le pregunté.

---Sí ---me contestó---. Nos casaremos apenas hayamos
vuelto a la civilización. ¿Cree usted que seremos felices,
señor Tecolote?

No le contesté nada. No puedo recordar en qué pensaba
yo en esos momentos, pero sí recuerdo que el corazón
me dolía ante la maldad humana. Esa mujer sonriente
y hermosa engañaba al profesor, se burlaba de mí. El
mundo era el de siempre y los hombres no tenían remedio.
Había que aniquilarlos por completo o dominarlos en
tal forma que vivieran en adelante como ganado. Solo así
irían por el camino recto, solo siendo esclavos de una raza
superior se librarían de todo el peso de sus maldades.

La entrada de Godínez cortó mis meditaciones.
Apenas si me saludó por dirigirse inmediatamente a la
señorita Johnes.

---Vamos ---dijo ella levantándose.

Me quedé solo, pero con una nueva amargura. ¿Pero
qué amargura podía tener el hombre que iba a ser el amo
del mundo? Dejé a un lado mis pensamientos y me dediqué
a madurar mi plan para capturar a los expedicionarios.

</section>
<section epub:type="chapter" role="doc-chapter">

# XV

Temprano en la mañana mandé llamar al profesor Wassell,
rogándole que se presentara en mi choza con toda
su gente, ya que era necesario que hablara yo con ellos.
Cuando llegaron los acomodé como se pudo y me senté
frente a ellos, tras la mesa. La señorita Johnes se quiso
sentar a la mesa pero le rogué que se colocara junto a los
otros. Así lo hizo sin protestar, pero viéndome con curiosidad
y sonriendo siempre.

«Pronto dejará de sonreír», pensé.

Ya que todos estuvieron acomodados, los principales
al frente y los cargadores y chicleros al fondo, empecé
mi discurso con voz lenta y clara, para que todos los presentes
me pudieran entender perfectamente y no hubiera,
más tarde, malas interpretaciones.

---Señores y señoras ---les dije---, varias veces les he
dicho a ustedes, especialmente al doctor Wassell y a la
señorita Johnes aquí presentes, que estaba yo llevando a
cabo un experimento importantísimo, tal vez el más importante
que se ha llevado a cabo en el mundo. La señorita
Johnes se ha reído constantemente de mí por esto y
me ha parecido que el profesor Wassell no ha creído mucho
en mis dotes de investigador…

---¡Oh, no! ---exclamó el profesor---. Yo nunca…

---Le ruego que no me interrumpa ---dije---. Ahora
bien, creo que ha llegado el momento de darles a conocer
mis experimentos y rogarles que me ayuden a llevarlos
a feliz término…

---Estamos completamente dispuestos ---volvió a interrumpir
el profesor---. Desde el otro día me permití
decirle que nos agradaría mucho cooperar con usted en
sus investigaciones, pero no teniendo ni la más remota
idea de lo que tratan, no he podido…

---Ahora lo sabrá usted. En pocas palabras se trata de
esto. Yo he aprendido el idioma de los moscos. No me
interrumpan por favor ---me apresuré a añadir viendo
que varios de los presentes pretendían hablar---. Sí, he
aprendido el idioma de los moscos, el idioma que hablan
todos los moscos del mundo, y, por medio de ese idioma,
me he comunicado con ellos, hasta llegar a conocer
perfectamente su organización y su vida. Como ustedes
comprenderán, creo que es la primera vez que tal cosa
pasa en el mundo: es la primera vez que un ser de la creación
logra aprender el idioma de alguno de los animales
que llamaríamos irracionales y comunicarse con ellos.

Varios de los presentes, especialmente el tal Godínez,
esbozaron una sonrisa idiota. Pero yo ya sabía que no
iba a ser comprendido y no me importaba. Lo único interesante
era que se dieran cuenta cabal del papel que
iban a desempeñar en la organización del nuevo mundo
del cual yo sería el amo. Con toda clase de pormenores
les expliqué lo que se pretendía de ellos, que no debían
alejarse del lugar en que estábamos y las leyes que desde
ese día regirían sus vidas, o sea, las leyes que habían
dictado los moscos para el día de la victoria.

Mientras hablaba, el profesor parecía aburrido, la señorita
Johnes me veía con curiosidad y los otros ponían
cara de interés, como cuando está uno oyendo un cuento
divertido. Pero a las claras se veía que no creían una
palabra de lo que les estaba diciendo. Por eso concluí así,
después de hablarles durante dos horas:

---Naturalmente que el espíritu científico de algunos
de ustedes y la tonta incredulidad de los otros pedirán
pruebas de lo que les he dicho. La prueba es terrible, pero
va a ser definitiva. Un mosco picará al señor Godínez
esta misma tarde, y dentro de unos días estará muerto
de una enfermedad hasta ahora desconocida por el hombre.
Primero se le hará una ampolla en el lugar del piquete,
luego esa ampolla crecerá hasta cubrirle todo el
cuerpo y morirá entre dolores atroces. Esa es la prueba
que les ofrezco.

---Yo creo que está loco ---dijo la señorita Johnes.

---Mi prueba es irrefutable ---alegué yo---. Cuando el
señor Godínez haya muerto…

---¿Y cómo sabremos que murió de una enfermedad
contagiada por un mosco? ---preguntó de nuevo la señorita
Johnes---. Es fácil para usted el darle una hierba
venenosa que provoque una enfermedad como la que
acaba de describir…

---Un momento, por favor ---interrumpí---. Quiero
que la prueba que voy a ofrecerles sea completamente
científica, completamente controlada. Para ello quiero
que el sujeto de la experiencia se recluya en su choza y
no coma más que lo que él mismo prepare, para evitar la
posibilidad de envenenamiento que ha sugerido la señorita
Johnes. El profesor Wassell y las personas que así lo
deseen pueden quedarse en la choza con el señor
Godínez, para cuidar de él y ver que no se haga trampa.
Esta noche habrá dentro de la choza una gran cantidad
de moscos, pero tan solo picarán al sujeto del experimento,
así que el resto de los que están presentes pueden no
tomar precauciones. El sujeto puede tomar las precauciones
que guste, pero desde ahora le digo que serán inútiles,
porque tanto los moscos como yo hemos resuelto
que el señor Godínez muera, para experimentar en él esta
nueva enfermedad, y el señor Godínez va a morir irremediablemente.

---Vámonos ---dijo la señorita Johnes, levantándose
e interrumpiéndome---. Este Tecolote Sabio está mucho
más loco de lo que creíamos. Señor Godínez, acompáñeme
usted al río: vamos a ver si encontramos un lugar
donde bañarnos. Buenas tardes, don Tecolote, su charla
ha sido de lo más divertida, pero ya se va alargando
demasiado.

Y la señorita Johnes salió de mi choza, seguida por
Godínez. El profesor Wassell se les quedó viendo, como
si pensara seguirlos, pero cambió de parecer y se quedó
sentado, meditando. Claramente veía yo que meditaba
mucho más en la señorita Johnes y en el señor Godínez
que en todo el maravilloso experimento que había yo
puesto frente a sus ojos. Esto me afirmó en mi opinión
sobre la pequeñez y miseria del hombre. Para despertarlo
de sus meditaciones, dije con una voz completamente
natural:

---Es una lástima que el señor Godínez tenga que morir.
La señorita Johnes lo va a sentir mucho, porque es su novia.

El profesor levantó los ojos, pero no veía nada: los tenía
perdidos en el espacio. Lentamente se puso de pie y
salió de mi choza rumbo al campamento. Los otros se
desbandaron sin decir una palabra.

Sol Bueno se detuvo junto a mí y le conté todo lo que
había pasado con los hombres, pero sin adentrarme en
el conflicto sentimental entre el profesor y el señor
Godínez. No sé por qué, mientras hablaba con él, mis
ojos buscaban a través de la puerta la ribera del río, pero
no pude ver nada.

---¿Qué haremos entonces? ---preguntó Sol Bueno---.
Por lo que me dices, estos hombres son la crema de la
ciencia de los hombres y no creen lo que les has dicho.
Menos aún lo creerán los demás hombres.

---Vamos a demostrárselos ---le contesté---. Para ello
les he anunciado la muerte del señor Godínez. Quiero que
esta noche un cuerpo grande de guerreros y de hembras se
arme con los gérmenes de la enfermedad de que ya hemos
hablado y se ocupen en picar al señor Godínez, a quien ya
te he enseñado. Que no vayan a picar a ningún otro hombre,
porque se echaría a perder la prueba. Que lo piquen
tan solo a él, pero asegurándose de que le inoculen los gérmenes,
para que sin falta muera en el plazo dicho.

---Está bien ---me dijo---. Voy a prepararlo todo.

---Cuando el señor Godínez haya muerto ---proseguí---,
o cuando ya esté enfermo y toda la ciencia médica
de los hombres no baste para curarlo, quiero hacer
una demostración más ante estos hombres, para la cual
necesito varios escuadrones de moscos que obedezcan
mis órdenes. Entonces comprenderán que ninguno de
ellos debe apartarse de estos contornos, so pena de
muerte inmediata. Pero también han de comprender tus
guerreros que no deben tratar de matar a ninguno de los
hombres…

---Eso ya lo saben. El Gran Consejo y el Consejo
Superior han dicho que quienquiera que mate a uno de
estos hombres sin una orden del Gran Consejo será
muerto irremisiblemente.

---Me parece muy bien ---le dije---. Nos es necesario
conservar a estos hombres vivos, si es que hemos de utilizarlos
para nuestra guerra. Ahora, anda y prepara todo
lo necesario.

Desapareció Sol Bueno y yo salí de mi choza sin rumbo
fijo, pero mis pasos me llevaron a la ribera del río.
Con los ojos busqué por todo el playón. En una punta,
sentados bajo una manta que habían puesto para el sol,
estaban la señorita Johnes y Godínez, hablando en voz
baja, la cabeza de ella apoyada en el hombro de él. Me
era necesario saber qué premeditaban, así que volví a
meterme en la espesura, y con grandes precauciones me
acerqué al lugar donde estaban sin ser notado por ellos.
Bajo un caulote me acomodé y me puse a escuchar.

Mas para nada hablaban de lo que yo les había dicho
ni de la muerte que tan cerca tenía él: hablaban de amor
y de su vida futura. Por no hacer ruido no quise retirarme
y tuve que escuchar, durante toda una hora, las palabras
necias que les nacían del corazón. Por fin se levantaron
para irse y yo me pude retirar.

En el campamento no había ningún blanco a la vista.
El profesor, según me dijeron, estaba encerrado en su
tienda estudiando. Sus ayudantes habían salido de cacería
con los chicleros y los guías. Pajarito Amarillo salió
a hablarme, haciéndome grandes caravanas:

---¡Oh, gran Kukulcán, Tecolote Sabio! ---me dijo---.
Estos hombres blancos son buenos, porque no nos dan
aguardiente en el que han encerrado los espíritus del
mal, sino que nos dan mantas blancas y de colores y cuchillos
y hachas y no nos piden trabajo a cambio, tan
solo quieren que les cantemos y les hablemos mientras
ellos hacen que un círculo negro dé vueltas.

---Sí ---le dije---, son buenos y por eso he permitido
que se queden aquí, y se van a quedar para siempre, y
tú, Pajarito Amarillo, vas a ser el jefe de ellos y de grandes
tribus. Yo te lo prometo y ya ves que mis palabras
no fallan.

---Gracias, ¡oh, Kukulcán! ---me dijo---. Tú has traído
el bien entre nosotros y mis palabras no son bastantes
para decirte la alegría que hay en mi corazón y en los
corazones de los hombres y de las mujeres de mis tribus.
Pero quiero decirte una cosa. En el corazón del jefe de
los blancos hay tristeza, porque no puede comprar a la
rubia que se ha ido con el otro. Si tú eres su amigo, debes
ayudar a que la compre, porque su corazón se está
marchitando en la soledad y sufre mucho su alma. Si
quieres podemos comprarla con puna dura y buena, que
hay mucha en la selva, y con chicle.

---¿Cómo sabes tú eso? ---le pregunté.

---Los chicleros hablan de noche junto a las hogueras,
y varios de ellos hablan en maya, y yo los he oído.
También he visto la tristeza del jefe blanco y cómo sigue
con los ojos a la mujer, con la misma mirada que tiene
el cachorro cuando ve venir a su madre.

---Pensaremos sobre ello ---le dije---. Tal vez podamos
hacer algo.

---Pero, ¡oh, Kukulcán! ---añadió---, perdona la audacia
de mis palabras, que yo bien sé que no puedo aconsejarte.
También he visto en tus ojos el deseo. En las noches
he pensado sobre ello y me he dicho: «No la desea,
porque si la deseara fuera suya y ella iría a su casa y se
sentaría en el suelo a sus pies y escucharía sus palabras,
porque él es Kukulcán, el que todo lo puede». Pero ahora
me he dicho: si tú la deseas, llévala a tu casa, que toda
mi tribu cortará madera y sacará chicle para pagarla.

---Yo sé que lo harías, Pajarito Amarillo: lo sé con el
corazón y con él te lo agradezco; pero no la deseo.

Vi que mis palabras lo entristecieron y que no las creyó,
pero no se atrevió a replicar. Yo no quedé contento
con lo que le había dicho. Tal vez lo mejor hubiera sido
decirle que los dioses no deseamos a las mujeres o algo
semejante. Pero ya era tarde.

Me fui a mi choza y quise pensar en mi grandeza futura,
pero las palabras de Pajarito Amarillo me daban
vuelta en la cabeza: «Si él la deseara, ella iría a su casa
y se sentaría a sus pies y escucharía sus palabras». Pero
no era posible que yo deseara eso: yo era un hombre demasiado
grande, demasiado poderoso, para sufrir por
una mujer, para desearla. Frente a mí se abría un mundo
de oportunidades grandiosas: sería el hombre más poderoso
de la tierra, el rey más grande y temido, pero «ella
se sentaría a mis pies y escucharía mis palabras» y tal vez
pudiera yo acariciar suavemente su cabello rubio, que le
caía sobre los hombros. No, lo importante es la guerra,
es la organización perfecta, sin sentimientos. «Toda la
tribu cortaría madera de caoba y sacaría chicle para pagarla
y que sea tuya». Mía, mía, sentada a mis pies en
las noches, escuchando mis palabras, bebiéndolas con
sus grandes ojos azules, mientras yo acariciaría su cabello
rubio que le caía sobre los hombros. Pero todos los
hombres, de todos los climas, me obedecerían, escucharían
mis palabras, yo sería el amo del mundo. Ella tenía
que escuchar mis palabras, tenía que saberlas, tenía que
sentarse a mis pies…

No sé por qué el delirio hizo dar vueltas a estas ideas.
Al anochecer salí de mi choza y corrí al playón, pero estaba
ya vacío. Tan solo, en la arena estaba la marca donde
se había sentado ella y la colilla de un cigarro que había
apagado, clavándolo en el suelo húmedo.

Me lavé la cara y las manos y el agua fría me despertó.
Lentamente regresé a mi choza.

</section>
<section epub:type="chapter" role="doc-chapter">

# XVI

Sol Bueno llegó ya tarde. Yo estaba acostado en la hamaca,
luchando lentamente por restablecer el equilibrio,
después del delirio extraordinario de la tarde.

---Todo está dispuesto ---me dijo---; los escuadrones,
debidamente armados, están ya en las casas de los enemigos
y mañana podremos observar el resultado.

---Está bien ---le dije.

---Les indiqué a los recordadores que vinieran aquí a
darnos parte de lo que suceda, así que los esperaremos.

---Hablaremos mientras tanto ---le rogué, temeroso de
que me volviera ese extraño delirio, temeroso de seguir
representando en mi mente la figura de la señorita Johnes.
Y así hablamos durante largo tiempo, no sé cuántas horas.
Yo apenas si contestaba a lo que Sol Bueno me decía,
y muchas veces mi pensamiento vagó hacia cosas que yo
no quería recordar. Esta era la primera noche de mi grandeza,
era la noche en que debería estar con todos los sentidos
puestos en lo que estaba sucediendo. Debía yo hacer
como Sol Bueno, que no hacía más que hablar de
nuestro futuro poder y de la guerra que se estaba iniciando.
Pero no pensaba yo en eso. Sin quererlo, mi cerebro
se empeñaba en resucitar imágenes muertas y aparecía la
forma de mi madre, muerta hace ya tantos años, y la adusta
figura de mi padre que nunca supo educarme ni llevarme
por el camino que él había pensado. Y veía a mis abuelos,
a la dulce viejecilla esa que, cuando era yo niño, me
enseñaba la doctrina cristiana en el corredor de su casa,
sentados los dos en la sombra, ella en su equipal y yo en
un banquillo. Y luego, acabada la lección, me regalaba dulces
de nuez y unos centavos. Aún veía yo sus amplias enaguas,
negras, de raso opaco, contra las que me gustaba
apretar la cara para oír el crujido de los hilos; y su bolsa
pequeña donde guardaba las llaves de la casa, el devocionario,
el pañuelo y la costura que no dejaba nunca. Otras
veces la ayudaba yo a lavar las hojas de las camelias y a remover
la tierra de las macetas, cubiertas siempre de flores.
Yo sabía que las flores más hermosas de aquellas macetas
iban al altar de la Virgen que estaba en la sala, siempre cerrada,
así que las cuidaba con la misma unción con la que
ayudaba en misa al viejo señor cura de la parroquia.

Pero entre todas estas imágenes, se colaba en mi cerebro
el constante zumbar de Sol Bueno y el monótono
canto de la selva que nos rodeaba con su murmullo. Y
aparecía la sombra de la señorita Johnes, sentada a mis
pies, mientras yo acariciaba el cabello rubio que le caía
por la espalda hasta apoyarse en los hombros.

Entonces quería yo volver a la realidad, asirme a las
palabras que zumbaba Sol Bueno y a las que yo, por medio
de mi flauta, le contestaba; pero todo era inútil.

Aquella noche fue la peor que he pasado en muchos
años. Era la noche más grande de mi vida: yo lo sabía, lo
sentía dentro de mí; pero en lugar de pensar en ello, se
me venían otros pensamientos que debieron desaparecer
hace muchos años o que nunca debí tener, y esos pensamientos
se me hacían angustiosos en la garganta, me
apretaban el pecho, como una mano pesada puesta sobre
el corazón, y me quitaban la alegría de ser fuerte y tan
poderoso. Sin quererlo, mis labios empezaron a musitar
una oración, la oración que siempre repetía mi abuela:

«Dulce Madre, no te alejes, \
tu vista de mí no apartes»… {.espacio-arriba1 .centrado}

Pero por más que hice no pude recordar el resto. Los
versos se me quedaron pegados y me daban vueltas en la
cabeza, junto con todas las otras imágenes y pensamientos
que me atormentaban. {.espacio-arriba1 .sin-sangria}

Por un momento, al recordar a mi abuela, pensé en
Dios, en ese Dios en el que yo apenas si creía, en el que
los moscos creían pero ocultaban, para dominar así a sus
inferiores.

«¿Qué pasaría», me pregunté, «si yo les hablara de
Dios a los moscos, a las especies oprimidas?».

Pero no había que pensar en eso: había que pensar en
la guerra que estaba por desatarse, en la guerra que me
llevaría al poder.

«Nada hay fuera de Dios», me había dicho mi abuela
un día.

Pero entonces, mi poder, el inmenso poder que iba yo
a adquirir, dentro de unos días, estaría también en Dios.
Pero eso no podía ser: mi poder era contra Dios, era para
quitarles a los hombres la creencia en Dios, porque
esa creencia les impedía ser verdaderos esclavos. Los
hombres sin Dios serían una presa fácil para nosotros,
los hombres como yo, que por eso fui una presa fácil para
todos los vicios y para todas las desventuras, que por
eso me entregué a la amargura y al odio.

«Fuera de Dios no hay nada», decía mi abuela, la pobre
viejecilla que había vivido en otro mundo. Pero yo
creía, esa noche amarga de mi primer triunfo, que fuera
de Dios sí había algo: estaba mi poder, mi fuerza.

Un recordador llegó con noticias:

---Ya se encuentran todos los escuadrones dentro de
la tienda de campaña de la víctima señalada. Una mujer
está con él.

---No la toquen a ella ---dije.

---No la tocaremos ---me contestó---. A la víctima la
hemos atacado ya, pero se ha sabido defender y ha matado
a cuatro compañeros. Ahora se ha refugiado dentro
de un velo.

No acababa de hablar este, cuando llegó otro recordador:

---Hemos encontrado un resquicio en el velo por el
que pueden pasar algunos de los guerreros.

---Muy bien ---le dije---: que pasen y lo ataquen.

---Así lo han hecho, pero él sigue defendiéndose y ha
matado ya a tres de los cuatro compañeros que entraron.

---Que lo ataquen en silencio ---le ordené---, que no
hagan ningún ruido. Lo mejor sería que entraran varios
por el resquicio y, ya que estuvieran dentro, lo atacaran
todos a un tiempo. Así es indudable que alguno llegará
a picarlo. Lleva esas órdenes.

El mosco dijo que así lo harían y desapareció.

---Fingieron no creer mis palabras ---le dije a Sol
Bueno---, pero de todos modos se han precavido. Ya empiezan
a temernos.

Quedamos en silencio largo rato, hasta que volvió el
recordador que había llevado las órdenes.

---Hicimos como lo ordenaste ---nos dijo---. Entraron
unos cincuenta compañeros por el resquicio y lo atacaron
a un tiempo, en silencio. La víctima se había cubierto totalmente
con unas telas y se defendía desesperadamente,
pero tuvo que sacar la cara para respirar y aprovechamos
la ocasión para picarlo. Tres compañeros lograron su intento,
aunque a medias, pues los aplastó antes que acabaran.
Entonces llamó a la mujer que está con él para que
lo ayudara. Ella levantó el velo y esto lo aprovechamos
para meternos más de doscientos, logrando picarlo o inocularlo
repetidas veces. La mujer peleaba desesperadamente,
así que ordené a una nube de moscos sin gérmenes
que la alejaran del lugar. Se juntaron frente a su cara,
la picaron, la atormentaron, pero ella seguía luchando por
defender al hombre. Así los dejé, en esa lucha.

---Corre ---le dije--- y diles que se retiren. Ya está
cumplida la misión y no hay que seguirlos atormentando
inútilmente, sobre todo a la mujer.

El mosco desapareció y volvió a los pocos minutos,
diciendo que ya todos sus compañeros se retiraban y que
habían inoculado bastantes gérmenes en la sangre de
Godínez para asegurar su muerte.

Ya eran casi las cuatro de la mañana y decidí acostarme,
pero no pude dormir. Los pensamientos antiguos me siguieron
atormentado, dándome vueltas en la cabeza en
una loca procesión de delirios e imágenes. Apenas salido
el sol me levanté y me fui a bañar al río. El agua fría calmó
un poco mi angustia y pude pensar tranquilamente.

La primera batalla estaba dada y ganada. Lo importante
ahora era el aprovechar esa victoria para convencer
a los hombres y no tener que matarlos a todos, ya que
íbamos a necesitarlos como arsenales ambulantes. Para
esto había que predecir todos los síntomas de la enfermedad
que iba a padecer Godínez y convencer científicamente
al profesor Wassell. Convencido este, se prestarían
todos a lo que quisiéramos, pues si no morirían. Tal vez
se les ofrecería que si se portaban bien, ganada ya la guerra
y dominado el mundo, los mismos moscos los curarían
de las enfermedades de las que hubieran sido arsenales y
ya se verían libres del impuesto de sangre.

Con estas ideas fui en busca del profesor Wassell. Lo
encontré sentado en un tronco de caobo, fumando nerviosamente.
Sin saludarlo me senté junto a él y lo observé.
Tampoco él había dormido esa noche: tenía los ojos
hinchados, las ojeras negras, las manos temblorosas. Pero
yo adiviné que no lo habían desvelado pensamientos de
la futura destrucción del género humano, sino los celos
que llevaba dentro. Creo que durante mi observación no
se dio cuenta de que yo estaba sentado junto a él, pues
cuando le hablé dio un salto, como un hombre que despierta
de un sueño:

---Parece ser ---le dije--- que el señor Godínez pasó
mala noche.

---Anoche hablé con la señorita Johnes. Se va a casar
con el señor Godínez ---me repuso.

---No se va a casar ---le dije---: el señor Godínez tendrá
el cuerpo cubierto por una inmensa ampolla que le
causará terribles dolores y pasado mañana habrá muerto.

---Supongo que está usted pensando en sus patrañas
---me dijo---. No tengo humor ahora de oírlas y le ruego
me deje en paz.

---Quiero que vaya usted a la tienda de Godínez y le
pregunte qué pasó anoche.

---Nada tengo que preguntarle al señor Godínez ---replicó
el profesor Wassell---. Ese señor y yo no tenemos
nada que hablar.

Estuvo un momento en silencio, pero en los rasgos de
su cara se veía la lucha que sostenía dentro, una lucha sorda
y terrible, la lucha del dolor que quiere salir y llenarlo
todo, contra la educación que lo reprime en el fondo del
alma. Por fin, me vio de arriba abajo, como estudiándome
detenidamente, bajó luego los ojos y empezó a hablar:

---Yo formé a Godínez, yo lo traje a esta expedición.
Él se estaba muriendo de hambre en la ciudad: es un musiquillo
miserable y me dio lástima y lo traje para que pudiera
cobrar su sueldo y tuviera que comer. Desde el primer
día me di cuenta de que le prestaba demasiada
atención a la señorita Johnes, pero yo confiaba en ella;
la conozco desde que era una niña, conocí a su padre,
que era mi maestro de arqueología. Es cierto que soy algo
mayor que ella, pero yo puedo darle todo: dinero, respetabilidad,
un hogar serio. Todo esto se lo dije anoche,
pero todo lo que dije se perdió. Está resuelta y va a casarse
con ese, con ese musiquillo…

---No va a casarse con él ---le dije---. Godínez morirá
mañana en la noche. Ahora, en estos momentos, ya
se ha de sentir enfermo…

Pero el profesor Wassell no parecía oírme: estaba sumido
en su dolor y en su angustia, revolviendo por dentro la
traición del amigo y de la mujer amada, y no le cabían otros
pensamientos. Allí lo dejé y me dirigí a la tienda de
Godínez. Sin anunciarme entré. El músico estaba tirado en
su cama mientras la señorita Johnes le refrescaba la frente
con un lienzo húmedo. Ella me vio primero y se detuvo de
golpe, pero un quejido del enfermo la hizo reaccionar y siguió
adelante con su tarea. Cuando le hubo puesto el lienzo
en la frente, se levantó y se acercó a mi.

---Tiene calentura ---me dijo.

---Ya lo sé. Va a morir mañana en la noche. Recuerde
usted que se los avisé.

---Está usted loco ---me contestó ella---. No puede
ser, es una locura, una locura.

---Es la verdad ---le dije---. Tampoco el profesor Wassell
ha querido creerme, está muy ocupado con su dolor.

Una nube de pena cruzó por la cara blanca; los ojos
azules se nublaron y cuando habló creí percibir un temblor
de llanto en sus palabras:

---Había que decírselo. Pero si él se muere yo también
me muero, no puedo seguir viviendo, no puedo…

El enfermo la llamó desde la cama. Por la debilidad
de su voz me di cuenta de que estaba grave y me acerqué
a él sin temor. Una gran ampolla le cubría más de la
mitad de la cara y le iba creciendo lentamente. La mujer
se ocupaba en ponerle aceite en la ampolla y en refrescarle
la frente ardorosa por la calentura. En silencio
salí de la tienda y volví a donde estaba el profesor:

---Godínez se está muriendo ---le dije---. Ya tiene
ampollada la cara y una calentura terrible. Creo que no
vivirá hasta mañana en la noche.

Wassell se volvió hacia mí. En sus ojos había una vida
nueva, una esperanza, pero esa luz desapareció de
pronto. Él era un hombre de ciencia y un hombre honrado,
fuerte.

---¿Qué enfermedad es? ---me dijo.

---No es conocida ---le repuse---. Esta es la primera
vez que la usan los moscos.

Sin contestarme se levantó y se dirigió a la tienda de
Godínez, de la que salió al poco rato para ir a la suya
volver con un maletín, supuse que de medicinas. Yo me
quedé sentado en el tronco, observándolo todo. En la
casa de Pajarito Amarillo varios niños hablaban frente a
un grabador de discos, mientras los mayores veían asombrados
la escena.

</section>
<section epub:type="chapter" role="doc-chapter">

# XVII

Godínez murió al amanecer de ese día, murió en medio
de unos dolores terribles, todo el cuerpo cubierto por
una inmensa ampolla que se reventaba en ciertos lugares,
dejando salir un líquido maloliente y pegajoso. La
señorita Johnes no se había separado de su lado y lo cuidaba,
ayudada por el profesor Wassell y la otra mujer
que venía en la expedición, cuyo nombre no recuerdo.

Sabiendo que había muerto, le pedí a Pajarito Amarillo
que labrara un ataúd y que cavara una fosa en un lugar
lejano de la selva. Mandé también a algunos niños
que buscaran flores y las llevaran a la tienda de campaña,
para aminorar así en lo posible, el dolor de la señorita
Johnes.

Sol Bueno vino a verme, y le indiqué que el experimento
había tenido buen resultado y que en la tarde de
ese mismo día hablaría yo con los expedicionarios para
hacerles comprender que lo mejor era el sujetarse. Sol
Bueno se mostró contento y llevó mis palabras al Gran
Consejo. Yo me senté en la puerta de la choza a esperar
que pasara el entierro para, después de él, hablar con todos
y llegar a un arreglo definitivo.

Pero dieron las tres de la tarde y no daban trazas de
empezar con la ceremonia, así que me levanté y me dirigí
al caribal. Mapache Nocturno salió a mi encuentro
con grandes muestras de veneración y le pregunté la razón
por la cual no se procedía al entierro.

---La mujer está llorando ---me dijo--- y no quiere dejar
al muerto, pero ya huele y hay que enterrarlo.

---Anda y di que lo entierran inmediatamente.

Florentino Kimbol había salido tras de su padre y se
quedó junto a mí, como deseando hablarme. Para animarlo
lo saludé. Me contestó con su acostumbrada sonrisa
y me dijo:

---Los moscos mataron al blanco, yo lo sé.

---Tal vez ---le contesté---. Yo no he sabido.

---Tú pudiste impedirlo, porque tú tienes poder sobre
los moscos. ¿Por qué no lo hiciste, ¡oh, Tecolote Sabio!?

---Yo tan solo a mis amigos protejo, Florentino
Kimbol, y los blancos no son mis amigos.

---Tú les has dicho que van a morir todos ---siguió diciendo---.
Los chicleros, de noche junto a la lumbre donde
cuecen su comida, hablan en maya y yo entiendo lo
que dicen. También dicen que tú estás loco y que los espíritus
malos han entrado dentro de tu corazón, pero mi
padre no lo ha creído, porque has sido bueno con nosotros…

---¿Y tú lo crees? ---le pregunté.

---No, yo no lo creo.

Diciendo esto se alejó, pero yo noté la duda en sus
ojos. Iba a seguirlo, cuando salió el entierro y por verlo
ya no lo hice. Adelante caminaban cuatro chicleros, cargando
el ataúd. Seguía la señorita Johnes, apoyada en el
brazo del profesor Wassell, y, detrás de ellos, todo el grupo
de expedicionarios, los hombres con la cabeza descubierta
y la mujer con un velo. Esta iba rezando y las palabras
sonaron nuevas y antiguas en mis oídos. Rezaba el
rosario, que coreaban algunos de los hombres a media
voz, como con pocas ganas. Todo aquello me quería recordar
algo, pero la imagen no se pudo fijar en mi mente,
se me escurría y se me escondía en los repliegues del
pasado. El entierro llegó frente a mí y siguió adelante.
Todos hicieron como que no me veían. Algunos lacandones
se unieron al cortejo, pidiendo mi muda aprobación
con los ojos; yo les indiqué que podían ir, y fueron precedidos
por Pajarito Amarillo y Mapache Nocturno.

Allí en el caribal esperé a que volvieran, pensando en
la muerte de los hombres. Suelen recubrirla con tanta
ceremonia, aun en los lugares más apartados, que la hacen
imponente, pero en el mundo nuevo que iba a surgir,
en el mundo de los moscos, la muerte de un hombre
tendría la importancia que tiene ahora la muerte de una
vaca en los rastros de las ciudades. Con esto se remediaban,
a mi juicio, muchos de los males de la humanidad,
entre otros el dolor y el llanto. Creo que la imagen de la
señorita Johnes, con la cara cubierta, convulsa en sus lágrimas,
apoyada en el brazo del profesor, pisando débilmente
la tierra, me había conmovido. Ya sabía yo que
toda obra grande requiere dolor para efectuarse, que todos
los grandes cambios del mundo, los cambios que se
han hecho hacia el bien, se han hecho cimentados en el
dolor y en la sangre, como el cristianismo. Pero ¿era
hacia el bien el cambio que yo premeditaba? Sin sentirlo
me había yo formado la teoría de que tan solo mediante
este cambio podría vivir la humanidad y ya no concebía
yo para ella ningún otro modo de vida. Sin mí, el
género humano hubiera desaparecido bajo el ataque de
los moscos y yo era su salvador. No se trataba tan solo
de salvar una cultura o una idea, se trataba de salvar la
existencia misma de los hombres y a mí me había tocado
ese trabajo, que estaba yo desempeñando a conciencia.

Con estas teorías había yo logrado que durmiera en mi
cerebro la duda, pero ahora, ante la realidad, la duda renacía,
oprimiéndome la garganta, lastimándome el pecho;
porque una cosa es pensar en el dolor en general y otra es
verlo palpable, en los pies de una mujer que se arrastran
por el polvo, significando que el alma de esa mujer también
se arrastra, que está vacía de esperanzas, que está vacía
de todo en el mundo y llena tan solo de dolor y de angustia.

De pronto sentí que alguien me miraba y volví la cara.
Florentino Kimbol asomado a la puerta de su casa,
me veía atentamente, como estudiándome.

Al verlo le dije:

---Creí que irías al entierro. Florentino. Tu padre y
Pajarito Amarillo fueron. ¿Por qué tú no has ido?

---No quise ---me dijo---. Mi corazón está triste.

---¿Por qué? ---le pregunté---. Tú apenas si conocías
al blanco…

---No está triste por los blancos ---me contestó---.
Está triste por mi tribu.

Un rato estuvimos en silencio. Él, con la vista baja,
se ocupaba en levantar pequeños montones de polvo con
los pies descalzos. Yo lo veía, veía sus pies anchos, ennegrecidos,
las uñas romas; y de pronto pensé en los
otros pies, en los pies que se arrastraban por el polvo,
como símbolos del dolor humano, en los pies que se
arrastraban por todo el mundo, por todo ese mundo que
pronto iba a ser mío; esos pies conmovidos y lentos que
levantaban la tierra suelta, como buscando dentro de
ella una esperanza, como labrando un surco para poner
en él la semilla de la esperanza, pero pasando adelante
sin poner nada en el surco, pasando adelante, buscando
lentamente algo, una cosa, en el mundo.

Florentino Kimbol habló:

---También tú, ¡oh, Tecolote Sabio!, sientes tristeza
en tu corazón y no hay alegría en tus ojos cuando los niños
van hacia ti.

---Son hombres de mi raza ---le dije.

---La mujer rubia también es de tu raza y ella prefirió
al blanco que ha muerto.

---¿Por qué dices eso?

---Porque tu corazón la deseaba para tenerla junto a
ti. Mi padre y Pajarito Amarillo han hablado en las noches
y han consultado con Hormiga Negra, que todo lo
sabe; y Hormiga Negra les ha dicho que tu corazón la
desea, porque tú eres Kukulcán, el blanco y el bueno,
que quiere una compañera. Pero yo sé que eso no puede
ser, que los dioses no desean porque todo lo tienen. Eso
enseñaba un padre de San Quintín, cuando yo estuve
allá. Nos dijo un día que Dios todo lo podía, que Él había
hecho todo y todo era suyo.

---Mi corazón no la desea ---repliqué.

Las palabras de Florentino habían brotado rápidas y
temblorosas, como las palabras de un hombre que ha dudado
mucho tiempo en decir algo y por fin lo dice.

---Entonces, ¿por qué has dejado que maten al blanco?

---Ese era su destino ---le dije.

---Si lo has matado por la mujer ---siguió diciendo como
sin escuchar mis palabras--- la tribu va a entristecerse.
Todos hemos confiado en ti, por ti nos hemos unido
por ti hemos soñado sueños muy grandes y la esperanza
ha tomado un lugar en nuestro caribal y la hemos recibido
en nuestras casas, junto a las lumbres que no apagamos
nunca, porque nuestros dioses nunca nos daban
nada, nunca nos decían nada y tú nos has hablado y hemos
encontrado la bondad en tus palabras y nuestro corazón
se ha regocijado. Pero la llegada de los blancos ha
traído la tristeza entre nosotros y la muerte ha ocupado
el lugar que en nuestras casas tenía la esperanza. La mujer
rubia es la que traído la muerte y si tú nos dejas…

---No pienso irme ---le dije, para cortar por un momento
siquiera ese hilo de palabras rápidas que me lastimaba
tanto.

---Hay muchas maneras de dejarnos ---me dijo, hablando
más despacio---. Puedes irte y no volver a nosotros,
pero puedes también quedarte y vaciar tus pensamientos
de nosotros para llenarlos de esa mujer, y, de todos
modos, hemos de quedarnos solos y la tribu va a entristecerse
por tu ausencia y habrá llanto entre nosotros,
porque te buscaremos y no te encontraremos; y ya nos
hemos acostumbrado a ti, a tus palabras y a tus consejos,
que son buenos. Sí, Tecolote Sabio, mi corazón me lo dice:
la mujer rubia va a traer tristeza a nuestro caribal.

Y diciendo esto, se metió a su casa. Yo me dirigí a mi
choza con la tristeza dentro.

En la noche me habló Sol Bueno:

---Ya ha muerto el que sentenciamos y quiere el Gran
Consejo saber qué es lo que te han dicho los compañeros
del que ha muerto. Si están dispuestos a obedecernos
o si quieren la guerra, porque si la quieren morirán
todos, uno por otro. Tenemos ya pruebas de la sangre
de todos y nos será fácil matarlos.

---No he hablado aún con ellos ---le dije.

---Has hecho mal. No podemos perder el tiempo. La
sangre de casi todos ellos es buena, algunos tienen paludismo
y a esos hay que sanarlos antes que empecemos,
pero algunos tienen una sangre de primera para alimentar
con ella al Gran Consejo, especialmente la mujer esa
que ha venido a tu casa.

---¿La mujer? ---pregunté.

---Sí, la mujer. Urge que hables con ellos, por lo tanto,
para que empecemos los preparativos. A la mujer la
llevaremos cerca del Gran Consejo para que lo alimente
y así poder disponer de mayor número de hembras con
las que criar larvas.

---¿La sangre de la mujer? ---volví a preguntarle.

---Sí, ya te he dicho que sí; es la mejor de todas, la
más fuerte. Pocas veces hemos visto aquí sangre como
esa. El Gran Consejo está muy contento con ella y la va
a tomar como principal alimento durante estos días…

---¡Cállate! ---le dije bruscamente---. No hables más
de esa mujer…

---No te entiendo, no comprendo lo que pasa por ti.
Ayer eras todo entusiasmo…

---Déjame ---le rogué---. Mañana hablaré con ellos y
se hará lo que quieres.

Pero Sol Bueno no se iba. Parado allí sobre el lazo
con el que sostenía mi hamaca, seguía hablando. Por un
instante pensé en lo fácil que sería el aplastarlo entre mis
maños, pero algo me retuvo inmóvil. Él decía:

---No sé qué pasa por ti, pero quiero decirte esto.
Cuando entre nosotros se empieza una cosa, cualquiera
que sea, se acaba, pase lo que pase. Así que vamos a seguir
adelante con nuestro proyecto.

---Ya lo sé ---le dije---. Tan solo es que estoy un poco
nervioso esta noche. Mañana hablaré con ellos y se arreglará
todo.

Pero Sol Bueno no se convencía y me seguía hablando.
Sus zumbidos se me hicieron insoportables y le rogué
que se callara y se fuera. No sé por qué la imagen de los
pies arrastrándose en la tierra suelta me volvía sin cesar,
junto con las palabras de Florentino Kimbol: «La mujer
rubia va a traer tristeza a nuestro caribal». Siempre la mujer,
la mujer rubia, la señorita Johnes sentada a mis pies,
escuchando mis palabras: la señorita Johnes llorando tras
del velo que la cubría, la señorita Johnes arrastrando los
pies por la tierra suelta; ¡siempre ella, ella!

Desesperado salí de mi choza y vagué por la selva nocturna.

</section>
<section epub:type="chapter" role="doc-chapter">

# XVIII

A eso de las nueve de la mañana ya estaban todos reunidos
frente a mi choza. Todos menos la señorita Johnes.
Pajarito Amarillo los había convocado y habían venido
obedeciendo mis órdenes. La cara del profesor no me indicaba
nada: parecía indiferente, muerta. En las caras de
los otros se notaban diversas expresiones: en unas había
curiosidad, en otras aburrimiento, en otras temor, sobre todo
en las de los chicleros. Me paré frente a ellos y les dije:

---Falta uno de ustedes. Quiero que todos estén aquí
reunidos.

Nadie me contestó, así que repetí mis palabras. El
profesor pareció salir de un sueño para decir:

---Tendrá usted que disculpar a la señorita Johnes. Su
estado de salud no le permite salir de su tienda. Le he
puesto una inyección de morfina y duerme.

Diciendo esto, volvió a perder su mirada en el vacío,
con lo cual di principio a mi charla:

---Ya han visto ustedes cómo no los engañé. Un mosco
picó al señor Godínez, y este ha muerto. Creo que estarán
convencidos y no pedirán más pruebas.

Todos se vieron en silencio, pero ninguno habló. Así
que proseguí:

---Ahora bien, desde este momento se han convertido
todos en esclavos del Gran Consejo de los moscos y
obedecerán mis órdenes ciegamente. Ya saben los castigos
a los que se exponen si no obedecen. Cualquiera de
ustedes que no haga caso de una orden, que mate a un
mosco o que pretenda huir, será muerto irremisiblemente
sin juicio previo…

El profesor Wassell se levantó, interrumpiéndome.

---Basta ya de sus imbecilidades ---gritó---. No le parece
bastante todo el dolor que ha provocado con sus locuras,
para que siga atormentándonos y molestándonos.
No creemos una palabra de sus cuentos; y en cuanto la
señorita Johnes se encuentre bien de salud, nos iremos de
aquí; pero en el primer lugar civilizado comunicaremos a
las autoridades todo lo que está usted haciendo y cómo
no nos ha permitido trabajar en la misión que traíamos.

---Entonces ---le pregunté fríamente--- ¿no cree usted
en el poder de los moscos?

---No ---me contestó---. ¿Cómo voy a creer esas patrañas?

---¿Y la muerte del señor Godínez?

---Permítame que le diga una cosa, señor Tecolote: aún
no investigo plenamente la muerte del señor Godínez, pero
si averiguo que usted ha tenido algo que ver en ello, no
esperaré a que las autoridades se encarguen del asunto. Yo
mismo haré justicia con mi mano.

Y diciendo esto se marchó rumbo a la tienda de la señorita
Johnes. Todos los demás permanecieron en silencio
frente a mí y poco a poco se fueron dispersando, empezando
por los sabios y acabando por los guías y chicleros.
La prueba hecha con el señor Godínez había fallado
y era necesario hacer otra cosa.

Sol Bueno me habló:

---¿Qué ha pasado? ---me dijo---. He visto que el
hombre ese grande, que tú dices es el jefe de ellos, se ha
ido gritando…

Yo le conté lo que había pasado y le dije cómo el profesor
me había amenazado con la muerte. El prometió
cuidarme.

</section>
<section epub:type="chapter" role="doc-chapter">

# XIX

Mi corazón estaba triste. Durante todo el día pretendí
dormir, pero una amargura angustiosa me agarraba la
garganta y me ahogaba. Pensé en el aguardiente que el
profesor Wassell había dejado en un rincón de mi choza,
pero supe resistir la tentación y no lo toqué.

Sol Bueno vino varias veces en la tarde. Una, para
decirme que varios escuadrones de moscos me cuidaban
y que no debía temer nada, pero yo no temía lo
que pudiera venir de afuera: temía lo que llevaba dentro
y a cada rato me asaltaban pensamientos de Dios,
que hacía lo posible para rechazarlos de mí. Este no era el
momento de pensar en Dios, era el momento de actuar
y yo no actuaba: estaba acostado en mi hamaca, dejando
pasar las horas, mientras afuera, en el sol de los
claros, y en la penumbra de la selva, se estaba jugando
el porvenir de todo el género humano. Mientras, en su
tienda de campaña la señorita Johnes dormía y el profesor
velaba junto a ella, viéndolo todo, sabiéndolo
todo. Pero Dios no podía existir, no debía existir,
aunque el Gran Consejo creyera en Él, aunque yo mismo,
allá en el fondo atormentado de mi alma, creyera
en Él.

Una proveedora se acercó a mí y me habló:

---Queremos que nos digas las palabras que te hemos
oído antes. Todas las proveedoras estamos esperando,
no la guerra que se publica en contra de los hombres,
sino el momento de nuestra liberación. ¿Qué nos importa
que el mundo sea nuestro si nosotras somos del
Gran Consejo y no tenemos libertad para disfrutar la
vida?

---Calla ---le dije---. Si los del Consejo te oyen, has
de morir con seguridad.

---No temas ---me contestó---. Gran parte de los guerreros
piensan como nosotras y saben que podemos ser
libres si alguien nos ayuda a pensar, porque no estamos
acostumbrados a hacerlo. Hemos visto los otros animales
de la selva que son libres y creemos que adoran a ese
Dios del que nos has hablado algunas veces. Queremos
ser como ellos, aunque no tengamos tanta fuerza.

El zumbar de la proveedora era suplicante, en tono de
niño, agudo; y me llegaba hasta el alma. También los
moscos querían ser libres como los hombres, a quienes
yo iba a esclavizar.

---Todos confiamos en ti ---me siguió diciendo---.
Afuera están los escuadrones de guerreros que han puesto
para que te vigilen y te cuiden y todos quieren obedecer
tus órdenes. Hemos logrado convencer hasta a algunos
de los lógicos y podremos tomar el tesoro y el
arsenal y ser más poderosos que el Gran Consejo. Y si
no podemos tomarlo, tú puedes destruirlo, pues nosotros
sabemos el lugar donde se encuentra…

---Cállate ---le volví a decir---. La suerte está echada
y hay que librar la guerra en contra de los hombres.
Después ya veremos.

---Está bien ---me contestó---. Pero dime tan solo una
cosa, dime que existe ese Dios y que, frente a Él, todos
somos iguales, los del Gran Consejo y las proveedoras…

---Sí ---le dije---, sí existe. Pero ahora déjame. ---Se
alejó la proveedora y me dejó más confuso de lo que estaba.
Si los moscos pretendían ser libres, ¿cómo era que
yo quería esclavizar a los hombres? Dios no habría de
permitirlo, pero yo no podía creer en Dios. Y además,
todos mis sueños de poder, de venganza y de odio tenían
que realizarse. El hombre era como Godínez, que le roba
su mujer al que lo ha ayudado, al que le ha dado el
pan. Así eran todos los hombres, como perros y perras,
o peor que ellos, y nada malo había en esclavizarlos y ordenarlos.
Seguramente que los hombres eran más perversos
que los moscos; así que estos merecían el dominio
del mundo y yo el poder sobre los hombres.

Entienda bien quien esto leyere que estas palabras no
son de disculpa ni para pedir perdón. Estas palabras las
pongo aquí tan solo para que se comprenda todo lo que
he dicho. Las escribo frente a la muerte y son verdaderas
porque me nacen del corazón destrozado por el miedo
y por el dolor.

Afuera de mi choza sentí pasos y salí a la puerta para
ver quién se acercaba. Era el profesor Wassell, que
caminaba lentamente, los ojos fijos en mí, las manos en
las bolsas de su americana. Sobre su cabeza pude observar
una verdadera nube de moscos, compacta y oscura.
Ya el sol empezaba a caer, pero había aún bastante luz
para ver todo claramente. El profesor se acercó hasta mí,
se detuvo un instante en silencio y me dijo:

---He hablado con la señorita Johnes y he pensado en
todas estas cosas, pero hay muchas que no comprendo…

Se veía que luchaba entre creer todo lo que yo le había
dicho o creer en su ciencia y su experiencia de sabio.
Su disciplina no podía romperse tan fácilmente: una disciplina
de estudio, de ciencia basada tan solo en la propia
experiencia, en el testimonio de los sentidos. Para
ayudarlo le dije:

---Es necesario creer muchas cosas. Todo lo que yo le
he dicho es la verdad, es la verdad pura. Se lo he demostrado
con la muerte del señor Godínez…

---Ya la señorita Johnes me ha contado lo que pasó
esa noche.

---Sí, lo sé, me lo contaron los moscos. Ha de haber
sido terrible para ella, pero era necesario.

---¿Y usted sostiene que todo lo que ha dicho es verdad?

---Sí ---le contesté---, es verdad, la verdad absoluta.
Los moscos se adueñarán de toda la tierra y harán de los
hombres sus esclavos, para que les sirvan de alimento,
como hemos hecho nosotros con las vacas y las ovejas…

---¿Y usted se ha prestado a eso? ---me preguntó.

---¿Por qué no? ---le contesté---. Para los hombres no
tengo más que odio y desprecio. Yo creo que estarían
mucho mejor gobernados por los moscos.

Hubo un rato de silencio. El profesor me veía fijamente,
como pensando en algo, dudando. Por fin dijo:

---Yo creo que está usted completamente loco, pero que
es un loco peligroso. Los lacandones creen que usted es un
dios; y esa divinidad risible se le ha subido a la cabeza y,
valiéndose de no sé qué medios, ha matado usted al señor
Godínez para hacernos creer en sus patrañas. Y lo más grave
del caso es que ya varios de mis hombres creen en ello.

---Hacen bien en creerlo ---le dije---. Tal vez con eso
salven sus vidas y encuentren un acomodo en el nuevo
mundo que se va a organizar.

---Pero yo no creo en ello. Yo creo que usted deliberadamente
ha asesinado al señor Godínez porque deseaba
a la señorita Johnes…

---Miente usted ---le grité.

---Sí ---siguió diciendo---, esa es la razón. Usted ha
asesinado a un hombre, ha puesto en peligro la vida de
toda la expedición y he venido a cumplir mi palabra. He
venido a matarlo.

Su cara estaba pálida, tan blanca como su saco. Los
bigotes le temblaban.

---No crea usted ---siguió diciendo--- que lo hago con
gusto. Nunca he matado a un hombre y me repugna el
homicidio, pero en este caso obro bien y mi conciencia
estará tranquila ante Dios.

---¿Usted cree en Dios? ---le pregunté.

---Sí ---me contestó---. Creo en Dios, y por eso voy a
matarlo, porque usted está dañando al mundo, usted ha
matado, usted ha engañado vilmente a estos pobres indios,
haciéndoles creer en una divinidad ridícula, no sé
con qué fines, pero que puedo asegurar que son perversos.
Lo voy a matar, no por venganza personal, no para
que pague usted la muerte del señor Godínez, sino para
preservar al mundo de un loco peligroso. Lo voy a matar,
por más que me repugna hacerlo, porque creo que ese es
mi deber ante Dios. Así que le ruego que se prepare.

Y diciendo esto sacó de una de las bolsas de su americana
una pistola. Yo iba a decirle algo, pero no tuve tiempo.
La nube de moscos cayó sobre él cegándolo y asfixiándolo.
Dos veces disparó, pero los moscos lo habían cegado
y sus balas se perdieron en la selva. Entonces soltó la pistola
y quiso quitarse los moscos con las manos, desgarrándose
la cara con las uñas, en un desesperado afán de librarse.
Como un loco corría de un lado a otro, mientras
yo levantaba la pistola del suelo. Yo casi ya no lo veía, cubierto
como estaba por la nube de moscos. Por fin cayó
al suelo y se cubrió de una masa negra en movimiento.
Varios de sus hombres se acercaron a la carrera, alarmados
seguramente por los dos tiros, pero al verlo revolcarse
entre los moscos, todo cubierto de la sangre que le escurría
de los rasguños que se había dado y de los miles
de piquetes, se quedaron petrificados. Yo lo señalé con
la pistola y les dije:

---Regresen a sus tiendas. El profesor ha querido matarme
y vean los resultados: los moscos lo han matado.

Dócilmente regresaron todos. La masa negra dejó de
moverse y se dispersaron los moscos. El profesor Wassell
estaba muerto.

Con el pie moví el cuerpo, para ver si aún vivía, pero
estaba muerto. Tal vez se había asfixiado o se había
muerto de miedo y de horror.

Por un momento pensé en enterrarlo, pero comprendí
que el trabajo sería demasiado duro, así que lo tomé de los
pies, lo arrastré hasta el río y lo eché en las aguas cenagosas,
donde los caimanes pronto darían cuenta de él. Toda
esta escena la había yo pasado en una especie de vacío intelectual,
sin pensar en nada, con la mente en blanco.

Cuando regresé a mi choza me dijo Pajarito Amarillo,
que esperaba en la puerta, que los blancos habían huido
como locos, dejando casi todas sus cosas.

Inmediatamente me puse en contacto con Sol Bueno.
Esos hombres no debían llegar a un lugar civilizado, no
debían comunicarse con nadie. Sol Bueno salió, seguido
de grandes escuadrones, para detenerlos.

Por la noche oí cómo gritaban los hombres en la selva.
Los moscos los habían cegado y vagaban al azar, cayendo
y levantándose entre los árboles, chapoteando por
los caños.

En la tienda, la señorita Johnes dormía apaciblemente.
Enloquecidos por el pavor los expedicionarios se habían
olvidado de ella y la habían dejado sola, en mi poder.
Mucho rato estuve de pie viéndola dormir. Su cara
tenía un aspecto apacible y dulce, su cabello rubio le caía
a un lado, llegando casi hasta el suelo.

Cerca del amanecer dejaron de gritar los hombres que
habían huido. De la expedición del profesor Wassell no
quedaba más que la señorita Johnes, dormida frente a mí.

No me dolía la muerte de tantos hombres. El señor
Godínez la había merecido por su traición al profesor.
Este por haberme querido matar, y quien mata en defensa
propia no hace nada malo. Los otros merecían la
muerte por su cobardía al dejar abandonada a la señorita
Johnes. La muerte de ellos, con toda seguridad, había
sido horrible: cegados por los moscos vagaron gritando
por la selva, maldiciendo y rezando a un tiempo, lo mismo
los hombres que la mujer, hasta caer extenuados en
algún charco o al pie de un árbol. Pronto las hormigas
llegarían hasta ellos y los empezarían a despojar de su
carne, para dejar tan solo los huesos limpios y desnudos.

La primera batalla de los moscos estaba ganada.

</section>
<section epub:type="chapter" role="doc-chapter">

# XX

Sol Bueno me vio temprano por la mañana, en la tienda
de la señorita Johnes. Esta aún no se despertaba y yo
me había quedado toda la noche sentado junto a ella,
viendo su cara blanca y pensando en cosas que me llenaban
de amargura.

Sol Bueno me habló:

---Los que huyeron han muerto en la selva, como se
les había dicho que sucedería si pretendían huir. Ahora
el Gran Consejo dispone que se prepare la mujer esta para
servir de alimento. Todos los días deberá presentarse
en el lugar que se le indique y permitir que las proveedoras
tomen su sangre. Esto se repetirá a diario mientras
no tengamos otras personas que nos den sangre.

---No puede ser ---le dije---: está muy enferma…

---¿Y quién eres tú ---me preguntó--- para juzgar las
órdenes del Gran Consejo? Están muy disgustados contigo
por todos los hombres que han muerto inútilmente
y creen que no explicaste bien las cosas. Así que ahora
debes obedecer…

---Tal vez si no peligra su vida, pudiéramos usar a alguno
de los indios ---dije lentamente. Algo dentro de mí
se había roto, que me dolía, que me ahogaba.

Sol Bueno se rió.

---Así son los hombres ---dijo---. Primero te preocupabas
por tus amigos y nos rogaste que no los tocáramos.
Ahora quieres entregárnoslos para salvar a esta mujer…

---Es que no comprendes ---le dije---. Esta mujer está
enferma y…

---Sí ---me interrumpió---: comprendo perfectamente.

---¿Entonces? ---le pregunté lleno de esperanza.

---Los moscos sabemos cumplir nuestra palabra. No
tocaremos a tus amigos los lacandones. La mujer servirá
de alimento para el Gran Consejo.

---¡No puede ser! Les daré sangre de venados…

---El Gran Consejo ha ordenado y a ti no te corresponde
más que obedecer.

---No lo haré ---le dije.

---La mujer estará mañana en el lugar que se te indique
y dejará que las proveedoras tomen su sangre. Si no
lo haces así, morirán tú y ella.

Diciendo esto, desapareció Sol Bueno y me quedé solo
de nuevo, frente a la mujer dormida, con mis pensamientos
que me atormentaban. Por fin, ya entrada la
mañana, creí encontrar un camino.

En la puerta de la tienda estaba Florentino Kimbol.

---Voy a salir ---le dije---. Cuida a la mujer blanca
mientras vengo. Si despierta dile que no tema nada y dale
comida.

Florentino me vio con tristeza. Sus ojos oscuros y
profundos me siguieron mientras me dirigía a mi choza.
También en mi alma había oscuridad y tristeza.

La selva, la selva terrible y destructora de impulsos,
es como la vida del hombre. Durante años cría pacientemente
la grandeza de sus árboles y, junto a esa grandeza,
el germen destructor que ha de acabar con ella. Y
llega el día en que toda esa grandeza cae por tierra y se
convierte en polvo y en ceniza, no por la casualidad, sino
por la acción de ese germen destructor que la selva
ha creado junto a su grandeza. Así es el hombre, así fui
yo, como la selva: yo destruí mi grandeza, yo, con estas
mismas manos, con las que escribo ahora, eché por tierra
mis sueños monumentales. Eso es lo que había resuelto
mientras estaba sentado junto a la mujer de cara
blanca. Pero ahora lo comprendo: no fue una obra de la
casualidad; yo mismo traía dentro del alma atormentada
y enferma el germen de esa destrucción y ese germen lo
había yo cultivado, yo mismo, amando a los niños y a los
lacandones que ahora traicionaba. Si todo esto hubiera
sucedido antes, cuando en mi alma había tan solo odio,
mi grandeza hubiera subsistido. Pero el odio y el amor
no pueden vivir juntos, el uno destruye al otro; y en mi
alma triunfó el amor.

Lentamente caminé hasta mi choza por la soledad del
claro. Creo que mis pies, como los de la señorita Johnes,
se arrastraron por el polvo. De los pies nada puedo decir
porque no los observaba, pero el alma sí se arrastraba
por el polvo. Llegando al playón pequeño busqué algún
mosco con el que hablar. Pronto oí zumbar a una
proveedora y la llamé. Al punto vino hacia mí:

---¿Me conoces? ---le pregunté.

---Sí ---me contestó---. ¿Quién no va a conocerte?

---¿Sabes sin duda las cosas que he hablado?

---Las sé, las sabemos todos los que estamos abajo y
todos esperamos de ti muchas cosas.

---Reúne a tus compañeros y a los guerreros ---le dije---,
a los más que puedas. Creo que ha llegado el momento
en que todos ustedes sean libres. Pero hazlo en secreto:
no quiero que el Gran Consejo se entere de lo que
vamos a planear.

---El Gran Consejo lo sabe todo ---dijo ella.

---El Gran Consejo no sabe muchas cosas ---le repliqué---.
No sabe por ejemplo lo que estamos pensando, ni
sabe lo que va a suceder. Ustedes, junto con los guerreros,
son muchos más que todos los Grandes Consejos juntos.
Estos no han hecho más que explotarlos, más que vivir
en la indolencia a costillas de ustedes. El Gran Consejo
les ha ocultado la verdad más importante de todas, les ha
ocultado la existencia de Dios.

---Luego, ¿es cierto eso?

---Sí, es cierto, tan cierto como que existimos tú y yo.
Y el hecho de que Dios exista, de que Dios lo haya creado
todo, nos hace a todos iguales. El Gran Consejo nunca
les ha dicho esto porque temía que ustedes pretendieran
igualarse a él, como va a suceder. Anda, junta a los
más que puedas y nos vemos dentro de tres horas en este
playón. Yo esperaré aquí.

La proveedora se fue zumbando y yo me senté a esperar
y a pensar en lo que iba a hacer. Era esta la única
manera de salvar a la señorita Johnes y, si la revolución
salía triunfante, de salvar a toda la humanidad de la esclavitud
que le esperaba. Entonces yo sería el héroe, el
hombre más grande del mundo y mi fama sería eterna.

La selva callada en el bochorno del medio día se agitó
poco a poco. La sombra de cada árbol se llenó de moscos
que se paraban en silencio en el lugar que podían.
Pero por más silenciosos que llegaban, las hojas se agitaban
provocando un murmullo extraño. A eso de las tres
de la tarde apareció un capitán con varias proveedoras.

---Hombre ---me dijo el capitán---, creo que ya estamos
reunidos todos los que interesan para lo que pensamos
hacer. Hay más de seis millones de guerreros, todos
los que se habían creado para la preparación de la guerra
que se avecina, y hay, además, unos cuatro millones de
proveedoras. Tú debes decirnos lo que debemos hacer
primero.

---Lo primero ---les dije--- es nombrar jefes y jurar todos
que estarán con nosotros hasta el final. Creo, salvo
su mejor opinión, que por lo pronto debemos nombrar jefe
al capitán principal que haya entre ustedes y la proveedora
que ustedes quieran.

Hubo un ligero murmullo entre las hojas, al cabo del
cual, con la disciplina a la que están acostumbrados desde
hace siglos, eligieron a un capitán, que me presentaron, diciendo
que se llamaba Fuerza y Audacia. Las proveedoras eligieron
para que las dirigiera a la que había yo mandado a
llamarlos a todos, y supe que se llamaba Agilidad.

---Ahora ---les dije--- juraremos todos luchar hasta la
muerte para conseguir nuestros fines, que son estos: igualdad
para todos los moscos; que la sangre que logren las
proveedoras sea para ellas; que los guerreros que trabajen
y arriesguen su vida en defensa de la comunidad sean
recompensados debidamente; que el Gran Consejo sea
electo por todos y todos puedan llegar a ser miembros del
Gran Consejo.

---Aprobado ---dijo Fuerza y Audacia---. Eso es lo
que hemos venido pensando desde el día en que hablaste
de Dios y de la igualdad.

---Entonces ---les dije--- hay que jurar.

Todos emitieron su juramento. Aquello se convirtió
en un babel de sonidos y zumbidos en voz de bajo.
Cuando se restableció la calma, dije:

---Ahora tenemos que pensar en lo que conviene hacer.

---Eso es lo difícil ---dijo Fuerza y Audacia---: nosotros
no estamos acostumbrados a pensar, no sabemos
hacerlo. Queremos que tú pienses por nosotros.

---Está bien ---les contesté---. Lo primero es obrar
con rapidez para agarrar el Gran Consejo por sorpresa.
Para esto necesitamos saber con qué fuerzas cuenta el
enemigo. ¿Alguno de ustedes lo sabe?

---Yo lo sé ---dijo un capitán---. Hay en la guarda del
tesoro unos cien mil guerreros armados con diferentes
gérmenes. En la del arsenal hay unos seiscientos mil,
más unos dos millones de proveedoras que llevan la sangre.
La Guardia del Gran Consejo consiste en un millón
de guerreros, de los cuales más de la mitad están con nosotros,
pero los sobrantes están bien armados.

---Hay además ---dijo una proveedora--- muchas de
nuestras compañeras que andan dispersas, con los exploradores,
y no sabemos si estarán con nosotros. Y hay que
tener en cuenta a los lógicos, que son más de cien mil y
pueden pelear, y a los recordadores, cuyo número desconozco.

---¿Todos los lógicos están con el Gran Consejo? ---pregunté
extrañando a Sol Bueno.

---Sí ---me contestaron---, todos están de parte del
Consejo.

---Por lo que veo ---les dije---, nosotros somos más
fuertes que ellos y tenemos de nuestro lado la fuerza que
nos da la sorpresa. Creo que debemos empezar las operaciones
esta misma noche, atacando a un tiempo en todos
lados. Yo estaré aquí toda la noche dando las órdenes,
con una guardia fuerte. Tú, Fuerza y Audacia,
tomarás un millón de guerreros y atacarás la guardia del
Gran Consejo, llevando como auxiliares a un millón de
proveedoras. Otro capitán atacará la guardia del tesoro
con doscientos mil guerreros y un millón de proveedoras.
Cuando lo hayan tomado, destruirán el mayor número
posible de larvas, ya que esas son controladas por
los contadores que no están con nosotros…

Noté, el estar diciendo esto, que todos comentaban con
asombro; así que creí oportuno explicar:

---Si por cualquier causa perdemos la posesión del
Tesoro, los contadores criarán inmediatamente millones
de guerreros que acabarán con nosotros. Por esto quiero
que destruyan las larvas, para estar más seguros. De
todos modos, acabada la revuelta, las proveedoras, que
no tendrán que llevar el alimento al Gran Consejo y morir,
se pueden ocupar en poner huevos.

Con esta explicación parecieron quedar todos conformes
y yo seguí adelante con mis órdenes:

---Otro capitán, con un millón de guerreros y dos millones
de proveedoras, o más si es menester, atacarán el
arsenal y lo guardarán con gran cuidado. El resto quedará
aquí de reserva, formado en escuadrones y listo para
entrar al combate si es necesario. Cada Capitán tendrá
un cuerpo especial de proveedoras que llevarán los mensajes.
¿Han entendido todo esto?

---Sí ---me contestaron--- y nos parece bien.

---Solo falta entonces una cosa. Para reconocernos y
saber que nuestros mensajes son verdaderos, usaremos la
palabra «Libertad», que será como nuestro santo y seña.

---¡Libertad! ---zumbaron todos.

---Muy bien ---les dije---. Ahora vuelvan todos a sus
ocupaciones y no hablen una palabra sobre el asunto.
Una hora después de caído el sol nos veremos aquí y empezaremos.
Creo que esta noche todos ustedes serán libres
y felices.

Con un ligero murmullo de hojas desapareció la multitud
de moscos. Yo me levanté, guardé mi flauta y me
dirigí al caribal. En la puerta de la tienda de la señorita
Johnes estaba sentado Florentino Kimbol, tal como lo había
yo dejado. Sin saludarlo entré a la tienda. La señorita
Johnes dormía aún, en la misma postura, y me detuve
a verla un rato largo, sintiendo los ojos de Florentino clavados
en mi espalda. Cayendo el sol salí de la tienda y
hablé con Florentino:

---Quiero ---le dije---, que pasada una hora de la caída
del sol despiertes a la mujer y la lleves, lo más aprisa
que puedas, a San Quintín. No te detengas para nada, y
si no puede andar cárgala; pero te lo ruego: llega a San
Quintín con ella esta misma noche.

---Se hará como tú dices ---me repuso.

Pero en sus ojos había una tristeza aún más profunda.

---Si no la llevas, si no la alejas de aquí como te lo he
ordenado, los espíritus del mal caerán sobre la tribu.
Cuando yo vuelva al amanecer, la mujer no debe estar
aquí, debe haberse ido.

---Sí ---me repuso---, se hará como dices, ¡oh, Kukulcán!

---Toma este dinero ---le dije---. Y se lo das a ella
cuando hayan llegado a San Quintín. Si te pregunta por
mí no le digas nada.

Diciendo esto le di una cantidad de dinero que había
yo encontrado entre las cosas del profesor Wassell y me
marché rumbo al playón.

</section>
<section epub:type="chapter" role="doc-chapter">

# XXI

Cuando el reloj del profesor Wassell, que también me
había yo apropiado, marcaba las ocho de la noche, despaché
los primeros escuadrones para atacar la guarida del
Gran Consejo. Rápidamente se organizaron y partieron.
Inmediatamente me puse a organizar los escuadrones
que deberían atacar el Tesoro y el Arsenal. Con la disciplina
que tenían después de tantos siglos de entrenamiento,
era fácil el ordenarlos y despacharlos; así que en
menos de dos horas lo logré, quedando tan solo en el playón,
apenas alumbrado por la luna, el grupo que me había
de servir de guardia, y en la sombra de los árboles
los que estaban de reserva.

A las once y media llegaron las primeras noticias. Los
escuadrones que habían atacado a la guardia del Tesoro
la habían derrotado y hecho huir a los pocos sobrevivientes,
que se habían refugiado en unas cuevas, y se ocupaban,
como habíamos convenido, en destruir las larvas a
toda la velocidad. Por lo pronto mandé un grupo fuerte,
de unos quinientos mil, a que cercaran en la cueva a los que
habían huido y, de ser posible, los destruyeran.

Cerca ya de las once de la noche recibí noticias de los
que atacaron el arsenal. Resulta que se presentaron sobre
la laguneta donde criaban los gérmenes, pero alguien
había avisado ya a los guerreros que cuidaban; así que la
batalla fue más reñida, quedando por fin victoriosos los
míos, aunque habiendo padecido gran cantidad de bajas.
Mandé varios escuadrones, unos trescientos mil guerreros
y un millón de proveedoras, para que hicieran guardia
sobre el arsenal, ordenándoles que mataran sin piedad
a todos los prisioneros.

Los que me tenían preocupado eran los que habían
ido a atacar la guardia del Gran Consejo. Hacía más de
tres horas que se habían ido y no recibía noticias de
ellos. Mandé a un pequeño grupo de proveedoras a que
se informaran; y mientras tanto, pensé que lo indicado
era acabar con los recordadores, para dar así un golpe
definitivo, al Gran Consejo.

Uno de los capitanes que estaba junto a mí me informó
que los recordadores solían juntarse, por las noches,
en una laguneta pequeña que está río abajo y allá mandé
a toda la reserva, con órdenes de que los mataran a
todos, y volvieran cuanto antes.

Cerca ya de la una de la mañana volvieron las proveedoras
que había mandado a ver qué pasaba con Fuerza
y Audacia. Los informes que me dieron eran por demás
confusos. No habían encontrado al capitán por ningún
lado. La batalla parece que fue terrible, ya que el suelo,
las hojas de los árboles y el río estaban cubiertos de cadáveres.
En algunos sitios se seguía peleando, pero en
otros reinaba el silencio. Algunos decían que el triunfo
era nuestro y otros que habíamos sido derrotados.

Ante tan contradictorias noticias, despaché emisarios
a los que guardaban el arsenal para que acudieran en ayuda
de Fuerza y Audacia con todos los escuadrones que
pudieran. El mismo recado mandé a los que se ocupaban
de destruir el tesoro, diciéndoles que dejaran ese trabajo
a las proveedoras y los guerreros fueran al lugar de la batalla.
Yo me quedé tan solo con la guardia reducida a la
mitad.

Llegaron algunos guerreros de los de Fuerza y Audacia
y me sobresaltaron más. Según ellos, la guardia del Gran
Consejo se había doblado esa noche y, cuando llegaron
los rebeldes, los guerreros leales estaban prevenidos.
Fuerza y Audacia atacó resueltamente, esperando llevar
la victoria por tener mayor cantidad de guerreros, pero
de las cuevas salían todo el tiempo nuevos adversarios.
Uno de los que llegaron, después de ver a su escuadrón
deshecho, me dijo que probablemente el Gran Consejo
tenía siempre a mano una fuerte reserva, que todos ignoraban,
en unas cuevas cercanas al lugar donde residía, y
que había echado mano de ella.

Yo los consolé como pude diciéndoles cómo había
mandado yo refuerzos y cómo en las otras dos acciones
habíamos salido vencedores; pero no acababa de hablar
cuando llegaron algunas proveedoras diciendo que venían
del Tesoro y que, al salir mis guerreros en ayuda de Fuerza
y Audacia, habían llegado miles de guerreros leales y las
habían derrotado, matándolas a todas.

---¿Habían destruido ya las larvas? ---les pregunté.

---Casi todas ---me contestaron.

---Siquiera eso llevamos ganado ---les dije---; los del
Gran Consejo no podrán reponer a sus guerreros y están
en las mismas condiciones que nosotros. Anda ---le dije
a una de ellas---, busca a los escuadrones que mandé a
destruir a los recordadores y diles que acudan al Gran
Consejo, a dar ayuda a Fuerza y Audacia.

---Ya no es necesario ---me dijo un guerrero que llegaba
en esos momentos, maltrecho de la lucha---: Fuerza
y Audacia ha sido apresado por los del Gran Consejo y
nuestros escuadrones están destrozados.

---No importa ---le dije---, que vayan los escuadrones
y den la pelea hasta el fin. Que quede aquí tan solo una
guardia pequeña.

Todos emprendieron el camino en silencio. Había ya
entre nosotros una tristeza profunda, la tristeza de la derrota,
que contrastaba con el cielo azul oscuro, en la luz
clara de la luna. No sé por qué recordé mi infancia, cuando
soñé en grandes batallas e imaginé las derrotas bajo
cielos nublados, como una estampa que había yo visto
de Napoleón huyendo en Waterloo. Ese cielo limpio, sin
nubes, me molestaba más aún que la derrota misma.

Un mensajero me trajo la nueva de que la guardia del
Gran Consejo, formada ahora por más de tres millones
de guerreros, atacaba y rechazaba poco a poco a los nuestros
a través de la selva. Los escuadrones que había yo
mandado a destrozar a los recordadores no habían podido
cumplir su misión y regresaban desperdigados, uniéndose
a los que trataban de resistir a la guardia del Gran
Consejo.

Ya no di más órdenes: todo era inútil. Por la selva se
aproximaba el murmullo del combate. Los míos regresaban
al playón, destrozados y vencidos. Tan solo me senté
en el suelo y esperé. Mi guardia, posada en la arena,
estaba muda. De pronto no pude resistir su silencio y los
mandé a la lucha. Se alzaron todos y fueron en silencio,
como seres que van a morir irremediablemente.

No quise meditar en el porqué de mi derrota. El plan
había fallado, habían muerto millones de moscos, yo moriría
probablemente; pero lo importante se había logrado:
la señorita Johnes, mientras duraba la batalla, que
yo prolongaba lo más posible, llegaría sana y salva a San
Quintín y a la civilización. Cuando terminara la batalla
y los moscos quisieran perseguirla, ya sería tarde.

El murmullo del combate se acercaba cada vez más al
playón. Ya escuchaba yo claramente los gritos de mis
partidarios, los gritos de «¡Libertad!», y los del Gran
Consejo que apellidaban «¡Ideal!», sin que comprendiera
yo por qué este extraño santo y seña.

Mi reloj, el del profesor Wassell, marcaba las tres de
la mañana cuando aparecieron en el playón los primeros
combatientes. Luchaban uno contra otro, trenzados por
las patas; pero como los leales al Gran Consejo eran más,
en muchos casos tomaban a uno de los míos entre dos o
tres y lo mataban. Cuando los triunfadores quedaban libres
se lanzaban al instante sobre el enemigo más cercano
y repetían la misma operación. Así fueron avanzando
unos y retrocediendo los otros, hasta que el combate
se empezó a librar sobre el lago. Caían los cadáveres al
agua y pronto se vio cubierta de ellos, lo mismo que la
arena del playón.

Yo me había puesto de pie y observaba la escena en
silencio. No había nadie a quien le pudiera dar órdenes;
era como un general sin estado mayor, abandonado. Un
grupo de enemigos me cercó, ninguno se acercaba a mí;
pero me cercaban por todos lados, como una muralla flotante.
Era la misma nube que había yo visto sobre la cabeza
del profesor Wassell antes de que lo mataran; y de
pronto sentí miedo, un miedo terrible ante la muerte horrorosa
que me esperaba. Como pude tomé mi flauta y
zumbé las órdenes a mis guerreros:

---¡Huyan, huyan! Hemos perdido la batalla.

Un capitán de los que me rodeaban me dijo:

---¿Para qué quieres que huyan, hombre? Solos morirán
pronto y su raza se extinguirá. Tú los has conducido
al mal, a la muerte sin remedio, pues sin el Gran
Consejo no podemos vivir.

Me senté de nuevo en la arena a esperar. La nube de
mi alrededor se hacía cada vez más espesa, cada vez más
silenciosa y terrible. El murmullo del combate se alejaba
sobre el lago y yo quedaba en la zona enemiga, solo,
terriblemente solo con mi miedo. Traté de pensar en la
señorita Johnes, ya a salvo en San Quintín, pero su imagen
me dio tristeza, su imagen que no volvería yo a ver
nunca. Quise no pensar en nada, pero los pensamientos
me asaltaban para herirme, los pensamientos de mi poder
perdido, de mi vida inútil, de la señorita Johnes…

Una voz zumbante me interrumpió:

---Hombre ---dijo---, el Gran Consejo te espera. Serás
juzgado por él, así que camina hacia donde ya sabes.

Me puse de pie y caminé por el playón, rodeado por la
nube que se movía conmigo, sin acercarse ni alejarse.
Pensé en arrojarme al agua y huir de ellos pero ¿a dónde
huir? En el agua me hubieran ahogado con más facilidad
que en la tierra.

Llegado al lugar donde, por primera vez, hablé con el
Consejo Superior, vi bajo el tronco del caobo la masa negra.
Frente a ella me detuve y una voz dijo:

---Hombre, ¿qué tienes que decir?

---Nada ---le contesté---. Mátenme o hagan de mí lo
que quieran. Me han vencido en la lucha…

---No nos interesan tus frases ---me dijeron---. Has
traicionado al Gran Consejo, has roto tu palabra y mereces
la muerte.

---Sí. Pero quiero decirles que…

---No nos interesan tampoco tus razones. Nos has
traicionado y vas a morir como todos los moscos a quienes
indujiste al mal. El daño que nos has causado es grave
y pasarán cien años para que podamos reponerlo…

---Me alegra oír eso ---le interrumpí---. Lo que quiero
decirles es que…

---Calla. Vas a morir dentro de un día. Mañana a estas
horas ya habrás muerto, donde quiera que estés.
Nosotros sabremos encontrarte. Anda.

Quise contestar, pero la masa negra ya se había disuelto.
Yo ya sabía desde antes que me iban a condenar
a muerte, pero esperaba otra clase de juicio: un juicio
con grandes frases, frases heroicas como las que había
soñado en mis mocedades. Este juicio en el que no
se me permitió ni hablar, me llenó de amargura. Tan
solo el pensamiento de que la señorita Johnes se había
salvado me consoló en algo y quise gritárselos a ellos,
para que vieran que su victoria no era tan completa, y
se los grité, pero me respondió tan solo el silencio de
la selva.

Lentamente volví a mi choza, pensando en qué ocuparía
yo ese día, el último de mi vida. Entonces fue
cuando resolví escribir todas estas cosas para que mi
nombre viviera para siempre.

Empezaba a amanecer con el escándalo acostumbrado
de pájaros y animales que se escurren entre las hojas.
Me sentí solo, muy solo. En la puerta de mi choza estaba
un hombre en cuclillas, teniendo en la mano un bulto
pequeño, como una cazuela. Me acerqué y vi que era
Florentino Kimbol:

---¿Qué haces aquí? ---le pregunté---. Te mandé que
llevaras…

---No te enojes conmigo, ¡oh, Kukulcán! ---me dijo---.
Yo te he desobedecido, pero sé que mi desobediencia no
te va a irritar y que me mirarás con ojos buenos, los mismos
que a mi tribu…

---¿Dónde está la mujer? ---le pregunté interrumpiéndolo.

---Déjame hablarte, ¡oh, Kukulcán!, ¡oh, Quetzalcóatl,
como te llaman los nahuas!, ¡oh, gran pájaro blanco! ¡Tú
el barbado, el fuerte, el bueno!

Diciendo todo esto se había puesto de rodillas frente
a mí y me ofrecía la cazuela que llevaba en las manos,
cubierta con una manta blanca.

---Yo he pensado dentro de mí y he dicho: El dios sufre
en su corazón porque el corazón de la mujer rubia no
es suyo. Y él me ha dicho: Florentino Kimbol, lleva a la
mujer rubia a San Quintín. Pero yo sé que el corazón del
hombre, aunque ese hombre sea un dios, sigue a la mujer,
porque quiere tener generación. Y yo me he dicho: el dios,
Kukulcán, no debe alejarse de nosotros; a ti te corresponde
el trabajo, la fatiga, de que se quede entre nosotros. Y
he tomado un puñal y le he sacado el corazón a la mujer
y te lo traigo para que tu corazón se regocije.

Diciendo esto, descubrió la cazuela y vi que dentro
de ella estaba un corazón rojizo, seco.

No dije nada, recibí la cazuela y me dirigí a la tienda
de campaña, tomé el cadáver en mis brazos, y con él y
el corazón volví a mi choza.

Florentino Kimbol, de rodillas hacia el sol naciente,
oraba en la puerta y lloraba.

---¡Oh, Kukulcán!, recibe esta sangre que te entrego.
Yo sé que los dioses aman la sangre, y por eso, por agradarte,
porque eres un dios poderoso, te he ofrecido la
sangre.

Sin poder soportar más sus voces, le ordené que se
fuera y cerré la puerta.

</section>
<section epub:type="chapter" role="doc-chapter">

# XXII

Aquí sigo escribiendo, en mi choza. Ya cayó la noche y
he encendido la lámpara del profesor Wassell. En la cama
está el cadáver de la señorita Johnes y en mi mesa,
frente a mí, la cazuela con su corazón, seco y negruzco,
del que he tenido que espantar varias veces a las moscas.

Esto es el corazón de una mujer, me digo, este pedazo
de carne negruzca y seca es el corazón de una mujer
a la que yo vi llena de vida. Y eso que está sobre mi cama,
con la cara blanca como la cera y el pelo rubio que
le escurre hasta el suelo, con la horrible mancha roja en
el pecho, es una mujer. Con el calor del día, su carne firme
se ha empezado a descomponer y nace de ella un olor
duro y agrio que lastima la garganta.

Pero voy a morir también. Pienso en Dios, pienso en
Él y trato de hacerlo con la fe de mi abuela, la que me
enseñaba el catecismo del padre Ripalda, sentada en el
amplio corredor, junto a las macetas. Pero Dios se me
escapa de las manos. ¿Será quizá mi orgullo? Alguien,
tal vez un sacerdote, me dijo un día que para llegar a
Dios se necesitaba humildad.

Ahora, con mis sueños deshechos, frente a este cadáver,
comprendo muchas cosas. Florentino Kimbol se ha
alejado rumbo a la selva donde va a morirse porque su
dios lo ha visto con ojos displicentes. Tal vez, como último
acto de mi vida, debí consolarlo, pero hay una pereza
terrible en mis miembros, una pereza que es un
principio de la muerte.

La muerte. Yo he traído la muerte en mis sueños alocados
de poder. Pero no he de morir. Viviré en las hojas
que he escrito, viviré para siempre, y mi nombre no
se va a perder como el de los moscos que me siguieron
en mi rebelión. Por eso lo he puesto claramente en la primera
página de este cuaderno y de todos los cuadernos,
el que contiene la lengua mosquil y el que contiene su
gramática. Mi nombre está allí para vivir durante todos
los siglos.

¿Pero el cuerpo? Este cuerpo que mi abuela llamaba
deleznable y que yo he amado durante cerca de cincuenta
años. Este cuerpo para el que busqué el poder, la gloria,
la fuerza. Mañana será como el de la señorita Johnes
y, de pensar en esto, la angustia me aprieta la garganta
y quiero gritar en la noche, quiero gritar entre la selva.
No es justo, no es posible. Yo, que soy el centro de un
mundo, de mi mundo; yo que he llegado a tanto, que he
llegado a más que cualquier otro hombre; yo… no puedo,
no debo morir.

Tal vez también para morir se necesite fuerza. Cuando
empecé a escribir estas memorias me sentía sereno y
fuerte, pero ahora, frente al cadáver de la señorita Johnes,
tan blanco, tan frío, tan impávido, siento miedo, un miedo
terrible.

En un rincón de mi choza están las cajas que contienen
el aguardiente del profesor Wassell…

Me siento más reconfortado. No tengo ya miedo,
no tiemblan ya mis manos al escribir: ¡Soy yo, el hombre
de más poder! Soy el hombre inmortal, inmortal
porque fui hecho a semejanza de Dios. Pero ¿cómo me
presentaré ante Dios?, ¿cómo presentaré mis sueños y
ese cadáver absurdo tendido en mi cama y ese corazón
reseco en la cazuela que son la conclusión de mis sueños?

Estoy triste, el aguardiente es triste, sus gotas se derraman
dentro de mí lentamente y llevan la tristeza por
todos los caminos de la sangre. Estoy triste y estoy hablando
de mí con amargura, me estoy viendo con amargura
y me estoy copiando en el papel, para que mi nombre,
el que he escrito en la primera hoja de los cuadernos,
mi nombre inmortal, no se pierda. Estoy triste, quisiera
arrodillarme en la soledad de la selva y decir algo, tal vez
las palabras que me enseñó mi abuela, las palabras que,
siendo niño, dije con fe. Pero mi lengua no responde a
las palabras y mis rodillas tiemblan cuando quiero levantarme.
El olor del cadáver se hace cada vez más agrio, la
cara se va afilando y yo me siento vacío y no le temo a
la muerte. ¡Yo me río de la muerte!

Tomaré el cadáver de ella, de la señorita Johnes. ¡Qué
curioso que nunca haya yo sabido su primer nombre! Tal
vez se llamaba María. Y nos iremos juntos, por la selva,
yo la cargaré. Ella sin corazón y yo con un corazón remendado
y vuelto a romper. Ella con la cara afilada y su
olor acre y duro. Quisiera arrodillarme junto a ella y besar
su mano y pedirle perdón.

Afuera de la choza la selva me está llamando. Voy a
ella, con el cadáver en brazos, en busca de mi muerte.

</section>
<section epub:type="epilogue" role="doc-epilogue">

# Epílogo

El coronel Pérez cerró el cuaderno y le pidió a Pajarito
Amarillo que le diera los otros cuadernos y las primeras
hojas que faltaban en el que había leído. Pajarito Amarillo
le contestó:

---Kukulcán, el blanco, el bueno, nos enseñó a hacer
barcos de papel y lanzarlos por el río para que las aguas
se llevaran a los malos espíritus. Diariamente hemos tomado
veinticuatro hojas, a diario desde que Kukulcán se
fue de entre nosotros, dejándonos su corazón seco en esa
cazuela que ves. Ya pronto se acabarán todas las hojas
de papel y entonces Kukulcán, el Tecolote Sabio, hablará
con nosotros y nos dirá su voluntad.

---Yo me tengo que llevar este cuaderno ---dijo el coronel---.
¿Cómo se llamaba este hombre?

---Él era Kukulcán, el sabio, el blanco, el bueno, el
grande, el barbado, el que los nahuas llaman Quetzalcóatl,
que regresó a la tierra para que los indios volvieran a reinar
en la tierra.

---¿Pero cómo se llamaba? ---preguntó el coronel, que
no estaba para bromas.

---Nosotros le decíamos el Tecolote Sabio, pero sabemos
que era Kukulcán, el bueno, el…

---¡Basta! ---interrumpió el coronel, que no quería
por ningún motivo volver a escuchar la letanía que se le
esperaba---. Me llevo este cuaderno y me voy ahora mismo,
que no tardan en empezar las aguas y no quiero que
me agarren aquí.

El sargento envolvió el manuscrito en una manta impermeable.
El coronel estaba de un humor de los demonios.
Haber caminado por aquella selva infernal, durante
siete días, en busca de una expedición de sabios imbéciles
que se meten en honduras y no saben cómo salir de ellas,
para encontrar al final de su viaje una tumba, decían los
indios que la de uno de los sabios, y el diario o memorias
de un mariguano, del que ni siquiera se pudo averiguar el
nombre exacto para consignarlo en el parte. ¿Cómo iba poner
en un documento oficial que los miembros de la expedición,
mundialmente conocidos, habían sido muertos por
los moscos a las órdenes de Kukulcán o de Tecolote Sabio?

---Vámonos ---dijo el coronel.

Los cargueros se echaron al hombro lo que había quedado
de la impedimenta del profesor Wassell y el destacamento
se perdió por la selva.

Pajarito Amarillo, con toda su tribu, lloraba:

---Oh, Kukulcán, el sabio y el bueno, han venido los
hombres como tú y se han llevado las hojas donde grabaste
los signos que ahuyentan a los malos espíritus. Pero tú
te has ido con la mujer blanca y rubia y te has llevado, para
que te sirva de criado, a Florentino Kimbol. ¡Vuelve a
nosotros, ave de plumas blancas, Kukulcán!

</section>
