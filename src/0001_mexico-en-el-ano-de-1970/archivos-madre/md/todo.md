<section epub:type="chapter" role="doc-chapter">

# México en el año de 1970

_¡Cuántas cruces se harán nuestros bisnietos
cuando en la mano tomen los anales
de este siglo! Dirán: «Fueron discretos
nuestros abuelos, cultos, teatrales:
en charlar y escribir, hombres completos,
en alabanza propia, sin iguales;
pero en medio de tantas perfecciones
fueron unos grandísimos bribones»._ \
++J. J. Mora++ {.epigrafe}

## __+++Don Próspero+++__

Es preciso confesar, sobrino mío, que los adelantamientos
del siglo +++XX+++ en todas materias son
gigantescos; pero el que más me entusiasma, y
me hace concebir las más lisonjeras esperanzas
de que nuestra juventud causará una revolución
brillante en las ciencias y artes, es que por fin
los hombres se han convencido íntimamente de
que la piedra filosofal para todas las empresas
es que cada individuo se dedique exclusivamente
a un solo ramo y trate de hacer en él cuantas
reformas juzgue convenientes. El defecto más
pronunciado de nuestros mayores en los siglos
+++XVIII+++ y +++XIX+++ era el espíritu enciclopédico; y el que no
podía dar su opinión sobre varias materias, no
era tenido por sabio; lo cual, como debes suponer,
solo producía charlatanes, los más superficiales
que pueden concebirse. Registra la mayor
parte de los periódicos literarios de México del
siglo pasado y los hallarás llenos ---principalmente
algunos que había de _pane lucrando el stomacho
deponendo_--- de artículos de ningún interés,
regularmente de costumbres; pero ¡¡¡Qué costumbres!!!…
y necesitas echarte a nadar para hallar en ellos
algún buen artículo científico o histórico. ¿Quién
habrá muerto, que están doblando en todas las iglesias
de México?

## __+++Ruperto+++__

El teléfono eléctrico avisó esta mañana a las siete
que ha muerto repentinamente, a las cinco y
media de la misma mañana, el gobernador de las
Californias, hombre muy apreciable por sus virtudes,
su vasta instrucción y su laboriosidad. El
presidente ha dispuesto se le haga un suntuoso
funeral; se han preparado ciento veinte globos para conducir
las guarniciones de México, Puebla, Veracruz,
Jalisco, Matamoros, Monterrey y Chihuahua
al lugar de dicho funeral; y se han citado a
los gobernadores y autoridades principales de
todos los departamentos para que estén a las
diez del día de mañana en el palacio del difunto
para que asistan a la función fúnebre que debe
verificarse en la catedral de la misma ciudad en
que falleció.

## __+++Don Próspero+++__

Si no me perjudicase tanto el movimiento de los
globos aerostáticos, iría al funeral, pero a los noventa
años nada puede un pobre viejo y desgraciadamente
es la edad en que se desea todo, aún
con más ahínco que en la infancia.

## __+++Ruperto+++__

Pierda usted cuidado, tío, pues el presidente ha
mandado que se grabe la visita de la comitiva del
paseo fúnebre en una lámina de daguerrotipo que
tenga ocho varas de largo y seis de ancho, y que
se coloque en un salón del palacio de California,
pero sacándose otro igual que debe colocarse en
las casas consistoriales de México, para que recuerde
siempre a los gobernadores de este departamento
que el buen porte produce siempre la estimación
pública. Además se ha de publicar en
los periódicos la descripción del funeral.

## __+++Don Próspero+++__

¿Y los ministros concurrirán?

## __+++Ruperto+++__

Se dice que no; porque están muy disgustados
con el presidente, y no quieren acompañarlo.

## __+++Don Próspero+++__

¿De qué ha provenido esta incomodidad?

## __+++Ruperto+++__

De haberles circulado una orden para que den
audiencia a todo el mundo dos horas antes del
despacho; pues ha tenido repetidas quejas de
que se encierran en sus gabinetes y no quieren
oír las solicitudes de los que a ellos ocurren.

## __+++Don Próspero+++__

¿No has sabido si por fin ha dado su consentimiento
el ministro de comercio, para que se case
su sobrina con Pedro Benan?

## __+++Ruperto+++__

Si le ha sucedido la aventura más graciosa. Como
se había opuesto tanto a este matrimonio, el
amante fue anoche a las doce y media a la casa
del ministro y se robó a la sobrina llevándosela en
un globo; cuando le avisaron de que estaba montando
en el globo salió corriendo; pero ya el aerostático
había subido más de cincuenta varas, y
ella desde el carro saludábale burlescamente a su
tío@note[*]; este, furioso, corrió a tomar su globo para alcanzar
a los amantes; pero ¡cuál fue su sorpresa al
encontrarlo desinflado! pues la astuta sobrina había
tenido cuidado de darle sus buenas cortadas.
He oído decir que van a casarse en Roma.

## __+++Don Próspero+++__

Dice bien el proverbio; que la desgracia nunca
viene sola; este hombre que ha perdido su reputación
acaba de perder el caudal que a su sobrina
le dejó su padre; pues quería casarla con su hijo.

## __+++Ruperto+++__

¿Por qué dice usted que ha perdido su reputación?

## __+++Don Próspero+++__

Porque el diario de la oposición de ayer ha dicho
que es socio secreto de la compañía de
compra de vales; y el presidente ha mandado
que se entable un juicio formal para averiguarlo.
Dos de los redactores del diario han estado aquí
anoche y me han dicho que tienen pruebas irrefragables;
me han impuesto del negocio, y juzgo
imposible que el bribonzuelo pueda sincerarse.

## __+++Ruperto+++__

¿Y qué pena debe sufrir?

## __+++Don Próspero+++__

Si queda plenamente probado el delito, la de
muerte. Te parecerá muy rígido; pero solamente
así se ha conseguido desterrar el infame abuso
de que los que tienen el poder comercien vilmente
con él. Hace muchos años que ni aún se
oye hablar en México de estos desórdenes; y
hoy es preciso ver que la justicia no tiene miramientos
con nadie, sino que al contrario, los
hombres públicos son los que deben tratarse con
un rigor más implacable cuando delinquen.

## __+++Ruperto+++__

¿Qué caudal tendrá más o menos?

## __+++Don Próspero+++__

Antes de entrar al ministerio, cinco años ha, tenía
sesenta mil pesos, hoy tiene mas de trescientos
mil, además de lo que ha gastado, pues es
hombre que se trata muy bien. Entre otras cosas
de gusto posee una colección de treinta mil monedas
sacadas al electrotipo; le ha costado más
de sesenta mil pesos; es una de las mejores del
mundo, y hace un siglo se hubiera valuado en
dos millones. Es uno de los cuatro accionistas
del teatro de la calle de Bucareli.

## __+++Ruperto+++__

¿De cuál, del que está en la esquina de la calle
de la Acordada, o el de cerca de la Ciudadela?

## __+++Don Próspero+++__

Del segundo, que es una mina inagotable para
los empresarios; según he oído decir, ha tenido
entrada de seis mil pesos el domingo pasado,
pues como por allí hasta Tacubaya viven tantos
artesanos extranjeros, y la compañía francesa
está compuesta de los mejores actores franceses
que hay en Europa, el teatro siempre está pleno.

## __+++Ruperto+++__

Me han dicho que esta compañía está ya ajustada
para Orleans.

## __+++Don Próspero+++__

Sí, pero deberá venir de Orleans los lunes y jueves;
y las demás noches dará óperas la segunda
compañía de Milán; en fin, creo que con el tiempo
este teatro llegará a ser el tercero o segundo
de México. Si uno de nuestros seudohombres
grandes del siglo pasado, resucitara y viera en
México veintidós teatros, cuarenta y tres bibliotecas, ciento sesenta y cuatro institutos
literarios, treinta y dos hospitales; en fin, si viera ochocientos mil
habitantes disfrutar de libertad, de salubridad y
de una paz en la ciudad más hermosa de la América,
pediría se le volviese inmediatamente al sepulcro
por temor de encontrarse por todas partes
con la maldición de los hombres.

</section>
